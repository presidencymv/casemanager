Installation
	Install vcredist_x64.exe (It is very important this version is installed inorder for WAMP to work)
	Install wampserver2.5-Apache-2.4.9-Mysql-5.6.17-php5.5.12-64b.exe

Database (casemanager)
	1 - import file Database.sql
	2 - copy and run the query in CallCenterReport.sql to generate the store procedure
		If database was restored in another name please edit the following lines in file callcenter.sql
		LINE 116, 117, 118, 119 change casemanager to your prefered name.
	
Configurations
1 - Active Directory Setup
	Edit following lines from file CaseManager\login.php
		line 14 -> DOMAIN NAME Example:finance.gov.mv
		line 17 -> IP ADDRESS OF DOMAIN CONTROLLER
		line 20 -> LDAP Container
		line 21 -> User Name format
	
2 - Chancellery IP Change
	Edit casemanager\js\chancellery.js
		line 10 -> IP of the server

3 - If you wish to change the name of casemanager to anything else
	Edit casemanager\js\chancellery.js
		line 30 -> change the name 'casemanager' in the link
		line 95 -> change the name 'casemanager' in the link
		line 123 -> change the name 'casemanager' in the link
	
4 - If you are using dhiraagu bulk sms
	Edit casemanager\_ex\master.php
		line 652 -> bulk sms account username
		line 653 -> bulk sms account password
		line 680 -> Comment out if you do not have a proxy server
		
5 - To prevent proxy ip from appearing on file access logs
	Edit casemanager\_ex\proxy.php
		line 7 -> change the IP to your proxy IP.
		
6 - To receive SMS
	Edit casemanager\_pages\data\get_sms.php
		change this page to fit to the format you would like to  receive SMS and use the insertSMS function to insert it to the database
		

7 - To change file type and size settings
	Edit casemanager\_ex\attachments.php
		Line 4 -> Add Files types
		Line 5 -> maximum file size settings

8 - Connection Configuration changes
	Edit casemanager\_ex\connections.php

Default Admin User. Use the local user option to login 
	UserName ADMIN
	Password t#is!$CM

	
When using and creating users
	1 - When creating users enter user name without the domain name @trade.gov.mv
	2 - Also during login dont use domain name. Example Correct way is john.doe not john.doe@finance.gov.mv

		