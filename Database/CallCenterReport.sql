-- callcenter report procedure


DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`ismail`@`%` PROCEDURE `CALL_CAT_REPORT` (IN `P_StartDate` DATE, IN `P_EndDate` DATE)  BEGIN
	DECLARE done BOOLEAN DEFAULT FALSE;
	DECLARE ADate DATE;
    DECLARE CUR_ACT_DATES CURSOR FOR SELECT DISTINCT(DATE(CR_ON)) as ACTIVEDATES FROM `call_records` WHERE DATE(CR_ON) BETWEEN P_StartDate AND P_EndDate ORDER BY `CR_ID` DESC;
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    
    OPEN CUR_ACT_DATES; 
	
    DROP TEMPORARY TABLE IF EXISTS CatsForADate;
    CREATE TEMPORARY TABLE CatsForADate(
			CatName VARCHAR(150),
            Calls	INT
		) engine = memory;
	
    DROP TEMPORARY TABLE IF EXISTS ColsAdded;
    CREATE TEMPORARY TABLE ColsAdded(
			CatName VARCHAR(150)
		) engine = memory;
        
    DROP TEMPORARY TABLE IF EXISTS CallCatReportFinalRecords;
    CREATE TEMPORARY TABLE CallCatReportFinalRecords(
			CallDate DATE primary key
		) engine = memory;
    
	read_dates: LOOP
    
		FETCH CUR_ACT_DATES INTO ADate;
        
		IF done THEN
			CLOSE CUR_ACT_DATES;
			LEAVE read_dates;
		END IF;
        
        INSERT INTO CatsForADate SELECT DC_NAME as CATEGORY, COUNT(CR_ID) as CALLS FROM `call_records` INNER JOIN documentcat ON CR_DC_ID = DC_ID WHERE DATE(CR_ON) = ADate GROUP BY CR_DC_ID;
        
        INSERT INTO CallCatReportFinalRecords (CallDate) VALUES (ADate);
        
        CATSBLOCK: BEGIN
        
			DECLARE SQL_STMT VARCHAR(1024);
			DECLARE COL_NAME varchar(150) DEFAULT NULL;
            DECLARE CNAME varchar(150);
            DECLARE COL_VALUE int;
            
			DECLARE CUR_SEL_CATS CURSOR FOR SELECT * FROM CatsForADate;
            
			OPEN CUR_SEL_CATS;
            
            read_cols: LOOP
				
				FETCH CUR_SEL_CATS INTO COL_NAME, COL_VALUE;
                
                IF done THEN
                    SET done = FALSE;
					CLOSE CUR_SEL_CATS;
					LEAVE read_cols;
				END IF;
				
                IF NOT EXISTS (SELECT CatName FROM ColsAdded WHERE CatName = COL_NAME) THEN
                
					INSERT INTO ColsAdded SELECT COL_NAME;

                    SET @stmt = CONCAT('ALTER TABLE CallCatReportFinalRecords ADD `',COL_NAME,'` varchar(150) DEFAULT NULL');
                    
					PREPARE SQL_STMT FROM @stmt;
                    EXECUTE SQL_STMT;
                    DEALLOCATE PREPARE SQL_STMT;
                    
                END IF; 
                
                SET @stmt = CONCAT('UPDATE CallCatReportFinalRecords SET `',COL_NAME,'` = ? WHERE CallDate = ?');
                SET @val = COL_VALUE;
                SET @callday = ADate;
                
				PREPARE SQL_STMT FROM @stmt;
				EXECUTE SQL_STMT USING @val,@callday;
				DEALLOCATE PREPARE SQL_STMT;
                
            END LOOP read_cols;
            
        END CATSBLOCK;
        
        TRUNCATE TABLE CatsForADate;
        
	END LOOP read_dates;

SELECT 
    *
FROM
    CallCatReportFinalRecords
ORDER BY CallDate;
    
END$$

CREATE DEFINER=`ismail`@`%` PROCEDURE `CALL_REPORTS` (IN `START_DATE` TIMESTAMP, IN `END_DATE` TIMESTAMP)  BEGIN

SELECT 
	CR_ID,
	CR_ON,
	CR_ISSUE, 
	CR_RESPONSE, 
	UNIT.UNIT_NAME,
	IF(CR_SMS_R_ID IS NULL, 'NO', 'YES') AS `SMS`,
	IF(CR_SOLVED = 1, 'RESOLVED', 'UNRESOLVED') AS `STATUS`,
	DOC_ID,
	IF(DOC_STATUS IN (4) OR DOC_ID IS NULL, 'CLOSED', 'OPEN') AS `DOC STATUS`,
	U_NAME
	FROM casemanager.call_records 
	INNER JOIN casemanager.unit ON UNIT_ID = CR_UNIT_ID 
	INNER JOIN casemanager.user ON CR_BY = U_ID
	LEFT OUTER JOIN casemanager.document ON CR_DOC_ID = DOC_ID
	WHERE CR_ON 
	BETWEEN START_DATE AND END_DATE 
	ORDER BY CR_ON;
    
SELECT COUNT(CR_ID),DC_NAME FROM call_records 
	INNER JOIN documentcat ON CR_DC_ID = DC_ID 
	WHERE CR_ON 
	BETWEEN START_DATE AND END_DATE 
	GROUP BY DC_ID;
    
SELECT COUNT(CR_ID),UNIT_NAME,IF(CR_SOLVED = 1, 'RESOLVED', 'UNRESOLVED') AS `CALL STATUS` 
	FROM call_records INNER JOIN unit ON CR_UNIT_ID = UNIT_ID 
	WHERE CR_ON 
	BETWEEN START_DATE AND END_DATE 
	GROUP BY UNIT_ID,CR_SOLVED;

END$$

DELIMITER ;