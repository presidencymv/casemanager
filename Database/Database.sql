-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 25, 2019 at 04:25 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `casemanager`
--


-- --------------------------------------------------------

--
-- Table structure for table `aga`
--

CREATE TABLE `aga` (
  `AGA_ID` int(11) NOT NULL,
  `AGA_NAME` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aga`
--

INSERT INTO `aga` (`AGA_ID`, `AGA_NAME`) VALUES
(1, 'Raeesul Jumhooriyya ge Office'),
(2, 'Ministry of Finance and Treasury'),
(3, 'Maldives Customs Services'),
(4, 'Maldives Inland revenue Authority'),
(5, 'Ministry of Defense & National Security'),
(6, 'Ministry of Home Affairs'),
(7, 'Ministry of Education'),
(8, 'Maldives National University'),
(9, 'Ministry Of Law & Gender'),
(10, 'Ministry of Foreign Affairs'),
(11, 'Ministry of Health '),
(12, 'Ministry of Economic Development'),
(13, 'Ministry of Tourism'),
(14, 'Ministry of Youth'),
(15, 'Employment Tribunal'),
(16, 'Ministry of Housing and Infrastructure'),
(17, 'Ministry of Environment and Energy'),
(18, 'Ministry of Fisheries & Agriculture'),
(19, 'Ministry of Islamic Affairs'),
(20, 'Rayyithunge Majlis ge Idhaaraa'),
(21, 'Auditor General ge Office'),
(22, 'Elections Commission'),
(23, 'Anti-Corruption Commission'),
(24, 'Human Rights Commission'),
(25, 'Judicial Services Commission'),
(26, 'Department of Judicial Administration'),
(27, 'Civil Service Commission'),
(28, 'Prosecutor General ge Office'),
(29, 'Police Integrity Commission'),
(30, 'Maldives Media Council'),
(31, 'Tax Appeal Tribunal'),
(32, 'Local Government Authority'),
(33, 'Maldives Broadcasting Commission'),
(34, 'Maldives Broadcasting Corporation'),
(35, 'Customs Integrity Commission'),
(36, 'Information Commisioners Office'),
(37, 'Councils'),
(38, 'Maldives Civil Aviation Authority'),
(39, 'Individuals');

-- --------------------------------------------------------

--
-- Table structure for table `alerts`
--

CREATE TABLE `alerts` (
  `A_ID` int(11) NOT NULL,
  `A_U_ID` int(11) NOT NULL,
  `A_DOC_ID` int(11) NOT NULL,
  `A_TYPE` varchar(40) NOT NULL,
  `A_TYPE_DESC` int(11) NOT NULL,
  `A_CREATEDBY` int(11) NOT NULL,
  `A_CREATEDON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `A_SEEN` varchar(1) NOT NULL DEFAULT 'N',
  `A_SEENON` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `alert_description`
--

CREATE TABLE `alert_description` (
  `AD_ID` int(11) NOT NULL,
  `AD_DESC` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alert_description`
--

INSERT INTO `alert_description` (`AD_ID`, `AD_DESC`) VALUES
(1, 'Case Routed/Forwarded to Section'),
(2, 'Case Assigned to Me'),
(3, 'Case Shared with Me'),
(4, 'Case in My Section Being Shared'),
(5, 'Case Assigned to Me Being Shared'),
(6, 'Case Shared With Me Being Shared'),
(7, 'Comment to Case in My Section'),
(8, 'Comment to Case Assigned to Me'),
(9, 'Comment to Case Shared with Me'),
(10, 'Updates/Instructions to Case in My Section'),
(11, 'Updates/Instructions on Case Assigned to Me'),
(12, 'Updates/Instructions on Case Shared with Me'),
(13, 'New Attachment to a Case in My Section'),
(14, 'New Attachment to a Case Assigned to Me'),
(15, 'New Attachment to a Case Shared with Me'),
(16, 'Attachment Updated in a Case in My Section'),
(17, 'Attachment Updated in a Case Assigned to Me'),
(18, 'Attachment Updated in a Case Shared with Me'),
(19, 'Edit Case in a Case in My Section'),
(20, 'Edit Case in a Case Assigned to Me'),
(21, 'Edit Case in a Case Shared with Me'),
(22, 'Case Closed in My Section'),
(23, 'Case Closed in a Case Shared with Me'),
(24, 'Case Reopened in My Section'),
(25, 'Case Reopened in a Case Shared with Me'),
(26, 'Case Rejected');

-- --------------------------------------------------------

--
-- Table structure for table `call_records`
--

CREATE TABLE `call_records` (
  `CR_ID` int(11) NOT NULL,
  `CR_LOGID` varchar(10) NOT NULL,
  `CR_REF` int(11) DEFAULT NULL,
  `CR_CALLER` varchar(250) DEFAULT NULL,
  `CR_TEL` bigint(20) DEFAULT NULL,
  `CR_COMPANY` varchar(250) DEFAULT NULL,
  `CR_EMAIL` varchar(250) DEFAULT NULL,
  `CR_PV` varchar(20) DEFAULT NULL,
  `CR_O_ID` int(11) DEFAULT '966',
  `CR_UNIT_ID` int(11) DEFAULT '4',
  `CR_DC_ID` int(11) DEFAULT '1',
  `CR_P_ID` int(11) DEFAULT NULL,
  `CR_ISSUE` text,
  `CR_RESPONSE` text,
  `CR_DOC_ID` int(11) DEFAULT NULL,
  `CR_CRCAT_ID` int(11) DEFAULT NULL,
  `CR_BY` int(11) NOT NULL,
  `CR_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CR_END_ON` timestamp NULL DEFAULT NULL,
  `CR_ACTIVE` varchar(1) NOT NULL DEFAULT 'Y',
  `CR_SOLVED` int(11) NOT NULL DEFAULT '0',
  `CR_ID_FOLLOWUP` int(11) DEFAULT NULL,
  `CR_SMS_R_ID` bigint(20) DEFAULT NULL,
  `CR_LASTUPDATEBY` int(11) DEFAULT NULL,
  `CR_LASTUPDATEON` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `call_records_cats`
--

CREATE TABLE `call_records_cats` (
  `CRCAT_ID` int(11) NOT NULL,
  `CRCAT_NAME` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `call_records_cats`
--

INSERT INTO `call_records_cats` (`CRCAT_ID`, `CRCAT_NAME`) VALUES
(1, 'CLOSED'),
(2, 'PASSED'),
(3, 'MISSED'),
(4, 'CANCELLED'),
(5, 'ACTIVE'),
(6, 'SMS');

-- --------------------------------------------------------

--
-- Table structure for table `case_comments`
--

CREATE TABLE `case_comments` (
  `CC_ID` int(11) NOT NULL,
  `CC_DOC_ID` int(11) NOT NULL,
  `CC_TEXT` text NOT NULL,
  `CC_BY` int(11) NOT NULL,
  `CC_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `case_contact`
--

CREATE TABLE `case_contact` (
  `CC_ID` int(11) NOT NULL,
  `CC_DOC_ID` int(11) NOT NULL,
  `CC_NAME` varchar(150) NOT NULL,
  `CC_DESIG` varchar(250) DEFAULT NULL,
  `CC_ADDRESS` varchar(250) DEFAULT NULL,
  `CC_TEL` varchar(30) NOT NULL,
  `CC_FAX` varchar(30) DEFAULT NULL,
  `CC_EMAIL` varchar(250) DEFAULT NULL,
  `CC_BY` int(11) NOT NULL,
  `CC_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CC_CR_CONTACT` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `case_ref_ctrl`
--

CREATE TABLE `case_ref_ctrl` (
  `CRC_ID` int(11) NOT NULL,
  `CRC_DSrc_ID` int(11) NOT NULL,
  `CRC_YEAR` year(4) NOT NULL,
  `CRC_NUM` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `case_special_amount`
--

CREATE TABLE `case_special_amount` (
  `CSA_ID` int(11) NOT NULL,
  `CSA_DOC_ID` int(11) NOT NULL,
  `CSA_DESC` varchar(250) NOT NULL,
  `CSA_AMOUNT` varchar(50) NOT NULL,
  `CSA_BY` int(11) NOT NULL,
  `CSA_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `chan_num_ctrl`
--

CREATE TABLE `chan_num_ctrl` (
  `CNC_ID` int(11) NOT NULL,
  `CNC_YEAR` int(11) NOT NULL,
  `CNC_MONTH` int(11) NOT NULL,
  `CNC_IO` int(1) NOT NULL,
  `CNC_SEQ` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `document`
--

CREATE TABLE `document` (
  `DOC_ID` int(11) NOT NULL,
  `DOC_SYSID` varchar(20) NOT NULL DEFAULT 'NOID',
  `DOC_SRC` int(11) NOT NULL,
  `DOC_SENDER_TYPE` tinyint(1) NOT NULL DEFAULT '0',
  `DOC_OFFICE` int(11) DEFAULT NULL,
  `DOC_TXT1` varchar(250) DEFAULT NULL,
  `DOC_TXT2` varchar(250) DEFAULT NULL,
  `DOC_TXT3` varchar(15) DEFAULT NULL,
  `DOC_CATEGORY` int(11) NOT NULL,
  `DOC_UNIT` int(11) DEFAULT NULL,
  `DOC_DIVISIONS` text,
  `DOC_C_DIVISIONS` text,
  `DOC_ASSIGNEDTO` int(11) DEFAULT NULL,
  `DOC_REFNUMBER` varchar(150) DEFAULT NULL,
  `DOC_NAME` text,
  `DOC_DESCRIPTION` text NOT NULL,
  `DOC_DEADLINE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DOC_PRIORITY` int(11) NOT NULL,
  `DOC_STATUS` int(11) NOT NULL,
  `DOC_CREATEDBY` int(11) NOT NULL,
  `DOC_CREATEDON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DOC_REJECT` varchar(1) NOT NULL DEFAULT 'N',
  `DOC_HIDDEN` varchar(1) NOT NULL DEFAULT 'N',
  `DOC_LASTUPDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DOC_STATUSON` timestamp NULL DEFAULT NULL,
  `DOC_CR_DOC` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `documentcat`
--

CREATE TABLE `documentcat` (
  `DC_ID` int(11) NOT NULL,
  `DC_NAME` varchar(150) NOT NULL,
  `DC_CREATEDBY` int(11) NOT NULL,
  `DC_TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `documentcat`
--

INSERT INTO `documentcat` (`DC_ID`, `DC_NAME`, `DC_CREATEDBY`, `DC_TIMESTAMP`) VALUES
(1, 'Request for information', 1, '2019-11-24 03:58:24');

-- --------------------------------------------------------

--
-- Table structure for table `doc_character`
--

CREATE TABLE `doc_character` (
  `DCH_ID` int(11) NOT NULL,
  `DCH_NAME` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doc_character`
--

INSERT INTO `doc_character` (`DCH_ID`, `DCH_NAME`) VALUES
(1, 'Incoming Letter'),
(2, 'Outgoing Letter'),
(3, 'Incoming Response Letter'),
(4, 'Outgoing Response Letter');

-- --------------------------------------------------------

--
-- Table structure for table `doc_classification`
--

CREATE TABLE `doc_classification` (
  `DCL_ID` int(11) NOT NULL,
  `DCL_NAME` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doc_classification`
--

INSERT INTO `doc_classification` (`DCL_ID`, `DCL_NAME`) VALUES
(1, 'Public'),
(2, 'Internal use'),
(3, 'Personal information'),
(4, 'Secret');

-- --------------------------------------------------------

--
-- Table structure for table `doc_follow`
--

CREATE TABLE `doc_follow` (
  `DF_ID` int(11) NOT NULL,
  `DF_DOC_ID` int(11) NOT NULL,
  `DF_U_ID` int(11) NOT NULL,
  `DF_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `doc_history`
--

CREATE TABLE `doc_history` (
  `DH_ID` int(11) NOT NULL,
  `DH_DOC_ID` int(11) NOT NULL,
  `DH_DESC` varchar(600) NOT NULL,
  `DH_NT_ID` int(11) NOT NULL,
  `DH_SETBY` int(11) NOT NULL,
  `DH_SETON` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doc_related_cases`
--

CREATE TABLE `doc_related_cases` (
  `DRC_ID` int(11) NOT NULL,
  `DRC_DOC_ID` int(11) NOT NULL,
  `DRC_RELATED_DOC_ID` int(11) NOT NULL,
  `DRC_ATTACHEDBY` int(11) NOT NULL,
  `DRC_ATTAHCED_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `doc_remarks`
--

CREATE TABLE `doc_remarks` (
  `DR_ID` int(11) NOT NULL,
  `DR_IOR_ID` int(11) DEFAULT NULL,
  `DR_DOC_ID` int(11) NOT NULL,
  `DR_OUT_REF` varchar(150) DEFAULT NULL,
  `DR_DETAILS` varchar(150) DEFAULT NULL,
  `DR_DESC` varchar(250) NOT NULL,
  `DR_FILE` varchar(250) DEFAULT NULL,
  `DR_PAGES` int(11) NOT NULL DEFAULT '0',
  `DR_HARDCOPY` tinyint(1) NOT NULL DEFAULT '0',
  `DR_AUTHOR` varchar(250) DEFAULT NULL,
  `DR_AUTHOR_TEL1` varchar(50) DEFAULT NULL,
  `DR_AUTHOR_TEL2` varchar(50) DEFAULT NULL,
  `DR_AUTHOR_EMAIL` varchar(250) DEFAULT NULL,
  `DR_CREATION_DATE` date DEFAULT NULL,
  `DR_DCH_ID` int(11) DEFAULT NULL,
  `DR_FILE_UNIT` int(11) DEFAULT NULL,
  `DR_BY` int(11) NOT NULL,
  `DR_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doc_sender_type`
--

CREATE TABLE `doc_sender_type` (
  `DSENT_ID` tinyint(11) NOT NULL,
  `DSENT_DESC` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doc_sender_type`
--

INSERT INTO `doc_sender_type` (`DSENT_ID`, `DSENT_DESC`) VALUES
(0, 'Government'),
(1, 'Companies'),
(2, 'Individuals');

-- --------------------------------------------------------

--
-- Table structure for table `doc_share`
--

CREATE TABLE `doc_share` (
  `DS_ID` int(11) NOT NULL,
  `DS_U_ID` int(11) NOT NULL,
  `DS_DOC_ID` int(11) NOT NULL,
  `DS_BY` int(11) NOT NULL,
  `DS_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doc_source`
--

CREATE TABLE `doc_source` (
  `DSrc_ID` int(11) NOT NULL,
  `DSrc_Name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doc_source`
--

INSERT INTO `doc_source` (`DSrc_ID`, `DSrc_Name`) VALUES
(1, 'GEMS'),
(2, 'COUNTER'),
(3, 'FAX'),
(4, 'E-MAIL'),
(5, 'CALL'),
(6, 'MEETING'),
(7, 'INDIVIDUAL LETTERS'),
(8, 'PRIVATE LETTERS'),
(9, 'NEW CASE'),
(10, 'MINISTER\'S DIRECTIVES');

-- --------------------------------------------------------

--
-- Table structure for table `doc_status`
--

CREATE TABLE `doc_status` (
  `DST_ID` int(11) NOT NULL,
  `DST_NAME` varchar(20) NOT NULL,
  `DST_COLOR` varchar(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doc_status`
--

INSERT INTO `doc_status` (`DST_ID`, `DST_NAME`, `DST_COLOR`) VALUES
(1, 'NEW', NULL),
(2, 'ASSIGNED', NULL),
(3, 'ON GOING', NULL),
(4, 'CLOSED', NULL),
(5, 'REJECTED', NULL),
(6, 'FORWARDED', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fileaccess_log`
--

CREATE TABLE `fileaccess_log` (
  `fl_id` int(11) NOT NULL,
  `fl_user_id` int(11) NOT NULL,
  `fl_username` varchar(250) NOT NULL,
  `fl_filename` varchar(250) NOT NULL,
  `fl_authorized` varchar(1) NOT NULL,
  `fl_direct` varchar(1) NOT NULL DEFAULT 'N',
  `fl_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fl_hostaddress` varchar(250) DEFAULT NULL,
  `fl_hostip` varchar(21) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `in_out_registery`
--

CREATE TABLE `in_out_registery` (
  `IOR_ID` int(11) NOT NULL,
  `IOR_CHAN_NUM` varchar(50) NOT NULL,
  `IOR_EXPECTED_REPLY` tinyint(1) NOT NULL,
  `IOR_DOC_ID` int(11) NOT NULL,
  `IOR_BY` int(11) NOT NULL,
  `IOR_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `in_out_remarks`
--

CREATE TABLE `in_out_remarks` (
  `IORR_ID` int(11) NOT NULL,
  `IORR_IOR_ID` int(11) NOT NULL,
  `IORR_NOTES` varchar(250) NOT NULL,
  `IORR_BY` int(11) NOT NULL,
  `IORR_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notification_type`
--

CREATE TABLE `notification_type` (
  `NT_ID` int(11) NOT NULL,
  `NT_NAME` varchar(30) NOT NULL,
  `NT_COLOR` varchar(20) NOT NULL DEFAULT 'default'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notification_type`
--

INSERT INTO `notification_type` (`NT_ID`, `NT_NAME`, `NT_COLOR`) VALUES
(1, 'calendar', 'primary'),
(2, 'edit', 'default'),
(3, 'pause', 'default'),
(4, 'floppy-disk', 'warning'),
(5, 'usd', 'default'),
(6, 'phone', 'default'),
(7, 'envelope', 'default'),
(8, 'paperclip', 'default'),
(9, 'comment', 'default'),
(10, 'user', 'primary'),
(11, 'play', 'danger'),
(12, 'check', 'success'),
(13, 'erase', 'default'),
(14, 'star', 'warning'),
(15, 'info-sign', 'primary');

-- --------------------------------------------------------

--
-- Table structure for table `office`
--

CREATE TABLE `office` (
  `O_ID` int(11) NOT NULL,
  `O_AGA` int(11) DEFAULT NULL,
  `O_BA` int(4) DEFAULT NULL,
  `O_NAME` varchar(250) NOT NULL,
  `O_ADDRESS` text,
  `O_DEFAULTPRIO` int(11) NOT NULL DEFAULT '1',
  `O_CREATEDBY` int(11) NOT NULL,
  `O_TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `office`
--

INSERT INTO `office` (`O_ID`, `O_AGA`, `O_BA`, `O_NAME`, `O_ADDRESS`, `O_DEFAULTPRIO`, `O_CREATEDBY`, `O_TIMESTAMP`) VALUES
(1, NULL, NULL, 'Aa Sandha Pvt. Ltd.\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(2, NULL, NULL, 'Aboobakur School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(3, NULL, NULL, 'Adaaran Club Rannalhi\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(4, NULL, NULL, 'Adaaran Prestig Vaadhoo\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(5, NULL, NULL, 'Adaaran Select Hudhuranfushi\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(6, NULL, NULL, 'Adaaran Select Meedhupparu\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(7, NULL, NULL, 'Addu City Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(8, NULL, NULL, 'Addu High School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(9, NULL, NULL, 'Afeefuddin School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(10, NULL, NULL, 'Air Maldives Ltd\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(11, NULL, NULL, 'Al Madhrasathul Arabiyyathul Islaamiyyaa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(12, NULL, NULL, 'Al Madhrasathul Munawwaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(13, NULL, NULL, 'Alidhoo Island Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(14, NULL, NULL, 'Alifu Alifu Atholhu Madharusaa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(15, NULL, NULL, 'Alifu Alifu Atholhu Thauleemee Marukaz\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(16, NULL, NULL, 'Alifu Alifu Atoll Hospital\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(17, NULL, NULL, 'Alifu Alifu Bodufolhudhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(18, NULL, NULL, 'Alifu Alifu Bodufolhudhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(19, NULL, NULL, 'Alifu Alifu Family and children service centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(20, NULL, NULL, 'Alifu Alifu Feridhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(21, NULL, NULL, 'Alifu Alifu Himandhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(22, NULL, NULL, 'Alifu Alifu Himandhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(23, NULL, NULL, 'Alifu Alifu Maalhohu School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(24, NULL, NULL, 'Alifu Alifu Maalhos Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(25, NULL, NULL, 'Alifu Alifu Maalhos Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(26, NULL, NULL, 'Alifu Alifu Mathiveree School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(27, NULL, NULL, 'Alifu Alifu Mathiveri Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(28, NULL, NULL, 'Alifu Alifu Thoddoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(29, NULL, NULL, 'Alifu Alifu Thoddoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(30, NULL, NULL, 'Alifu Alifu Ukulhahu School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(31, NULL, NULL, 'Alifu Alifu Ukulhas Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(32, NULL, NULL, 'Alifu Dhaalu Atholhu Madharusaa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(33, NULL, NULL, 'Alifu Dhaalu Atholhu Thauleemee Marukazu\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(34, NULL, NULL, 'Alifu Dhaalu Atoll Hospital\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(35, NULL, NULL, 'Alifu Dhaalu Dhan\'gethee School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(36, NULL, NULL, 'Alifu Dhaalu Dhangethi Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(37, NULL, NULL, 'Alifu Dhaalu Dhidhdhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(38, NULL, NULL, 'Alifu Dhaalu Dhigurashu Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(39, NULL, NULL, 'Alifu Dhaalu Dhigurashu School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(40, NULL, NULL, 'Alifu Dhaalu Family and children service centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(41, NULL, NULL, 'Alifu Dhaalu Fenfushee School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(42, NULL, NULL, 'Alifu Dhaalu Fenfushi Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(43, NULL, NULL, 'Alifu Dhaalu Hangnaameedhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(44, NULL, NULL, 'Alifu Dhaalu Hanna Meedhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(45, NULL, NULL, 'Alifu Dhaalu Kun\'burudhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(46, NULL, NULL, 'Alifu Dhaalu Kunburudhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(47, NULL, NULL, 'Alifu Dhaalu Maamigilee Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(48, NULL, NULL, 'Alifu Dhaalu Mandhoo Health Post\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(49, NULL, NULL, 'Alifu Dhaalu Mandhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(50, NULL, NULL, 'Alifu Dhaalu Omadhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(51, NULL, NULL, 'Alifu Dhaalu Omadhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(52, NULL, NULL, 'Alimatha Aquatic Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(53, NULL, NULL, 'Allied Insurance Company of the Maldives\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(54, NULL, NULL, 'Aminiya School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(55, NULL, NULL, 'Anantara Kihavah Villas Maldives\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(56, NULL, NULL, 'Anantara Resort & Spa Maldives\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(57, NULL, NULL, 'Angaaga Island Resort & Spa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(58, NULL, NULL, 'Angsana Resort & Spa Maldives Ihuru\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(59, NULL, NULL, 'Angsana Resort & Spa Maldives Velavaru\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(60, NULL, NULL, 'Anti Corruption Commission\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(61, NULL, NULL, 'Ari Atholhu Dhekunuburi Atholhu Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(62, NULL, NULL, 'Ari Atholhu Dhekunuburi Dhan\'gethi Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(63, NULL, NULL, 'Ari Atholhu Dhekunuburi Dhangethi Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(64, NULL, NULL, 'Ari Atholhu Dhekunuburi Dhidhdhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(65, NULL, NULL, 'Ari Atholhu Dhekunuburi Dhigurah Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(66, NULL, NULL, 'Ari Atholhu Dhekunuburi Dhigurashu Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(67, NULL, NULL, 'Ari Atholhu Dhekunuburi Didhdhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(68, NULL, NULL, 'Ari Atholhu Dhekunuburi Fenfushi Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(69, NULL, NULL, 'Ari Atholhu Dhekunuburi Fenfushi Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(70, NULL, NULL, 'Ari Atholhu Dhekunuburi Hangnaameedhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(71, NULL, NULL, 'Ari Atholhu Dhekunuburi Hanyaameedhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(72, NULL, NULL, 'Ari Atholhu Dhekunuburi Kun\'burudhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(73, NULL, NULL, 'Ari Atholhu Dhekunuburi Kunburudhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(74, NULL, NULL, 'Ari Atholhu Dhekunuburi Maamigili Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(75, NULL, NULL, 'Ari Atholhu Dhekunuburi Maamigili Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(76, NULL, NULL, 'Ari Atholhu Dhekunuburi Mahibadhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(77, NULL, NULL, 'Ari Atholhu Dhekunuburi Mahibadhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(78, NULL, NULL, 'Ari Atholhu Dhekunuburi Mandhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(79, NULL, NULL, 'Ari Atholhu Dhekunuburi Mandhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(80, NULL, NULL, 'Ari Atholhu Dhekunuburi Omadhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(81, NULL, NULL, 'Ari Atholhu Dhekunuburi Omadhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(82, NULL, NULL, 'Ari Atholhu Uthuruburi Bodufolhudhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(83, NULL, NULL, 'Ari Atholhu Uthuruburi Bodufulhadho Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(84, NULL, NULL, 'Ari Atholhu Uthuruburi Feridhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(85, NULL, NULL, 'Ari Atholhu Uthuruburi Feridhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(86, NULL, NULL, 'Ari Atholhu Uthuruburi Himandhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(87, NULL, NULL, 'Ari Atholhu Uthuruburi Himandhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(88, NULL, NULL, 'Ari Atholhu Uthuruburi Maalhohu Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(89, NULL, NULL, 'Ari Atholhu Uthuruburi Maalhos Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(90, NULL, NULL, 'Ari Atholhu Uthuruburi Mathiveri Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(91, NULL, NULL, 'Ari Atholhu Uthuruburi Mathiveri Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(92, NULL, NULL, 'Ari Atholhu Uthuruburi Rasdhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(93, NULL, NULL, 'Ari Atholhu Uthuruburi Rasdhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(94, NULL, NULL, 'Ari Atholhu Uthuruburi Thoddoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(95, NULL, NULL, 'Ari Atholhu Uthuruburi Thoddoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(96, NULL, NULL, 'Ari Atholhu Uthuruburi Ukulhahu Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(97, NULL, NULL, 'Ari Atholhu Uthuruburi Ukulhas Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(98, NULL, NULL, 'Ariatholhu Uthuruburi Atholhu Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(99, NULL, NULL, 'Asdhu Sun Island\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(100, NULL, NULL, 'Asian Development Bank\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(101, NULL, NULL, 'Athletics Association of Maldives\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(102, NULL, NULL, 'Athuruga Island Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(103, NULL, NULL, 'Attorney Generals Office\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(104, NULL, NULL, 'Auditor General\'s Office\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(105, NULL, NULL, 'Aviation Security Command\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(106, NULL, NULL, 'Ayada Maldives\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(107, NULL, NULL, 'Baa Atholhu Madharusaa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(108, NULL, NULL, 'Baa Atholhu Thauleemee Marukaz\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(109, NULL, NULL, 'Baa Atoll Hospital\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(110, NULL, NULL, 'Baa Dharavandhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(111, NULL, NULL, 'Baa Dhonfanu Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(112, NULL, NULL, 'Baa Dhonfanu Health Post\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(113, NULL, NULL, 'Baa Dhonfanu School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(114, NULL, NULL, 'Baa Eydhafushi Youth Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(115, NULL, NULL, 'Baa Family and children service centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(116, NULL, NULL, 'Baa Fehendhoo Health Post\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(117, NULL, NULL, 'Baa Fehendhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(118, NULL, NULL, 'Baa Fulhadhoo Health Post\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(119, NULL, NULL, 'Baa Fulhadhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(120, NULL, NULL, 'Baa Goidhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(121, NULL, NULL, 'Baa Goidhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(122, NULL, NULL, 'Baa Goidhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(123, NULL, NULL, 'Baa Hithaadhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(124, NULL, NULL, 'Baa Hithaadhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(125, NULL, NULL, 'Baa Kamadhoo Health post\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(126, NULL, NULL, 'Baa Kamadhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(127, NULL, NULL, 'Baa Kendhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(128, NULL, NULL, 'Baa Kendhoo health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(129, NULL, NULL, 'Baa Kihaadhoo Health Post\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(130, NULL, NULL, 'Baa Kihaadhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(131, NULL, NULL, 'Baa Kudarikilu Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(132, NULL, NULL, 'Baa Kudarikilu School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(133, NULL, NULL, 'Baa Maalhohu School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(134, NULL, NULL, 'Baa Maalhos Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(135, NULL, NULL, 'Baa Thulhaadhoo Health Center\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(136, NULL, NULL, 'Baa Thulhaadhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(137, NULL, NULL, 'Bandos Island Resort & Spa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(138, NULL, NULL, 'Bank of Ceylon\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(139, NULL, NULL, 'Bank of Maldives Plc\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(140, NULL, NULL, 'Banyan Tree Maldives Vabbinfaru\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(141, NULL, NULL, 'Baros Holiday Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(142, NULL, NULL, 'Bathala Island Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(143, NULL, NULL, 'Beach House Maldives, The Waldorf Astoria Collection\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(144, NULL, NULL, 'Biyaadhoo Island Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(145, NULL, NULL, 'Boduthakurufaanu ge Handhaanee Marukazu\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(146, NULL, NULL, 'Capital Market Development Authority\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(147, NULL, NULL, 'Centara Grand Island Resort & Spa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(148, NULL, NULL, 'Central Health Service Corporation Limited\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(149, NULL, NULL, 'Central Utilities Limited\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(150, NULL, NULL, 'Centre For Higher Secondary Education\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(151, NULL, NULL, 'Centre for Continuing Education\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(152, NULL, NULL, 'Centre for Dhivehi Landuage and History\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(153, NULL, NULL, 'Centre for Holy Quran\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(154, NULL, NULL, 'Centre for Maritime Studies\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(155, NULL, NULL, 'Centre for Open Learning\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(156, NULL, NULL, 'Chaaya Island Dhonveli\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(157, NULL, NULL, 'Chaaya Lagoon Hakuraa Huraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(158, NULL, NULL, 'Civil Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(159, NULL, NULL, 'Civil Service Commision\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(160, NULL, NULL, 'Club Faru\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(161, NULL, NULL, 'Club Med Kanifinolhu\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(162, NULL, NULL, 'Coco Palm Boduhithi\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(163, NULL, NULL, 'Coco Palm Dhuni Kolhu\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(164, NULL, NULL, 'Coco Palm Kudahithi\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(165, NULL, NULL, 'Cocoa Island\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(166, NULL, NULL, 'Communication Authority of Maldives\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(167, NULL, NULL, 'Conrad Maldives Rangali Island\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(168, NULL, NULL, 'Constance Halaveli Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(169, NULL, NULL, 'Constance Moofushi Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(170, NULL, NULL, 'Consulate of Maldives in Thiruvananthapuram\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(171, NULL, NULL, 'Criminal Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(172, NULL, NULL, 'Customs Integrity Commission\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(173, NULL, NULL, 'Department of Heritage\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(174, NULL, NULL, 'Department of Higher Education\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(175, NULL, NULL, 'Maldives Immigration', NULL, 3, 1, '2015-08-19 06:59:01'),
(176, NULL, NULL, 'Department of Information\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(177, NULL, NULL, 'Department of Judicial Administration\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(178, NULL, NULL, 'Department of Medical Services\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(179, NULL, NULL, 'Department of National Registration\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(180, NULL, NULL, 'Department of Public Examinations\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(181, NULL, NULL, 'Dhaalu Atholhu Madharusaa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(182, NULL, NULL, 'Dhaalu Atholhu Thauleemee Marukaz\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(183, NULL, NULL, 'Dhaalu Atoll Hospital\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(184, NULL, NULL, 'Dhaalu Ban\'didhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(185, NULL, NULL, 'Dhaalu Bandidhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(186, NULL, NULL, 'Dhaalu Family and children service centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(187, NULL, NULL, 'Dhaalu Hulhudhelee School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(188, NULL, NULL, 'Dhaalu Hulhudheli Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(189, NULL, NULL, 'Dhaalu Maaen\'boodhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(190, NULL, NULL, 'Dhaalu Maaenboodhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(191, NULL, NULL, 'Dhaalu Meedhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(192, NULL, NULL, 'Dhaalu Rin\'budhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(193, NULL, NULL, 'Dhaalu Rinbudhoo Health Post\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(194, NULL, NULL, 'Dhaalu Vaanee Health Post\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(195, NULL, NULL, 'Dhaalu Vaanee School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(196, NULL, NULL, 'Dharumavantha School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(197, NULL, NULL, 'Dhiggiri Tourist Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(198, NULL, NULL, 'Dhivehi Bahuge Academy\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(199, NULL, NULL, 'Dhivehi Raahjeyge Adu\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(200, NULL, NULL, 'Dhivehi Raajjeyge Islaamee University\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(201, NULL, NULL, 'Dhivehiraajjeyge Gaumee University\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(202, NULL, NULL, 'Diamonds Thudufushi Beach & Water Villas\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(203, NULL, NULL, 'Diva Maldives\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(204, NULL, NULL, 'Dr Abdul Samad Memorial Hospital\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(205, NULL, NULL, 'Dream Island Maldives\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(206, NULL, NULL, 'Drug Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(207, NULL, NULL, 'Educational Development Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(208, NULL, NULL, 'Elections Commission\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(209, NULL, NULL, 'Ellaidhoo Tourist Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(210, NULL, NULL, 'Embassy of Maldives in China\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(211, NULL, NULL, 'Embassy of Maldives in Japan\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(212, NULL, NULL, 'Embassy of Maldives in Saudi Arabia\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(213, NULL, NULL, 'Embassy of Maldives in the United Arab Emirates\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(214, NULL, NULL, 'Embudhu Village\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(215, NULL, NULL, 'Employment Tribunal\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(216, NULL, NULL, 'Environmental Protection Agency\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(217, NULL, NULL, 'Eriyadhoo Island Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(218, NULL, NULL, 'Faadhippolhu Atholhu Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(219, NULL, NULL, 'Faadhippolhu Hinnavaru Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(220, NULL, NULL, 'Faadhippolhu Hinnavaru Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(221, NULL, NULL, 'Faadhippolhu Kurendhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(222, NULL, NULL, 'Faadhippolhu Kurendhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(223, NULL, NULL, 'Faadhippolhu Naifaru Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(224, NULL, NULL, 'Faadhippolhu Naifaru Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(225, NULL, NULL, 'Faadhippolhu Olhuvelifushi Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(226, NULL, NULL, 'Faadhippolhu Olhuvelifushi Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(227, NULL, NULL, 'Faafu Atholhu Madharusaa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(228, NULL, NULL, 'Faafu Atholhu Thauleemee Marukaz\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(229, NULL, NULL, 'Faafu Atoll Hospital\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(230, NULL, NULL, 'Faafu Biledhdhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(231, NULL, NULL, 'Faafu Bileiydhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(232, NULL, NULL, 'Faafu Dharaboodhoo Health Post\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(233, NULL, NULL, 'Faafu Dharan\'boodhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(234, NULL, NULL, 'Faafu Family and children service centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(235, NULL, NULL, 'Faafu Feeali Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(236, NULL, NULL, 'Faafu Magoodhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(237, NULL, NULL, 'Faafu Magoodhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(238, NULL, NULL, 'Faafu Nilandhoo Youth Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(239, NULL, NULL, 'Faculty of Arts\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(240, NULL, NULL, 'Faculty of Education\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(241, NULL, NULL, 'Faculty of Engineering Technology\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(242, NULL, NULL, 'Faculty of Health Sciences\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(243, NULL, NULL, 'Faculty of Hospitality and Tourism Studies\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(244, NULL, NULL, 'Faculty of Islamic Studies\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(245, NULL, NULL, 'Faculty of Management & Computing\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(246, NULL, NULL, 'Faculty of Sharia and Law\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(247, NULL, NULL, 'Family Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(248, NULL, NULL, 'Family Protection Authority\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(249, NULL, NULL, 'Fareedhiyya School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(250, NULL, NULL, 'Felidhe Atholhu Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(251, NULL, NULL, 'Felidhe Atholhu Felidhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(252, NULL, NULL, 'Felidhe Atholhu Fulidhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(253, NULL, NULL, 'Felidhe Atholhu Fulidhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(254, NULL, NULL, 'Felidhe Atholhu Keyodhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(255, NULL, NULL, 'Felidhe Atholhu Keyodhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(256, NULL, NULL, 'Felidhe Atholhu Rakeedhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(257, NULL, NULL, 'Felidhe Atholhu Rakeedhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(258, NULL, NULL, 'Felidhe Atholhu Thinadhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(259, NULL, NULL, 'Felidhe Atholhu Thinadhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(260, NULL, NULL, 'Felidheatholhu Felidhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(261, NULL, NULL, 'Felivaru Fisheries Maldives Limited\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(262, NULL, NULL, 'Felivaru Port Limited\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(263, NULL, NULL, 'Fenaka Corporation Limited\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(264, NULL, NULL, 'Fihaalhohi Island Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(265, NULL, NULL, 'Filitheyo Island Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(266, NULL, NULL, 'Fiyoary Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(267, NULL, NULL, 'Football Association of Maldives\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(268, NULL, NULL, 'Four Seasons Resort Maldives at Landaa Giraavaru\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(269, NULL, NULL, 'Four Seasons Resort at Kudahura\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(270, NULL, NULL, 'Fun Island Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(271, NULL, NULL, 'Fuvahmulaku Dhadimagu Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(272, NULL, NULL, 'Fuvahmulaku Dhiguwaadu Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(273, NULL, NULL, 'Fuvahmulaku Dhoodigan Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(274, NULL, NULL, 'Fuvahmulaku Funaadu Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(275, NULL, NULL, 'Fuvahmulaku Hoadhadu Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(276, NULL, NULL, 'Fuvahmulaku Maadhadu Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(277, NULL, NULL, 'Fuvahmulaku Maleâ€™Gan Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(278, NULL, NULL, 'Fuvahmulaku Miskiymagu Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(279, NULL, NULL, 'Fuvammulah Atholhu Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(280, NULL, NULL, 'Fuvammulahku  Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(281, NULL, NULL, 'Fuvammulaku School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(282, NULL, NULL, 'Gaafu Alif Family and children service centre \r', NULL, 3, 1, '2015-08-19 06:59:01'),
(283, NULL, NULL, 'Gaafu Alifu  Atoll Hospital\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(284, NULL, NULL, 'Gaafu Alifu Atholhu Madharusaa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(285, NULL, NULL, 'Gaafu Alifu Atholhu Thauleemee Marukaz\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(286, NULL, NULL, 'Gaafu Alifu Dhaandhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(287, NULL, NULL, 'Gaafu Alifu Dhaandhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(288, NULL, NULL, 'Gaafu Alifu Dhevvadhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(289, NULL, NULL, 'Gaafu Alifu Dhevvadhoo Madhrasathul Sultan Mohamed\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(290, NULL, NULL, 'Gaafu Alifu Dhiyadhoo Island Office\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(291, NULL, NULL, 'Gaafu Alifu Dhiyadhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(292, NULL, NULL, 'Gaafu Alifu Dhiyadhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(293, NULL, NULL, 'Gaafu Alifu Gemanafushee School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(294, NULL, NULL, 'Gaafu Alifu Gemanafushi Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(295, NULL, NULL, 'Gaafu Alifu Kan\'duhulhudhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(296, NULL, NULL, 'Gaafu Alifu Kanduhulhudhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(297, NULL, NULL, 'Gaafu Alifu Kolamaafushi Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(298, NULL, NULL, 'Gaafu Alifu Kondey Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(299, NULL, NULL, 'Gaafu Alifu Kondey School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(300, NULL, NULL, 'Gaafu Alifu Maamendhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(301, NULL, NULL, 'Gaafu Alifu Maamendhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(302, NULL, NULL, 'Gaafu Alifu Nilandhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(303, NULL, NULL, 'Gaafu Alifu Nilandhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(304, NULL, NULL, 'Gaafu Alifu Villingili Youth Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(305, NULL, NULL, 'Gaafu Dhaalu Atholhu Madharusaa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(306, NULL, NULL, 'Gaafu Dhaalu Atholhu Thauleemee Marukaz\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(307, NULL, NULL, 'Gaafu Dhaalu Family and Children Service Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(308, NULL, NULL, 'Gaafu Dhaalu Farehu Huvadhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(309, NULL, NULL, 'Gaafu Dhaalu Fares-mathoda Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(310, NULL, NULL, 'Gaafu Dhaalu Fiyoaree Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(311, NULL, NULL, 'Gaafu Dhaalu Fiyoaree School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(312, NULL, NULL, 'Gaafu Dhaalu Gadhdhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(313, NULL, NULL, 'Gaafu Dhaalu Hoadedhdhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(314, NULL, NULL, 'Gaafu Dhaalu Hoan\'dedhdhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(315, NULL, NULL, 'Gaafu Dhaalu Madavelee School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(316, NULL, NULL, 'Gaafu Dhaalu Madaveli Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(317, NULL, NULL, 'Gaafu Dhaalu Nadalla Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(318, NULL, NULL, 'Gaafu Dhaalu Nadellaa School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(319, NULL, NULL, 'Gaafu Dhaalu Rathafandhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(320, NULL, NULL, 'Gaafu Dhaalu Rathafandhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(321, NULL, NULL, 'Gaafu Dhaalu Thinadhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(322, NULL, NULL, 'Gaafu Dhaalu Thinadhoo Stadium\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(323, NULL, NULL, 'Gaafu Dhaalu Thinadhoo Youth Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(324, NULL, NULL, 'Gaafu Dhaalu Vaadhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(325, NULL, NULL, 'Gaafu Dhaalu Vaadhoo Jamaaluddeen Madhrasaa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(326, NULL, NULL, 'Galolhu Avashu Office\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(327, NULL, NULL, 'Galolhu Madharusa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(328, NULL, NULL, 'Gan Airport Company Limited\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(329, NULL, NULL, 'Gan Ragional Hospital\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(330, NULL, NULL, 'Gangehi Island Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(331, NULL, NULL, 'Gasfinolhu Island Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(332, NULL, NULL, 'Gaumee Arusheefu\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(333, NULL, NULL, 'Ghaazee School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(334, NULL, NULL, 'Ghiyasuddin School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(335, NULL, NULL, 'Giraavaru Tourist Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(336, NULL, NULL, 'Gnaviyani Atoll Education Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(337, NULL, NULL, 'Gnaviyani Atoll Hospital\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(338, NULL, NULL, 'Gnaviyani Family and children service centre \r', NULL, 3, 1, '2015-08-19 06:59:01'),
(339, NULL, NULL, 'Gnaviyani Fuvahmulah Youth Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(340, NULL, NULL, 'Gnaviyani Fuvamulah Stadium\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(341, NULL, NULL, 'Gulhifalhu Industrial Zone Ltd\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(342, NULL, NULL, 'Haa Alif Berimandhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(343, NULL, NULL, 'Haa Alif Molhadhoo Health Centre \r', NULL, 3, 1, '2015-08-19 06:59:01'),
(344, NULL, NULL, 'Haa Alifu Atholhu Madharusaa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(345, NULL, NULL, 'Haa Alifu Atholhu Thauleemee Marukazu\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(346, NULL, NULL, 'Haa Alifu Atoll Hospital\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(347, NULL, NULL, 'Haa Alifu Baarah Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(348, NULL, NULL, 'Haa Alifu Baarashu School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(349, NULL, NULL, 'Haa Alifu Dhidhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(350, NULL, NULL, 'Haa Alifu Dhidhoo Youth Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(351, NULL, NULL, 'Haa Alifu Family and children service centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(352, NULL, NULL, 'Haa Alifu Filladhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(353, NULL, NULL, 'Haa Alifu Filladhoo Madhrasathul Sobah\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(354, NULL, NULL, 'Haa Alifu Hathifushee Madhrasathul Ameen\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(355, NULL, NULL, 'Haa Alifu Hathifushi Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(356, NULL, NULL, 'Haa Alifu Hoarafushi Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(357, NULL, NULL, 'Haa Alifu Ihavandhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(358, NULL, NULL, 'Haa Alifu Ihavandhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(359, NULL, NULL, 'Haa Alifu Kela Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(360, NULL, NULL, 'Haa Alifu Kelaa Madhrasathul Shaikh Ibrahim\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(361, NULL, NULL, 'Haa Alifu Maarandhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(362, NULL, NULL, 'Haa Alifu Maarandhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(363, NULL, NULL, 'Haa Alifu Molhadhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(364, NULL, NULL, 'Haa Alifu Muraidhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(365, NULL, NULL, 'Haa Alifu Muraidhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(366, NULL, NULL, 'Haa Alifu Thakandhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(367, NULL, NULL, 'Haa Alifu Thuraakunu School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(368, NULL, NULL, 'Haa Alifu Thuraakurun Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(369, NULL, NULL, 'Haa Alifu Uligamu Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(370, NULL, NULL, 'Haa Alifu Uligamu School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(371, NULL, NULL, 'Haa Alifu Utheem Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(372, NULL, NULL, 'Haa Alifu Vashafaru Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(373, NULL, NULL, 'Haa Alifu Vashafaru Madhrasaa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(374, NULL, NULL, 'Haa Dhaalu Atholhu Madharusaa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(375, NULL, NULL, 'Haa Dhaalu Atholhu Tha\"Leemee Marukaz\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(376, NULL, NULL, 'Haa Dhaalu Family and children service centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(377, NULL, NULL, 'Haa Dhaalu Faridhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(378, NULL, NULL, 'Haa Dhaalu Finey Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(379, NULL, NULL, 'Haa Dhaalu Finey School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(380, NULL, NULL, 'Haa Dhaalu Hanimaadhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(381, NULL, NULL, 'Haa Dhaalu Hanimaadhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(382, NULL, NULL, 'Haa Dhaalu Hirimaradhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(383, NULL, NULL, 'Haa Dhaalu Hirimaradhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(384, NULL, NULL, 'Haa Dhaalu Kuburudhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(385, NULL, NULL, 'Haa Dhaalu Kulhudhuffushi Regional Hospital\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(386, NULL, NULL, 'Haa Dhaalu Kulhudhufushi Stadium\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(387, NULL, NULL, 'Haa Dhaalu Kumundhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(388, NULL, NULL, 'Haa Dhaalu Kumundhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(389, NULL, NULL, 'Haa Dhaalu Kun\'burudhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(390, NULL, NULL, 'Haa Dhaalu Kuribi Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(391, NULL, NULL, 'Haa Dhaalu Kurin\'bee School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(392, NULL, NULL, 'Haa Dhaalu Maavaidhoo Madhrasathul Hussainiyyah\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(393, NULL, NULL, 'Haa Dhaalu Maavaidhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(394, NULL, NULL, 'Haa Dhaalu Makunudhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(395, NULL, NULL, 'Haa Dhaalu Makunudhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(396, NULL, NULL, 'Haa Dhaalu Naivaadhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(397, NULL, NULL, 'Haa Dhaalu Naivaadhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(398, NULL, NULL, 'Haa Dhaalu Nellaidhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(399, NULL, NULL, 'Haa Dhaalu Nellaidhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(400, NULL, NULL, 'Haa Dhaalu Neykurandhoo Sihee Marukazu\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(401, NULL, NULL, 'Haa Dhaalu Neykurendhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(402, NULL, NULL, 'Haa Dhaalu Neykurendhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(403, NULL, NULL, 'Haa Dhaalu Nolhivaramu Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(404, NULL, NULL, 'Haa Dhaalu Nolhivaramu School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(405, NULL, NULL, 'Haa Dhaalu Nolhivaranfaru Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(406, NULL, NULL, 'Haa Dhaalu Nolhivaranfaru School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(407, NULL, NULL, 'Haa Dhaalu Vaikaradhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(408, NULL, NULL, 'Habib Bank\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(409, NULL, NULL, 'Hadhunmathi Atholhu Councilge Idhaaraa', NULL, 3, 1, '2015-08-19 06:59:01'),
(410, NULL, NULL, 'Hadhdhunmathi Dhan\'bidhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(411, NULL, NULL, 'Hadhdhunmathi Dhanbidhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(412, NULL, NULL, 'Hadhdhunmathi Fonadhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(413, NULL, NULL, 'Hadhdhunmathi Fonadhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(414, NULL, NULL, 'Hadhdhunmathi Gaadhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(415, NULL, NULL, 'Hadhdhunmathi Gaadhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(416, NULL, NULL, 'Hadhdhunmathi Gan Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(417, NULL, NULL, 'Hadhdhunmathi Gan/Mathimaradhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(418, NULL, NULL, 'Hadhdhunmathi Gan/Mukurimagu Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(419, NULL, NULL, 'Hadhdhunmathi Gan/Thundee Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(420, NULL, NULL, 'Hadhdhunmathi Hithadhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(421, NULL, NULL, 'Hadhdhunmathi Hithadhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(422, NULL, NULL, 'Hadhdhunmathi Isdhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(423, NULL, NULL, 'Hadhdhunmathi Isdhoo Kalhaidhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(424, NULL, NULL, 'Hadhdhunmathi Isdhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(425, NULL, NULL, 'Hadhdhunmathi Kalaidhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(426, NULL, NULL, 'Hadhdhunmathi Kunahandhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(427, NULL, NULL, 'Hadhdhunmathi Kunahandhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(428, NULL, NULL, 'Hadhdhunmathi Maabaidhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(429, NULL, NULL, 'Hadhdhunmathi Maabaidhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(430, NULL, NULL, 'Hadhdhunmathi Maamendhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(431, NULL, NULL, 'Hadhdhunmathi Maamendhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(432, NULL, NULL, 'Hadhdhunmathi Maavah Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(433, NULL, NULL, 'Hadhdhunmathi Maavashu Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(434, NULL, NULL, 'Hadhdhunmathi Mundoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(435, NULL, NULL, 'Hadhdhunmathi Mundoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(436, NULL, NULL, 'Hafiz Ahmed School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(437, NULL, NULL, 'Hamad Bin Khalifa Al-Thani School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(438, NULL, NULL, 'Hanimaadhoo Airport Company Limited\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(439, NULL, NULL, 'Health Protection Agency\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(440, NULL, NULL, 'Helengeli Island Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(441, NULL, NULL, 'Henveiru Avashu Office\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(442, NULL, NULL, 'Herethere Island Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(443, NULL, NULL, 'High Commission of Bangladesh\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(444, NULL, NULL, 'High Commission of Maldives in Bangladesh\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(445, NULL, NULL, 'High Commission of Maldives in India\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(446, NULL, NULL, 'High Commission of Maldives in Malaysia\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(447, NULL, NULL, 'High Commission of Maldives in Pakistan\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(448, NULL, NULL, 'High Commission of Maldives in Singapore\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(449, NULL, NULL, 'High Commission of Maldives in Sri Lanka\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(450, NULL, NULL, 'High Commission of Maldives in the United Kingdom of Great Britain and Northern Ireland\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(451, NULL, NULL, 'High Commission of the Democratic Socialist Republic of Sri Lanka\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(452, NULL, NULL, 'High Commission of the Islamic Republic of Pakistan\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(453, NULL, NULL, 'High Commission of the Republic of India\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(454, NULL, NULL, 'High Court of Maldives\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(455, NULL, NULL, 'Hilaaleege\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(456, NULL, NULL, 'Hilton Maldives - Irufushi Resort & Spa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(457, NULL, NULL, 'Hiriyaa School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(458, NULL, NULL, 'Hithadhoo Regional Port\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(459, NULL, NULL, 'Holiday Inn Resort Kandooma Maldives\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(460, NULL, NULL, 'Holiday Island\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(461, NULL, NULL, 'Hongkong and Shanghai Banking Corporation Limited\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(462, NULL, NULL, 'House for people with special needs\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(463, NULL, NULL, 'Housing Development Corporation Limited\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(464, NULL, NULL, 'Housing Development Finance Corporation Plc\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(465, NULL, NULL, 'Hulhumale Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(466, NULL, NULL, 'Hulhumale\' Hospital\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(467, NULL, NULL, 'Hulhumale\' Integrated Industrial Zone Ltd.\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(468, NULL, NULL, 'Human Rights Commission Maldives\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(469, NULL, NULL, 'Huvadhu Atholhu Dhekunuburi Fares-Maathodaa Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(470, NULL, NULL, 'Huvadhu Atholhu Dhekunuburi Faresmaathodaa Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(471, NULL, NULL, 'Huvadhu Atholhu Dhekunuburi Fiyoaree Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(472, NULL, NULL, 'Huvadhu Atholhu Dhekunuburi Fiyoaree Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(473, NULL, NULL, 'Huvadhu Atholhu Dhekunuburi Gadhdhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(474, NULL, NULL, 'Huvadhu Atholhu Dhekunuburi Gadhdhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(475, NULL, NULL, 'Huvadhu Atholhu Dhekunuburi Hoadehdhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(476, NULL, NULL, 'Huvadhu Atholhu Dhekunuburi Hoandedhdhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(477, NULL, NULL, 'Huvadhu Atholhu Dhekunuburi Madaveli Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(478, NULL, NULL, 'Huvadhu Atholhu Dhekunuburi Madavli Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(479, NULL, NULL, 'Huvadhu Atholhu Dhekunuburi Nadehllaa Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(480, NULL, NULL, 'Huvadhu Atholhu Dhekunuburi Nadellaa Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(481, NULL, NULL, 'Huvadhu Atholhu Dhekunuburi Rathafandhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(482, NULL, NULL, 'Huvadhu Atholhu Dhekunuburi Rathafandhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(483, NULL, NULL, 'Huvadhu Atholhu Dhekunuburi Thinadhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(484, NULL, NULL, 'Huvadhu Atholhu Dhekunuburi Thinadhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(485, NULL, NULL, 'Huvadhu Atholhu Dhekunuburi Vaadhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(486, NULL, NULL, 'Huvadhu Atholhu Dhekunuburi Vaadhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(487, NULL, NULL, 'Huvadhu Atholhu Uthuruburi Dhaandhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(488, NULL, NULL, 'Huvadhu Atholhu Uthuruburi Dhaandhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(489, NULL, NULL, 'Huvadhu Atholhu Uthuruburi Dhevvadhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(490, NULL, NULL, 'Huvadhu Atholhu Uthuruburi Dhevvadhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(491, NULL, NULL, 'Huvadhu Atholhu Uthuruburi Gemanafushi Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(492, NULL, NULL, 'Huvadhu Atholhu Uthuruburi Gemanafushi Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(493, NULL, NULL, 'Huvadhu Atholhu Uthuruburi Kan\'duhulhudhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(494, NULL, NULL, 'Huvadhu Atholhu Uthuruburi Kanduhulhudhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(495, NULL, NULL, 'Huvadhu Atholhu Uthuruburi Kolamaafushi Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(496, NULL, NULL, 'Huvadhu Atholhu Uthuruburi Kolamaafushi Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(497, NULL, NULL, 'Huvadhu Atholhu Uthuruburi Kon\'dey Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(498, NULL, NULL, 'Huvadhu Atholhu Uthuruburi Kondey Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(499, NULL, NULL, 'Huvadhu Atholhu Uthuruburi Maamendhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(500, NULL, NULL, 'Huvadhu Atholhu Uthuruburi Maamendhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(501, NULL, NULL, 'Huvadhu Atholhu Uthuruburi Nilandhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(502, NULL, NULL, 'Huvadhu Atholhu Uthuruburi Nilandhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(503, NULL, NULL, 'Huvadhu Atholhu Uthuruburi Vilin\'gili Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(504, NULL, NULL, 'Huvadhu Atholhu Uthuruburi Vilingili Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(505, NULL, NULL, 'Huvadhuatholhu Dhekunuburi Atholhu Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(506, NULL, NULL, 'Huvadhuatholhu Uthuruburi Atholhu Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(507, NULL, NULL, 'Huvafenfushi\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(508, NULL, NULL, 'Huvandhumaa Fushi\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(509, NULL, NULL, 'Ihadhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(510, NULL, NULL, 'Imaduddin School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(511, NULL, NULL, 'Indira Gandhi Memorial Hospital\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(512, NULL, NULL, 'Information Commissioner ge Office\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(513, NULL, NULL, 'Irushadhiyya School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(514, NULL, NULL, 'Iskandhar School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(515, NULL, NULL, 'Islamic Development Bank\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(516, NULL, NULL, 'Island Aviation Services Limited\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(517, NULL, NULL, 'Island of Bolifushi\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(518, NULL, NULL, 'Jalaluddin School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(519, NULL, NULL, 'Jamaaluddeen School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(520, NULL, NULL, 'Judicial Service Commission\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(521, NULL, NULL, 'Jumeirah Dhevanafushi\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(522, NULL, NULL, 'Juvenile Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(523, NULL, NULL, 'Juvenile Justice Unit\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(524, NULL, NULL, 'K. Atoll Education Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(525, NULL, NULL, 'Kaafu Atholhu Madharusaa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(526, NULL, NULL, 'Kaafu Dhiffushee School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(527, NULL, NULL, 'Kaafu Dhiffushi Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(528, NULL, NULL, 'Kaafu Family and children service centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(529, NULL, NULL, 'Kaafu Gaafaru Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(530, NULL, NULL, 'Kaafu Gaafaru School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(531, NULL, NULL, 'Kaafu Gulhee School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(532, NULL, NULL, 'Kaafu Gulhi Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(533, NULL, NULL, 'Kaafu Guraidhoo Heath Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(534, NULL, NULL, 'Kaafu Guraidhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(535, NULL, NULL, 'Kaafu Himmafushi Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(536, NULL, NULL, 'Kaafu Hinmafushee School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(537, NULL, NULL, 'Kaafu Huraa Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(538, NULL, NULL, 'Kaafu Huraa School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(539, NULL, NULL, 'Kaafu Kaashidhoo Health Centre \r', NULL, 3, 1, '2015-08-19 06:59:01'),
(540, NULL, NULL, 'Kaafu Maafushee School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(541, NULL, NULL, 'Kaafu Maafushi Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(542, NULL, NULL, 'Kaafu Male Youth Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(543, NULL, NULL, 'Kaafu Villingili Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(544, NULL, NULL, 'Kaafu Villingili Youth Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(545, NULL, NULL, 'Kalaafaanu School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(546, NULL, NULL, 'Kanduhulhudhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01');
INSERT INTO `office` (`O_ID`, `O_AGA`, `O_BA`, `O_NAME`, `O_ADDRESS`, `O_DEFAULTPRIO`, `O_CREATEDBY`, `O_TIMESTAMP`) VALUES
(547, NULL, NULL, 'Kanuhuraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(548, NULL, NULL, 'Kihaadhuffaru Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(549, NULL, NULL, 'Kolhumadulu Atholhu Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(550, NULL, NULL, 'Kolhumadulu Burunee Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(551, NULL, NULL, 'Kolhumadulu Buruni Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(552, NULL, NULL, 'Kolhumadulu Dhiyamigili Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(553, NULL, NULL, 'Kolhumadulu Dhiyamigili Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(554, NULL, NULL, 'Kolhumadulu Gaadhiffushi Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(555, NULL, NULL, 'Kolhumadulu Gaadhiffushi Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(556, NULL, NULL, 'Kolhumadulu Guraidhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(557, NULL, NULL, 'Kolhumadulu Guraidhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(558, NULL, NULL, 'Kolhumadulu Hirilandhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(559, NULL, NULL, 'Kolhumadulu Hirilandhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(560, NULL, NULL, 'Kolhumadulu Kan\'doodhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(561, NULL, NULL, 'Kolhumadulu Kandoodhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(562, NULL, NULL, 'Kolhumadulu Kinbidhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(563, NULL, NULL, 'Kolhumadulu Kinbidhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(564, NULL, NULL, 'Kolhumadulu Madifushi Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(565, NULL, NULL, 'Kolhumadulu Madifushi Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(566, NULL, NULL, 'Kolhumadulu Omadhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(567, NULL, NULL, 'Kolhumadulu Omadhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(568, NULL, NULL, 'Kolhumadulu Thimarafushi Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(569, NULL, NULL, 'Kolhumadulu Thimarafushi Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(570, NULL, NULL, 'Kolhumadulu Vandhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(571, NULL, NULL, 'Kolhumadulu Vandhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(572, NULL, NULL, 'Kolhumadulu Veymandoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(573, NULL, NULL, 'Kolhumadulu Veymandoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(574, NULL, NULL, 'Kolhumadulu Vilifushi Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(575, NULL, NULL, 'Kolhumadulu Vilufushi Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(576, NULL, NULL, 'Komandoo Maldive Island Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(577, NULL, NULL, 'Koodoo Fisheries Maldives Limited\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(578, NULL, NULL, 'Kudakudhinge Hiya\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(579, NULL, NULL, 'Kudarah Island Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(580, NULL, NULL, 'Kulhudhuffushi Port Limited\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(581, NULL, NULL, 'Kuramathi Tourist Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(582, NULL, NULL, 'Kuredhdhu Island Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(583, NULL, NULL, 'Kurumba Maldives\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(584, NULL, NULL, 'Kuwait Fund for Arab Economic Development\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(585, NULL, NULL, 'Laamu Atholhu Madharusaa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(586, NULL, NULL, 'Laamu Atoll Education Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(587, NULL, NULL, 'Laamu Dhan\'bidhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(588, NULL, NULL, 'Laamu Dhanbidhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(589, NULL, NULL, 'Laamu Family and children service centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(590, NULL, NULL, 'Laamu Fonadhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(591, NULL, NULL, 'Laamu Gaadhoo Health Post\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(592, NULL, NULL, 'Laamu Gaadhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(593, NULL, NULL, 'Laamu Gamu Mathimaradhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(594, NULL, NULL, 'Laamu Gamu Mukurimagu School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(595, NULL, NULL, 'Laamu Gamu Thun\'dee School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(596, NULL, NULL, 'Laamu Hithadhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(597, NULL, NULL, 'Laamu Hithadhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(598, NULL, NULL, 'Laamu Isdhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(599, NULL, NULL, 'Laamu Isdhoo Kalhaidhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(600, NULL, NULL, 'Laamu Isdhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(601, NULL, NULL, 'Laamu Isdhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(602, NULL, NULL, 'Laamu Isdhoo-Kalaidhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(603, NULL, NULL, 'Laamu Kalhaidhoo Madhrasaa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(604, NULL, NULL, 'Laamu Kalhaidhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(605, NULL, NULL, 'Laamu Kanahandhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(606, NULL, NULL, 'Laamu Kunahandhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(607, NULL, NULL, 'Laamu Maabaidhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(608, NULL, NULL, 'Laamu Maamendhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(609, NULL, NULL, 'Laamu Maamendhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(610, NULL, NULL, 'Laamu Maavah Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(611, NULL, NULL, 'Laamu Maavashu School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(612, NULL, NULL, 'Laamu Mundhoo Health Post\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(613, NULL, NULL, 'Laamu Mundoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(614, NULL, NULL, 'Labour Relations Authority\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(615, NULL, NULL, 'Lhaviyani Atholhu Madharusaa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(616, NULL, NULL, 'Lhaviyani Atholhu Tha\"Leemee Marukaz\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(617, NULL, NULL, 'Lhaviyani Atoll Hospital\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(618, NULL, NULL, 'Lhaviyani Family and children service centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(619, NULL, NULL, 'Lhaviyani Hinnavaru Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(620, NULL, NULL, 'Lhaviyani Hinnavaru Youth Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(621, NULL, NULL, 'Lhaviyani Kurendhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(622, NULL, NULL, 'Lhaviyani Maafilaafushee School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(623, NULL, NULL, 'Lhaviyani Maafilaafushi Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(624, NULL, NULL, 'Lhaviyani Olhuvelifushee School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(625, NULL, NULL, 'Lhaviyani Olhuvelifushi Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(626, NULL, NULL, 'Lily Beach Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(627, NULL, NULL, 'Local Government Authority\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(628, NULL, NULL, 'Maafannu Avashu Office\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(629, NULL, NULL, 'Maafannu Madharusaa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(630, NULL, NULL, 'Maalhosmadhulu Dhekunuburi Biosphere Reserve Office\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(631, NULL, NULL, 'Maalhosmadulu Dhekunuburi Atholhu Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(632, NULL, NULL, 'Maalhosmadulu Dhekunuburi Dharavandhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(633, NULL, NULL, 'Maalhosmadulu Dhekunuburi Dharavandhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(634, NULL, NULL, 'Maalhosmadulu Dhekunuburi Dhonfanu Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(635, NULL, NULL, 'Maalhosmadulu Dhekunuburi Dhonfanu Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(636, NULL, NULL, 'Maalhosmadulu Dhekunuburi Eydhafushi Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(637, NULL, NULL, 'Maalhosmadulu Dhekunuburi Eydhafushi Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(638, NULL, NULL, 'Maalhosmadulu Dhekunuburi Fehendhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(639, NULL, NULL, 'Maalhosmadulu Dhekunuburi Fehendhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(640, NULL, NULL, 'Maalhosmadulu Dhekunuburi Fulhadhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(641, NULL, NULL, 'Maalhosmadulu Dhekunuburi Fulhadhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(642, NULL, NULL, 'Maalhosmadulu Dhekunuburi Goidhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(643, NULL, NULL, 'Maalhosmadulu Dhekunuburi Goidhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(644, NULL, NULL, 'Maalhosmadulu Dhekunuburi Hithaadhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(645, NULL, NULL, 'Maalhosmadulu Dhekunuburi Hithaadhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(646, NULL, NULL, 'Maalhosmadulu Dhekunuburi Kamadhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(647, NULL, NULL, 'Maalhosmadulu Dhekunuburi Kamadhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(648, NULL, NULL, 'Maalhosmadulu Dhekunuburi Kendhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(649, NULL, NULL, 'Maalhosmadulu Dhekunuburi Kendhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(650, NULL, NULL, 'Maalhosmadulu Dhekunuburi Kihaadhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(651, NULL, NULL, 'Maalhosmadulu Dhekunuburi Kihaadhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(652, NULL, NULL, 'Maalhosmadulu Dhekunuburi Kudarikilu Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(653, NULL, NULL, 'Maalhosmadulu Dhekunuburi Kudarikilu Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(654, NULL, NULL, 'Maalhosmadulu Dhekunuburi Maalhohu Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(655, NULL, NULL, 'Maalhosmadulu Dhekunuburi Maalhos Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(656, NULL, NULL, 'Maalhosmadulu Dhekunuburi Thulhaadhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(657, NULL, NULL, 'Maalhosmadulu Dhekunuburi Thulhaadhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(658, NULL, NULL, 'Maalhosmadulu Uthuruburi Alifushee Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(659, NULL, NULL, 'Maalhosmadulu Uthuruburi Alifushi Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(660, NULL, NULL, 'Maalhosmadulu Uthuruburi An\'golhitheemu Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(661, NULL, NULL, 'Maalhosmadulu Uthuruburi Angolhitheemu Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(662, NULL, NULL, 'Maalhosmadulu Uthuruburi Atholhu Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(663, NULL, NULL, 'Maalhosmadulu Uthuruburi Dhuvaafaru Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(664, NULL, NULL, 'Maalhosmadulu Uthuruburi Dhuvaafaru Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(665, NULL, NULL, 'Maalhosmadulu Uthuruburi Fainu Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(666, NULL, NULL, 'Maalhosmadulu Uthuruburi Fainu Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(667, NULL, NULL, 'Maalhosmadulu Uthuruburi Hulhudhuffaaru Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(668, NULL, NULL, 'Maalhosmadulu Uthuruburi Hulhuduffaaru Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(669, NULL, NULL, 'Maalhosmadulu Uthuruburi Inguraidhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(670, NULL, NULL, 'Maalhosmadulu Uthuruburi Inguraidhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(671, NULL, NULL, 'Maalhosmadulu Uthuruburi Innamaadhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(672, NULL, NULL, 'Maalhosmadulu Uthuruburi Innamaadhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(673, NULL, NULL, 'Maalhosmadulu Uthuruburi Kinolhas Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(674, NULL, NULL, 'Maalhosmadulu Uthuruburi Kinolhas Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(675, NULL, NULL, 'Maalhosmadulu Uthuruburi Maakurathu Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(676, NULL, NULL, 'Maalhosmadulu Uthuruburi Maakurathu Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(677, NULL, NULL, 'Maalhosmadulu Uthuruburi Maduvvaree Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(678, NULL, NULL, 'Maalhosmadulu Uthuruburi Maduvvari Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(679, NULL, NULL, 'Maalhosmadulu Uthuruburi Meedhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(680, NULL, NULL, 'Maalhosmadulu Uthuruburi Meedhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(681, NULL, NULL, 'Maalhosmadulu Uthuruburi Rasgetheemu Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(682, NULL, NULL, 'Maalhosmadulu Uthuruburi Rasgetheemu Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(683, NULL, NULL, 'Maalhosmadulu Uthuruburi Rasmaadhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(684, NULL, NULL, 'Maalhosmadulu Uthuruburi Rasmaadhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(685, NULL, NULL, 'Maalhosmadulu Uthuruburi Un\'goofaaru Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(686, NULL, NULL, 'Maalhosmadulu Uthuruburi Ungoofaaru Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(687, NULL, NULL, 'Maalhosmadulu Uthuruburi Vaadhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(688, NULL, NULL, 'Maalhosmadulu Uthuruburi Vaadhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(689, NULL, NULL, 'Maayafushi Tourist Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(690, NULL, NULL, 'Machangolhi Avashu Office\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(691, NULL, NULL, 'Madharusathul Irushaadhiyya (S Maradhoo)\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(692, NULL, NULL, 'Madharusathul Shaikh Muhammadh Jamaaludhdheen\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(693, NULL, NULL, 'Madhrasathul Ahmadhiyya\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(694, NULL, NULL, 'Madhrasathul Ameer Ahmed\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(695, NULL, NULL, 'Madhrasathul Ifthithaah\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(696, NULL, NULL, 'Madhrasathul Shaheed Ali Thakurufaanu\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(697, NULL, NULL, 'Madhrasathul Sultan Mohamed Thakurufaanul Auzam\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(698, NULL, NULL, 'Madivaru Holdings Private Limited\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(699, NULL, NULL, 'Madoogali Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(700, NULL, NULL, 'Majeediyya School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(701, NULL, NULL, 'Makunudhoo Island\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(702, NULL, NULL, 'Maldives Airports Company Limited\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(703, NULL, NULL, 'Maldives Basketball Association\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(704, NULL, NULL, 'Maldives Broadcasting Commission\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(705, NULL, NULL, 'Maldives Centre for Social Education\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(706, NULL, NULL, 'Maldives Civil Aviation Authority\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(707, NULL, NULL, 'Maldives Correctional Service\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(708, NULL, NULL, 'Maldives Customs Services\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(709, NULL, NULL, 'Maldives Energy Authority\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(710, NULL, NULL, 'Maldives Entertainment Company Limited\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(711, NULL, NULL, 'Maldives Finance Leasing Company\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(712, NULL, NULL, 'Maldives Food and Drug Authority\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(713, NULL, NULL, 'Maldives Girl Guide Association\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(714, NULL, NULL, 'Maldives Hajj Corporation Ltd\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(715, NULL, NULL, 'Maldives Industrial Agriculture Copany\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(716, NULL, NULL, 'Maldives Industrial Fisheries Company Limited\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(717, NULL, NULL, 'Maldives Inflight Catering Pte Ltd\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(718, NULL, NULL, 'Maldives Inland Revenue Authority\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(719, NULL, NULL, 'Maldives Islamic Bank\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(720, NULL, NULL, 'Maldives Land and Survey Authority\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(721, NULL, NULL, 'Maldives Marketing and Public Relations Corporation Limited\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(722, NULL, NULL, 'Maldives Media Council\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(723, NULL, NULL, 'Maldives Meteorological Services\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(724, NULL, NULL, 'Maldives Monetary Authority\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(725, NULL, NULL, 'Maldives National Chamber of Commerce and Industry\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(726, NULL, NULL, 'Maldives National Defense Force\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(727, NULL, NULL, 'Maldives National Institute of Sports\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(728, NULL, NULL, 'Maldives National Shipping Ltd\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(729, NULL, NULL, 'Maldives Olympic Committee\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(730, NULL, NULL, 'Maldives Pension Administration Office\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(731, NULL, NULL, 'Maldives Police Service\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(732, NULL, NULL, 'Maldives Polytechnic\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(733, NULL, NULL, 'Maldives Ports Limited\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(734, NULL, NULL, 'Maldives Post Limited\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(735, NULL, NULL, 'Maldives Qualifications Authority\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(736, NULL, NULL, 'Maldives Road Development Corporation\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(737, NULL, NULL, 'Maldives Tourism Development Corporation Plc.\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(738, NULL, NULL, 'Maldives Transport and Contracting Company\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(739, NULL, NULL, 'Maldives Water and Sanitation Authority\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(740, NULL, NULL, 'Maldivian Blood Services\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(741, NULL, NULL, 'Male\' Atholhu Dhiffushi Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(742, NULL, NULL, 'Male\' Atholhu Gaafaru Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(743, NULL, NULL, 'Male\' Atholhu Gulhi Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(744, NULL, NULL, 'Male\' Atholhu Guraidhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(745, NULL, NULL, 'Male\' Atholhu Hinmafushi Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(746, NULL, NULL, 'Male\' Atholhu Huraa Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(747, NULL, NULL, 'Male\' Atholhu Kaashidhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(748, NULL, NULL, 'Male\' Atholhu Maafushi Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(749, NULL, NULL, 'Male\' Atholhu Thulusdhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(750, NULL, NULL, 'Male\' Atoll Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(751, NULL, NULL, 'Male\' City Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(752, NULL, NULL, 'Male\' Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(753, NULL, NULL, 'Male\' Health Services Corporation Ltd.\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(754, NULL, NULL, 'Male\' Water and Sewerage Company Pvt. Ltd.\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(755, NULL, NULL, 'Male\'Atholhu Dhiffushi Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(756, NULL, NULL, 'Male\'Atholhu Gaafaru Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(757, NULL, NULL, 'Male\'Atholhu Gulhi Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(758, NULL, NULL, 'Male\'Atholhu Guraidhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(759, NULL, NULL, 'Male\'Atholhu Hinmafushi Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(760, NULL, NULL, 'Male\'Atholhu Huraa Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(761, NULL, NULL, 'Male\'Atholhu Kaashidhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(762, NULL, NULL, 'Male\'Atholhu Maafushi Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(763, NULL, NULL, 'Male\'Atholhu Thulusdhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(764, NULL, NULL, 'Male\' Atholhu Councilge Idhaaraa', NULL, 3, 1, '2015-08-19 06:59:01'),
(765, NULL, NULL, 'Marine Research Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(766, NULL, NULL, 'Medhufushi Island Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(767, NULL, NULL, 'Meemu Atholhu Madharusaa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(768, NULL, NULL, 'Meemu Atholhu Thauleemee Marukaz\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(769, NULL, NULL, 'Meemu Dhiggaru School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(770, NULL, NULL, 'Meemu Dhihggaru Health Centre \r', NULL, 3, 1, '2015-08-19 06:59:01'),
(771, NULL, NULL, 'Meemu Family and children service centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(772, NULL, NULL, 'Meemu Kolhufushi Health centre \r', NULL, 3, 1, '2015-08-19 06:59:01'),
(773, NULL, NULL, 'Meemu Maduhvvaree Health Centre \r', NULL, 3, 1, '2015-08-19 06:59:01'),
(774, NULL, NULL, 'Meemu Maduvvaree School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(775, NULL, NULL, 'Meemu Mulak Health Centre \r', NULL, 3, 1, '2015-08-19 06:59:01'),
(776, NULL, NULL, 'Meemu Mulaku School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(777, NULL, NULL, 'Meemu Naalaafushee School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(778, NULL, NULL, 'Meemu Naalaafushi Health Post \r', NULL, 3, 1, '2015-08-19 06:59:01'),
(779, NULL, NULL, 'Meemu Raimandhoo Health Post \r', NULL, 3, 1, '2015-08-19 06:59:01'),
(780, NULL, NULL, 'Meemu Raiymandhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(781, NULL, NULL, 'Meemu Vevashu Health Post \r', NULL, 3, 1, '2015-08-19 06:59:01'),
(782, NULL, NULL, 'Meemu Veyvashu School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(783, NULL, NULL, 'Meeru Island Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(784, NULL, NULL, 'Miladhunmadulu Dhekunuburi Atholhu Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(785, NULL, NULL, 'Miladhunmadulu Dhekunuburi Fodhdhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(786, NULL, NULL, 'Miladhunmadulu Dhekunuburi Fohdhdhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(787, NULL, NULL, 'Miladhunmadulu Dhekunuburi Hen\'badhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(788, NULL, NULL, 'Miladhunmadulu Dhekunuburi Henbadhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(789, NULL, NULL, 'Miladhunmadulu Dhekunuburi Holhudhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(790, NULL, NULL, 'Miladhunmadulu Dhekunuburi Holhudhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(791, NULL, NULL, 'Miladhunmadulu Dhekunuburi Kedhikulhudhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(792, NULL, NULL, 'Miladhunmadulu Dhekunuburi Ken\'dhikulhudhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(793, NULL, NULL, 'Miladhunmadulu Dhekunuburi Kudafari Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(794, NULL, NULL, 'Miladhunmadulu Dhekunuburi Kudafari Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(795, NULL, NULL, 'Miladhunmadulu Dhekunuburi Landhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(796, NULL, NULL, 'Miladhunmadulu Dhekunuburi Landhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(797, NULL, NULL, 'Miladhunmadulu Dhekunuburi Lhohi Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(798, NULL, NULL, 'Miladhunmadulu Dhekunuburi Lhohi Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(799, NULL, NULL, 'Miladhunmadulu Dhekunuburi Maafaru Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(800, NULL, NULL, 'Miladhunmadulu Dhekunuburi Maafaru Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(801, NULL, NULL, 'Miladhunmadulu Dhekunuburi Maalhendhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(802, NULL, NULL, 'Miladhunmadulu Dhekunuburi Maalhendhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(803, NULL, NULL, 'Miladhunmadulu Dhekunuburi Magoodhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(804, NULL, NULL, 'Miladhunmadulu Dhekunuburi Magoodhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(805, NULL, NULL, 'Miladhunmadulu Dhekunuburi Manadhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(806, NULL, NULL, 'Miladhunmadulu Dhekunuburi Manadhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(807, NULL, NULL, 'Miladhunmadulu Dhekunuburi Miladhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(808, NULL, NULL, 'Miladhunmadulu Dhekunuburi Miladhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(809, NULL, NULL, 'Miladhunmadulu Dhekunuburi Velidhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(810, NULL, NULL, 'Miladhunmadulu Dhekunuburi Velidhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(811, NULL, NULL, 'Miladhunmadulu Uthuruburee Kanditheemu Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(812, NULL, NULL, 'Miladhunmadulu Uthuruburi Atholhu Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(813, NULL, NULL, 'Miladhunmadulu Uthuruburi Bilehffahi Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(814, NULL, NULL, 'Miladhunmadulu Uthuruburi Feevah Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(815, NULL, NULL, 'Miladhunmadulu Uthuruburi Feydhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(816, NULL, NULL, 'Miladhunmadulu Uthuruburi Foakaidhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(817, NULL, NULL, 'Miladhunmadulu Uthuruburi Funadhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(818, NULL, NULL, 'Miladhunmadulu Uthuruburi Funadhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(819, NULL, NULL, 'Miladhunmadulu Uthuruburi Goidhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(820, NULL, NULL, 'Miladhunmadulu Uthuruburi Goidhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(821, NULL, NULL, 'Miladhunmadulu Uthuruburi Kanditheemu Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(822, NULL, NULL, 'Miladhunmadulu Uthuruburi Komandoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(823, NULL, NULL, 'Miladhunmadulu Uthuruburi Komandoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(824, NULL, NULL, 'Miladhunmadulu Uthuruburi Lhaimagu Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(825, NULL, NULL, 'Miladhunmadulu Uthuruburi Lhaimagu Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(826, NULL, NULL, 'Miladhunmadulu Uthuruburi Maaun\'goodhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(827, NULL, NULL, 'Miladhunmadulu Uthuruburi Maaungoodhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(828, NULL, NULL, 'Miladhunmadulu Uthuruburi Maroshi Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(829, NULL, NULL, 'Miladhunmadulu Uthuruburi Maroshi Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(830, NULL, NULL, 'Miladhunmadulu Uthuruburi Milandhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(831, NULL, NULL, 'Miladhunmadulu Uthuruburi Milandhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(832, NULL, NULL, 'Miladhunmadulu Uthuruburi Narudhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(833, NULL, NULL, 'Miladhunmadulu Uthuruburi Narudhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(834, NULL, NULL, 'Miladhunmadulu Uthuruburi Noomaraa Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(835, NULL, NULL, 'Miladhunmadulu Uthuruburi Noomaraa Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(836, NULL, NULL, 'Miladhunmadulu Uthuruburi Shaviyani Bileffahi Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(837, NULL, NULL, 'Miladhunmadulu Uthuruburi Shaviyani Feevaku Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(838, NULL, NULL, 'Miladhunmadulu Uthuruburi Shaviyani Feydhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(839, NULL, NULL, 'Miladhunmadulu UthuruburiShaviyani Foakaidhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(840, NULL, NULL, 'Ministry of Defense', NULL, 3, 1, '2015-08-19 06:59:01'),
(841, NULL, NULL, 'Ministry of Economic Development\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(842, NULL, NULL, 'Ministry of Education\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(843, NULL, NULL, 'Ministry of Environment', NULL, 3, 1, '2015-08-19 06:59:01'),
(844, NULL, NULL, 'Ministry of Finance', NULL, 3, 1, '2015-08-19 06:59:01'),
(845, NULL, NULL, 'Ministry of Fisheries Marine Resources and Agriculture', NULL, 3, 1, '2015-08-19 06:59:01'),
(846, NULL, NULL, 'Ministry of Foreign Affairs\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(847, NULL, NULL, 'Ministry of Gender, Family and Human Rights\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(848, NULL, NULL, 'Ministry of Health \r', NULL, 3, 1, '2015-08-19 06:59:01'),
(849, NULL, NULL, 'Ministry of Home Affairs\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(850, NULL, NULL, 'Ministry of Housing and Urban Development', NULL, 3, 1, '2015-08-19 06:59:01'),
(851, NULL, NULL, 'Ministry of Islamic Affairs\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(852, NULL, NULL, 'Ministry of Gender,Family and Social Services', NULL, 3, 1, '2015-08-19 06:59:01'),
(853, NULL, NULL, 'Ministry of Tourism\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(854, NULL, NULL, 'Ministry of Transport and Civil Aviation', NULL, 3, 1, '2015-08-19 06:59:01'),
(855, NULL, NULL, 'Ministry of Youth, Sports and Community Empowerment', NULL, 3, 1, '2015-08-19 06:59:01'),
(856, NULL, NULL, 'Mirihi Island Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(857, NULL, NULL, 'Muhyiddin School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(858, NULL, NULL, 'Mulakatholhu Atholhu Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(859, NULL, NULL, 'Mulaku Atholhu  Dhiggaru Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(860, NULL, NULL, 'Mulaku Atholhu Dhiggaru Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(861, NULL, NULL, 'Mulaku Atholhu Kolhufushi Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(862, NULL, NULL, 'Mulaku Atholhu Kolhufushi Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(863, NULL, NULL, 'Mulaku Atholhu Maduvvari   Maduvvari Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(864, NULL, NULL, 'Mulaku Atholhu Maduvvari Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(865, NULL, NULL, 'Mulaku Atholhu Mulah Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(866, NULL, NULL, 'Mulaku Atholhu Mulah Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(867, NULL, NULL, 'Mulaku Atholhu Muli  Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(868, NULL, NULL, 'Mulaku Atholhu Muli Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(869, NULL, NULL, 'Mulaku Atholhu Naalaafushi Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(870, NULL, NULL, 'Mulaku Atholhu Nailaafushi Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(871, NULL, NULL, 'Mulaku Atholhu Raiyamandhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(872, NULL, NULL, 'Mulaku Atholhu Raiymandhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(873, NULL, NULL, 'Mulaku Atholhu Veyvah Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(874, NULL, NULL, 'Mulaku Atholhu Veyvah Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(875, NULL, NULL, 'Mulee Aage\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(876, NULL, NULL, 'Muli Regional Hospital\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(877, NULL, NULL, 'Muniufa Youth Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(878, NULL, NULL, 'Naaibu Aboobakuru School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(879, NULL, NULL, 'Naladhu\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(880, NULL, NULL, 'National Art Gallery\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(881, NULL, NULL, 'National Bureau of Classification\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(882, NULL, NULL, 'National Bureau of Statistics\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(883, NULL, NULL, 'National Centre for Information Technology\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(884, NULL, NULL, 'National Centre for Linguistic and Historical Research\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(885, NULL, NULL, 'National Centre for the Arts\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(886, NULL, NULL, 'National Disaster Management Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(887, NULL, NULL, 'National Drug Agency\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(888, NULL, NULL, 'National Elections Complaints Bureau\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(889, NULL, NULL, 'National Institute of Education\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(890, NULL, NULL, 'National Law Reform Commission\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(891, NULL, NULL, 'National Library\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(892, NULL, NULL, 'National Social Protection Agency\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(893, NULL, NULL, 'National Thalasaemia Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(894, NULL, NULL, 'Netball Association of Maldives\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(895, NULL, NULL, 'Nika Island Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(896, NULL, NULL, 'Nilandhe Atholhu Dhekunuburi  Meedhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(897, NULL, NULL, 'Nilandhe Atholhu Dhekunuburi  Rinbudhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(898, NULL, NULL, 'Nilandhe Atholhu Dhekunuburi Atholhu Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(899, NULL, NULL, 'Nilandhe Atholhu Dhekunuburi Ban\'didhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(900, NULL, NULL, 'Nilandhe Atholhu Dhekunuburi Bandidhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(901, NULL, NULL, 'Nilandhe Atholhu Dhekunuburi Hulhudheli Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(902, NULL, NULL, 'Nilandhe Atholhu Dhekunuburi Hulhudheli Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(903, NULL, NULL, 'Nilandhe Atholhu Dhekunuburi Kudahuvadhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(904, NULL, NULL, 'Nilandhe Atholhu Dhekunuburi Kudahuvadhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(905, NULL, NULL, 'Nilandhe Atholhu Dhekunuburi Maaen\'boodhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(906, NULL, NULL, 'Nilandhe Atholhu Dhekunuburi Maaenboodhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(907, NULL, NULL, 'Nilandhe Atholhu Dhekunuburi Meedhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(908, NULL, NULL, 'Nilandhe Atholhu Dhekunuburi Rin\'budhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(909, NULL, NULL, 'Nilandhe Atholhu Dhekunuburi Vaanee Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(910, NULL, NULL, 'Nilandhe Atholhu Uthuruburi Atholhu Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(911, NULL, NULL, 'Nilandhe Atholhu Uthuruburi Bileiydhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(912, NULL, NULL, 'Nilandhe Atholhu Uthuruburi Bileiydhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(913, NULL, NULL, 'Nilandhe Atholhu Uthuruburi Dharan\'boodhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(914, NULL, NULL, 'Nilandhe Atholhu Uthuruburi Dharanboodhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(915, NULL, NULL, 'Nilandhe Atholhu Uthuruburi Feeali Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(916, NULL, NULL, 'Nilandhe Atholhu Uthuruburi Feeali Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(917, NULL, NULL, 'Nilandhe Atholhu Uthuruburi Magoodhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(918, NULL, NULL, 'Nilandhe Atholhu Uthuruburi Magoodhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(919, NULL, NULL, 'Nilandhe Atholhu Uthuruburi Nilandhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(920, NULL, NULL, 'Nilandhe Atholhu Uthuruburi Nilandhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(921, NULL, NULL, 'Nilandhe Atoll Dhekunuburee Vaani Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(922, NULL, NULL, 'Noonu Atholhu Tha\"Leemee Marukaz\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(923, NULL, NULL, 'Noonu Atoll Hospital\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(924, NULL, NULL, 'Noonu Atoll School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(925, NULL, NULL, 'Noonu Family and children service centre \r', NULL, 3, 1, '2015-08-19 06:59:01'),
(926, NULL, NULL, 'Noonu Fodhdhoo Health Post\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(927, NULL, NULL, 'Noonu Fodhdhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(928, NULL, NULL, 'Noonu Fohdhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(929, NULL, NULL, 'Noonu Hen\'badhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(930, NULL, NULL, 'Noonu Hen`badhoo Health Center\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(931, NULL, NULL, 'Noonu Holhudhoo Health Center\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(932, NULL, NULL, 'Noonu Holhudhoo Meynaa School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(933, NULL, NULL, 'Noonu Ken\'dhikulhudhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(934, NULL, NULL, 'Noonu Kendhikulhudhoo Health Center\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(935, NULL, NULL, 'Noonu Kudafaree School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(936, NULL, NULL, 'Noonu Kudafari Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(937, NULL, NULL, 'Noonu Landhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(938, NULL, NULL, 'Noonu Landhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(939, NULL, NULL, 'Noonu Lhohee Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(940, NULL, NULL, 'Noonu Lhohee School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(941, NULL, NULL, 'Noonu Maafaru Health Center\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(942, NULL, NULL, 'Noonu Maafaru School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(943, NULL, NULL, 'Noonu Maalhendhoo Health Center\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(944, NULL, NULL, 'Noonu Maalhendhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(945, NULL, NULL, 'Noonu Magoodhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(946, NULL, NULL, 'Noonu Magoodhoo Health Post\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(947, NULL, NULL, 'Noonu Magoodhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(948, NULL, NULL, 'Noonu Miladhoo Health Center\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(949, NULL, NULL, 'Noonu Miladhoo Hidhaya School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(950, NULL, NULL, 'Noonu Velidhoo Health Center\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(951, NULL, NULL, 'North Central Health Service Corporation Limited\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(952, NULL, NULL, 'North Central Utilities Limited\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(953, NULL, NULL, 'Northern Utilities Limited\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(954, NULL, NULL, 'Nothern Health Service Corporation\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(955, NULL, NULL, 'Olhuveli Beach & Spa Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(956, NULL, NULL, 'One & Only Reethi Rah\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(957, NULL, NULL, 'Palm Beach Island\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(958, NULL, NULL, 'Paradise Island Resort & Spa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(959, NULL, NULL, 'Park Hyatt Maldives Hadahaa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(960, NULL, NULL, 'Peoples Majlis\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(961, NULL, NULL, 'Permanent Mission of Maldives in the European Communities\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(962, NULL, NULL, 'Permanent Mission of the Maldives to the United Nations\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(963, NULL, NULL, 'Permanent Mission of the Maldives to the United Nations office in Geneva\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(964, NULL, NULL, 'Police Integrity Commission\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(965, NULL, NULL, 'Presidents Office\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(966, NULL, NULL, 'Private\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(967, NULL, NULL, 'Privatization and Corporatization Board\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(968, NULL, NULL, 'Prosecutor General\'s Office\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(969, NULL, NULL, 'Public Compliants Bureau\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(970, NULL, NULL, 'Public Service Media\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(971, NULL, NULL, 'Public Works Services\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(972, NULL, NULL, 'Quality Assurance Department\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(973, NULL, NULL, 'Raa Alifushee School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(974, NULL, NULL, 'Raa Alifushi Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(975, NULL, NULL, 'Raa An\'golhitheemu School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(976, NULL, NULL, 'Raa Angolhitheem Health Post\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(977, NULL, NULL, 'Raa Atholhu Madharusaa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(978, NULL, NULL, 'Raa Atholhu Tha\'Leemee Marukaz\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(979, NULL, NULL, 'Raa Dhuvaafaru Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(980, NULL, NULL, 'Raa Dhuvaafaru Primary School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(981, NULL, NULL, 'Raa Dhuvaafaru Secondary School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(982, NULL, NULL, 'Raa Fainu Health Post\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(983, NULL, NULL, 'Raa Fainu School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(984, NULL, NULL, 'Raa Family and children service centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(985, NULL, NULL, 'Raa Hulhudhuffaaru Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(986, NULL, NULL, 'Raa Hulhudhuffaaru School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(987, NULL, NULL, 'Raa Inguraidhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(988, NULL, NULL, 'Raa Inguraidhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(989, NULL, NULL, 'Raa Innamaadhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(990, NULL, NULL, 'Raa Innamaadhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(991, NULL, NULL, 'Raa Kinolhahu School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(992, NULL, NULL, 'Raa Kinolhohu Health Post\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(993, NULL, NULL, 'Raa Maakurath Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(994, NULL, NULL, 'Raa Maakurathu School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(995, NULL, NULL, 'Raa Madduvari Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(996, NULL, NULL, 'Raa Maduvvaree School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(997, NULL, NULL, 'Raa Meedhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(998, NULL, NULL, 'Raa Rasgetheem Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(999, NULL, NULL, 'Raa Rasgetheemu School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1000, NULL, NULL, 'Raa Rasmaadhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1001, NULL, NULL, 'Raa Rasmaadhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1002, NULL, NULL, 'Raa Ugoofaaru Youth Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1003, NULL, NULL, 'Raa Un\'goofaaru School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1004, NULL, NULL, 'Raa Ungoofaaru Regional Hospital\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1005, NULL, NULL, 'Raa Vaadhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1006, NULL, NULL, 'Raa Vaadhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1007, NULL, NULL, 'Ranveli Village\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1008, NULL, NULL, 'Reethi Beach Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1009, NULL, NULL, 'Regional Airports\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1010, NULL, NULL, 'Rehendhi School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1011, NULL, NULL, 'Rihiveli Beach Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1012, NULL, NULL, 'Riyaasee Commission (16/2009)\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1013, NULL, NULL, 'Royal Island Resort & Spa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1014, NULL, NULL, 'Saarc Development Fund\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1015, NULL, NULL, 'Sarukaaru Muvazafunge Nadhee\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1016, NULL, NULL, 'Seenu Atholhu Madharusaa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1017, NULL, NULL, 'Seenu Atholhu Tha\"Leemee Marukaz\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1018, NULL, NULL, 'Seenu Family and children service centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1019, NULL, NULL, 'Seenu Feydhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1020, NULL, NULL, 'Seenu Feydhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1021, NULL, NULL, 'Seenu Feydhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1022, NULL, NULL, 'Seenu Gan Youth Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1023, NULL, NULL, 'Seenu Hithadhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1024, NULL, NULL, 'Seenu Hithadhoo Nooraanee School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1025, NULL, NULL, 'Seenu Hithadhoo Regional Hospital\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1026, NULL, NULL, 'Seenu Hithadhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1027, NULL, NULL, 'Seenu Hithadhoo Stadium\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1028, NULL, NULL, 'Seenu Hulhu-Meedhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1029, NULL, NULL, 'Seenu Hulhudhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1030, NULL, NULL, 'Seenu Hulhudhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1031, NULL, NULL, 'Seenu Maradhoo Feydhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1032, NULL, NULL, 'Seenu Maradhoo Feydhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1033, NULL, NULL, 'Seenu Maradhoo Feydhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1034, NULL, NULL, 'Seenu Maradhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1035, NULL, NULL, 'Seenu Maradhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1036, NULL, NULL, 'Seenu Maradhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1037, NULL, NULL, 'Seenu Meedhoo Ghaazee Mohamed Shamsudhdheen School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1038, NULL, NULL, 'Seenu Meedhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1039, NULL, NULL, 'Shangri - La Villigili Resort & Spa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1040, NULL, NULL, 'Sharafuddeen School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1041, NULL, NULL, 'Shaviyani Atholhu Madharusaa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1042, NULL, NULL, 'Shaviyani Atholhu Tha\"Leemee Marukaz\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1043, NULL, NULL, 'Shaviyani Bilehfahi Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1044, NULL, NULL, 'Shaviyani Bileiyfahi School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1045, NULL, NULL, 'Shaviyani Family and children service centre \r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1046, NULL, NULL, 'Shaviyani Feevah Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1047, NULL, NULL, 'Shaviyani Feevaku School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1048, NULL, NULL, 'Shaviyani Feydhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1049, NULL, NULL, 'Shaviyani Feydhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1050, NULL, NULL, 'Shaviyani Foakaidhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1051, NULL, NULL, 'Shaviyani Fokaidhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1052, NULL, NULL, 'Shaviyani Funadhoo Atoll Hospital\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1053, NULL, NULL, 'Shaviyani Funadhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1054, NULL, NULL, 'Shaviyani Funadhoo Youth Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1055, NULL, NULL, 'Shaviyani Goidhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1056, NULL, NULL, 'Shaviyani Goidhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1057, NULL, NULL, 'Shaviyani Kanditheem Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1058, NULL, NULL, 'Shaviyani Komandoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1059, NULL, NULL, 'Shaviyani Lhaimagu Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1060, NULL, NULL, 'Shaviyani Lhaimagu School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1061, NULL, NULL, 'Shaviyani Maakan\'doodhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1062, NULL, NULL, 'Shaviyani Maaugoodhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1063, NULL, NULL, 'Shaviyani Maaun\'goodhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1064, NULL, NULL, 'Shaviyani Maroshi Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1065, NULL, NULL, 'Shaviyani Milandhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1066, NULL, NULL, 'Shaviyani Milandhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1067, NULL, NULL, 'Shaviyani Milandhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1068, NULL, NULL, 'Shaviyani Narudhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1069, NULL, NULL, 'Shaviyani Narudhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1070, NULL, NULL, 'Shaviyani Noomara Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1071, NULL, NULL, 'Shaviyani Noomaraa School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1072, NULL, NULL, 'Sheraton Madlives Full Moon Resort & Spa\r', NULL, 3, 1, '2015-08-19 06:59:01');
INSERT INTO `office` (`O_ID`, `O_AGA`, `O_BA`, `O_NAME`, `O_ADDRESS`, `O_DEFAULTPRIO`, `O_CREATEDBY`, `O_TIMESTAMP`) VALUES
(1073, NULL, NULL, 'Six Senses Laamu\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1074, NULL, NULL, 'Soneva Fushi by Six Senses\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1075, NULL, NULL, 'Soneva Gili by Six Senses\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1076, NULL, NULL, 'South Central Health Service Corporation\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1077, NULL, NULL, 'South Central Utilities Limited\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1078, NULL, NULL, 'Southern Health Service Corportation\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1079, NULL, NULL, 'Southern Utilities Limited\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1080, NULL, NULL, 'State Bank of India\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1081, NULL, NULL, 'State Electric Company\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1082, NULL, NULL, 'State Trading Organization Public Limited\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1083, NULL, NULL, 'Summer Island Village\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1084, NULL, NULL, 'Sun Island Resort & Spa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1085, NULL, NULL, 'Supreme Court of the Maldives\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1086, NULL, NULL, 'Swimming Association of Maldives\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1087, NULL, NULL, 'Taj Exotica Resort & Spa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1088, NULL, NULL, 'Tax Appeal Tribunal\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1089, NULL, NULL, 'Technical and Vocational Education Training Authority\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1090, NULL, NULL, 'Television Maldives\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1091, NULL, NULL, 'Thaa Atholhu Madharusaa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1092, NULL, NULL, 'Thaa Atholhu Tha\"Leemee Marukaz\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1093, NULL, NULL, 'Thaa Atoll Hospital\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1094, NULL, NULL, 'Thaa Burunee School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1095, NULL, NULL, 'Thaa Buruni Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1096, NULL, NULL, 'Thaa Dhiyamigili Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1097, NULL, NULL, 'Thaa Dhiyamigili School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1098, NULL, NULL, 'Thaa Family and children service centre \r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1099, NULL, NULL, 'Thaa Gaadhiffushee School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1100, NULL, NULL, 'Thaa Gaadhiffushi Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1101, NULL, NULL, 'Thaa Guraidhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1102, NULL, NULL, 'Thaa Hirilandhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1103, NULL, NULL, 'Thaa Hirilandhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1104, NULL, NULL, 'Thaa Kan\'doodhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1105, NULL, NULL, 'Thaa Kandoodhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1106, NULL, NULL, 'Thaa Kin\'bidhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1107, NULL, NULL, 'Thaa Kinbidhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1108, NULL, NULL, 'Thaa Madifushee School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1109, NULL, NULL, 'Thaa Madifushi Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1110, NULL, NULL, 'Thaa Omadhoo Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1111, NULL, NULL, 'Thaa Omadhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1112, NULL, NULL, 'Thaa Thimarafushi Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1113, NULL, NULL, 'Thaa Vandhoo Health Post\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1114, NULL, NULL, 'Thaa Vandhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1115, NULL, NULL, 'Thaa Vilifushi Health Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1116, NULL, NULL, 'Thaa Villifushi School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1117, NULL, NULL, 'Thaajuddeen School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1118, NULL, NULL, 'Thauleemaai Masakkaiy Ungannaidhey Kuda Kudhinge Marukaz\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1119, NULL, NULL, 'Thiladhunmathi Dhekunuburi  Hanimaadhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1120, NULL, NULL, 'Thiladhunmathi Dhekunuburi Atholhu Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1121, NULL, NULL, 'Thiladhunmathi Dhekunuburi Finey Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1122, NULL, NULL, 'Thiladhunmathi Dhekunuburi Finey Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1123, NULL, NULL, 'Thiladhunmathi Dhekunuburi Hanimaadhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1124, NULL, NULL, 'Thiladhunmathi Dhekunuburi Hirimaradhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1125, NULL, NULL, 'Thiladhunmathi Dhekunuburi Hirimaradhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1126, NULL, NULL, 'Thiladhunmathi Dhekunuburi Kulhudhuffushi Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1127, NULL, NULL, 'Thiladhunmathi Dhekunuburi Kulhudhuffushi Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1128, NULL, NULL, 'Thiladhunmathi Dhekunuburi Kumundhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1129, NULL, NULL, 'Thiladhunmathi Dhekunuburi Kumundhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1130, NULL, NULL, 'Thiladhunmathi Dhekunuburi Kurin\'bi Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1131, NULL, NULL, 'Thiladhunmathi Dhekunuburi Kurinbi Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1132, NULL, NULL, 'Thiladhunmathi Dhekunuburi Makunudhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1133, NULL, NULL, 'Thiladhunmathi Dhekunuburi Makunudhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1134, NULL, NULL, 'Thiladhunmathi Dhekunuburi Naivaadhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1135, NULL, NULL, 'Thiladhunmathi Dhekunuburi Naivaadhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1136, NULL, NULL, 'Thiladhunmathi Dhekunuburi Nehlaidhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1137, NULL, NULL, 'Thiladhunmathi Dhekunuburi Nellaidhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1138, NULL, NULL, 'Thiladhunmathi Dhekunuburi Neykurendhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1139, NULL, NULL, 'Thiladhunmathi Dhekunuburi Neykurendhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1140, NULL, NULL, 'Thiladhunmathi Dhekunuburi Nolhivaramfaru Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1141, NULL, NULL, 'Thiladhunmathi Dhekunuburi Nolhivaramu Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1142, NULL, NULL, 'Thiladhunmathi Dhekunuburi Nolhivaran Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1143, NULL, NULL, 'Thiladhunmathi Dhekunuburi Nolhivaranfaru Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1144, NULL, NULL, 'Thiladhunmathi Dhekunuburi Vaikaradhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1145, NULL, NULL, 'Thiladhunmathi Dhekunuburi Vaikarudhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1146, NULL, NULL, 'Thiladhunmathi Uthuruburi Atholhu Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1147, NULL, NULL, 'Thiladhunmathi Uthuruburi Baarah Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1148, NULL, NULL, 'Thiladhunmathi Uthuruburi Baarashu Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1149, NULL, NULL, 'Thiladhunmathi Uthuruburi Dhidhdhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1150, NULL, NULL, 'Thiladhunmathi Uthuruburi Dhidhdhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1151, NULL, NULL, 'Thiladhunmathi Uthuruburi Filadhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1152, NULL, NULL, 'Thiladhunmathi Uthuruburi Filladhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1153, NULL, NULL, 'Thiladhunmathi Uthuruburi Hoarafushi Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1154, NULL, NULL, 'Thiladhunmathi Uthuruburi Hoarafushi Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1155, NULL, NULL, 'Thiladhunmathi Uthuruburi Ihavandhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1156, NULL, NULL, 'Thiladhunmathi Uthuruburi Ihavandhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1157, NULL, NULL, 'Thiladhunmathi Uthuruburi Kela Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1158, NULL, NULL, 'Thiladhunmathi Uthuruburi Kelaa Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1159, NULL, NULL, 'Thiladhunmathi Uthuruburi Maarandhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1160, NULL, NULL, 'Thiladhunmathi Uthuruburi Maarandhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1161, NULL, NULL, 'Thiladhunmathi Uthuruburi Molhadhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1162, NULL, NULL, 'Thiladhunmathi Uthuruburi Molhadhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1163, NULL, NULL, 'Thiladhunmathi Uthuruburi Muraidhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1164, NULL, NULL, 'Thiladhunmathi Uthuruburi Muraidhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1165, NULL, NULL, 'Thiladhunmathi Uthuruburi Thakandhoo Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1166, NULL, NULL, 'Thiladhunmathi Uthuruburi Thakandhoo Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1167, NULL, NULL, 'Thiladhunmathi Uthuruburi Thuraakunu Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1168, NULL, NULL, 'Thiladhunmathi Uthuruburi Thuraakunu Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1169, NULL, NULL, 'Thiladhunmathi Uthuruburi Uligamu Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1170, NULL, NULL, 'Thiladhunmathi Uthuruburi Uligan Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1171, NULL, NULL, 'Thiladhunmathi Uthuruburi Utheemu Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1172, NULL, NULL, 'Thiladhunmathi Uthuruburi Utheemu Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1173, NULL, NULL, 'Thiladhunmathi Uthuruburi Vashafaru Councilge Idhaaraa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1174, NULL, NULL, 'Thiladhunmathi Uthuruburi Vashafaru Magistrate Court\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1175, NULL, NULL, 'Thilafushi Corporation Limited\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1176, NULL, NULL, 'Thulhaagiri Island Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1177, NULL, NULL, 'Maldives Transport Authority', NULL, 3, 1, '2015-08-19 06:59:01'),
(1178, NULL, NULL, 'Twin Island Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1179, NULL, NULL, 'Upper North Health Service Corporation \r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1180, NULL, NULL, 'Upper North Utilities Limited\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1181, NULL, NULL, 'Upper South Health Service Corporation \r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1182, NULL, NULL, 'Upper South Utilities Limited\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1183, NULL, NULL, 'Vaavu Atholhu Madharusaa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1184, NULL, NULL, 'Vaavu Atoll Education Centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1185, NULL, NULL, 'Vaavu Atoll Hospital\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1186, NULL, NULL, 'Vaavu Family and children service centre\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1187, NULL, NULL, 'Vaavu Fulidhoo Health Post\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1188, NULL, NULL, 'Vaavu Keyodhoo Health Center\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1189, NULL, NULL, 'Vaavu Keyodhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1190, NULL, NULL, 'Vaavu Rakeedhoo Health Post\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1191, NULL, NULL, 'Vaavu Rakeedhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1192, NULL, NULL, 'Vaavu Thinadhoo Health Post\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1193, NULL, NULL, 'Vaavu Thinadhoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1194, NULL, NULL, 'Vakarufalhi Island Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1195, NULL, NULL, 'Velassaru Maldives\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1196, NULL, NULL, 'Velidhoo Island Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1197, NULL, NULL, 'Veligandu Island Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1198, NULL, NULL, 'Veymandoo School\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1199, NULL, NULL, 'Vilamendhoo Island Resort\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1200, NULL, NULL, 'Villingili Avashu Office\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1201, NULL, NULL, 'Villingili Investments Pvt. Ltd.\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1202, NULL, NULL, 'Vilu Reef Beach Resort & Spa\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1203, NULL, NULL, 'Vivanta by Taj - Coral Reef Maldives\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1204, NULL, NULL, 'Volleyball Association of Maldives\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1205, NULL, NULL, 'W Retreat and Spa Maldives\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1206, NULL, NULL, 'Waste Management Corporation Limited\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1207, NULL, NULL, 'World Bank\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1208, NULL, NULL, 'Youth Health Cafe\r', NULL, 3, 1, '2015-08-19 06:59:01'),
(1209, NULL, NULL, 'Zitahli Resorts & Spa Kudafunafaru', NULL, 3, 1, '2015-08-19 06:59:01'),
(1210, NULL, 1515, 'National Integrity Commssion', 'National Integrity Commission, Haveeree Hin\'gun, Tel:3302582, Fax:3302580', 3, 1, '2015-10-20 19:14:12'),
(1211, NULL, NULL, 'MRH Investment ', 'Male\', Maldives ', 2, 1, '2015-11-25 05:05:15'),
(1212, NULL, NULL, 'Islamic University of Maldives', 'Male\', Maldives', 3, 1, '2016-05-22 05:26:58'),
(1213, NULL, NULL, 'Maldives Center for Islamic Finance Ltd', 'Mookai Suits 3rd Floor M.Sakai Moo Haveeree Hingun', 3, 1, '2016-10-26 05:46:49'),
(1214, NULL, NULL, 'Maldives Sports Coporation Limited', NULL, 2, 1, '2016-11-28 06:06:45'),
(1215, NULL, NULL, 'Fuvammulaku City Councilge Idaaraa', 'Fuvamulaku', 2, 1, '2017-07-16 05:57:35'),
(1216, NULL, NULL, 'Fiyavathi', NULL, 2, 1, '2017-10-05 04:50:46'),
(1217, NULL, NULL, 'Hazana Maldives', 'Hazana Maldives', 2, 1, '2017-10-17 07:57:05'),
(1218, NULL, NULL, 'Maldives Integrated Tourism Development Corporation (MITDC)', '5th Floor Velanage, Ameer Ahmed Magu', 2, 1, '2018-10-02 05:40:55'),
(1219, NULL, NULL, 'Islaamee Fathuvaa dhey Enme Mathee Majlis', NULL, 2, 1, '2018-10-02 05:44:00'),
(1220, NULL, NULL, 'President Elect Office', NULL, 2, 1, '2018-10-18 05:31:26'),
(1221, NULL, NULL, 'Ministry of Higher Education', NULL, 2, 1, '2018-11-19 08:01:37'),
(1222, NULL, NULL, 'Ministry of National Planning and Infrastructure', NULL, 2, 1, '2018-11-19 08:02:27'),
(1223, NULL, NULL, 'Ministry of Communication science and Technology', NULL, 2, 1, '2018-11-19 08:03:05'),
(1224, NULL, NULL, 'Ministry of Arts, Culture and Heritage', NULL, 2, 1, '2018-11-22 03:12:01'),
(1225, NULL, NULL, 'official Residence of the President', NULL, 2, 1, '2018-11-22 09:11:54'),
(1226, NULL, NULL, 'Official Residence of The Vice President', 'Official Residence of The VIce President, Hilaaleege, Orchid Magu, 20223', 2, 1, '2019-01-20 05:59:34'),
(1227, NULL, NULL, 'Maldives Judicial Academy', 'Supreme Court of the Maldives, Malé, Republic of Maldives Address: Askaree hingun, 21022, Vilimale, Republic of Maldives', 2, 1, '2019-02-05 03:43:52');

-- --------------------------------------------------------

--
-- Table structure for table `priority`
--

CREATE TABLE `priority` (
  `P_ID` int(11) NOT NULL,
  `P_NAME` varchar(250) NOT NULL,
  `P_COLORCODE` varchar(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `priority`
--

INSERT INTO `priority` (`P_ID`, `P_NAME`, `P_COLORCODE`) VALUES
(1, 'DEFAULT', '4cb43e'),
(2, 'LOW', 'd09d2e'),
(3, 'MEDIUM', 'd02e2e'),
(4, 'HIGH', '333333');

-- --------------------------------------------------------

--
-- Table structure for table `prtg_notification_group`
--

CREATE TABLE `prtg_notification_group` (
  `PRTG_N_G_ID` bigint(20) NOT NULL,
  `PRTG_N_G_NAME` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `psender`
--

CREATE TABLE `psender` (
  `PSENDER_ID` int(11) NOT NULL,
  `PSENDER_NAME` varchar(250) NOT NULL,
  `PSENDER_NIC` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sess`
--

CREATE TABLE `sess` (
  `sess_id` int(11) NOT NULL,
  `sess_key` varchar(100) NOT NULL,
  `sess_user` varchar(250) NOT NULL,
  `sess_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sms_received`
--

CREATE TABLE `sms_received` (
  `SMS_R_ID` bigint(11) NOT NULL,
  `SMS_R_SENDER_NUMBER` varchar(15) NOT NULL,
  `SMS_R_SENDER_NAME` varchar(150) NOT NULL,
  `SMS_R_MESSAGE` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `SMS_R_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `SMS_DELETED` int(11) NOT NULL DEFAULT '0',
  `SMS_R_D_BY` int(11) DEFAULT NULL,
  `SMS_R_M_BY` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sms_standard_response`
--

CREATE TABLE `sms_standard_response` (
  `SMS_S_R_ID` int(11) NOT NULL,
  `SMS_S_R_DESC` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sms_standard_response`
--

INSERT INTO `sms_standard_response` (`SMS_S_R_ID`, `SMS_S_R_DESC`) VALUES
(1, 'Your message has been passed to the relevant Division/Section for processing. '),
(2, 'Your payment has been sent to MMA.'),
(3, 'Please send a request to helpdesk@finance.gov.mv'),
(4, 'Your payment has been processed.'),
(5, 'Request has been sent to process the payment.'),
(6, 'Request has been sent to send the payment.'),
(7, 'We are sorry to inform you that we have not received the requested Payment voucher.'),
(8, 'Please provide us the voucher/document number for the requested payment.'),
(9, 'Call center staff will contact you shortly'),
(10, 'Mail received. Will reply to the mail shortly.'),
(11, 'For further information regarding the issue, please contact 1617'),
(12, 'Your payment(s) has been received. The payment(s) will be processed after 7 working days'),
(13, 'Your Payment(s) has been parked'),
(14, 'Your payment(s) has been blocked');

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

CREATE TABLE `unit` (
  `UNIT_ID` int(11) NOT NULL,
  `UNIT_NAME` varchar(150) NOT NULL,
  `UNIT_CREATEDBY` int(11) NOT NULL,
  `UNIT_CREATEDON` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `unit`
--

INSERT INTO `unit` (`UNIT_ID`, `UNIT_NAME`, `UNIT_CREATEDBY`, `UNIT_CREATEDON`) VALUES
(1, 'Counter', 1, '2019-11-24 08:55:09'),
(2, 'Call Center', 1, '2019-11-24 08:55:18'),
(3, 'Minister\'s Directives', 1, '2019-11-24 08:55:30'),
(4, 'Admin', 1, '2019-11-24 09:33:33');

-- --------------------------------------------------------

--
-- Table structure for table `unit_member`
--

CREATE TABLE `unit_member` (
  `um_id` int(11) NOT NULL,
  `um_unit_id` int(11) NOT NULL,
  `um_u_id` int(11) NOT NULL,
  `um_handler` varchar(1) NOT NULL DEFAULT 'N',
  `um_added_by` int(11) NOT NULL,
  `um_added_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `U_ID` int(11) NOT NULL,
  `U_NAME` varchar(250) NOT NULL,
  `U_PASSWORD` varchar(100) DEFAULT NULL,
  `U_PHONE` bigint(20) DEFAULT NULL,
  `U_TYPE` varchar(6) NOT NULL DEFAULT 'AD',
  `U_ACTIVE` int(11) NOT NULL DEFAULT '1',
  `U_COUNTER` varchar(1) NOT NULL DEFAULT 'N',
  `U_ADMIN` varchar(1) NOT NULL DEFAULT 'N',
  `U_REPORTS` varchar(1) NOT NULL DEFAULT 'N',
  `U_SENDALL` varchar(1) NOT NULL DEFAULT 'N',
  `U_PREFERENCES` text,
  `U_CREATEDBY` int(11) NOT NULL,
  `U_TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`U_ID`, `U_NAME`, `U_PASSWORD`, `U_PHONE`, `U_TYPE`, `U_ACTIVE`, `U_COUNTER`, `U_ADMIN`, `U_REPORTS`, `U_SENDALL`, `U_PREFERENCES`, `U_CREATEDBY`, `U_TIMESTAMP`) VALUES
(1, 'ADMIN', '94dfa641bc0aa3d21c62e9741006c6df', NULL, 'local', 1, 'N', 'Y', 'N', 'N', '{\"caseRouted\":1,\"caseAssigned\":1,\"caseSharedWithMe\":1,\"caseSharedSection\":1,\"caseSharedAssigned\":1,\"caseSharedShared\":1,\"caseCommentSection\":1,\"caseCommentAssigned\":1,\"caseCommentShared\":1,\"caseUpdateSection\":1,\"caseUpdateAssigned\":1,\"caseUpdateShared\":1,\"caseNewFileSection\":1,\"caseNewFileAssigned\":1,\"caseNewFileShared\":1,\"caseFileUpdatedSection\":1,\"caseFileUpdatedAssigned\":1,\"caseFileUpdatedShared\":1,\"caseEditSection\":1,\"caseEditAssigned\":1,\"caseEditShared\":1,\"caseClosedSection\":1,\"caseClosedShared\":1,\"caseReopenedSection\":1,\"caseReopenedShared\":1,\"caseRejected\":1}', 1, '2019-11-25 03:24:26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aga`
--
ALTER TABLE `aga`
  ADD PRIMARY KEY (`AGA_ID`);

--
-- Indexes for table `alerts`
--
ALTER TABLE `alerts`
  ADD PRIMARY KEY (`A_ID`),
  ADD KEY `A_U_ID` (`A_U_ID`),
  ADD KEY `A_DOC_ID` (`A_DOC_ID`),
  ADD KEY `A_TYPE_DESC` (`A_TYPE_DESC`),
  ADD KEY `A_CREATEDBY` (`A_CREATEDBY`);

--
-- Indexes for table `alert_description`
--
ALTER TABLE `alert_description`
  ADD PRIMARY KEY (`AD_ID`);
ALTER TABLE `alert_description` ADD FULLTEXT KEY `AD_DESC` (`AD_DESC`);

--
-- Indexes for table `call_records`
--
ALTER TABLE `call_records`
  ADD PRIMARY KEY (`CR_ID`),
  ADD UNIQUE KEY `CR_LOGID` (`CR_LOGID`),
  ADD KEY `CR_REF` (`CR_REF`),
  ADD KEY `CR_TEL` (`CR_TEL`),
  ADD KEY `CR_EMAIL` (`CR_EMAIL`),
  ADD KEY `CR_O_ID` (`CR_O_ID`),
  ADD KEY `CR_DC_ID` (`CR_DC_ID`),
  ADD KEY `CR_P_ID` (`CR_P_ID`),
  ADD KEY `CR_BY` (`CR_BY`),
  ADD KEY `CR_LASTUPDATEBY` (`CR_LASTUPDATEBY`),
  ADD KEY `CR_DOC_ID` (`CR_DOC_ID`),
  ADD KEY `CR_UNIT_ID` (`CR_UNIT_ID`),
  ADD KEY `CR_CRCAT_ID` (`CR_CRCAT_ID`),
  ADD KEY `CR_SMS_R_ID` (`CR_SMS_R_ID`);

--
-- Indexes for table `call_records_cats`
--
ALTER TABLE `call_records_cats`
  ADD PRIMARY KEY (`CRCAT_ID`);

--
-- Indexes for table `case_comments`
--
ALTER TABLE `case_comments`
  ADD PRIMARY KEY (`CC_ID`),
  ADD KEY `CC_BY` (`CC_BY`),
  ADD KEY `CC_DOC_ID` (`CC_DOC_ID`);

--
-- Indexes for table `case_contact`
--
ALTER TABLE `case_contact`
  ADD PRIMARY KEY (`CC_ID`),
  ADD KEY `CC_DOC_ID` (`CC_DOC_ID`),
  ADD KEY `CC_BY` (`CC_BY`);

--
-- Indexes for table `case_ref_ctrl`
--
ALTER TABLE `case_ref_ctrl`
  ADD PRIMARY KEY (`CRC_ID`),
  ADD KEY `CRC_DSrc_ID` (`CRC_DSrc_ID`);

--
-- Indexes for table `case_special_amount`
--
ALTER TABLE `case_special_amount`
  ADD PRIMARY KEY (`CSA_ID`),
  ADD KEY `CSA_DOC_ID` (`CSA_DOC_ID`),
  ADD KEY `CSA_BY` (`CSA_BY`);

--
-- Indexes for table `chan_num_ctrl`
--
ALTER TABLE `chan_num_ctrl`
  ADD PRIMARY KEY (`CNC_ID`);

--
-- Indexes for table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`DOC_ID`),
  ADD UNIQUE KEY `DOC_OFFICE_2` (`DOC_OFFICE`,`DOC_REFNUMBER`),
  ADD KEY `DOC_OFFICE` (`DOC_OFFICE`),
  ADD KEY `DOC_CATEGORY` (`DOC_CATEGORY`),
  ADD KEY `DOC_UNIT` (`DOC_UNIT`),
  ADD KEY `DOC_ASSIGNEDTO` (`DOC_ASSIGNEDTO`),
  ADD KEY `DOC_STATUS` (`DOC_STATUS`),
  ADD KEY `DOC_CREATEDBY` (`DOC_CREATEDBY`),
  ADD KEY `DOC_PRIORITY` (`DOC_PRIORITY`),
  ADD KEY `DOC_SRC` (`DOC_SRC`);

--
-- Indexes for table `documentcat`
--
ALTER TABLE `documentcat`
  ADD PRIMARY KEY (`DC_ID`),
  ADD UNIQUE KEY `DC_NAME` (`DC_NAME`),
  ADD KEY `DC_CREATEDBY` (`DC_CREATEDBY`);

--
-- Indexes for table `doc_character`
--
ALTER TABLE `doc_character`
  ADD PRIMARY KEY (`DCH_ID`);

--
-- Indexes for table `doc_classification`
--
ALTER TABLE `doc_classification`
  ADD PRIMARY KEY (`DCL_ID`);

--
-- Indexes for table `doc_follow`
--
ALTER TABLE `doc_follow`
  ADD PRIMARY KEY (`DF_ID`),
  ADD KEY `DF_DOC_ID` (`DF_DOC_ID`,`DF_U_ID`),
  ADD KEY `DF_U_ID` (`DF_U_ID`);

--
-- Indexes for table `doc_history`
--
ALTER TABLE `doc_history`
  ADD PRIMARY KEY (`DH_ID`),
  ADD KEY `DH_NT_ID` (`DH_NT_ID`),
  ADD KEY `DH_SETBY` (`DH_SETBY`),
  ADD KEY `DH_DOC_ID` (`DH_DOC_ID`),
  ADD KEY `DH_DOC_ID_2` (`DH_DOC_ID`);

--
-- Indexes for table `doc_related_cases`
--
ALTER TABLE `doc_related_cases`
  ADD PRIMARY KEY (`DRC_ID`),
  ADD KEY `DRC_DOC_ID` (`DRC_DOC_ID`,`DRC_RELATED_DOC_ID`,`DRC_ATTACHEDBY`),
  ADD KEY `DRC_RELATED_DOC_ID` (`DRC_RELATED_DOC_ID`),
  ADD KEY `DRC_ATTACHEDBY` (`DRC_ATTACHEDBY`);

--
-- Indexes for table `doc_remarks`
--
ALTER TABLE `doc_remarks`
  ADD PRIMARY KEY (`DR_ID`),
  ADD KEY `DR_BY` (`DR_BY`),
  ADD KEY `DR_DOC_ID` (`DR_DOC_ID`),
  ADD KEY `DR_FILE_UNIT` (`DR_FILE_UNIT`),
  ADD KEY `DR_IOR_ID` (`DR_IOR_ID`),
  ADD KEY `DR_DCH_ID` (`DR_DCH_ID`);

--
-- Indexes for table `doc_sender_type`
--
ALTER TABLE `doc_sender_type`
  ADD PRIMARY KEY (`DSENT_ID`);

--
-- Indexes for table `doc_share`
--
ALTER TABLE `doc_share`
  ADD PRIMARY KEY (`DS_ID`),
  ADD UNIQUE KEY `DS_U_ID_2` (`DS_U_ID`,`DS_DOC_ID`),
  ADD UNIQUE KEY `DS_U_ID_3` (`DS_U_ID`,`DS_DOC_ID`),
  ADD KEY `DS_U_ID` (`DS_U_ID`),
  ADD KEY `DS_DOC_ID` (`DS_DOC_ID`),
  ADD KEY `DS_BY` (`DS_BY`);

--
-- Indexes for table `doc_source`
--
ALTER TABLE `doc_source`
  ADD PRIMARY KEY (`DSrc_ID`);

--
-- Indexes for table `doc_status`
--
ALTER TABLE `doc_status`
  ADD PRIMARY KEY (`DST_ID`);

--
-- Indexes for table `fileaccess_log`
--
ALTER TABLE `fileaccess_log`
  ADD PRIMARY KEY (`fl_id`),
  ADD KEY `fl_user_id` (`fl_user_id`);

--
-- Indexes for table `in_out_registery`
--
ALTER TABLE `in_out_registery`
  ADD PRIMARY KEY (`IOR_ID`),
  ADD KEY `IOR_DOC_ID` (`IOR_DOC_ID`);

--
-- Indexes for table `in_out_remarks`
--
ALTER TABLE `in_out_remarks`
  ADD PRIMARY KEY (`IORR_ID`),
  ADD KEY `IORR_IOR_ID` (`IORR_IOR_ID`);

--
-- Indexes for table `notification_type`
--
ALTER TABLE `notification_type`
  ADD PRIMARY KEY (`NT_ID`),
  ADD UNIQUE KEY `NT_NAME` (`NT_NAME`);

--
-- Indexes for table `office`
--
ALTER TABLE `office`
  ADD PRIMARY KEY (`O_ID`),
  ADD UNIQUE KEY `O_NAME` (`O_NAME`),
  ADD UNIQUE KEY `O_BA` (`O_BA`),
  ADD KEY `O_CREATEDBY` (`O_CREATEDBY`),
  ADD KEY `O_DEFAULTPRIO` (`O_DEFAULTPRIO`),
  ADD KEY `O_AGA` (`O_AGA`);

--
-- Indexes for table `priority`
--
ALTER TABLE `priority`
  ADD PRIMARY KEY (`P_ID`);

--
-- Indexes for table `prtg_notification_group`
--
ALTER TABLE `prtg_notification_group`
  ADD PRIMARY KEY (`PRTG_N_G_ID`);

--
-- Indexes for table `psender`
--
ALTER TABLE `psender`
  ADD PRIMARY KEY (`PSENDER_ID`);

--
-- Indexes for table `sess`
--
ALTER TABLE `sess`
  ADD PRIMARY KEY (`sess_id`);

--
-- Indexes for table `sms_received`
--
ALTER TABLE `sms_received`
  ADD PRIMARY KEY (`SMS_R_ID`),
  ADD KEY `SMS_R_D_BY` (`SMS_R_D_BY`),
  ADD KEY `SMS_R_M_BY` (`SMS_R_M_BY`);

--
-- Indexes for table `sms_standard_response`
--
ALTER TABLE `sms_standard_response`
  ADD PRIMARY KEY (`SMS_S_R_ID`);

--
-- Indexes for table `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`UNIT_ID`),
  ADD UNIQUE KEY `UNIT_NAME` (`UNIT_NAME`),
  ADD KEY `UNIT_CREATEDBY` (`UNIT_CREATEDBY`);

--
-- Indexes for table `unit_member`
--
ALTER TABLE `unit_member`
  ADD PRIMARY KEY (`um_id`),
  ADD UNIQUE KEY `um_unit_id_2` (`um_unit_id`,`um_u_id`),
  ADD KEY `um_unit_id` (`um_unit_id`),
  ADD KEY `um_u_id` (`um_u_id`),
  ADD KEY `um_added_by` (`um_added_by`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`U_ID`),
  ADD UNIQUE KEY `U_NAME` (`U_NAME`),
  ADD KEY `U_CREATEDBY` (`U_CREATEDBY`);
ALTER TABLE `user` ADD FULLTEXT KEY `U_PREFERENCES` (`U_PREFERENCES`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aga`
--
ALTER TABLE `aga`
  MODIFY `AGA_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `alerts`
--
ALTER TABLE `alerts`
  MODIFY `A_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `call_records`
--
ALTER TABLE `call_records`
  MODIFY `CR_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=223;
--
-- AUTO_INCREMENT for table `call_records_cats`
--
ALTER TABLE `call_records_cats`
  MODIFY `CRCAT_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `case_comments`
--
ALTER TABLE `case_comments`
  MODIFY `CC_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `case_contact`
--
ALTER TABLE `case_contact`
  MODIFY `CC_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `case_ref_ctrl`
--
ALTER TABLE `case_ref_ctrl`
  MODIFY `CRC_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `case_special_amount`
--
ALTER TABLE `case_special_amount`
  MODIFY `CSA_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `chan_num_ctrl`
--
ALTER TABLE `chan_num_ctrl`
  MODIFY `CNC_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `document`
--
ALTER TABLE `document`
  MODIFY `DOC_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `documentcat`
--
ALTER TABLE `documentcat`
  MODIFY `DC_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `doc_character`
--
ALTER TABLE `doc_character`
  MODIFY `DCH_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `doc_classification`
--
ALTER TABLE `doc_classification`
  MODIFY `DCL_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `doc_follow`
--
ALTER TABLE `doc_follow`
  MODIFY `DF_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doc_history`
--
ALTER TABLE `doc_history`
  MODIFY `DH_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `doc_related_cases`
--
ALTER TABLE `doc_related_cases`
  MODIFY `DRC_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doc_remarks`
--
ALTER TABLE `doc_remarks`
  MODIFY `DR_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `doc_share`
--
ALTER TABLE `doc_share`
  MODIFY `DS_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `doc_source`
--
ALTER TABLE `doc_source`
  MODIFY `DSrc_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `doc_status`
--
ALTER TABLE `doc_status`
  MODIFY `DST_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `fileaccess_log`
--
ALTER TABLE `fileaccess_log`
  MODIFY `fl_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `in_out_registery`
--
ALTER TABLE `in_out_registery`
  MODIFY `IOR_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `in_out_remarks`
--
ALTER TABLE `in_out_remarks`
  MODIFY `IORR_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `notification_type`
--
ALTER TABLE `notification_type`
  MODIFY `NT_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `office`
--
ALTER TABLE `office`
  MODIFY `O_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1228;
--
-- AUTO_INCREMENT for table `priority`
--
ALTER TABLE `priority`
  MODIFY `P_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `psender`
--
ALTER TABLE `psender`
  MODIFY `PSENDER_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sess`
--
ALTER TABLE `sess`
  MODIFY `sess_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `sms_received`
--
ALTER TABLE `sms_received`
  MODIFY `SMS_R_ID` bigint(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sms_standard_response`
--
ALTER TABLE `sms_standard_response`
  MODIFY `SMS_S_R_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `unit`
--
ALTER TABLE `unit`
  MODIFY `UNIT_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `unit_member`
--
ALTER TABLE `unit_member`
  MODIFY `um_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `U_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `alerts`
--
ALTER TABLE `alerts`
  ADD CONSTRAINT `alerts_ibfk_1` FOREIGN KEY (`A_U_ID`) REFERENCES `user` (`U_ID`),
  ADD CONSTRAINT `alerts_ibfk_2` FOREIGN KEY (`A_DOC_ID`) REFERENCES `document` (`DOC_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `alerts_ibfk_3` FOREIGN KEY (`A_CREATEDBY`) REFERENCES `user` (`U_ID`),
  ADD CONSTRAINT `alerts_ibfk_4` FOREIGN KEY (`A_TYPE_DESC`) REFERENCES `alert_description` (`AD_ID`);

--
-- Constraints for table `call_records`
--
ALTER TABLE `call_records`
  ADD CONSTRAINT `call_records_ibfk_1` FOREIGN KEY (`CR_O_ID`) REFERENCES `office` (`O_ID`),
  ADD CONSTRAINT `call_records_ibfk_10` FOREIGN KEY (`CR_SMS_R_ID`) REFERENCES `sms_received` (`SMS_R_ID`),
  ADD CONSTRAINT `call_records_ibfk_2` FOREIGN KEY (`CR_DC_ID`) REFERENCES `documentcat` (`DC_ID`),
  ADD CONSTRAINT `call_records_ibfk_3` FOREIGN KEY (`CR_P_ID`) REFERENCES `priority` (`P_ID`),
  ADD CONSTRAINT `call_records_ibfk_4` FOREIGN KEY (`CR_BY`) REFERENCES `user` (`U_ID`),
  ADD CONSTRAINT `call_records_ibfk_5` FOREIGN KEY (`CR_LASTUPDATEBY`) REFERENCES `user` (`U_ID`),
  ADD CONSTRAINT `call_records_ibfk_6` FOREIGN KEY (`CR_REF`) REFERENCES `call_records` (`CR_ID`),
  ADD CONSTRAINT `call_records_ibfk_7` FOREIGN KEY (`CR_DOC_ID`) REFERENCES `document` (`DOC_ID`),
  ADD CONSTRAINT `call_records_ibfk_8` FOREIGN KEY (`CR_UNIT_ID`) REFERENCES `unit` (`UNIT_ID`),
  ADD CONSTRAINT `call_records_ibfk_9` FOREIGN KEY (`CR_CRCAT_ID`) REFERENCES `call_records_cats` (`CRCAT_ID`);

--
-- Constraints for table `case_comments`
--
ALTER TABLE `case_comments`
  ADD CONSTRAINT `case_comments_ibfk_1` FOREIGN KEY (`CC_DOC_ID`) REFERENCES `document` (`DOC_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `case_comments_ibfk_2` FOREIGN KEY (`CC_BY`) REFERENCES `user` (`U_ID`);

--
-- Constraints for table `case_contact`
--
ALTER TABLE `case_contact`
  ADD CONSTRAINT `case_contact_ibfk_1` FOREIGN KEY (`CC_DOC_ID`) REFERENCES `document` (`DOC_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `case_contact_ibfk_2` FOREIGN KEY (`CC_BY`) REFERENCES `user` (`U_ID`);

--
-- Constraints for table `case_ref_ctrl`
--
ALTER TABLE `case_ref_ctrl`
  ADD CONSTRAINT `case_ref_ctrl_ibfk_1` FOREIGN KEY (`CRC_DSrc_ID`) REFERENCES `doc_source` (`DSrc_ID`);

--
-- Constraints for table `case_special_amount`
--
ALTER TABLE `case_special_amount`
  ADD CONSTRAINT `case_special_amount_ibfk_1` FOREIGN KEY (`CSA_DOC_ID`) REFERENCES `document` (`DOC_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `case_special_amount_ibfk_2` FOREIGN KEY (`CSA_BY`) REFERENCES `user` (`U_ID`);

--
-- Constraints for table `document`
--
ALTER TABLE `document`
  ADD CONSTRAINT `document_ibfk_1` FOREIGN KEY (`DOC_OFFICE`) REFERENCES `office` (`O_ID`),
  ADD CONSTRAINT `document_ibfk_2` FOREIGN KEY (`DOC_CATEGORY`) REFERENCES `documentcat` (`DC_ID`),
  ADD CONSTRAINT `document_ibfk_3` FOREIGN KEY (`DOC_UNIT`) REFERENCES `unit` (`UNIT_ID`),
  ADD CONSTRAINT `document_ibfk_4` FOREIGN KEY (`DOC_ASSIGNEDTO`) REFERENCES `user` (`U_ID`),
  ADD CONSTRAINT `document_ibfk_5` FOREIGN KEY (`DOC_STATUS`) REFERENCES `doc_status` (`DST_ID`),
  ADD CONSTRAINT `document_ibfk_6` FOREIGN KEY (`DOC_CREATEDBY`) REFERENCES `user` (`U_ID`),
  ADD CONSTRAINT `document_ibfk_7` FOREIGN KEY (`DOC_PRIORITY`) REFERENCES `priority` (`P_ID`),
  ADD CONSTRAINT `document_ibfk_8` FOREIGN KEY (`DOC_SRC`) REFERENCES `doc_source` (`DSrc_ID`);

--
-- Constraints for table `documentcat`
--
ALTER TABLE `documentcat`
  ADD CONSTRAINT `documentcat_ibfk_1` FOREIGN KEY (`DC_CREATEDBY`) REFERENCES `user` (`U_ID`);

--
-- Constraints for table `doc_follow`
--
ALTER TABLE `doc_follow`
  ADD CONSTRAINT `doc_follow_ibfk_1` FOREIGN KEY (`DF_DOC_ID`) REFERENCES `document` (`DOC_ID`),
  ADD CONSTRAINT `doc_follow_ibfk_2` FOREIGN KEY (`DF_U_ID`) REFERENCES `user` (`U_ID`);

--
-- Constraints for table `doc_history`
--
ALTER TABLE `doc_history`
  ADD CONSTRAINT `doc_history_ibfk_1` FOREIGN KEY (`DH_NT_ID`) REFERENCES `notification_type` (`NT_ID`),
  ADD CONSTRAINT `doc_history_ibfk_2` FOREIGN KEY (`DH_SETBY`) REFERENCES `user` (`U_ID`),
  ADD CONSTRAINT `doc_history_ibfk_3` FOREIGN KEY (`DH_DOC_ID`) REFERENCES `document` (`DOC_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `doc_related_cases`
--
ALTER TABLE `doc_related_cases`
  ADD CONSTRAINT `doc_related_cases_ibfk_1` FOREIGN KEY (`DRC_DOC_ID`) REFERENCES `document` (`DOC_ID`),
  ADD CONSTRAINT `doc_related_cases_ibfk_2` FOREIGN KEY (`DRC_RELATED_DOC_ID`) REFERENCES `document` (`DOC_ID`),
  ADD CONSTRAINT `doc_related_cases_ibfk_3` FOREIGN KEY (`DRC_ATTACHEDBY`) REFERENCES `user` (`U_ID`);

--
-- Constraints for table `doc_remarks`
--
ALTER TABLE `doc_remarks`
  ADD CONSTRAINT `doc_remarks_ibfk_1` FOREIGN KEY (`DR_DOC_ID`) REFERENCES `document` (`DOC_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `doc_remarks_ibfk_2` FOREIGN KEY (`DR_BY`) REFERENCES `user` (`U_ID`),
  ADD CONSTRAINT `doc_remarks_ibfk_3` FOREIGN KEY (`DR_FILE_UNIT`) REFERENCES `unit` (`UNIT_ID`),
  ADD CONSTRAINT `doc_remarks_ibfk_4` FOREIGN KEY (`DR_IOR_ID`) REFERENCES `in_out_registery` (`IOR_ID`),
  ADD CONSTRAINT `doc_remarks_ibfk_5` FOREIGN KEY (`DR_DCH_ID`) REFERENCES `doc_character` (`DCH_ID`);

--
-- Constraints for table `doc_share`
--
ALTER TABLE `doc_share`
  ADD CONSTRAINT `doc_share_ibfk_1` FOREIGN KEY (`DS_U_ID`) REFERENCES `user` (`U_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `doc_share_ibfk_2` FOREIGN KEY (`DS_DOC_ID`) REFERENCES `document` (`DOC_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `doc_share_ibfk_3` FOREIGN KEY (`DS_BY`) REFERENCES `user` (`U_ID`);

--
-- Constraints for table `fileaccess_log`
--
ALTER TABLE `fileaccess_log`
  ADD CONSTRAINT `fileaccess_log_ibfk_1` FOREIGN KEY (`fl_user_id`) REFERENCES `user` (`U_ID`);

--
-- Constraints for table `in_out_registery`
--
ALTER TABLE `in_out_registery`
  ADD CONSTRAINT `in_out_registery_ibfk_1` FOREIGN KEY (`IOR_DOC_ID`) REFERENCES `document` (`DOC_ID`);

--
-- Constraints for table `in_out_remarks`
--
ALTER TABLE `in_out_remarks`
  ADD CONSTRAINT `in_out_remarks_ibfk_1` FOREIGN KEY (`IORR_IOR_ID`) REFERENCES `in_out_registery` (`IOR_ID`);

--
-- Constraints for table `office`
--
ALTER TABLE `office`
  ADD CONSTRAINT `office_ibfk_1` FOREIGN KEY (`O_CREATEDBY`) REFERENCES `user` (`U_ID`),
  ADD CONSTRAINT `office_ibfk_2` FOREIGN KEY (`O_DEFAULTPRIO`) REFERENCES `priority` (`P_ID`),
  ADD CONSTRAINT `office_ibfk_3` FOREIGN KEY (`O_AGA`) REFERENCES `aga` (`AGA_ID`);

--
-- Constraints for table `sms_received`
--
ALTER TABLE `sms_received`
  ADD CONSTRAINT `sms_received_ibfk_1` FOREIGN KEY (`SMS_R_D_BY`) REFERENCES `user` (`U_ID`),
  ADD CONSTRAINT `sms_received_ibfk_2` FOREIGN KEY (`SMS_R_M_BY`) REFERENCES `user` (`U_ID`);

--
-- Constraints for table `unit`
--
ALTER TABLE `unit`
  ADD CONSTRAINT `unit_ibfk_1` FOREIGN KEY (`UNIT_CREATEDBY`) REFERENCES `user` (`U_ID`);

--
-- Constraints for table `unit_member`
--
ALTER TABLE `unit_member`
  ADD CONSTRAINT `unit_member_ibfk_1` FOREIGN KEY (`um_unit_id`) REFERENCES `unit` (`UNIT_ID`),
  ADD CONSTRAINT `unit_member_ibfk_2` FOREIGN KEY (`um_u_id`) REFERENCES `user` (`U_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `unit_member_ibfk_3` FOREIGN KEY (`um_added_by`) REFERENCES `user` (`U_ID`);

DELIMITER $$
--
-- Events
--
CREATE DEFINER=`root`@`localhost` EVENT `ClearOldAlerts` ON SCHEDULE EVERY 2 MONTH STARTS '2015-12-20 00:00:00' ON COMPLETION NOT PRESERVE ENABLE DO DELETE FROM alerts WHERE `A_SEEN` = 'Y' AND `A_CREATEDON` < now() - INTERVAL 2 MONTH$$

DELIMITER ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
