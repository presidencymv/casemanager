<?php
header('Content-Type: application/json');

require('_ex/connections.php');    
require('_ex/authentications.php');
require('_ex/master.php');
require('_ex/call.php');

$commitresult = "GOOD";


$currentuser = new authentication;


if ($_SESSION['callcentrestaff'] != 'Y'){
    $commitresult = 'NO RIGHTS';
}else{

	if (isset($_POST['st'])){
	$ex = preg_replace( "/[^0-9$]/","",$_POST['st']);
		if ($ex == ''){
		$commitresult = 'Missing EX';
		}
	}else{
		$commitresult = 'Missing EX';
	}
}


$newcall = new call;

if($commitresult == "GOOD"){
	switch($ex){
			case '100':
			//AUTO SAVE
				$values= array(
				$_SESSION['userid'],
				trim($_POST['followup']),
				trim($_POST['callernum']),
				trim($_POST['callername']),
				trim($_POST['calleremail']),
				trim($_POST['from']),
				trim($_POST['to']),
				trim($_POST['doccat']),
				trim($_POST['priority']),
				$newcall->pre_cleanup(trim($_POST['issue'])),
				$newcall->pre_cleanup(trim($_POST['response'])),
				trim($_POST['resolved']),
				5,
				trim($_POST['pvnumber']),
				trim($_POST['companyname'])
				);
				
				$fieldlist = array ("userid","followup","callernum","callername","calleremail","from","to","doccat","priority","issue","response","resolved","state","pvnumber","companyname");
				$case = array('n','tn_nr','n_nr','t_nr','else_nr','n','n','n','n','else_nr','else_nr','n','n','else_nr','else_nr');
				
				$returnresult = $newcall->invalidchar2($values,$fieldlist,$case);
				
				if ($returnresult['STATUS'] == 'PASS'){
					if($newcall->savecall($values) == TRUE){
						$commitresult = "GOOD";
					}else{
						$commitresult = "AUTO SAVE CALL FAIL";
						//$commitresult = $values;
					}
		
				}else{
					$commitresult = $returnresult['FLD'];
				}
			
			break;
			case '200':
			//NORMAL CALL
				$values= array(
				$_SESSION['userid'],
				trim($_POST['followup']),
				trim($_POST['callernum']),
				trim($_POST['callername']),
				trim($_POST['calleremail']),
				trim($_POST['from']),
				trim($_POST['to']),
				trim($_POST['doccat']),
				trim($_POST['priority']),
				$newcall->pre_cleanup(trim($_POST['issue'])),
				$newcall->pre_cleanup(trim($_POST['response'])),
				trim($_POST['resolved']),
				1,
				trim($_POST['pvnumber']),
				trim($_POST['companyname'])
				);
										
				$fieldlist = array ("userid","followup","callernum","callername","calleremail","from","to","doccat","priority","issue","response","resolved","state","pvnumber","companyname");
				$case = array('n','tn_nr','n','t','else_nr','n','n','n','n','else','else','n','n','else_nr','else_nr');
				
				$returnresult = $newcall->invalidchar2($values,$fieldlist,$case);
				
				if ($returnresult['STATUS'] == 'PASS'){
					if($newcall->savecall($values) == TRUE){
						$commitresult = "GOOD";
					}else{
						$commitresult = "SAVE CALL FAIL";
					}
				}else{
					$commitresult = $returnresult['FLD'];
				}
			
			break;
			
			case '201':
			//PASSED CALL
				$values= array(
				$_SESSION['userid'],
				trim($_POST['followup']),
				trim($_POST['callernum']),
				trim($_POST['callername']),
				trim($_POST['calleremail']),
				trim($_POST['from']),
				trim($_POST['to']),
				trim($_POST['doccat']),
				trim($_POST['priority']),
				$newcall->pre_cleanup(trim($_POST['issue'])),
				$newcall->pre_cleanup(trim($_POST['response'])),
				trim($_POST['resolved']),
				2,
				trim($_POST['pvnumber']),
				trim($_POST['companyname'])
				);
										
				$fieldlist = array ("userid","followup","callernum","callername","calleremail","from","to","doccat","priority","issue","response","resolved","state","pvnumber","companyname");
				$case = array('n','tn_nr','n','t','else_nr','n','n','n','n','else','else_nr','n','n','else_nr','else_nr');
				
				$returnresult = $newcall->invalidchar2($values,$fieldlist,$case);
				
				if ($returnresult['STATUS'] == 'PASS'){
					if($newcall->savecall($values) == TRUE){
						$commitresult = "GOOD";
					}else{
						$commitresult = "SAVE CALL FAIL";
					}
				}else{
					$commitresult = $returnresult['FLD'];
				}
			
			break;
			
			case '202':
			//MISSED CALL
				$values= array(
				$_SESSION['userid'],
				NULL,
				trim($_POST['callernum']),
				trim($_POST['callername']),
				NULL,
				NULL,
				NULL,
				NULL,
				NULL,
				'MISSED CALL',
				'MISSED CALL',
				1,
				3,
				NULL,
				NULL
				);
										
				$fieldlist = array ("userid","followup","callernum","callername","calleremail","from","to","doccat","priority","issue","response","resolved","state","pvnumber","companyname");
				$case = array('n','tn_nr','n','t_nr','else_nr','n_nr','n_nr','n_nr','n_nr','else_nr','else_nr','n','n','else_nr','else_nr');
				
				$returnresult = $newcall->invalidchar2($values,$fieldlist,$case);
				
				if ($returnresult['STATUS'] == 'PASS'){
					if($newcall->savecall($values) == TRUE){
						$commitresult = "GOOD";
					}else{
						$commitresult = "SAVE CALL FAIL";
					}
				}else{
					$commitresult = $returnresult['FLD'];
				}
			
			break;
			
			case '203':
			//SMS RECORD CREATION
			
				$values= array(
				$_SESSION['userid'],
				NULL,
				trim($_POST['callernum']),
				trim($_POST['callername']),
				trim($_POST['calleremail']),
				trim($_POST['from']),
				trim($_POST['to']),
				trim($_POST['doccat']),
				trim($_POST['priority']),
				$newcall->pre_cleanup(trim($_POST['issue'])),
				$newcall->pre_cleanup(trim($_POST['response'])),
				0,
				6,
				trim($_POST['pvnumber']),
				trim($_POST['companyname']),
				trim($_POST['smsid']),
				trim($_POST['efid'])
				);
				
				$fieldlist = array ("userid","followup","callernum","callername","calleremail","from","to","doccat","priority","issue","response","resolved","state","pvnumber","companyname","smsid","formid");
				$case = array('n','else_nr','n_nr','t_nr','else_nr','n','n','n','n','else_nr','else_nr','n','n','else_nr','else_nr','n','tn');
				
				$returnresult = $newcall->invalidchar2($values,$fieldlist,$case);
				
				if ($returnresult['STATUS'] == 'PASS'){
				 	if($newcall->savesms_callrecord($values) == TRUE){
						$commitresult = "GOOD";
					}else{
						$commitresult = "FAILED";
					} 
				}else{
					$commitresult = $returnresult['FLD'];
				}
			
			break;
			
			case '300':
			
			require('_ex/document.php');
			require('_ex/contact.php');
			require('_ex/alert.php');
			//AUTO SAVE
				$values= array(
				$_SESSION['userid'],
				trim($_POST['followup']),
				trim($_POST['callernum']),
				trim($_POST['callername']),
				trim($_POST['calleremail']),
				trim($_POST['from']),
				trim($_POST['to']),
				trim($_POST['doccat']),
				trim($_POST['priority']),
				$newcall->pre_cleanup(trim($_POST['issue'])),
				$newcall->pre_cleanup(trim($_POST['response'])),
				trim($_POST['crid']),
				trim($_POST['efid']),
				trim($_POST['pvnumber']),
				trim($_POST['companyname'])
				);
										
				$fieldlist = array ("userid","followup","callernum","callername","calleremail","from","to","doccat","priority","issue","response","id","ef","pvnumber","companyname");
				$case = array('n','tn_nr','n','t','else_nr','n','n','n','n','else','else','n','else','else_nr','else_nr');
				
				$returnresult = $newcall->invalidchar2($values,$fieldlist,$case);
				
				
				$calldetails = $newcall->get_call_info( $values[11]);
				
				if($calldetails['CR_SOLVED'] == 0){
						
						//START HERE
						
						
					if ($returnresult['STATUS'] == 'PASS'){
					if($newcall->updatecall($values) == TRUE){
						$calldetails = $newcall->get_call_info( $values[11]);
						$docid = $newcall->get_docid($values[11]);
						
						if($docid != NULL || $docid != ''){

						$DOC_OFFICE = $values[5];
						$DOC_CATEGORY = $values[7];
				
						$DOC_DESCRIPTION = <<<EOL
<h3>CALL CENTRE CASE</h3>
<h4>ISSUE:</h4>
<p>{$values[9]}</p>
<h4>RESPONSE:</h4>
<p>{$values[10]}</p>		
EOL;
						$DOC_PRIORITY = $values[8];
							
						$docvalues = array( $_SESSION['userid'],$DOC_OFFICE, $DOC_CATEGORY, $DOC_DESCRIPTION, $DOC_PRIORITY, $docid);
							
							$document = new document;
							if ($document->call_updatedocument($docvalues) == TRUE){
								
								
								$ContactName = $calldetails['CR_CALLER'];
								$OfficeName = $calldetails['O_NAME'];
								$ContactNumber = $calldetails['CR_TEL'];
								$ContactEmail = $calldetails['CR_EMAIL'];
								
								
								//update default contact
								$contact = new contact;
								$contactvalues = array($_SESSION['userid'],$docid,$ContactName, $OfficeName, $ContactNumber, $ContactEmail);
								$contact->updatedefault_callcontact($contactvalues);
								
								$docinfo = $document->getdocumentinfo($docid);
								
								if($docinfo['DOC_UNIT'] != $values[6]  ){
								
									$DOC_ID = $docid;
									$UNIT_ID = $values[6];
									
									$sendvalues = array(
										$DOC_ID,
										$UNIT_ID
									);
									
									if($document->routedocument($sendvalues) == TRUE){
										$commitresult = "COMPLETED";
										$document->notification($docid,'Call Centre Case Changed by '.$_SESSION['username'],4);
										$newalert = new alert;
										$newalert->create_alert($DOC_ID,'Case Routed',$UNIT_ID);
									}
								}else{
									$commitresult = "CALL AND CASE UPDATES SAVED";
								}
								
							
							}else{
							
								$commitresult = "CASE UPDATES FAILED";
							}
						
						
						}else{
							$commitresult = "UPDATES SAVED";
						}
						
					}else{
						$commitresult = "FAILED";
					}
				}else{
					$commitresult = "Please Fill The ".$returnresult['FLD']." Field";
				}
						
						//END HERE
				}else{
					$commitresult = "CALL EDIT NOT ALLOWED";
				}
				
	
			
			break;
	}

}

$resultset = array(
'commitresult'=> $commitresult
);

echo  json_encode($resultset, JSON_HEX_QUOT | JSON_HEX_TAG);
?>