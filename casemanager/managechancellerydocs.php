<?php
header('Content-Type: application/json');

require('_ex/connections.php');    
require('_ex/authentications.php');
require('_ex/master.php');
require('_ex/document.php');
require('_ex/chancellery.php');
require('_ex/units.php');
require('_ex/alert.php');
$closeform = 0;
$commitresult = "GOOD";


$currentuser = new authentication;


if ($_SESSION['chancellerystaff'] != 'Y'){
    $commitresult = 'NO RIGHTS';
}else{

	if (isset($_POST['st'])){
	$ex = preg_replace( "/[^0-9$]/","",$_POST['st']);
		if ($ex == ''){
		$commitresult = 'Missing EX';
		}
	}else{
		$commitresult = 'Missing EX';
	}
}


$newdocument = new chancellery;

if($commitresult == "GOOD"){
	switch($ex){
			case '100':	
			$newdocid = 0;
			$values= array(trim($_POST['source']),trim($_POST['sender']),trim($_POST['type']),trim($_POST['reference']),$newdocument->pre_cleanup(trim($_POST['description'])),trim($_POST['deadline']),trim($_POST['priority']),trim($_POST['txt1']),trim($_POST['txt2']));
			$fieldlist = array ("source", "sender", "type", "reference", "description", "deadline", "priority", "txt1", "txt2");
			
			if($_POST['stype'] == 0){
				$case = array('n','n','n','else_nr','else','else','n','else_nr','else_nr');
			}else{
				$case = array('n','else_nr','n','else_nr','else','else','n','else','else_nr');
			}
			
			
			if ($newdocument->invalidchar($values,$fieldlist,$case,'off')){
				if ($newdocument->createdocument($values,$_POST['stype']) == TRUE){
					$newdocid = $newdocument->getlastdoc_id();
					$newdocument->notification($newdocid,'Case Created by '.$_SESSION['username'],4);
					
					if($newdocument->addtoregistery($_POST['exreply'],$newdocid, $_SESSION['userid'],1) == TRUE){

						//SEND CASE TO SECTION
						    $values= array($newdocid,trim($_POST['receiver']));
							$fieldlist = array ("document","receiver");
							$case = array('n','n');
							
							if ($newdocument->invalidchar($values,$fieldlist,$case,'off')){
								if ($newdocument->routedocument($values) == TRUE){
									
									$unit = new unit;
									$unitname = $unit->getunitname($_POST['receiver']);
									$newdocument->notification($newdocid,'Case Sent To Section '.$unitname.' By '.$_SESSION['username'],12);
									$newalert = new alert;
									$newalert->create_alert($newdocid,'Case Routed',$newdocid['receiver']);
									$commitresult = 'Registered And Sent';
								}else{
									$commitresult = 'Case Send Failure1';
								}
							}else{
								$commitresult = 'Partial Registration Failure, Please inform ICT immediately!';
							}
					}else{
						$commitresult = 'Partial Registration Failure, Please inform ICT immediately!';
					}
					$closeform = 1;
				}else{
					$commitresult = 'Registration Failed';
				}
			}else{
				$commitresult = 'Registration Failed Due To Input Error';
				
			}
			break;
	}

}

$resultset = array(
'commitresult'=> $commitresult,
'closeform'=> $closeform,
'newdocid'=> $newdocid
);

echo  json_encode($resultset, JSON_HEX_QUOT | JSON_HEX_TAG);
?>