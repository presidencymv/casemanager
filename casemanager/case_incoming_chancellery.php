<?php
header('Content-Type: application/json');
require('_ex/connections.php');
require('_ex/authentications.php');
require('_ex/master.php');
require('_ex/document.php');
require('_ex/attachments.php');
require('_ex/remark.php');
require('_ex/chancellery.php');
require('_ex/alert.php');


$closeform = 0;
$commitresult = "GOOD";


$currentuser = new authentication;


if ($_SESSION['chancellerystaff'] != 'Y'){
    $commitresult = 'NO RIGHTS';
}else{
	if (isset($_GET['hc'])){
		if($_GET['hc'] == 1){
			$hc = 1;
		}else{
			$hc = 0;
		}
	}else{
		$hc = 0;
	}
	if (isset($_GET['ex']) || isset($_GET['docid'])){
	$ex = preg_replace( "/[^0-9$]/","",$_GET['ex']);
	$docid = preg_replace( "/[^0-9$]/","",$_GET['docid']);

		if ($ex == '' || $docid == ''){
		$commitresult = 'Missing EX OR DOC ID';
		}
	}else{
		$commitresult = 'Missing EX OR DOC ID';
	}
}

$newdocument = new chancellery;

if($commitresult == "GOOD"){
switch($ex){

			case '102'://2
		
			if (!empty($_FILES)) {
				$newfile = new fileattachment($_FILES['file']);
				if ($newfile->checkfileattachment() == TRUE){
					$values= array($docid,$newfile->getfilename(),$newfile->getfilename(),'NOREF');
					$fieldlist = array ('DocumentID','Description','File Name','Reference');
					$case = array('n','else','else','else_nr');
					$newremark = new remark;
					if ($newremark->invalidchar($values,$fieldlist,$case,'on')){
						if ($newfile->uploadfile() == TRUE){
							$docunit = $newdocument->getdocunitid($docid);
							$chanid = $newdocument->getregidid($docid);
							
							if ($newremark->createremark_chancellery($values, $docunit, $chanid, $hc) == TRUE){
								$newfile->renamefile($newremark->generatedocname());
								
								$newdocument->notification($docid,'File Attachment :'.$values[2].' By:'.$_SESSION['username'],8);
								
								
								$newalert = new alert;
								$newalert->create_alert($docid,'Case New Attachment');
								$commitresult = 'Success';
							}
						}

					}else{
						$commitresult = 'Failed';
					}
				}
			}else{
				$commitresult = 'Failed';
			}
			break;
		

    }
}
$resultset = array(
'commitresult'=> $commitresult
);

echo  json_encode($resultset, JSON_HEX_QUOT | JSON_HEX_TAG);
?>
