<?php
error_reporting(0);
session_start();

function authenticate() {
	include('_pages/entercasemanager.php');
	header('WWW-Authenticate: Basic realm="Active Directory Login"');
	header('HTTP/1.0 401 Unauthorized');
}
	if (!isset($_SERVER['PHP_AUTH_USER']) || ($process_msg == 'tryagain' && isset($_SESSION['user']) && isset($_SESSION['domain'])) || ($_SESSION["check"] == '')){
		$_SESSION["check"] = 1;
	   authenticate();
   }else{
   $_SESSION["domain"] = $domain = 'trade.gov.mv'; // <- domain
   //$_SESSION["user"] = strtoupper($_SERVER["PHP_AUTH_USER"]);
   //$_SESSION["password"] = $_SERVER["PHP_AUTH_PW"];
   $LDAPServerAddress1="192.168.100.1"; // <- IP address for DC
   $LDAPServerPort="389";
   $LDAPServerTimeOut ="60";
   $LDAPContainer="dc=trade,dc=gov,dc=mv"; // <- domain info
   $BIND_username = 'trade\\'.$_SERVER["PHP_AUTH_USER"];
   $BIND_password = $_SERVER["PHP_AUTH_PW"];
   $filter = "sAMAccountName=".$_SESSION["user"];
   $login_error_code = 0;

   if(($ds=ldap_connect($LDAPServerAddress1)) || ($ds=ldap_connect($LDAPServerAddress2))) {
      ldap_set_option($ds, LDAP_OPT_REFERRALS, 0);
      ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
      
      if($r=ldap_bind($ds,$BIND_username,$BIND_password)) {
         if($sr=ldap_search($ds, $LDAPContainer, $filter, array('distinguishedName'))) {
            if($info = ldap_get_entries($ds, $sr)) {
               $BIND_username = $info[0]['distinguishedname'][0];
               $BIND_password = $_SERVER["PHP_AUTH_PW"];
               if ($r2=ldap_bind($ds,$BIND_username,$BIND_password)) {
                  if($sr2=ldap_search($ds, $LDAPContainer, $filter, array("givenName","sn","mail","displayName"))) {
                     if($info2 = ldap_get_entries($ds, $sr2)) {
                        $_SESSION["name"] = $info2[0]["givenname"][0]." ".$info2[0]["sn"][0];
                        $_SESSION["email"] = $info2[0]["mail"][0];
                        $_SESSION["displayname"] = $info2[0]["displayname"][0];
						
                     } else {
                        $login_error = "Could not read entries"; $login_error_code=1;
                     }
                  } else {
                     $login_error = "Could not search"; $login_error_code=2;
                  }
               } else {
                  $login_error = "User password incorrect"; $login_error_code=3;
               }
            } else {
               $login_error = "User name not found"; $login_error_code=4;
            }
         } else {
            $login_error = "Could not search"; $login_error_code=5;
         }
      } else {
         $login_error = "Could not bind"; $login_error_code=6;
      }
   } else {
      $login_error = "Could not connect"; $login_error_code=7;
   }
   
   if($login_error_code > 0){
	 $_SESSION["check"] = '';
		echo '<script type="text/javascript">';
		echo 'alert("Your Username or Password is incorrect please try again.");';
		echo '</script>';
	 header("location:index.php");
   } else {
		require ('_ex/connections.php');
		require ('_ex/users.php');

		if (isset( $_SERVER["PHP_AUTH_USER"])){

			$currentuser = new user;
			if ($currentuser->getuserinfoad($_SERVER["PHP_AUTH_USER"])){
				header('location: dashboard.php');
			}else{
				header('location: index.php?user=invalid');
			}

}
		
   }
}
?>