<?php
require('_ex/connections.php');
require('_ex/authentications.php');
require('_ex/master.php');
require('_ex/document.php');
require('_ex/attachments.php');
require('_ex/remark.php');
require('_ex/alert.php');
$currentuser = new authentication;

$case_change = 'caseremarks';

require ('_pages/subcomponents/case_change_auth.php');



if (isset($_GET['ex'])){
$ex = preg_replace( "/[^0-9$]/","",$_GET['ex']);
	if ($ex == ''){
	header('location: dashboard.php');
	die();
	}
}else{
	header('location: dashboard.php');
	die();
}

switch($ex){
        case '101':
			$values= array($docid,trim($_POST['remark_description']),'');
			$fieldlist = array ('DocumentID','Description','');
			$case = array('n','else','else_nr');
			$newremark = new remark;
			if ($newremark->invalidchar($values,$fieldlist,$case,'on')){
				if ($newremark->createremark($values) == TRUE){
					$document->notification($docid,'New Update :'.$_POST['remark_description'].' By:'.$_SESSION['username'],15);
					$newalert = new alert;
					$newalert->create_alert($docid,'Case Updated');
					header('location: dashboard.php?page=casedetails&id='.$docid);
					die();
				}else{
					header('location: dashboard.php?page=casedetails&msg=3&id='.$docid);
					die();
				}

			}

        break;
			case '102'://2
			if (!empty($_FILES)) {
				$newfile = new fileattachment($_FILES['file']);
				if ($newfile->checkfileattachment() == TRUE){
					$values= array($docid,$newfile->getfilename(),$newfile->getfilename(),'NOREF');
					$fieldlist = array ('DocumentID','Description','File Name','Reference');
					$case = array('n','else','else','else_nr');
					$newremark = new remark;
					if ($newremark->invalidchar($values,$fieldlist,$case,'on')){
						if ($newfile->uploadfile() == TRUE){
							$docunit = $document->getdocunitid($docid);
							if ($newremark->createremark($values, $docunit) == TRUE){
								$newfile->renamefile($newremark->generatedocname());
								$document->notification($docid,'File Attachment :'.$values[2].' By:'.$_SESSION['username'],8);
								$newalert = new alert;
								$newalert->create_alert($docid,'Case New Attachment');
								die();
							}
						}

					}else{
						die();
					}
				}
			}
			break;

			case '103':
			//EDIT ATTACHMENT
			if (isset($_GET['rec'])){
				$rec_id = preg_replace( "/[^0-9$]/","",$_GET['rec']);
				if ($rec_id == ''){
					header('location: dashboard.php?page=caseattachments&msg=3&id='.$docid);
					die();
				}
			}else{
				header('location: dashboard.php?page=caseattachments&msg=3&id='.$docid);
				die();
			}
			$newremark = new remark;
			$selectattch_info = $newremark->remarkinfo($rec_id,$docid);

			//this is to prevent changing attachment by unauthorized users
			//authorized users are the following
			//the user who uploaded the attachment, section admin for attachments by that section user
			//|| ($updates['DR_BY'] == $_SESSION['userid'] && $updates['DR_FILE_UNIT'] == NULL)
			if ($currentuser->attachment_auth($selectattch_info['DR_FILE_UNIT'], $selectattch_info['DOC_UNIT'],$selectattch_info['DOC_ASSIGNEDTO'], $selectattch_info['DR_BY'], $selectattch_info['DOC_CREATEDBY'],$selectattch_info['DOC_STATUS']) == FALSE) {
				header('location: dashboard.php?page=caseattachments&msg=33&id='.$docid);
				die();
			}

			$newname = NULL;

			if ($_FILES["userfile"]["error"] == 0){
				$newfile = new fileattachment($_FILES['userfile']);
				if ($newfile->checkfileattachment() == TRUE){
							if($selectattch_info['DR_ID'] != NULL OR $selectattch_info['DR_ID'] != ''){
								$currentfilename = 'files/attachments/'.$selectattch_info['DR_FILE'];
								unlink($currentfilename);
								if ($newfile->uploadfile() == TRUE){
											//get new file name
											$newfilename = $_FILES["userfile"]["name"];
											$newfilename = explode('.', $newfilename);
											//get old file name
											$oldfilename = explode('.', $selectattch_info['DR_FILE']);
											//join old file name with new file extension
											array_pop($oldfilename);
											$newname = implode("", $oldfilename).'.'.end($newfilename);

											//$extractfilename = explode ("/",$currentfilename);
											//$newfile->renamefile($extractfilename[count($extractfilename) - 1]);
											$newfile->renamefile($newname);
											$document->notification($docid,'File Attachment Changed By:'.$_SESSION['username'].'. Old File Description:'.$selectattch_info['DR_DESC'].'. New File Description:'.trim($_POST['remark_description']),8);
								}else{
									header('location: dashboard.php?page=caseattachments&msg=3&id='.$docid);
									die();
								}
							}else{
								header('location: dashboard.php?page=caseattachments&msg=3&id='.$docid);
								die();
							}
						}
				}
				$values= array($rec_id,$docid,trim($_POST['remark_description']),trim($_POST['reference']));
				$fieldlist = array ('RecordID','DocID','Description','Reference');
				$case = array('n','n','else','else_nr');
				if ($newremark->invalidchar($values,$fieldlist,$case,'on')){
						if ($newremark->updateremark($values,$newname) == TRUE){
							$document->notification($docid,'File Attachment Updated By:'.$_SESSION['username'].'. Old File Description:'.$selectattch_info['DR_DESC'].'. New File Description:'.trim($_POST['remark_description']),8);
							$newalert = new alert;
							$newalert->create_alert($docid,'Case Attachments Updated');
							header('location: dashboard.php?page=caseattachments&msg=1&id='.$docid);
							die();
						}
				}
			break;


			case 104:
			//REMOVE ATTACHMENTS
			if (isset($_GET['rec'])){
				$rec_id = preg_replace( "/[^0-9$]/","",$_GET['rec']);
				if ($rec_id == ''){
					header('location: dashboard.php?page=caseattachments&msg=3&id='.$docid);
					die();
				}
			}else{
				header('location: dashboard.php?page=caseattachments&msg=3&id='.$docid);
				die();
			}
			$newremark = new remark;

			$selectattch_info = $newremark->remarkinfo($rec_id,$docid);

			//this is to prevent changing attachment by unauthorized users
			//authorized users are the following
			//the user who uploaded the attachment, section admin for attachments by that section user
			if ($currentuser->attachment_auth($selectattch_info['DR_FILE_UNIT'], $selectattch_info['DOC_UNIT'],$selectattch_info['DOC_ASSIGNEDTO'], $selectattch_info['DR_BY'], $selectattch_info['DOC_CREATEDBY'],$selectattch_info['DOC_STATUS']) == FALSE) {
				header('location: dashboard.php?page=caseattachments&msg=3&id='.$docid);
				die();
			}
			
			if($newremark->delete_remarks($rec_id,$docid) == TRUE){
				$document->notification($docid,'File Attachment Removed By:'.$_SESSION['username'].'. Old File Description:'.$selectattch_info['DR_FILE'],8);
				$newalert = new alert;
				$newalert->create_alert($docid,'Case Attachments Updated');
				header('location: dashboard.php?page=caseattachments&msg=1&id='.$docid);
				die();
			}
			break;
    }
header('location: dashboard.php?page=casedetails&msg=2&id='.$docid);
die();
?>
