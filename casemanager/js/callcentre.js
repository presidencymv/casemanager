	
	'use strict';
	var sec = 0;
	var min = 0;
	var asavedelay = 10000; 
	var tsavedelay = 10000; 
	var asavetimer;
	var autosavestatus = 'enabled'
	var timer;
	var $table = $('#callstable');
	var startTimer_value = null;
	
	var takecall_request;
	function takecall(followUp){
		var r = confirm("Take Call?");
		if (r == true) {
			if(typeof takecall_request !== 'undefined')
			takecall_request.abort();
			takecall_request = $.ajax({
				url: '_pages/data/takecall.php?followup=' + followUp,
				dataType: 'json',
				success: function(data) {
					var form = data.form;
					
					$("html, body").animate({ scrollTop: $('#activecall').offset().top-90 }, 200);
					
					document.getElementById("activecall").innerHTML=data.form;
					$( "#form" ).slideDown("fast");
					
					autosavestatus = 'enabled';
					startclock();
					startTimer_value = startTimer();
					
					sec = data.sec;
					updateallselects();
					
					if (followUp != 0){
						opencalldetails();
					}
			
				}
			});
		}
	}
	
	var opencalldetails_request;
	function opencalldetails(){
	
		if(typeof opencalldetails_request !== 'undefined')
        opencalldetails_request.abort();
		opencalldetails_request =$.ajax({
			url: '_pages/data/reftocr.php',
			dataType: 'json',
			data:{'ref':$("#followup").val()},
			success: function(data) {
				calldetails(data.cr);
				if(data.cr != null){
					fillupform(data.cr);
				}
					
			}
		});
		
	}
	var fillupform_request;
	function fillupform(callid){
		if(typeof fillupform_request !== 'undefined')
        fillupform_request.abort();
		fillupform_request = $.ajax({
		url: '_pages/data/followup_call_formfill_data.php?id=' + callid,
		dataType: 'json',
		success: function(data) {
					
					$("#callernum").val(data.CR_TEL);
					$("#callername").val(data.CR_CALLER);
					$("#calleremail").val(data.CR_EMAIL);
					$("#from").val(data.CR_O_ID).change();
					$("#to").val(data.CR_UNIT_ID).change();
					$("#doccat").val(data.CR_DC_ID);
					$("#priority").val(data.CR_P_ID).change();
					$("#doccat").val(data.CR_DC_ID);
					$("#pvnumber").val(data.CR_PV);
					$("#companyname").val(data.CR_COMPANY);
		}
		
	});
	
	
	}
	
	var markasresolved_request;
	function markasresolved(id,t){
	var confirmmsg;
		if (t == 0){
		confirmmsg = "Mark Call As Resolved?";
		}else{
		confirmmsg = "Mark Call As Unresolved?";
		}
		
		var r = confirm(confirmmsg);
		if (r == true) {
		if(typeof markasresolved_request !== 'undefined')
        markasresolved_request.abort();
		markasresolved_request = $.ajax({
				url: 'markasresolved.php',
				dataType: 'json',
				data:{'id':id},
				success: function(data) {
					alert(data.commitresult);
					closecalldetail(id);
					$table.bootstrapTable('refresh');
				}
			});
		}
		
	}
	var calldetails_request;
	function calldetails(id){
		
		$("#calldetails"+id).focus();
		if(typeof calldetails_request !== 'undefined')
        calldetails_request.abort();
		calldetails_request = $.ajax({
			url: '_pages/data/calldetails.php',
			dataType: 'json',
			data:{'id':id},
			success: function(data) {
				$("#calldetails").append(data.callinfo);
				$("#calldetails"+id ).show("fast");
				$("html, body").animate({ scrollTop: $('#calldetails'+id).offset().top-90 }, 200);
				$("#callheader"+id ).effect( "pulsate",2000 );
		
			},
			
		});
		
	}
	
	function closecalldetail(id){
		
		$("#calldetails"+id ).hide("fast",function(){
			$("#calldetails"+id).remove();
		});
		
	}
	
	
	function reopen(id){
		
		$("#calldetails"+id ).hide("fast",function(){
			$("#calldetails"+id).remove(calldetails(id));
		});
		
	}
	
	function startTimer() {
	var start = Date.now();
		setTimeout(function() {
			autosave();
			var actual = Date.now() - start;
			tsavedelay = asavedelay - (actual - asavedelay);
			start = Date.now();
		}, tsavedelay);
		
	}
	
	
	function autosave(){
	
		if (autosavestatus == 'enabled'){
			savedata(100);
		}
		
	}
	
	var savedata_request;
	function savedata(type){
		
		if(typeof savedata_request !== 'undefined')
        savedata_request.abort();
		var resolvedstate;
		if(document.getElementById("resolved").checked == true){
			resolvedstate = 1;
		}else{
			resolvedstate = 0;
		}
		
		startTimer_value = null;
		
		savedata_request = $.post('savecall.php',
			{
				st			: type,
				followup 	: $("#followup").val(),
				callernum 	: $("#callernum").val(),
				callername 	: $("#callername").val(),
				calleremail : $("#calleremail").val(),
				from 		: $("#from").val(),
				to			: $("#to").val(),
				doccat		: $("#doccat").val(),
				priority 	: $("#priority").val(),
				issue 		: $("#issue").val(),
				response 	: $("#response").val(),
				resolved 	: resolvedstate,
				pvnumber 	: $("#pvnumber").val(),
				companyname : $("#companyname").val()
			},
			function(data,status){
				if (data.commitresult == 'GOOD' && type == '100'){
					if (autosavestatus == 'enabled'){
					document.getElementById("autosaveindicator").innerHTML = '<span class="glyphicon glyphicon-floppy-save text-success" title="High Priority"></span> <small>AUTO SAVING ENABLED</small><br/>';
					$("#autosaveindicator").effect( "pulsate", 2000 );
					startTimer_value = startTimer();
					}
				}else if (data.commitresult != 'GOOD' && type == '100'){
					alert('error in :' + data.commitresult);
					if (autosavestatus == 'enabled'){
						document.getElementById("autosaveindicator").innerHTML = '<span class="glyphicon glyphicon-floppy-save text-danger" title="High Priority"></span> <small>AUTO SAVING DISABLED (ERROR FOUND IN ' + data.commitresult +')</small><br/>';
						document.getElementById(data.commitresult).focus();
						startTimer_value = startTimer();
					}

				}else if (data.commitresult == 'GOOD' && type != '100'){
					autosavestatus = 'disabled';
					document.getElementById("autosaveindicator").innerHTML = '<span class="glyphicon glyphicon-floppy-save text-success" title="High Priority"></span> <small>CALL SAVED</small><br/>';
					closeform();
					clearInterval(timer);
					$table.bootstrapTable('refresh');
				}else if (data.commitresult != 'GOOD' && type != '100'){
					alert('(ERROR FOUND REQUIRED FIELD: ' + data.commitresult +')');
					document.getElementById(data.commitresult).focus();
					startTimer_value = startTimer();
				}
				
			}
		);
	
	}
	

	function pad(val) {
		return val > 9 ? val : "0" + val;
	}
	
	function startclock(){
			sec = 0;
			min = 0;
			timer= setInterval(function () {
			document.getElementById("seconds").innerHTML = pad(++sec % 60);
			min = (sec / 60);
			while (min >= 60){
				min = min - 60;
			}
			document.getElementById("minutes").innerHTML = pad(parseInt(min, 10));
			document.getElementById("hours").innerHTML = pad(parseInt((sec / 60 )/ 60, 10));
		}, 1000);
		

	}

	
	function checkcallerinfo(){
	AnimateRotate(360);
	
	}
	
	function AnimateRotate(d){
		$({deg: 0}).animate({deg: d}, {
			step: function(now, fx){
				$("#callerinfo-refresh").css({
					 transform: "rotate(" + now + "deg)"
				});
				
			}
		});
		
	}
	
	$('#followup').on('change', function (e) {
		alert($("#followup").val());
	});
	
	
	function closeform(){

		 $( "#form" ).slideUp( "slow", function() {
			document.getElementById("activecall").innerHTML='<button id="takecallbutton" type="button" class="btn btn-primary" onclick="takecall()">TAKE CALL</button>';
		  });
		
	}
	
	function automaticrefresh(){
		//document.getElementById("spinner").style.height='0px';	
		//document.getElementById("spinner").style.width='0px';
		$("#spinner" ).slideUp("slow")
		//$table.bootstrapTable('refresh');
		todayscalls();
		unattendedsms();
	}
	
	var sendcase_request;
	function sendcase(id){
	var r = confirm("Create And Send Case?");
    if (r == true) {
		if(typeof sendcase_request !== 'undefined')
        sendcase_request.abort();
		sendcase_request = $.ajax({
			url: 'sendcase.php',
			dataType: 'json',
			data:{'id':id},
			success: function(data) {
				$("#calldetails"+id).remove();
				calldetails(id);
			}
		});
	}
	
	}

	var editcalldetails_request;
	function editcalldetails(id){
	if(typeof editcalldetails_request !== 'undefined')
        editcalldetails_request.abort();
		editcalldetails_request = $.ajax({
		url: '_pages/data/editform.php',
		dataType: 'json',
		data:{'id':id},
		success: function(data) {

			if(data.selectedcount == 0){
				document.getElementById("editform").innerHTML='<h4 class="text-center text-info">Invalid Form Requested</h4>';
				setTimeout(function(){
				 $('#myModal').modal('hide');
				}, 3000);		
			}else{
				document.getElementById("editform").innerHTML=data.form;
			}
		}
	});
	
	}
	
	
	
	
	var createsmsrecord_request;
	function createsmsrecord(id){
	if(typeof createsmsrecord_request !== 'undefined')
        createsmsrecord_request.abort();
		createsmsrecord_request = $.ajax({
		url: '_pages/data/createsmsrecord.php',
		dataType: 'json',
		data:{'id':id},
		success: function(data) {
			
			if(data.selectedcount == 0){
				document.getElementById("editform").innerHTML='<h4 class="text-center text-info">Invalid Form Requested</h4>';
				setTimeout(function(){
				 $('#myModal').modal('hide');
				}, 3000);		
			}else{
				document.getElementById("editform").innerHTML=data.form;
				$('#myModal').modal('show');
			}
		}
	});
	
	}
	
	
	var updatecall_request;
	function updatecall(id){
		if(typeof updatecall_request !== 'undefined')
        updatecall_request.abort();
		updatecall_request = $.post('savecall.php',
			{
				st			: 300,
				followup 	: $("#effollowup").val(),
				callernum 	: $("#efcallernum").val(),
				callername 	: $("#efcallername").val(),
				calleremail : $("#efcalleremail").val(),
				from 		: $("#effrom").val(),
				to			: $("#efto").val(),
				doccat		: $("#efdoccat").val(),
				priority 	: $("#efpriority").val(),
				issue 		: $("#efissue").val(),
				response 	: $("#efresponse").val(),
				crid 		: $("#efcr_id").val(),
				efid 		: $("#ef_id").val(),
				pvnumber 	: $("#pvnumber").val(),
				companyname : $("#companyname").val()
			},
			function(data,status){
				alert(data.commitresult);
				$('#myModal').modal('hide');
				reopen(id);
				$table.bootstrapTable('refresh');
				
			}
		);
	
	}
	
	
	var createSMSRecord_request;
	function createSMSRecord(id){
		if(typeof createSMSRecord_request !== 'undefined')
        createSMSRecord_request.abort();
		createSMSRecord_request = $.post('savecall.php',
			{
				st			: 203,
				callernum 	: $("#efcallernum").val(),
				callername 	: $("#efcallername").val(),
				calleremail : $("#efcalleremail").val(),
				from 		: $("#effrom").val(),
				to			: $("#efto").val(),
				doccat		: $("#efdoccat").val(),
				priority 	: $("#efpriority").val(),
				issue 		: $("#efissue").val(),
				response 	: $("#efresponse").val(),
				smsid 		: $("#efsms_id").val(),
				efid 		: $("#ef_id").val(),
				pvnumber 	: $("#pvnumber").val(),
				companyname : $("#companyname").val()
			},
			function(data,status){
				alert(data.commitresult);
				$('#myModal').modal('hide');
				unattendedsms();
				$table.bootstrapTable('refresh');
			}
		);
	
	}
	
	function deleteRecord(id){
		var r = confirm("Delete SMS RECORD?");
		if (r == true) {
			if(typeof deleteRecord_request !== 'undefined')
			deleteRecord_request.abort();
			deleteRecord_request = $.post('managesms.php',
				{
					st			: 101,
					id 			: id
				},
				function(data,status){
					alert(data.commitresult);
					unattendedsms();
				}
			);
		}
	}
	
	function mergeRecord(id){
		var r = confirm("Merge SMS with previous message?");
		if (r == true) {
			if(typeof mergeRecord_request !== 'undefined')
			mergeRecord_request.abort();
			mergeRecord_request = $.post('managesms.php',
				{
					st			: 102,
					id 			: id
				},
				function(data,status){
					alert(data.commitresult);
					unattendedsms();
				}
			);
		}
	}
	
	var todayscalls_request;
	function todayscalls(){
		
		if(typeof todayscalls_request !== 'undefined')
        todayscalls_request.abort();
		todayscalls_request = $.ajax({
			url: '_pages/data/todayscalls.php',
			dataType: 'json',
			success: function(data) {
				document.getElementById("todayscalls").innerHTML = data.details
			}
		});
		
	}
	
	
	var unattendedSMS_request;
	function unattendedsms(){
		
		if(typeof unattendedSMS_request !== 'undefined')
        unattendedSMS_request.abort();
		unattendedSMS_request = $.ajax({
			url: '_pages/data/unattended_sms.php',
			dataType: 'json',
			success: function(data) {
				document.getElementById("unattendedsms").innerHTML = data.details
			}
		});
		
	}
	
	 
	

	$('#myModal').on('hidden.bs.modal', function () {
	 document.getElementById("editform").innerHTML='';
	})
	setInterval( "automaticrefresh()", 5000 );
	

	