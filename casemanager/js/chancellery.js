	var $table = $('#chancellery_active');
	var $cdoc; 
	var $dropzone;
	var $selecteddoc;
	var $chanid;
	var newdropzone;
	var stype = 0;
	var docid;
	var recid;
	var prd = "10.244.4.101";
	var dev = "localhost";
	var serv = prd;
	var formupdate;
	
	var newdoc_request;
	function newdoc(){

		if(typeof newdoc_request !== 'undefined')
        newdoc_request.abort();
		newdoc_request = $.ajax({
			url: '_pages/data/newchancellerydocform.php',
			dataType: 'json',
			success: function(data) {
				document.getElementById("myModalLabel").innerHTML= "<h2>Register Chancellery Document</h2>";
					document.getElementById("formloader").innerHTML=data.form;
					$("#private").hide();
					Dropzone.autoDiscover = false;
					newdropzone = new Dropzone('#dropnewfiles', {
						parallelUploads: 10,
					  url: "http://" + serv + "/casemanager/case_incoming_chancellery.php?ex=102&docid=" + $selecteddoc,                        
					  autoProcessQueue: false,
					});
			 //updateallselects();
			}
		});
	}


	function addnewdoc(){
		if(document.getElementById("exreply").checked == true){
			exreplystate = 1;
		}else{
			exreplystate = 0;
		}
		
		if(document.getElementById("hardcopy").checked == true){
			hardcopystate = 1;
		}else{
			hardcopystate = 0;
		}
		var val1;
		var val2;
		var senderval;
		
		if (stype == 0){
		val1 =  $("#byname").val();
		val2 =  $("#designation").val();
		senderval = $("#sender").val();

		}else{
			
		val1 =  $("#namecompany").val();
		val2 =  $("#nic").val();
		senderval = "NONE";

		}
		
		$.post('managechancellerydocs.php',
			{
				st			: 100,
				source	 	: $("#source").val(),
				stype		: stype,
				sender	 	: senderval,
				txt1	 	: val1,
				txt2	 	: val2,
				receiver 	: $("#receiver").val(),
				type		: $("#type").val(),
				reference 	: $("#reference").val(),
				description	: $("#description").val(),
				deadline	: $("#deadline").val(),
				priority 	: $("#priority").val(),	
				exreply 	: exreplystate
			},
			
			
			function(data,status){
			
				if (data.closeform == 1){
					$selecteddoc = data.newdocid;
				
					newdropzone.on("processing", function(file) {
						
					  newdropzone.options.url = "http://" + serv + "/casemanager/case_incoming_chancellery.php?ex=102&hc=" + hardcopystate + "&docid=" + $selecteddoc;
					});
					 newdropzone.on("queuecomplete", function(file) {
							newdropzone.removeAllFiles();
							alert(data.commitresult);
							$('#myModal').modal('hide');
							$table.bootstrapTable('refresh');
						});
					newdropzone.processQueue();
				}
				
			}
		);
	}
	
	function opendoc(id){
		cdoc = id;
		$.ajax({
			url: '_pages/data/chancellerycasedetailsform.php?docid=' + id,
			dataType: 'json',
			success: function(data,status) {
		
				$("html, body").animate({ scrollTop: $('#detailsform').offset().top-90 }, 200);
				document.getElementById("detailsform").innerHTML=data.form;
				
				Dropzone.autoDiscover = false;
				newdropzone = new Dropzone('#newattachments', {
					parallelUploads: 10,
					url: "http://" + serv + "/casemanager/case_incoming_chancellery.php?ex=102&docid=" + id,                        
					autoProcessQueue: true
				});
				
				newdropzone.on("queuecomplete", function(file) {
					newdropzone.removeAllFiles();
					loadattachments(cdoc);
				});
				
				loadattachments(cdoc);
			}
		});
	}

	function loadattachments(cdoc){
		$.ajax({
			url: '_pages/data/attachments_chancellery.php?id=' + cdoc,
			dataType: 'json',
			success: function (data,status) {
				document.getElementById("case_data").innerHTML=data.att_details;	
			}
		});		
	}
	
	$('#myModal').on('hidden.bs.modal', function () {
	 document.getElementById("formloader").innerHTML='';
	})
	
    //HIDE SUB FORMS
	function showgomform() {
		hideallforms();
		stype = 0;
		$("#gom").show();
	}

	function showprivateform() {
		hideallforms();
		stype = 1;
		$("#private").show();
	}

	function hideallforms() {
		$("#gom").hide();
		$("#private").hide();
	}
		
	function updatefileattributes(d,r){
			
			docid = d;
			recid = r;

			$.post('_pages/data/fileattributesupdateform.php',
			{
				docid		: docid,
				recid	 	: recid
			},
			
			function(data,status){
				document.getElementById("myModalLabel").innerHTML= "Update File Attributes";
				document.getElementById("formloader").innerHTML=data.form;
			}
		);
	}
	
	function updateattributes(d = docid,r = recid){
			var hardcopystate;

			if(document.getElementById("hardcopy").checked == true){
				hardcopystate = 1;
			}else{
				hardcopystate = 0;
			}
			$.post('managechancelleryattachments.php',
			{
				docid		: docid,
				recid	 	: recid,
				hardcopy	: hardcopystate,
				pages	 	: $("#pages").val(),
				reference	: $("#reference").val(),
				details		: $("#details").val(),
				authorname	: $("#authorname").val(),
				authortel1	: $("#authortel1").val(),
				authortel2	: $("#authortel2").val(),
				authoremail	: $("#authoremail").val(),
				docchar		: $("#docchar").val(),
				createddate	: $("#createddate").datepicker({ dateFormat: 'dd,mm,yyyy' }).val(),
				st			: 100
			},

			function(data,status){
				alert(data.commitresult);
				$('#myModal').modal('hide');
				loadattachments(docid);
			}
		);
	}
	
	function clearattachments(){
		Dropzone.forElement("#dropnewfiles").removeAllFiles(true);
		
	}
		
	$(function () {
        $('#createddate').datetimepicker({
            
            });
    });
	
	