<?php
require('../../_ex/connections.php');  
session_start();
$file = $_GET['file'];
//$file = "http://localhost/casemanager/files/attachments/134455ba9eb681b9464ece8b6a3c2da4b55.zip";

$has_authority = 'N';
logaccess($file, $has_authority);
    
  


function logaccess($file, $has_authority){

    $dbc = new dbconnection;

    $ip_address = getip();
		
    $hostaddress = gethostbyaddr($_SERVER['REMOTE_ADDR']);

    $ins_stmt = $dbc->dbconn->prepare("INSERT INTO fileaccess_log (`fl_user_id`, `fl_username`, `fl_filename`, `fl_authorized`, `fl_hostaddress`, `fl_hostip`,`fl_direct`) VALUES ( ?, ?, ?, ?, ?, ?, ?)");
    $ins_stmt->bind_param('dssssss', $fl_user_id, $fl_username, $fl_filename, $fl_authorized, $fl_hostaddress, $fl_hostip, $fl_direct);

    $fl_user_id = $_SESSION['userid'];
    $fl_username = $_SESSION['username'];
    $fl_filename = $file;
    $fl_authorized = $has_authority;
    $fl_hostaddress = $hostaddress;
    $fl_hostip = $ip_address;
	$fl_direct = 'Y';

    $ins_stmt->execute();
    $ins_stmt->close();

    $dbc->dbconn->close();
	
	
	header('location: ../../index.php?user=relogin');
	die();  

}

function getip(){

    $ip_address = '';

    if(!empty($_SERVER['HTTP_CLIENT_IP']))
    {
        $ip_address = $_SERVER['HTTP_CLIENT_IP'];
    }
    elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
    {
        $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
        $ip_address = $_SERVER['REMOTE_ADDR'];
    }

    return $ip_address;

}



?>