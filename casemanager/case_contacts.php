<?php
require('_ex/connections.php');    
require('_ex/authentications.php');
require('_ex/master.php');
require('_ex/document.php');
require('_ex/contact.php');
$currentuser = new authentication;

$case_change = 'casecontacts';

require ('_pages/subcomponents/case_change_auth.php');

if (isset($_GET['ex'])){
$ex = preg_replace( "/[^0-9$]/","",$_GET['ex']);

	if ($ex == ''){
	header('location: dashboard.php');
	die();
	}
}else{
	header('location: dashboard.php');
	die();
}

$contact = new contact;

switch($ex){
	//ADD CONTACT
	case '100':
	$values= array($docid,trim($_POST['fullname']),trim($_POST['designation']), trim($_POST['address']), trim($_POST['email']), trim($_POST['telephone1']), trim($_POST['telephone2']));
	$fieldlist = array ('DocumentID','Name','Designation','Address','Email','Telephone1','Telephone2');
	$case = array('n','else','else_nr','else_nr','else_nr','else','else_nr');

	if ($contact->invalidchar($values,$fieldlist,$case,'on')){
		if ($contact->addcontact($values) == TRUE){
			$document->notification($docid,'Contact: '.trim($_POST['fullname']).' TEL: '.trim($_POST['telephone1']).' Added By: '.$_SESSION['username'],6);
			header('location: dashboard.php?page=casecontacts&id='.$docid);
			die();
		}else{
			header('location: dashboard.php?page=casecontacts&msg=3&id='.$docid);
			die();
		}
		
	}
	break;
	
	
	//UPDATE CONTACT
	case '101':
	if( $contact->valid_contact($docid, $_POST['mod_contact_id']) == TRUE){
		$values= array($docid,trim($_POST['mod_contact_id']), trim($_POST['mod_fullname']), trim($_POST['mod_designation']), trim($_POST['mod_address']), trim($_POST['mod_email']), trim($_POST['mod_telephone1']), trim($_POST['mod_telephone2']));
		$fieldlist = array ('DocumentID','Record','Name','Designation','Address','Email','Telephone1','Telephone2');
		$case = array('n','n','else','else_nr','else_nr','else_nr','else','else_nr');

		if ($contact->invalidchar($values,$fieldlist,$case,'on')){

				if ($contact->updatecontact($values) == TRUE){
					$document->notification($docid,'Contact: '.trim($_POST['mod_contact_id']).' NAME: '.trim($_POST['mod_fullname']).' TEL: '.trim($_POST['mod_telephone1']).' Updated By: '.$_SESSION['username'],6);
					header('location: dashboard.php?page=casecontacts&id='.$docid);
					die();
				}else{
					header('location: dashboard.php?page=casecontacts&msg=3&id='.$docid);
					die();
				}
	
		}
	}else{
		header('location: dashboard.php?page=casecontacts&msg=3&id='.$docid);
		die();
	}
}   

header('location: dashboard.php?page=casecontacts&id='.$p_id);
die();
?>