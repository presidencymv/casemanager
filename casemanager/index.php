<?php 
require ('_ex/connections.php');
require ('_ex/master.php');
require ('_ex/users.php');
require('_ex/proxy.php');


if (isset($_POST['username']) && isset($_POST['password'])){

       $valueset = array ($_POST['username'],$_POST['password']);
       $fieldlist = array ('Username','Password');
       $case = array('tn','else');

       $usercredentials = new master;
       if ($usercredentials->invalidchar($valueset,$fieldlist,$case,'on')){
            $currentuser = new user;
            if ($currentuser->getuserinfo($_POST['username'],$_POST['password'])){
                header('location: dashboard.php');
            }else{
                header('location: index.php?user=invalid');
            }
       }

}

if (isset($_GET['user'])){
      switch ($_GET['user']){

      case 'invalid':
            echo '<div class="alert bg-danger">
					<button type="button" data-dismiss="alert" aria-hidden="true" class="close"><span class="glyphicon glyphicon-remove"></button>
					<span class="glyphicon glyphicon-exclamation-sign"></span> Invalid Username or Password.
				</div>';

				session_start();
	            session_destroy();   
	            session_unset();
                break;

      case 'login':
            echo '<div class="alert bg-warning">
                    <button type="button" data-dismiss="alert" aria-hidden="true" class="close"><span class="glyphicon glyphicon-remove"></button>
                  <span class="glyphicon glyphicon-warning-sign"></span> Please Login To The System.
                </div>';
                session_start();
	            session_destroy();   
	            session_unset();
            break;          
         
      case 'relogin':
            echo '<div class="alert bg-warning">
                    <button type="button" data-dismiss="alert" aria-hidden="true" class="close"><span class="glyphicon glyphicon-remove"></button>
                  <span class="glyphicon glyphicon-warning-sign"></span> Session Timeout, Please re-login to the system.
                </div>';
                session_start();
	            session_destroy();   
	            session_unset();
            break;
      
      case 'logoff':
             echo '<div class="alert bg-success">
                    <button type="button" data-dismiss="alert" aria-hidden="true" class="close"><span class="glyphicon glyphicon-remove"></button>
                  <span class="glyphicon glyphicon-successful-sign"></span> Log-off Successfull.
                </div>';
            session_start();
	        session_destroy();   
	        session_unset();
            break;
    }


}


?>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>CASE MANAGER</title>

<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/datepicker3.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">
<link href="css/login_styles.css" rel="stylesheet">
<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<script>alert('You are using an outdated browser. Please switch to Google Chrome or Mozilla Firefox.');</script>
<![endif]-->

</head>

<body>
	
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default ">
				<div class="panel-heading"><h3>CASE MANAGER</h3></div>
				<div class="panel-body">
					<?php if (isset($_GET['usertype'])): ?>
					
					<form action="index.php?usertype=local" enctype="multipart/form-data" method="post">
						<fieldset>
							<div class="form-group">
								<input class="form-control" placeholder="Username" name="username" type="text" autofocus="">
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="Password" name="password" type="password" value="">
							</div>
							 <input class ="btn btn-danger" type="SUBMIT" value="LOG-IN" title="LOG-IN"/>  <a href="index.php" class="btn btn-default"><span class="glyphicon glyphicon-remoe"></span>GO BACK</a>
						</fieldset>
					</form>
					<?php endif; ?>	
                    
					
					<?php if (!isset($_GET['usertype'])): ?>
					<a href="login.php" class="btn btn-warning"><span class="glyphicon glyphicon-log-in"></span> LOGIN USING ACTIVE DIRECTORY USER</a>
					<br>
					<br>
					<a href="index.php?usertype=local" class="btn btn-danger"><span class="glyphicon glyphicon-log-in"></span> USE LOCAL USER</a>
					<?php endif; ?>					
					<hr>
                    <p><small>Case Manager Version 1.8 | Ministry of Finance and Treasury (<?php echo date("Y"); ?>)</small></p>  
				</div>
			</div>
		</div><!-- /.col-->
	</div><!-- /.row -->	
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script>
		!function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){		  
				$(this).find('em:first').toggleClass("glyphicon-minus");	  
			}); 
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>	
</body>

</html>
