<?php

//MASS TRANSFER
require('_ex/connections.php');    
require('_ex/authentications.php');
require('_ex/master.php');
require('_ex/document.php');
require('_ex/alert.php');

$currentuser = new authentication;

$OLD_UNIT 			= 39;
$NEW_UNIT_ID 		= 11;

$document  = new document;
//GET LIST OF ALL DOCUMENTS IN ORIGINAL UNIT
$document_ids = $document->BudgetExDocs();

	//FOR EACH DOCUMENT UPDATE DOC_UNIT
		foreach ($document_ids as $doc_num){
			if ($document->changedocunit($doc_num['DOC_ID'],$NEW_UNIT_ID)){
				$document->changecallrecordunit($doc_num['DOC_ID'],$NEW_UNIT_ID);
				$document->changeremarkunit($doc_num['DOC_ID'],$NEW_UNIT_ID,$OLD_UNIT);
				$document->notification($doc_num['DOC_ID'],'DOCUMENT TRANSFERED FROM BUDGET EXECUTION ON 18052019- SYSTEM ADMIN',14);
				echo $doc_num['DOC_ID'].' UPDATED <br/>';
			}else{
				echo $doc_num['DOC_ID'].' FAILED TO UPDATE <br/>';
			}
		}
?>