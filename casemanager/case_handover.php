<?php
header('Content-Type: application/json');

require('_ex/connections.php');    
require('_ex/authentications.php');
require('_ex/master.php');
require('_ex/document.php');
require('_ex/alert.php');

$currentuser = new authentication;

$commitresult = 'GOOD';
$commitstatus = 'SUCCESS';



$case_change = 'casehandover';

require ('_pages/subcomponents/case_change_auth.php');


if ($assignedoptions != TRUE ){
$commitresult = 'NO RIGHTS';
$commitstatus = 'FAIL';
}else{

	if (isset($_GET['ex'])){
	$ex = preg_replace( "/[^0-9$]/","",$_GET['ex']);
		if ($ex == ''){
		$commitresult = 'Missing EX';
		$commitstatus = 'FAIL';
		}
	}else{
		$commitresult = 'Missing EX';
		$commitstatus = 'FAIL';
	}
}
if($commitresult == "GOOD"){
	switch($ex){
			case '100':
			$values= array($docid,trim($_POST['member']));
			$fieldlist = array ("document","member");
			$case = array('n','n');
			
			if ($document->invalidchar2($values,$fieldlist,$case)){
				$assigned_user_name = $currentuser->getusername($values[1]);
				
				if ($document->assigndocument($values) == TRUE){
					
					$document->notification($docid,'Case Handed Over To '.$assigned_user_name['USERNAME'].' By '.$_SESSION['username'],12);
					$newalert = new alert;
					$newalert->create_alert($docid,'Case Handed', null, $values[1]);
					
					$statusvalues = array($docid,2);
					if ($document->docstatus($statusvalues) == TRUE){
						$commitresult = "CASE TRANSFERED TO ".strtoupper($assigned_user_name['USERNAME']);
					}else{
						$commitstatus = 'FAIL';
						$commitresult = "SOMETHING WENT WRONG!";
					}
			
				}else{
					$commitstatus = 'FAIL';
					$commitresult = "USER ASSIGNMENT FAILED";
				}

			}

			break;
			
	}

}

$handover_resultset = array(
'commitstatus'=>$commitstatus,
'commitresult'=> $commitresult
);

echo  json_encode($handover_resultset, JSON_HEX_QUOT | JSON_HEX_TAG);
?>