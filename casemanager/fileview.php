<?php
require('_ex/connections.php');  
require('_ex/authentications.php');
require('_ex/master.php');
require('_ex/document.php');
require('_ex/remark.php');


$currentuser = new authentication;


$download = $_GET['view'];
$filedetails = getfilename($download);


$file = "files/attachments/".$filedetails['fname'];


//check if user is logged in
if($_SESSION['userid']){

    //if requested file exists
    if(file_exists($file)){

        //check if the user is authorized to access the file
        if(isAuthorized($filedetails['fid'],$filedetails['fdocid'])){

            //log Authorized access
            $has_authority = 'Y';
            logaccess($filedetails['fname'], $has_authority);
	
            //allow access to file
            showdownload($file,$filedetails['fdesc']);

        }else{

            //log Unauthorized access
            $has_authority = 'N';
            logaccess($filedetails['fname'], $has_authority);
			exit;
            //do more...?
        }

    }

}


function getfilename($file){
	$dbc = new dbconnection;
	$sql3="select DR_ID, DR_DOC_ID, DR_FILE, DR_DESC from doc_remarks WHERE DR_ID =".$file." LIMIT 1" ;
    $result3 = $dbc->dbconn->query($sql3);
    $row = mysqli_fetch_assoc($result3);
	$fid = $row['DR_ID'];
	$fdocid = $row['DR_DOC_ID'];
    $fname = $row['DR_FILE'];
	$fdesc = $row['DR_DESC'];
    $result3->free();
	$fileinfo = Array(
		'fid'=> $fid,
		'fdocid'=> $fdocid,
		'fname'=> $fname,
		'fdesc'=> $fdesc
	);
    return $fileinfo;
}

function isAuthorized($file,$docid){
	require('_pages/subcomponents/case_auth_checkstatements.php');
    return $caseflag;
}

function logaccess($file, $has_authority){

    $dbc = new dbconnection;

    $ip_address = getip();
		
    $hostaddress = gethostbyaddr($_SERVER['REMOTE_ADDR']);

    $ins_stmt = $dbc->dbconn->prepare("INSERT INTO fileaccess_log (`fl_user_id`, `fl_username`, `fl_filename`, `fl_authorized`, `fl_hostaddress`, `fl_hostip`) VALUES ( ?, ?, ?, ?, ?, ?)");
    $ins_stmt->bind_param('dsssss', $fl_user_id, $fl_username, $fl_filename, $fl_authorized, $fl_hostaddress, $fl_hostip);

    $fl_user_id = $_SESSION['userid'];
    $fl_username = $_SESSION['username'];
    $fl_filename = $file;
    $fl_authorized = $has_authority;
    $fl_hostaddress = $hostaddress;
    $fl_hostip = $ip_address;

    $ins_stmt->execute();
    $ins_stmt->close();

    $dbc->dbconn->close();

}




function getip(){

    $ip_address = '';

    if(!empty($_SERVER['HTTP_CLIENT_IP']))
    {
        $ip_address = $_SERVER['HTTP_CLIENT_IP'];
    }
    elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
    {
        $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
        $ip_address = $_SERVER['REMOTE_ADDR'];
    }

    return $ip_address;

}




function showdownload($file,$drfileID){



	$dlfile = explode ('.',$file);
	
	$ext = explode('.', $drfileID);
    $ext = strtolower(end($ext));
	
	$allowed = array('pdf','png','jpg');
	
	if(!in_array($ext,$allowed)){
		$withoutExt = $drfileID;
	}else{
		$withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $drfileID);
	}
	

    //header('Content-Description: File Transfer');
    //header('Content-Type: application/octet-stream');
	if($ext == 'pdf'){
		header("Content-type: application/pdf");
	}else{
		header("Content-type: image");
	}
    
	//header('Content-Disposition: attachment; filename='.basename($file));
	
		//header('Content-Disposition: attachment; filename='.$withoutExt.'.'.$dlfile[1]);

	//CHANGE THE INDEX TO 1 AFTER MOVING TO PRODUCTION
	//header('Content-Disposition: attachment; filename='.preg_replace( "/[^a-zA-Z 0-9()$]/","",$withoutExt).'.'.$dlfile[1]);
	header('Content-Disposition: inline; filename='.preg_replace( "/[^a-zA-Z 0-9()$]/","",$withoutExt).'.'.$dlfile[1]);
	header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    readfile($file);
    exit;

}

?>
?>
?>