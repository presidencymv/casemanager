<?php
header('Content-Type: application/json');

require('_ex/connections.php');    
require('_ex/authentications.php');
require('_ex/master.php');
require('_ex/document.php');
require('_ex/alert.php');

$currentuser = new authentication;

$commitresult = 'GOOD';
$commitstatus = 'SUCCESS';

if ($_SESSION['handler'] != 'Y' ){
$commitresult = 'NO RIGHTS';
$commitstatus = 'FAIL';
}


$case_change = 'quickassign';

require ('_pages/subcomponents/case_change_auth.php');


if ($handleroptions != TRUE ){
$commitresult = 'NO RIGHTS';
$commitstatus = 'FAIL';
}else{

	if (isset($_GET['ex'])){
	$ex = preg_replace( "/[^0-9$]/","",$_GET['ex']);
		if ($ex == ''){
		$commitresult = 'Missing EX';
		$commitstatus = 'FAIL';
		}
	}else{
		$commitresult = 'Missing EX';
		$commitstatus = 'FAIL';
	}
}
if($commitresult == "GOOD"){
	switch($ex){
			case '100':
			$values= array($docid,trim($_POST['member']));
			$fieldlist = array ("document","member");
			$case = array('n','n');
			
			if ($document->invalidchar2($values,$fieldlist,$case)){
				$assigned_user_name = $currentuser->getusername($values[1]);
				
				if ($document->assigndocument($values) == TRUE){
					
					$document->notification($docid,'Case Assigned To '.$assigned_user_name['USERNAME'].' By '.$_SESSION['username'],12);
					$newalert = new alert;
					$newalert->create_alert($docid,'Case Assigned', null, $values[1]);
					
					$statusvalues = array($docid,2);
					if ($document->docstatus($statusvalues) == TRUE){
						$commitresult = "CASE ASSIGNED TO ".strtoupper($assigned_user_name['USERNAME']);
					}else{
						$commitstatus = 'FAIL';
						$commitresult = "SOMETHING WENT WRONG!";
					}
			
				}else{
					$commitstatus = 'FAIL';
					$commitresult = "USER ASSIGNMENT FAILED";
				}

			}

			break;
			
	}

}

$handover_resultset = array(
'commitstatus'=>$commitstatus,
'commitresult'=> $commitresult
);

echo  json_encode($handover_resultset, JSON_HEX_QUOT | JSON_HEX_TAG);
?>