<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>CASE MANAGER</title>

<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/datepicker3.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">
<link href="css/login_styles.css" rel="stylesheet">
<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<script>alert('You are using an outdated browser. Please switch to Google Chrome or Mozilla Firefox.');</script>
<![endif]-->

</head>

<body>
	
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default ">
				<div class="panel-heading"><h3>CASE MANAGER</h3></div>
				<div class="panel-body">
					<div><p>You need to configure your proxy settings to access this application. Please follow below steps to configure your proxy settings.</p></div>
					<div>
						<ol>
							<li>
								Open Control Panel (Press the keys [Win]+[R] combination to open Run command, type "control" and the press enter to open Control Panel)
							</li>
							<li>
								Open Internet Options
							</li>
							<li>
								Go to Connections Tab
							</li>
							<li>
								Open LAN settings
							</li>
							<li>
								Click Advanced
							</li>
							<li>
								Copy and Paste ";intranet2.finance.gov.mv" to Exceptions and Click OK
							</li>
							<li>
								Make sure "Bypass proxy server for local addresses" is checked. Click OK.
							</li>
						</ol>
					</div>
					<div>That's it! You are all set and done. Click <a href="http://intranet2.finance.gov.mv/casemanager">here</a> to access the application.</div>
                    <hr>
                    <p><small>Case Manager Version 1.0 | MoFT (2015)</small></p>  
				</div>
			</div>
		</div><!-- /.col-->
	</div><!-- /.row -->	
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script>
		!function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){		  
				$(this).find('em:first').toggleClass("glyphicon-minus");	  
			}); 
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>	
</body>

</html>