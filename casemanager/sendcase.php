<?php
header('Content-Type: application/json');

require('_ex/connections.php');    
require('_ex/authentications.php');
require('_ex/master.php');
require('_ex/call.php');
require('_ex/document.php');
require('_ex/contact.php');
require('_ex/alert.php');

$commitresult = "GOOD";

$currentuser = new authentication;

if ($_SESSION['callcentrestaff'] != 'Y'){
    $commitresult = 'NO RIGHTS';
}else{

	if (isset($_GET['id'])){
	$ex = preg_replace( "/[^0-9$]/","",$_GET['id']);
		if ($ex == ''){
		$commitresult = 'Missing ID';
		}
	}else{
		$commitresult = 'Missing ID';
	}
}

$call = new call;

if($commitresult == "GOOD"){


	//GET CALL INFORMATION
	$details = $call->get_call_info($ex);
	if (($details['CR_ID'] != '' || $details['CR_ID'] != NULL) && $details['CR_DOC_ID'] == NULL ){

			//CREATE CASE DOCUMENT
			$newdocument = new document;
			
			$SOURCE = 5;
			$OFFICE = $details['O_ID'];
			$CATEGORY = $details['DC_ID'];
			$DOC_REFNUMBER = '';
			$DOC_DESCRIPTION = <<<EOL
<h3>CALL CENTRE CASE</h3>
<h4>ISSUE:</h4>
<p>{$details['CR_ISSUE']}</p>
<h4>RESPONSE:</h4>
<p>{$details['CR_RESPONSE']}</p>			
EOL;

			$date = new DateTime(date('Y-m-d'));
			
			$dayOfWeek = date('l');
			$plus2days = array('Tuesday','Wednesday','Thursday','Friday');
			$plus1days = array('Staturday');

			if (in_array($dayOfWeek, $plus2days)){
				$date->modify('+3 day');  
			}elseif(in_array($dayOfWeek, $plus1days)) {
				$date->modify('+2 day');  
			}else{
				$date->modify('+1 day');
			}

			$DEADLINE = $date->format('Y-m-d');
			
			$DOC_PRIORITY = $details['P_ID'];
			
			$docvalues = array( $SOURCE, $OFFICE, $CATEGORY, $DOC_REFNUMBER, $DOC_DESCRIPTION, $DEADLINE, $DOC_PRIORITY);
			//START HERE
			if($newdocument->createdocument($docvalues) == TRUE){
				$newdocid = $newdocument->getlastdoc_id();
				
				if($call->setdocument($details['CR_ID'],$newdocid) == TRUE){
					
					//ADD CONTACT TO THE CASE DOCUMENT
					$contact  = new contact;
					
					
					$CC_DOC_ID=$newdocid;
					$CC_NAME=$details['CR_CALLER'];
					$CC_DESIG='';
					$CC_ADDRESS=$details['O_NAME'];
					$CC_EMAIL=$details['CR_EMAIL'];
					$CC_TEL=$details['CR_TEL'];
					$CC_FAX='';
					
					$contvalues = array(
						$CC_DOC_ID,
						$CC_NAME,
						$CC_DESIG,
						$CC_ADDRESS,
						$CC_EMAIL,
						$CC_TEL,
						$CC_FAX
					);
			
					if($contact->addcontact($contvalues,1) == TRUE){
						$DOC_ID = $newdocid;
						$UNIT_ID = $details['UNIT_ID'];
						
						$sendvalues = array(
							$DOC_ID,
							$UNIT_ID
						);
						
						if($newdocument->routedocument($sendvalues) == TRUE){
							$commitresult = "COMPLETED";
							$newdocument->notification($newdocid,'Call Centre Case Created by '.$_SESSION['username'],4);
							$newalert = new alert;
							$newalert->create_alert($DOC_ID,'Case Routed',$UNIT_ID);
						}
					}

				}
				
			
			
			}
			

			//END HERE
	}else{		
		$commitresult = "BAD";
	}


}

$resultset = array(
'commitresult'=> $commitresult
);

echo  json_encode($resultset, JSON_HEX_QUOT | JSON_HEX_TAG);
?>