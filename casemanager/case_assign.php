<?php
require('_ex/connections.php');    
require('_ex/authentications.php');
require('_ex/master.php');
require('_ex/document.php');
require('_ex/alert.php');
$currentuser = new authentication;

if ($_SESSION['handler'] != 'Y' ){
	header('location: dashboard.php');
	die();
}


$case_change = 'caseassign';

require ('_pages/subcomponents/case_change_auth.php');


if (isset($_GET['ex'])){
$ex = preg_replace( "/[^0-9$]/","",$_GET['ex']);
	if ($ex == ''){
	header('location: dashboard.php');
	die();
	}
}else{
	header('location: dashboard.php');
	die();
}

switch($ex){
        case '100':
			$values= array($docid,trim($_POST['member']));
			$fieldlist = array ("document","member");
			$case = array('n','n');
			
			if ($document->invalidchar($values,$fieldlist,$case,'on')){

				//Get the Assigned Staff Name
				$assigned_user_name = $currentuser->getusername($values[1]);
				
				if ($document->assigndocument($values) == TRUE){
					$document->notification($docid,'Case Assigned To '.$assigned_user_name['USERNAME'].' By '.$_SESSION['username'],12);
					$newalert = new alert;
					$newalert->create_alert($docid,'Case Assigned', null, $values[1]);
					$statusvalues = array($docid,2);
					if ($document->docstatus($statusvalues) == TRUE){
						header('location: dashboard.php?page=activecases');
						die();
					}else{
						header('location: dashboard.php?page=caseedit&msg=2&id='.$docid);
						die(); 
					}	
				}else{
					header('location: dashboard.php?page=caseedit&msg=3&id='.$docid);
					die();
				}
			}
        break;	
    }
header('location: dashboard.php?page=caseedit&msg=2&id='.$docid);
die();	
?>