<?php
require('_ex/connections.php');    
require('_ex/authentications.php');
require('_ex/master.php');
require('_ex/sources.php');
$currentuser = new authentication;
if ($_SESSION['admins'] != 'Y'){
	header('location: dashboard.php');
	die();
}

if (isset($_GET['ex'])){
$ex = preg_replace( "/[^0-9$]/","",$_GET['ex']);
	if ($ex == ''){
	header('location: dashboard.php');
	die();
	}
}else{
	header('location: dashboard.php');
	die();
}

switch($ex){
        case '100':
			$values= array(trim($_POST['source']));
			$fieldlist = array ('Source');
			$case = array('else');
			$source = new source;
			if ($source->invalidchar($values,$fieldlist,$case,'on')){
				if ($source->createsource($values) == TRUE){
					header('location: dashboard.php?page=managesource');
					die();
				}else{
					header('location: dashboard.php?page=managesource&msg=3');
					die();
				}
			}
        break;
		case '101':
			if (isset($_GET['id'])){
			$id = preg_replace( "/[^0-9$]/","",$_GET['id']);
				if ($id == ''){
				header('location: dashboard.php');
				die();
				}
			}else{
				header('location: dashboard.php');
				die();
			}

			$values= array($id,trim($_POST['source']));
			$fieldlist = array ('ID','Source');
			$case = array('n','else');
			$source = new source;
			if ($source->invalidchar($values,$fieldlist,$case,'on')){
				if ($source->updatesource($values) == TRUE){
					header('location: dashboard.php?page=managesource');
					die();
				}else{
					header('location: dashboard.php?page=managesource&msg=3');
					die();
				}
			}
        break;	
    }
header('location: dashboard.php?page=managesource&msg=2');
die();	
?>