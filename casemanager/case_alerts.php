<?php
require('_ex/connections.php');    
require('_ex/authentications.php');
require('_ex/master.php');
require('_ex/document.php');
require('_ex/alert.php');

$currentuser = new authentication;


if(isset($_GET['ex'],$_GET['nid'],$_GET['sid'],$_GET['did'],$_GET['tid'])){

	$ex = preg_replace( "/[^0-9$]/","",$_GET['ex']);
	//alert id
	$nid = preg_replace( "/[^0-9$]/","",$_GET['nid']);
	//user id
	$sid = preg_replace( "/[^0-9$]/","",$_GET['sid']);
	//doc id
	$did = preg_replace( "/[^0-9$]/","",$_GET['did']);
	//type description
	$tid = preg_replace( "/[^0-9$]/","",$_GET['tid']);


	if ($ex == '' || $nid == '' || $sid == '' || $did == '' || $tid == ''){

		header('location: dashboard.php');
		die();

	}else{

		if($sid != $_SESSION['userid']){

			header('location: dashboard.php');
			die();

		}

	}

}else{

	header('location: dashboard.php');
	die();

}


switch($ex){

	case '100':
		$alert = new alert;
		$alert->markalertasread($nid, $sid, $did);
		$linkaddress = getalertlink($did, $tid);
		header('location: '.$linkaddress);
		die();
		break;

}


function getalertlink($did, $tid){
	$document = new document;

	$flag = FALSE;

	if($document->authorizeddoc($did) == TRUE ){
		$flag = TRUE;
	}elseif($document->authorizeddoc_assigned($did) == TRUE){
		$flag = TRUE;
	}elseif($document->authorizeddoc_shared($did) == TRUE){
		$flag = TRUE;
	}elseif($document->authorizeddoc_counter($did) == TRUE){
		$flag = TRUE;
	}

	if($flag == FALSE){

		header('location: dashboard.php?page=home&msg=4');
		die();

	}else{

		$type_casedetail = "1,2,3,6,10,11,12,19,20,21,22,23,24,25,26";
		$type_caseshare = "4,5";
		$type_casecomment = "7,8,9";
		$type_caseattachment = "13,14,15,16,17,18";

		if(in_array($tid, explode(',', $type_casedetail))){

			return $linkaddress = 'dashboard.php?page=casedetails&id='.$did;
			
		}elseif(in_array($tid, explode(',', $type_caseshare))){

			return $linkaddress = 'dashboard.php?page=caseshare&id='.$did;

		}elseif(in_array($tid, explode(',', $type_casecomment))){

			return $linkaddress = 'dashboard.php?page=casecomments&id='.$did;

		}elseif(in_array($tid, explode(',', $type_caseattachment))){

			return $linkaddress = 'dashboard.php?page=caseattachments&id='.$did;

		}else{

			return $linkaddress = 'dashboard.php?page=home&msg=2';

		}

	}
}
?>