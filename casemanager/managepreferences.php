<?php
require('_ex/connections.php');    
require('_ex/authentications.php');
require('_ex/master.php');
require('_ex/units.php');
$currentuser = new authentication;

if(!$_POST){
    header('location: dashboard.php');
    die();
}
if (isset($_GET['ex'])){
$ex = preg_replace( "/[^0-9$]/","",$_GET['ex']);
	if ($ex == ''){
	header('location: dashboard.php');
	die();
	}else{
		//user can only change his own preferences
		if ($_SESSION['userid'] != $_GET['sid']){
			header('location: dashboard.php');
			die();
		}
	}
}else{
	header('location: dashboard.php');
	die();
}

switch($ex){
        case '100':

        	if(trim($_POST['caseRouted'])){
        		$caseRouted = 1;
        	}else{
        		$caseRouted = 0;
        	}

        	if(trim($_POST['caseAssigned'])){
        		$caseAssigned = 1;
        	}else{
        		$caseAssigned = 0;
        	}

        	if(trim($_POST['caseSharedWithMe'])){
        		$caseSharedWithMe = 1;
        	}else{
        		$caseSharedWithMe = 0;
        	}

        	if(trim($_POST['caseSharedSection'])){
        		$caseSharedSection = 1;
        	}else{
        		$caseSharedSection = 0;
        	}

        	if(trim($_POST['caseSharedAssigned'])){
        		$caseSharedAssigned = 1;
        	}else{
        		$caseSharedAssigned = 0;
        	}

        	if(trim($_POST['caseSharedShared'])){
        		$caseSharedShared = 1;
        	}else{
        		$caseSharedShared = 0;
        	}

        	if(trim($_POST['caseCommentSection'])){
        		$caseCommentSection = 1;
        	}else{
        		$caseCommentSection = 0;
        	}

        	if(trim($_POST['caseCommentAssigned'])){
        		$caseCommentAssigned = 1;
        	}else{
        		$caseCommentAssigned = 0;
        	}

        	if(trim($_POST['caseCommentShared'])){
        		$caseCommentShared = 1;
        	}else{
        		$caseCommentShared = 0;
        	}

        	if(trim($_POST['caseUpdateSection'])){
        		$caseUpdateSection = 1;
        	}else{
        		$caseUpdateSection = 0;
        	}

        	if(trim($_POST['caseUpdateAssigned'])){
        		$caseUpdateAssigned = 1;
        	}else{
        		$caseUpdateAssigned = 0;
        	}

        	if(trim($_POST['caseUpdateShared'])){
        		$caseUpdateShared = 1;
        	}else{
        		$caseUpdateShared = 0;
        	}

        	if(trim($_POST['caseNewFileSection'])){
        		$caseNewFileSection = 1;
        	}else{
        		$caseNewFileSection = 0;
        	}

        	if(trim($_POST['caseNewFileAssigned'])){
        		$caseNewFileAssigned = 1;
        	}else{
        		$caseNewFileAssigned = 0;
        	}

        	if(trim($_POST['caseNewFileShared'])){
        		$caseNewFileShared = 1;
        	}else{
        		$caseNewFileShared = 0;
        	}

        	if(trim($_POST['caseFileUpdatedSection'])){
        		$caseFileUpdatedSection = 1;
        	}else{
        		$caseFileUpdatedSection = 0;
        	}

        	if(trim($_POST['caseFileUpdatedAssigned'])){
        		$caseFileUpdatedAssigned = 1;
        	}else{
        		$caseFileUpdatedAssigned = 0;
        	}

        	if(trim($_POST['caseFileUpdatedShared'])){
        		$caseFileUpdatedShared = 1;
        	}else{
        		$caseFileUpdatedShared = 0;
        	}

        	if(trim($_POST['caseEditSection'])){
        		$caseEditSection = 1;
        	}else{
        		$caseEditSection = 0;
        	}

        	if(trim($_POST['caseEditAssigned'])){
        		$caseEditAssigned = 1;
        	}else{
        		$caseEditAssigned = 0;
        	}

        	if(trim($_POST['caseEditShared'])){
        		$caseEditShared = 1;
        	}else{
        		$caseEditShared = 0;
        	}

        	if(trim($_POST['caseClosedSection'])){
        		$caseClosedSection = 1;
        	}else{
        		$caseClosedSection = 0;
        	}

        	if(trim($_POST['caseClosedShared'])){
        		$caseClosedShared = 1;
        	}else{
        		$caseClosedShared = 0;
        	}

            if(trim($_POST['caseReopenedSection'])){
                $caseReopenedSection = 1;
            }else{
                $caseReopenedSection = 0;
            }

            if(trim($_POST['caseReopenedShared'])){
                $caseReopenedShared = 1;
            }else{
                $caseReopenedShared = 0;
            }

        	if(trim($_POST['caseRejected'])){
        		$caseRejected = 1;
        	}else{
        		$caseRejected = 0;
        	}

        	if ($_SESSION['handler'] == 'Y'){
        		$arr = array('caseRouted' => $caseRouted,
	        		'caseAssigned' => $caseAssigned,
	        		'caseSharedWithMe' => $caseSharedWithMe,
	        		'caseSharedSection' => $caseSharedSection,
	        		'caseSharedAssigned' => $caseSharedAssigned,
	        		'caseSharedShared' => $caseSharedShared,
	        		'caseCommentSection' => $caseCommentSection,
	        		'caseCommentAssigned' => $caseCommentAssigned,
	        		'caseCommentShared' => $caseCommentShared,
	        		'caseUpdateSection' => $caseUpdateSection,
	        		'caseUpdateAssigned' => $caseUpdateAssigned,
	        		'caseUpdateShared' => $caseUpdateShared,
	        		'caseNewFileSection' => $caseNewFileSection,
	        		'caseNewFileAssigned' => $caseNewFileAssigned,
	        		'caseNewFileShared' => $caseNewFileShared,
	        		'caseFileUpdatedSection' => $caseFileUpdatedSection,
	        		'caseFileUpdatedAssigned' => $caseFileUpdatedAssigned,
	        		'caseFileUpdatedShared' => $caseFileUpdatedShared,
	        		'caseEditSection' => $caseEditSection,
	        		'caseEditAssigned' => $caseEditAssigned,
	        		'caseEditShared' => $caseEditShared,
	        		'caseClosedSection' => $caseClosedSection,
	        		'caseClosedShared' => $caseClosedShared,
                    'caseReopenedSection' => $caseReopenedSection,
                    'caseReopenedShared' => $caseReopenedShared,
	        		'caseRejected' => $caseRejected);
        	}else{
        		//we do not want to turn off section head notifications while the user is not section head
        		//so that when a user is promoted as section head, by default we want section head related notifications to be on
        		$arr = array('caseRouted' => 1,
	        		'caseAssigned' => $caseAssigned,
	        		'caseSharedWithMe' => $caseSharedWithMe,
	        		'caseSharedSection' => 1,
	        		'caseSharedAssigned' => $caseSharedAssigned,
	        		'caseSharedShared' => $caseSharedShared,
	        		'caseCommentSection' => 1,
	        		'caseCommentAssigned' => $caseCommentAssigned,
	        		'caseCommentShared' => $caseCommentShared,
	        		'caseUpdateSection' => 1,
	        		'caseUpdateAssigned' => $caseUpdateAssigned,
	        		'caseUpdateShared' => $caseUpdateShared,
	        		'caseNewFileSection' => 1,
	        		'caseNewFileAssigned' => $caseNewFileAssigned,
	        		'caseNewFileShared' => $caseNewFileShared,
	        		'caseFileUpdatedSection' => 1,
	        		'caseFileUpdatedAssigned' => $caseFileUpdatedAssigned,
	        		'caseFileUpdatedShared' => $caseFileUpdatedShared,
	        		'caseEditSection' => 1,
	        		'caseEditAssigned' => $caseEditAssigned,
	        		'caseEditShared' => $caseEditShared,
	        		'caseClosedSection' => 1,
	        		'caseClosedShared' => $caseClosedShared,
                    'caseReopenedSection' => 1,
                    'caseReopenedShared' => $caseReopenedShared,
	        		'caseRejected' => $caseRejected);
        	}

			$values = json_encode($arr);

        	if ($currentuser->updateuserpreferences($values, $_SESSION['userid']) == TRUE){
        		header('location: dashboard.php?page=preferences&msg=1');
        		die();
        	}else{
        		header('location: dashboard.php?page=preferences&msg=3');
        		die();
        	}
        break;
		
    }
header('location: dashboard.php?page=preferences&msg=2');
die();	
?>