<?php
require('_ex/connections.php');    
require('_ex/authentications.php');
require('_ex/master.php');
require('_ex/units.php');
$currentuser = new authentication;
if ( $_SESSION['admins'] != 'Y'){
	header('location: dashboard.php');
	die();
}

if (isset($_GET['ex'])){
$ex = preg_replace( "/[^0-9$]/","",$_GET['ex']);
	if ($ex == ''){
	header('location: dashboard.php');
	die();
	}
}else{
	header('location: dashboard.php');
	die();
}

switch($ex){
        case '100':
			$values= array(trim($_POST['uname']),trim($_POST['counter']),trim($_POST['admin']),trim($_POST['reports']),trim($_POST['sendall']),trim($_POST['uphone']));
			$fieldlist = array ('User Name','Counter','Administration','Reports','SendAll','Phone');
			$case = array('else','t','t','t','t','n_nr');
			$userinput = new master;
			if ($userinput->invalidchar($values,$fieldlist,$case,'on')){
				if ($currentuser->createuser($values) == TRUE){
					header('location: dashboard.php?page=manageuser&msg=1');
					die();
				}else{
					header('location: dashboard.php?page=manageuser&msg=3');
					die();
				}
			}
        break;
		
		case '101':
			if (isset($_GET['id'])){
			$id = preg_replace( "/[^0-9$]/","",$_GET['id']);
				if ($id == ''){
				header('location: dashboard.php');
				die();
				}
			}else{
				header('location: dashboard.php');
				die();
			}

			$values= array($id,trim($_POST['uname']),trim($_POST['counter']),trim($_POST['admin']),trim($_POST['reports']),trim($_POST['sendall']),trim($_POST['uphone']));
			$fieldlist = array ('ID','User Name','Counter','Administration','Reports','SendAll','Phone');
			$case = array('n','else','t','t','t','t','n_nr');
			$userinput = new master;
			if ($userinput->invalidchar($values,$fieldlist,$case,'on')){
				if ($currentuser->updateuser($values) == TRUE){
					header('location: dashboard.php?page=manageuser&msg=1');
					die();
				}else{
					header('location: dashboard.php?page=manageuser&msg=3');
					die();
				}
			}
        break;
		
		case '102':
			if (isset($_GET['id'])&&isset($_GET['status'])){
			$ID = preg_replace( "/[^0-9$]/","",$_GET['id']);
			$STATUS = preg_replace( "/[^0-9$]/","",$_GET['status']);
				if ($ID == ''||$STATUS ==''){
				header('location: dashboard.php');
				die();
				}
			}else{
				header('location: dashboard.php');
				die();
			}

			if ($currentuser->activate_deactivate($ID,$STATUS) == TRUE){
				header('location: dashboard.php?page=manageuser&msg=1');
				die();
			}else{
				header('location: dashboard.php?page=manageuser&msg=3');
				die();
			}
			
        break;

    }
header('location: dashboard.php?page=manageuser&msg=2');
die();	
?>