<?php
require('_ex/connections.php');    
require('_ex/authentications.php');
require('_ex/master.php');
require('_ex/units.php');
require('_ex/document.php');
require('_ex/follower.php');


$currentuser = new authentication;

$case_change = 'casefollow';

require ('_pages/subcomponents/case_change_auth.php');


//only process post requests
if($_POST){
	//only section heads can follow a case
	if($_SESSION['handler'] == 'Y'){
		if (isset($_GET['ex'])){
			$ex = preg_replace( "/[^0-9$]/","",$_GET['ex']);
			if ($ex == ''){
				header('location: dashboard.php');
				die();
			}
		}else{
			header('location: dashboard.php');
			die();
		}

		switch($ex){
			case '100':
				$values= array(trim($_POST['docid']));
				$fieldlist = array ("DocumentID");
				$case = array('n','n');
				$newfollower = new follower;
				if($newfollower->invalidchar($values,$fieldlist,$case,'on')){
					if($newfollower->createfollower($values) == TRUE){
						$document->notification($docid,'Case Followed By : '.$_SESSION['username'],15);
						header('location: dashboard.php?page=casedetails&msg=1&id='.$docid);
						die();
					}else{
						header('location: dashboard.php?page=casedetails&msg=3&id='.$docid);
						die();
					}
				}
			break;

			case '101':
				$values= array(trim($_POST['docid']));
				$fieldlist = array ("DocumentID");
				$case = array('n','n');
				$newfollower = new follower;
				if($newfollower->invalidchar($values,$fieldlist,$case,'on')){
					if($newfollower->stopfollowing($values) == TRUE){
						$document->notification($docid,'Case Unfollowed By : '.$_SESSION['username'],15);
						header('location: dashboard.php?page=casedetails&msg=1&id='.$docid);
						die();
					}else{
						header('location: dashboard.php?page=casedetails&msg=3&id='.$docid);
						die();
					}
				}
			break;
		}
	}
}
?>