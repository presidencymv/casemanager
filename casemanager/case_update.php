<?php
require('_ex/connections.php');    
require('_ex/authentications.php');
require('_ex/master.php');
require('_ex/document.php');
require('_ex/alert.php');
$currentuser = new authentication;

$case_change = 'caseupdate';

require ('_pages/subcomponents/case_change_auth.php');

if ($documentinfo['DST_ID'] == 4 || $editoptions == FALSE){
	header('location: dashboard.php');
	die();
}

if (isset($_GET['ex'])){
$ex = preg_replace( "/[^0-9$]/","",$_GET['ex']);
	if ($ex == ''){
	header('location: dashboard.php');
	die();
	}
}else{
	header('location: dashboard.php');
	die();
}

switch($ex){
        case '100':
			if($_POST['or1'] == 0){
				$values= array($docid,trim($_POST['source']),trim($_POST['sender']),trim($_POST['type']),$document->pre_cleanup(trim($_POST['description'])),trim($_POST['priority']),trim($_POST['byname']),trim($_POST['designation']),trim($_POST['reference']),trim($_POST['docname']));
				$fieldlist = array ("docid","source", "sender", "type", "description", "priority", "txt1", "txt2", "reference","docname");
				$case = array('n','n','n','n','else','n','else_nr','else_nr','else','else_nr');
			}else{
				$values= array($docid,trim($_POST['source']),trim($_POST['sender']),trim($_POST['type']),$document->pre_cleanup(trim($_POST['description'])),trim($_POST['priority']),trim($_POST['namecompany']),trim($_POST['nic']),trim($_POST['reference']),trim($_POST['docname']));
				$fieldlist = array ("docid","source", "sender", "type","description", "priority", "txt1", "txt2", "reference", "docname");
				$case = array('n','n','else_nr','n','else','n','else','else_nr','else','else_nr');
			}
			
			if ($document->invalidchar($values,$fieldlist,$case,'on')){
		
				if ($document->updatedocument($values,$_POST['or1']) == TRUE){
				
				
					$document->notification($docid,'Case Updated by '.$_SESSION['username'],12);
					$newalert = new alert;
					$newalert->create_alert($docid,'Case Details Edited');
					header('location: dashboard.php?page=caseedit&&msg=1&id='.$docid);
					die();
				}else{
					
					header('location: dashboard.php?page=caseedit&msg=3&id='.$docid);
					die();
				}
			}
        break;	
    }
header('location: dashboard.php?page=caseedit&msg=2&id='.$docid);
die();	
?>