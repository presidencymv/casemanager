<?php
require('_ex/connections.php');    
require('_ex/authentications.php');
require('_ex/master.php');
require('_ex/document.php');
require('_ex/units.php');
require('_ex/alert.php');
$currentuser = new authentication;

//REDIRECT INVALID USERS
if ($_SESSION['counter'] != 'Y'){
    header('location: dashboard.php');
    die();
}

if (isset($_GET['ex'])){
$ex = preg_replace( "/[^0-9$]/","",$_GET['ex']);
	if ($ex == ''){
	header('location: dashboard.php');
	die();
	}
}else{
	header('location: dashboard.php');
	die();
}

switch($ex){
        case '101':
		
			$newdocument = new document;
			
			
			if($_POST['or1'] == 0){
				$values= array(trim($_POST['source']),trim($_POST['sender']),trim($_POST['type']),trim($_POST['reference']),$newdocument->pre_cleanup(trim($_POST['description'])),trim($_POST['deadline']),trim($_POST['priority']),trim($_POST['byname']),trim($_POST['designation']));
				$fieldlist = array ("source", "sender", "type", "reference", "description", "deadline", "priority", "txt1", "txt2");
				$case = array('n','n','n','else_nr','else','else','n','else_nr','else_nr');
			}else{
				$values= array(trim($_POST['source']),trim($_POST['sender']),trim($_POST['type']),trim($_POST['reference']),$newdocument->pre_cleanup(trim($_POST['description'])),trim($_POST['deadline']),trim($_POST['priority']),trim($_POST['namecompany']),trim($_POST['nic']));
				$fieldlist = array ("source", "sender", "type", "reference", "description", "deadline", "priority", "txt1", "txt2");
				$case = array('n','else_nr','n','else_nr','else','else','n','else','else_nr');
			}
			
			if ($newdocument->invalidchar($values,$fieldlist,$case,'on')){
				if ($newdocument->createdocument($values,$_POST['or1']) == TRUE){
					$newdocid = $newdocument->getlastdoc_id();
					$newdocument->notification($newdocid,'Case Created by '.$_SESSION['username'],4);
					header('location: dashboard.php?page=casedetails&id='.$newdocid);
					die();
				}else{
					header('location: dashboard.php?page=newcase&msg=3');
					die();
				}
			}
		break;	
		case '102':
		
		$newdocument = new document;
		
		$values= array(10,844,trim($_POST['type']),'',$newdocument->pre_cleanup(trim($_POST['description'])),trim($_POST['deadline']),trim($_POST['priority']),$currentuser->user_name,'Executive Secretariat');
		$fieldlist = array ("source", "sender", "type", "reference", "description", "deadline", "priority", "txt1", "txt2");
		$case = array('n','n','n','else_nr','else','else','n','else_nr','else_nr');

		if ($newdocument->invalidchar($values,$fieldlist,$case,'on')){
			if ($newdocument->createdocument($values,0,'MD') == TRUE){
				$newdocid = $newdocument->getlastdoc_id();
				$newdocument->notification($newdocid,'Directive Case Created by '.$_SESSION['username'],4);
				$values= array($newdocid,40);
				$fieldlist = array ("document","section");
				$case = array('n','n');
				if ($newdocument->routedocument($values) == TRUE){
					$unit = new unit;
					$unitname = $unit->getunitname(40);
					$newdocument->notification($newdocid,'Directive Case Sent To Section '.$unitname.' By '.$_SESSION['username'],12);

					//MINISTERS DIRECTIVE SPECIFIC FIELDS
					$cd_values = array($newdocid,$newdocument->pre_cleanup(trim($_POST['cdivs'])),$newdocument->pre_cleanup(trim($_POST['casename'])),$newdocument->pre_cleanup(trim($_POST['divs'])));
					$newdocument->md_specificfields($cd_values);

					//ASSIGN DOC
					$avalues= array($newdocid,trim($_POST['member']));
					$afieldlist = array ("document","member");
					$acase = array('n','n');
					if ($newdocument->invalidchar($avalues,$afieldlist,$acase,'on')){
						//Get the Assigned Staff Name
						$assigned_user_name = $currentuser->getusername($avalues[1]);
						$newdocument->assigndocument($avalues);
						$newdocument->notification($newdocid,'Case Assigned To '.$assigned_user_name['USERNAME'].' By '.$_SESSION['username'],12);
						$newalert = new alert;
						$newalert->create_alert($newdocid,'Case Assigned', null, $avalues[1]);
						$statusvalues = array($newdocid,2);
						$newdocument->docstatus($statusvalues);
					}
					
					//SHARE WITH CORD STAFFS
					foreach ($_POST['cstaffs'] as $cstaff){
						$csvalues= array($newdocid,trim($cstaff));
						$csfieldlist = array ("document","user");
						$cscase = array('n','n');
			
						//Get the shared Staff Name
						$shared_user_name = $currentuser->getusername($csvalues[1]);
						
						if ($newdocument->invalidchar($csvalues,$csfieldlist,$cscase,'on')){
							if ($newdocument->docshare($csvalues) == TRUE){
								$newdocument->notification($newdocid,'Case Shared With '.$shared_user_name['USERNAME'].' By '.$_SESSION['username'],12);
								$newalert = new alert;
								$newalert->create_alert($newdocid,'Case Shared', null, $csvalues[1]);
							}
						}
					}					

					
					//SEND SMS
					if(isset($_POST['sms'])){
						//GET CASENUMBER
						$documentinfo = $newdocument->getdocumentinfo($newdocid);
						//GET CONTACT NUMBER
						$contactnumber = $currentuser->getcontactnumber($avalues[1]);
						
						if ($contactnumber['PHONE'] != ''){
							$MESSAGE = "You've been assigned a task by the Minister. Please check Case Manager REF: ".$documentinfo['DOC_REFNUMBER']." for further details.";
							$newdocument->sendsmsnotification($contactnumber['PHONE'],'MoFT Case Manager',$MESSAGE);
						}
					}		
				}
				header('location: dashboard.php?page=ministersdirective&msg=1');
				die();
			}else{
				header('location: dashboard.php?page=ministersdirective&msg=3');
				die();
			}
		}
	break;
    }
header('location: dashboard.php?page=ministersdirective&msg=2');
die();	
?>