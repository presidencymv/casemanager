<?php
header('Content-Type: application/json');

require('_ex/connections.php');    
require('_ex/authentications.php');
require('_ex/master.php');
require('_ex/document.php');
require('_ex/chancellery.php');
require('_ex/remark.php');
require('_ex/units.php');
require('_ex/alert.php');
$closeform = 0;
$commitresult = "GOOD";


$currentuser = new authentication;


if ($_SESSION['chancellerystaff'] != 'Y'){
    $commitresult = 'NO RIGHTS';
}else{

	if (isset($_POST['st'])){
	$ex = preg_replace( "/[^0-9$]/","",$_POST['st']);
		if ($ex == ''){
		$commitresult = 'Missing EX';
		}
	}else{
		$commitresult = 'Missing EX';
	}
}




if($commitresult == "GOOD"){
	switch($ex){
			case '100':
			$remark = new remark;
			$values= array(
			trim($_POST['docid']),
			trim($_POST['recid']),
			trim($_POST['hardcopy']),
			trim($_POST['pages']),
			trim($_POST['reference']),
			trim($_POST['details']),
			trim($_POST['authorname']),
			trim($_POST['authortel1']),
			trim($_POST['authortel2']),
			trim($_POST['authoremail']),
			trim($_POST['docchar']),
			trim($_POST['createddate'])
			);
			$fieldlist = array ("docid","recid", "hardcopy", "pages", "reference", "details", "authorname", "authortel1", "authortel2", "authoremail", "docchar", "createddate");
			$case = array('n','n','n','n','else_nr','else_nr','else_nr','else_nr','else_nr','else_nr','n_nr','else_nr');
			if ($remark->invalidchar($values,$fieldlist,$case,'off')){	
				$remark = new remark;
				if($remark->maintain_attributes($values) == TRUE){
					$commitresult = 'Attachment Updated';
				}else{
					$commitresult = 'Invalid Input Found';
				}
			}else{
				$commitresult = 'Invalid Input Found';
			}
			break;
	}
}

$resultset = array(
'commitresult'=> $commitresult,
'closeform'=> $closeform
);

echo  json_encode($resultset, JSON_HEX_QUOT | JSON_HEX_TAG);
?>