<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>CASE MANAGER</title>

<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/datepicker3.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">
<link href="css/login_styles.css" rel="stylesheet">
<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<script>alert('You are using an outdated browser. Please switch to Google Chrome or Mozilla Firefox.');</script>
<![endif]-->

</head>

<body>
	
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default ">
				<div class="panel-heading"><h3>CASE MANAGER</h3></div>
				<div class="panel-body">
					<?php
				
							IF (!$_GET ['process']) {
								$process_msg = 'none';
							} else {
								$process_msg = preg_replace( "/[^a-z$]/","",$_GET['process']);
							}		
							?>
						<p class="text-danger">Log-in Process Cancelled By User!</p>
						<a href="login.php" class="btn btn-warning"><span class="glyphicon glyphicon-log-in"></span> RE-LOGIN USING ACTIVE DIRECTORY USER</a>
						<a href="index.php" class="btn btn-default"><span class="glyphicon glyphicon-remoe"></span>GO BACK</a>
					<hr>
                    <p><small>Case Manager Version 1.2 | MoFT (2015)</small></p>  
				</div>
			</div>
		</div><!-- /.col-->
	</div><!-- /.row -->	
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script>
		!function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){		  
				$(this).find('em:first').toggleClass("glyphicon-minus");	  
			}); 
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>	
</body>

</html>
