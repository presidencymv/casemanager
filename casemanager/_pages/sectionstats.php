<?php
//REDIRECT INVALID USERS
if ($_SESSION['reports'] != 'Y' && $_SESSION['admins'] != 'Y' &&  $_SESSION['handler'] != 'Y' ){
    header('location: dashboard.php');
    die();
}
?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="dashboard.php?page=home"><span class="glyphicon glyphicon-home"></span></a></li>
				<li class="active">Unit Stats</li>
			</ol>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Section/Unit Stats</h1>
			</div>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
				    <div class="panel-body">
					    <form action="_pages/data/section_stats_active_count.php" name = "createcaseform" method="post">
                            <div class="form-group">
                                <div class="form-group col-lg-4">
									<label>Section / Unit</label>
									<select  class="form-control" name="section" id="section"></select>
								</div>
							</div>
                            <hr>
                            <div class="form-group col-lg-12">
                            <input type="submit" title="submit" value="DISPLAY">
                            </div>
                        </form>
					</div>	
				</div>
			</div>
		</div><!--/.row-->
		<?php if(isset($_SESSION['selectedunitname'])): ?>
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header"><?php echo @$_SESSION['selectedunitname']; ?>  -  <?php echo @$_SESSION['selectedunitactivecount']; ?> Active Case<?php echo @$_SESSION['selectedunitactivecount'] > 1 ? 's' : ''; ?>, <?php echo @$_SESSION['unitoverduecount']; ?> Overdue Case<?php echo @$_SESSION['unitoverduecount'] > 1 ? 's' : ''; ?>, <?php echo @$_SESSION['selectedunitclosedcount']; ?> Closed, <?php echo @$_SESSION['selectedunittotalcount']; ?> Total Case<?php echo @$_SESSION['selectedunittotalcount'] > 1 ? 's' : ''; ?>.</h1>
			</div>
		</div><!--/.row-->
		<?php endif; ?>
		<div class="row">
			<div class="col-lg-12" style =" min-width:1200px">
                <div class="panel panel-default">
					<div class="panel-body custom_text">
						<table data-toggle="table" data-url="_pages/data/section_stats_active.php"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="u_id" data-sort-order="desc">
							<thead>
							<tr>
								<th data-field="DOC_SYSID"  data-sortable="true">SYSID:</th>
								<th data-field="O_NAME"  data-sortable="true">SENDER:</th>
								<th data-field="UNIT_NAME"  data-sortable="true">RECEIVER:</th>
								<th data-field="DOC_REFNUMBER"  data-sortable="true">REFERENCE:</th>
								<th data-field="DOC_DESCRIPTION"  data-sortable="true">DESCRIPTION:</th>
								<th data-field="DC_NAME"  data-sortable="true">CATEGORY:</th>
								<th data-field="DEADLINEDATE"  data-sortable="true">DEADLINE:</th>
								<th data-field="DATE_DIFF"  data-sortable="true">OVERDUE:</th>
								<th data-field="ASSIGNEDTO"  data-sortable="true">ASSIGNED:</th>
								<th data-field="DST_NAME"  data-sortable="true">STATUS:</th>
								<th data-field="detailsURL"  data-sortable="true">ACTION:</th>
							</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->
		<hr>
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Staff Stats</h1>
			</div>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
				    <div class="panel-body">
					    <form action="_pages/data/section_stats_user_count.php" name = "display_member" method="post">
							<div class="form-group col-lg-6">
								 <label>Select Staff</label>
								<select  class="form-control" name="user" id="user"></select>
							</div>
							<hr>
                            <div class="form-group col-lg-12">
                            	<input type="submit" title="submit" value="DISPLAY">
                            </div>
						</form>
					</div>	
				</div>
			</div>
		</div><!--/.row-->
		<?php if(isset($_SESSION['selectedusername'])): ?>
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header"><?php echo @$_SESSION['selectedusername']; ?>'s Stats -  <?php echo @$_SESSION['selecteduseractivecount']; ?> Active, <?php echo @$_SESSION['selecteduseroverduecount']; ?> Overdue, <?php echo @$_SESSION['selecteduserclosedcount']; ?> Closed, <?php echo @$_SESSION['selectedusertotalcount']; ?> Total Cases.</h1>
			</div>
		</div><!--/.row-->
		<?php endif; ?>
		<div class="row">
			<div class="col-lg-12" style =" min-width:1200px">
                <div class="panel panel-default">
					<div class="panel-body custom_text">
						<table data-toggle="table" data-url="_pages/data/section_stats_user.php" data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar2" data-pagination="true" data-sort-name="u_id" data-sort-order="desc">
							<thead>
							<tr>
								<th data-field="DOC_SYSID"  data-sortable="true">SYSID:</th>
								<th data-field="O_NAME"  data-sortable="true">SENDER:</th>
								<th data-field="UNIT_NAME"  data-sortable="true">RECEIVER:</th>
								<th data-field="DOC_REFNUMBER"  data-sortable="true">REFERENCE:</th>
								<th data-field="DOC_DESCRIPTION"  data-sortable="true">DESCRIPTION:</th>
								<th data-field="DC_NAME"  data-sortable="true">CATEGORY:</th>
								<th data-field="DEADLINEDATE"  data-sortable="true">DEADLINE:</th>
								<th data-field="DATE_DIFF"  data-sortable="true">OVERDUE:</th>
								<th data-field="ASSIGNEDTO"  data-sortable="true">ASSIGNED:</th>
								<th data-field="DST_NAME"  data-sortable="true">STATUS:</th>
								<th data-field="detailsURL"  data-sortable="true">ACTION:</th>
							</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->
</div>	<!--/.main-->
<script type="text/javascript">
//source Options
        $.ajax({
            url: '_pages/data/unit_options.php',
            dataType: 'json',
            success: function (data) {
                var options = data;
                for (var option in options) {
                        document.getElementById("section").innerHTML += '<option value="' + options[option].UNIT_ID + '">' + options[option].UNIT_NAME + '</option>';
                }
            }
        });

//user Options
		$.ajax({
			url: '_pages/data/user_options.php',
			dataType: 'json',
			success: function (data) {
				var options = data;
				for (var option in options) {
						document.getElementById("user").innerHTML += '<option value="' + options[option].U_ID + '">' + options[option].U_NAME + '</option>';
				}
			}
		});
</script>