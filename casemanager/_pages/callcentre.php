<?php
if ($_SESSION['callcentrestaff'] != 'Y'){
    header('location: dashboard.php');
    die();
}
require('_ex/call.php');

?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="dashboard.php?page=callcentre"><span class="glyphicon glyphicon-home"></span></a></li>
				<li>Call Centre</li>
			</ol>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">CALL CENTRE </li>
			</div>
		</div><!--/.row-->
		<?php
        include('_pages/subcomponents/callcentre_submenu.php');
        ?>
		<div class="row">
			<div class="col-lg-12">
				<?php include('_pages/subcomponents/msgs.php'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<!-- Modal -->
				<div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close" >
						  <span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="myModalLabel">UPDATE</h4>
					  </div>
					  <div class="modal-body">
						<div id="editform">
						
						</div>
					  </div>
					</div>
				  </div>
				</div>
			</div>
		</div><!--/.row-->
		<div class="row">	
			<div class="col-lg-6">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-body">
							<div id="activecall">
								<button id="takecallbutton" type="button" class="btn btn-primary" onclick="takecall(0)">TAKE CALL</button>
							</div>
						</div>	
					</div>
				</div>
				<div class="col-lg-12">
					<div class="panel panel-default">
					<div class="panel-heading">
						<h3>SMS LIST</h3>
					</div>
					  <div class="table-responsive">
						  <table class="table table-striped">
							<thead>
								<tr>
									<th>#</th>
									<th>SENDER</th>
									<th>NAME</th>
									<th>MESSAGE</th>
									<th>RECEIVED ON</th>
								</tr>
							</thead>
							<tbody id="unattendedsms">
							
							</tbody>
						  </table>
					  </div>
					</div>
				</div>
				<div class="col-lg-12">
					<div class="panel panel-default">
					<div class="panel-heading">
						<h3>Calls:</h3>
					</div>
					  <div class="table-responsive">
						  <table class="table table-striped">
							<thead>
								<tr>
									<th>#</th>
									<th>STATUS</th>
									<th>TOKEN</th>
									<th>TIME</th>
									<th>DURATION</th>
									<th>BY</th>
								</tr>
							</thead>
							<tbody id="todayscalls">
							
							</tbody>
							
						  </table>
					  </div>
					</div>
				</div>
			</div>
			
			<div class="col-lg-6">
				<div id="calldetails">
				</div>
                <div class="panel panel-default">
					<div class="panel-heading">
					<h3>Unresolved Issues</h3>
					</div>
					<div class="panel-body custom_text">
						<table id="callstable" data-toggle="table" data-url="_pages/data/unresolvedcalls.php"  data-show-refresh="true" data-show-toggle="false" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="u_id" data-sort-order="desc">
							<thead>
							<tr>
								<th data-field="CR_LOGID"  data-sortable="true">#:</th>
								<th data-field="calldetails"  data-sortable="true">ISSUE DETAILS:</th>
								<th data-field="issuedetails"  data-sortable="true"></th>
								<th data-field="detailsURL"  data-sortable="true">ACTION:</th>
							</tr>
							</thead>
						</table>
						
					</div>
				</div>
			</div>
		</div><!--/.row-->
<script src="js/callcentre.js"></script>

</div>	<!--/.main-->

