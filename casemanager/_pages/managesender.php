<?php 
if ( $_SESSION['admins'] != 'Y'){
	header('location: dashboard.php');
	die();
}
require('_ex/office.php');
?>	

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="dashboard.php?page=home"><span class="glyphicon glyphicon-home"></span></a></li>
				<li class="active">Manage Senders</li>
			</ol>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12">
				<?php include('_pages/subcomponents/msgs.php'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Manage Senders</h1>
			</div>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12">
			   <div class="panel panel-default">
				    <div class="panel-body">
					    <div class="col-lg-12">
							<div class="col-lg-12">
								<?php 	
								 if (isset($_GET['sf'])){
                                    $sf = preg_replace( "/[^a-z$]/","",$_GET['sf']);
                                    if ($sf == ''){
                                        include ('_pages/subforms/add_sender.php');
                                    }else{
                                        include ('_pages/subforms/update_sender.php');
                                    }
                                }else{
                                   include ('_pages/subforms/add_sender.php');
                                }
								?>
			                </div>
						</div>
				    </div>	
				</div>
			</div>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12" style =" min-width:1200px">
                <div class="panel panel-default">
					<div class="panel-body custom_text">
						<table data-toggle="table" data-url="_pages/data/full_senders.php"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="u_id" data-sort-order="desc">
							<thead>
							<tr>
								<th data-field="COUNT"  data-sortable="true">#</th>
								<th data-field="URL"  data-sortable="true">NAME:</th>
								<th data-field="O_BA"  data-sortable="true">B-AREA:</th>
								<th data-field="AGA_NAME"  data-sortable="true">AGA:</th>
								<th data-field="O_ADDRESS"  data-sortable="true">ADDRESS:</th>
								<th data-field="P_NAME"  data-sortable="true">PRIORITY:</th>
							</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->
	
</div>	<!--/.main-->
<script type="text/javascript">

</script>
