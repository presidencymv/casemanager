<?php
require('../../_ex/connections.php');    
require('../../_ex/authentications.php');
$currentuser = new authentication;
if ( $_SESSION['admins'] != 'Y'){
	header('location: ../../dashboard.php');
	die();
}
if (isset($_GET['unit'])){
	$selectedunit = preg_replace( "/[^0-9$]/","",$_GET['unit']);
}else{
	header('location: ../../dashboard.php');
	die();
}

$dbc = new dbconnection;
$sql="
SELECT um_id, U_NAME, UNIT_NAME, um_handler, um_added_by, um_added_on, UNIT_ID, U_ACTIVE
FROM unit_member INNER JOIN user ON um_u_id = U_ID
INNER JOIN unit ON um_unit_id = UNIT_ID WHERE UNIT_ID = ".$selectedunit;
$result = $dbc->dbconn->query($sql);
$rows = array();
 $count = 1;
while ($options = mysqli_fetch_assoc($result)){
	
	if ($options['U_ACTIVE'] == 1){
		$options['A_STATUS'] = 'Activated';
	}else{
		$options['A_STATUS'] = 'Deactivated';
	}
	$options['COUNT'] = $count;
	$options['URL'] = '<a href="dashboard.php?page=managemembers&sf=up&unit='.$options['UNIT_ID'].'&rec='.$options['um_id'].'">OPEN</a>';
	$rows[] = $options;
	$count++;
}
$result->free();
$dbc->dbconn->close();
echo  json_encode($rows, JSON_HEX_QUOT | JSON_HEX_TAG);
?>
