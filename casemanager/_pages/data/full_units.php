<?php
require('../../_ex/connections.php');    
require('../../_ex/authentications.php');
$currentuser = new authentication;
if ( $_SESSION['admins'] != 'Y'){
	header('location: dashboard.php');
	die();
}
$dbc = new dbconnection;
$sql="
SELECT
UNIT_ID, UNIT_NAME
FROM unit";
$result = $dbc->dbconn->query($sql);
$rows = array();
 $count = 1;
while ($options = mysqli_fetch_assoc($result)){
	$options['COUNT'] = $count;
	if(in_array($options['UNIT_ID'],$dbc->noupdateSections)){
	$options['URL'] = '<p>'.$options['UNIT_NAME'].' <em>(DO NOT UPDATE)</em></p>';
	}else{
	$options['URL'] = '<a href="dashboard.php?page=manageunit&sf=up&rec='.$options['UNIT_ID'].'">'.$options['UNIT_NAME'].'</a>';
	}
	
	$options['ADDMEMBERS'] = '<a href="dashboard.php?page=managemembers&unit='.$options['UNIT_ID'].'"><span class="glyphicon glyphicon-plus"></span> ADD</a>';
	$rows[] = $options;
	$count++;
}
$result->free();
$dbc->dbconn->close();
echo  json_encode($rows, JSON_HEX_QUOT | JSON_HEX_TAG);
?>
