<?php
header('Content-Type: application/json');
require('../../_ex/connections.php');    
require('../../_ex/authentications.php');
require('../../_ex/master.php');
$currentuser = new authentication;
$masterdata = new master;
$dbc = new dbconnection;

$elements = $_POST['dataset'];

$elements_array = explode(',', $elements);


$array_count = count(array_filter($elements_array, 'strlen'));

$forms = '';
$backcolor=1;
for ($x = 0; $x < $array_count; $x++) {
	$val = preg_replace( "/[^0-9$]/","",$elements_array[$x]);

	$sql = "SELECT DOC_SYSID, O_NAME, DC_NAME, UNIT_NAME, DOC_REFNUMBER, DOC_DESCRIPTION, DEADLINEDATE,CONCAT(DATEDIFF(CURDATE(),DEADLINEDATE), ' DAYS') AS DATE_DIFF , P_NAME, DST_NAME, DOCUMENTDETAILS.U_NAME AS CREATEDBY, ASSIGNED.U_NAME AS ASSIGNEDTO, DOC_ID, UNIT_ID
			FROM(
			SELECT DOC_SYSID, DOC_ID, O_NAME, DC_NAME, UNIT_NAME, UNIT_ID, DOC_REFNUMBER, DOC_DESCRIPTION, DATE(DOC_DEADLINE) as DEADLINEDATE, P_NAME, DST_NAME, U_NAME, DOC_ASSIGNEDTO
			FROM document 
			inner join office on DOC_OFFICE = O_ID
			inner join documentcat on DOC_CATEGORY = DC_ID
			inner join unit on DOC_UNIT = UNIT_ID
			inner join priority on DOC_PRIORITY = P_ID
			inner join doc_status on DST_ID = DOC_STATUS
			inner join user on DOC_CREATEDBY = U_ID
			WHERE DOC_HIDDEN <> 'Y' AND DOC_STATUS <> 4 AND DOC_ASSIGNEDTO = ".$_SESSION['userid']." AND DOC_ID = ".$elements_array[$x].") as DOCUMENTDETAILS
			LEFT OUTER JOIN USER ASSIGNED ON DOCUMENTDETAILS.DOC_ASSIGNEDTO = ASSIGNED.U_ID
			ORDER BY DOC_ID DESC"; 
	$result = $dbc->dbconn->query($sql);
	$rows = array();
	while ($cases = mysqli_fetch_assoc($result)){
	
	 $forms .= 	
	'
	<div id="cont'.$cases['DOC_ID'].'" class="row back'.$backcolor.'">
		<div class="col-lg-12">
			<div id = "form'.$cases['DOC_ID'].'">
				<div class="form-group col-lg-12">
				<h3><span class="glyphicon glyphicon-folder-open"></span> CASE:'.$cases['DOC_SYSID'].'</h3>
				</div>
				<div class="form-group col-lg-8">
					<label class="label label-primary">'.strtoupper($cases['DC_NAME']).'</label>&nbsp;
					<label class="label label-danger">'.$cases['DST_NAME'].'</label>&nbsp;
					<label class="label label-default">'.strtoupper($cases['ASSIGNEDTO']).'</label><br/>
					<h4>'.$cases['O_NAME'].'</h4>
				</div>
				
				<div class="form-group col-lg-4 text-right">
					<a href="dashboard.php?page=casedetails&amp;id='.$cases['DOC_ID'].'" target="_blank" ><span class="glyphicon glyphicon-open"> </span> CASE DETAILS</a>
				</div>
				
				<div class="form-group col-lg-12">
					<br/>
					<label  class="text-danger" >'.$cases['DOC_REFNUMBER'].'</label>
					<p>'.$masterdata->sneakpeak($cases['DOC_DESCRIPTION'],300).'</p>
				</div>
				
				<div class="form-group col-lg-12">
					<select required class="form-control" name="member'.$cases['DOC_ID'].'" id="member'.$cases['DOC_ID'].'">'.$masterdata->selectoptionsrequest('U_ID','U_NAME','user INNER JOIN unit_member ON u_id = um_u_id','um_unit_id = '.$cases['UNIT_ID'].' AND u_id <> '.$_SESSION['userid']).'</select>
				</div>
				<div class="form-group col-lg-12">
					<input class = "btn btn-primary" onclick="committransfer('.$cases['DOC_ID'].')" value="HANDOVER">
					<input class = "btn btn-default" onclick="canceltransfer('.$cases['DOC_ID'].')" value="CANCEL">
				</div>
			</div>
		</div>
	</div>
	';
	if ($backcolor == 1){
		$backcolor = 0;
	}else{
		$backcolor = 1;
	}
	}
	$result->free();
}

$dbc->dbconn->close();
$resultset = array(
'selectedcount'=> $array_count,
'forms'=> $forms
);

echo  json_encode($resultset, JSON_HEX_QUOT | JSON_HEX_TAG);

?>