<?php
require('../../_ex/connections.php');
require('../../_ex/authentications.php');
require('../../_ex/alert.php');

$currentuser = new authentication;

$dbc = new dbconnection;
$sql="SELECT A_ID, A_U_ID, U_NAME, A_DOC_ID, DOC_REFNUMBER, A_TYPE, A_TYPE_DESC, AD_DESC, A_CREATEDON FROM alerts LEFT OUTER JOIN alert_description ON A_TYPE_DESC = AD_ID LEFT OUTER JOIN user ON A_CREATEDBY = U_ID LEFT OUTER JOIN document ON A_DOC_ID = DOC_ID WHERE A_SEEN = 'N' AND A_U_ID = ".$_SESSION['userid']." ORDER BY A_CREATEDON DESC LIMIT 5";
$result = $dbc->dbconn->query($sql);
$rows = array();
$count = 0;
while ($options = mysqli_fetch_assoc($result)){
	
	$linkaddress = 'case_alerts.php?ex=100&nid='.$options['A_ID'].'&sid='.$options['A_U_ID'].'&did='.$options['A_DOC_ID'].'&tid='.$options['A_TYPE_DESC'];

	$createdtime = DateTime::createFromFormat('Y-m-d H:i:s', $options['A_CREATEDON']);
	
	$rows[$count]['commentitem'] = '<li class="message-preview"><a href="'.$linkaddress.'"><div class="media"><div class="media-body"><h5 class="media-heading"><strong>'.$options['AD_DESC'].'</strong></h5><p>Case No:'.$options['DOC_REFNUMBER'].'</p><p class="small">By:'.$options['U_NAME'].'</p><p class="small text-muted"><i class="glyphicon glyphicon-time"></i> '.$createdtime->format('d  M Y @ H:i A').'</p></div></div></a></li>';

	$count++;

}

$alert = new alert;
$unreadcount = $alert->countuserunseenalerts($_SESSION['userid']);
if($unreadcount > 5){
	$rows[$count]['commentitem'] = '<li class="message-footer"><a href="dashboard.php?page=notifications">View all new notifications</a></li>';
}
if(empty($rows)){
	$rows[$count]['commentitem'] = '<li class="message-preview"><a href="#"><div class="media"><div class="media-body"><h5 class="media-heading"><strong>No New Notifications</strong></h5><p></p><p class="small"></p><p class="small text-muted"></p></div></div></a></li>';
}
$result->free();
$dbc->dbconn->close();
echo  json_encode($rows);
?>
