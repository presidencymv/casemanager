<?php
 require('../../_ex/connections.php');    
 require('../../_ex/authentications.php');
 $currentuser = new authentication;    
 $dbc = new dbconnection;
 $sql="
 SELECT
 O_ID,
 AGA_NAME,
 O_BA,
 O_NAME,
 O_ADDRESS,
 P_NAME
 FROM office
 LEFT OUTER JOIN aga ON O_AGA = AGA_ID
 INNER JOIN priority ON O_DEFAULTPRIO = P_ID ORDER BY O_ID DESC";
 $result = $dbc->dbconn->query($sql);
 $rows = array();
 $count = 1;
 while ($options = mysqli_fetch_assoc($result)){
	$options['COUNT'] = $count;
	$options['URL'] = '<a href="dashboard.php?page=managesender&sf=up&rec='.$options['O_ID'].'">'.$options['O_NAME'].'</a>';
    $rows[] = $options;
	$count++;
 }
 $result->free();
 $dbc->dbconn->close();
 echo  json_encode($rows, JSON_HEX_QUOT | JSON_HEX_TAG);
?>