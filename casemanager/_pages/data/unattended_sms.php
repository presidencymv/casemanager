<?php
header('Content-Type: application/json');
require('../../_ex/connections.php');    
require('../../_ex/authentications.php');
$currentuser = new authentication;


if ($_SESSION['callcentrestaff'] != 'Y'){
    die('UNAUTHORIZED ACCESS');
}


/* <th>#</th>
<th>SENDER</th>
<th>NAME</th>
<th>MESSAGE</th>
<th>RECEIVED ON</th>
 */

$count = 1;
$att_details = '';
$dbc = new dbconnection;
$sql = "SELECT `SMS_R_ID`,`SMS_R_SENDER_NUMBER` as `NUMBER` , LEFT(`SMS_R_SENDER_NAME`,20) as `NAME`, LEFT(`SMS_R_MESSAGE`,20) as `MESSAGE` ,TIME(`SMS_R_ON`) as `TIME` 
FROM `sms_received` LEFT OUTER JOIN `call_records` ON (`SMS_R_ID` = `CR_SMS_R_ID` AND (CR_ON > SMS_R_ON))  
WHERE CR_ID IS NULL AND SMS_DELETED = 0 
ORDER BY SMS_R_ID";
$result = $dbc->dbconn->query($sql);
$combinemsgs = array();
while ($updates = mysqli_fetch_assoc($result)){
$att_details .= '<tr>
				<th scope="row">'.$count.'</th>
				<td>'.$updates['NUMBER'].'</td>
				<td>'.$updates['NAME'].'</td>
				<td><a class="custfinger" onclick="createsmsrecord('.$updates['SMS_R_ID'].')">'.$updates['MESSAGE'].'</a></td>
				<td>'.$updates['TIME'].'</td>
				<td><a class="custfinger" onclick="deleteRecord('.$updates['SMS_R_ID'].')"><span title="Delete Message" class="glyphicon glyphicon-minus-sign"></span></a></td>';
if (in_array($updates['NUMBER'],$combinemsgs)){
$att_details .= '<td><a class="custfinger text-success" onclick="mergeRecord('.$updates['SMS_R_ID'].')"><span title="Merge With Previous SMS" class="glyphicon glyphicon-plus-sign"></span></a></td>';	
}else{
$att_details .= '<td><a class="default"></a></td>';	
$combinemsgs[] = $updates['NUMBER'];
}
$att_details .= '</tr>';

$count++;

}
$result->free();
$dbc->dbconn->close();

		
$resultset = array(
	'details'=> $att_details
);
echo  json_encode($resultset, JSON_HEX_QUOT | JSON_HEX_TAG);
?>