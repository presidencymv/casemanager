<?php
header('Content-Type: application/json');
require('../../_ex/connections.php');    
require('../../_ex/authentications.php');
require('../../_ex/master.php');
require('../../_ex/call.php');
$currentuser = new authentication;


if ($_SESSION['callcentrestaff'] != 'Y'){
    die('UNAUTHORIZED ACCESS');
}
if (isset($_GET['id'])){
    $CR_ID = preg_replace( "/[^0-9$]/","",$_GET['id']);
}

$call = new call;
$details = $call->get_call_info($CR_ID);
$count = 1;
$form = 	
	'
	<div id = "form" class="row">
		<div class="form-group col-lg-12">
			<label>CALL RECORD</label>
			<input type="text" id = "efcr_id" name = "efcr_id" class="form-control" readonly="" value="'.$details['CR_ID'].'">
			<input type="hidden" id = "ef_id" name = "ef_id" class="form-control" readonly="" value="'.$call->formid($details['CR_ID']).'" >
			<hr>
			<div class="form-group col-lg-4">
			<label>TOKEN:</label>
			<h1>'.$details['CR_LOGID'].'</h1>';
if($call->main_call($CR_ID) == FALSE){
	$form .= 	
	'
		<label>FOLLOW UP TOKEN:</label>

		<select  class="form-control" name="effollowup" id="effollowup" onchange="opencalldetails()">'.
				$call->selectoptionsrequest_selected('CR_LOGID','CR_LOGID','call_records'," CR_LOGID <> '".$details['CR_LOGID']."' AND CR_SOLVED = 0 AND CR_ACTIVE <> 'Y' AND CR_CRCAT_ID <> 4 AND CR_REF IS NULL" ,$details['CR_REF'],'Y')
		.'</select>

		</div>';
	
}


$form .= 	
	'	</div>
		<hr>
		<div class="form-group col-lg-12">
		
			<div class="form-group col-lg-4">
			<label>Caller Number <span id="efcallerinfo-refresh" class="glyphicon glyphicon-refresh custfinger" onclick="checkcallerinfo()"></span></label>
			<input type="text" id = "efcallernum" name = "efcallernum" class="form-control" value="'.$details['CR_TEL'].'">
			</div>
	
			<div class="form-group col-lg-8">
			<label>Full Name</label>
			<input type="text" id = "efcallername" name = "efcallername" class="form-control" value="'.$details['CR_CALLER'].'">
			</div>
			<div class="form-group col-lg-12">
			<label>Company Name</label>
			<input type="text" id = "companyname" name = "companyname" class="form-control" value="'.$details['CR_COMPANY'].'">
			</div>
			<div class="form-group col-lg-12">
			<label>Email</label>
			<input type="email" id = "efcalleremail" name = "efcalleremail" class="form-control" value="'.$details['CR_EMAIL'].'">
			</div>
			
			<div class="form-group col-lg-12">
				<label>Category</label>
				<select  class="form-control" name="efdoccat" id="efdoccat">'.
					$call->selectoptionsrequest_selected('DC_ID','DC_NAME','documentcat','',$details['DC_ID'])
				.'</select>
			</div>
			
			<div class="form-group col-lg-6">
				<label>From</label>
				<select  class="form-control" name="effrom" id="effrom">'.
					$call->selectoptionsrequest_selected('O_ID','O_NAME','office','',$details['O_ID'])
				.'</select>
			</div>
			
			<div class="form-group col-lg-6">
				<label>To</label>
				<select  class="form-control" name="efto" id="efto">'.
					$call->selectoptionsrequest_selected('UNIT_ID','UNIT_NAME','unit','',$details['UNIT_ID'])
				.'</select>
			</div>
			<div class="form-group col-lg-12">
				<label>PV Number</label>
				<input type="text" id = "pvnumber" name = "pvnumber" class="form-control" value="'.$details['CR_PV'].'">
			</div>
			<div class="form-group col-lg-12">
				<label>Issue</label>
				<textarea id = "efissue" name = "efissue" class="form-control" required>'.$details['CR_ISSUE'].'</textarea>
			</div>
			
			<div class="form-group col-lg-12">
				<label>Response</label>
				<textarea id = "efresponse" name = "efresponse" class="form-control" required>'.$details['CR_RESPONSE'].'</textarea>
			</div>
			
			<div class="form-group col-lg-12">
				<label>Priority</label>
				<select  class="form-control" name="efpriority" id="efpriority">'.
					$call->selectoptionsrequest_selected('P_ID','P_NAME','priority','',$details['P_ID'])
				.'</select>
			</div>
		</div>
		<hr>
		<div class="form-group col-lg-12 text-center">
			<input class = "btn btn-primary" onclick="updatecall('.$CR_ID.')" value="UPDATE CALL DETAILS">
		</div>
	</div>
	';

$resultset = array(
'selectedcount'=>$count,
'form'=> $form
);

echo  json_encode($resultset, JSON_HEX_QUOT | JSON_HEX_TAG);
?>