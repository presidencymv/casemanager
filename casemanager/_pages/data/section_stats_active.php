<?php
require('../../_ex/connections.php');    
require('../../_ex/authentications.php');
require('../../_ex/master.php');
$master = new master;
$currentuser = new authentication;
	$dbc = new dbconnection;
	$sql = "SELECT DOC_SYSID, O_NAME, DC_NAME, UNIT_NAME, DOC_REFNUMBER, DOC_DESCRIPTION, DEADLINEDATE,CONCAT(DATEDIFF(CURDATE(),DEADLINEDATE), ' DAYS') AS DATE_DIFF , P_NAME, DST_NAME, DOCUMENTDETAILS.U_NAME AS CREATEDBY, ASSIGNED.U_NAME AS ASSIGNEDTO, DOC_ID, P_ID
                FROM(
                SELECT DOC_SYSID, DOC_ID, O_NAME, DC_NAME, UNIT_NAME, DOC_REFNUMBER, DOC_DESCRIPTION, DATE(DOC_DEADLINE) as DEADLINEDATE, P_NAME, DST_NAME, U_NAME, DOC_ASSIGNEDTO, P_ID
                FROM document 
                inner join office on DOC_OFFICE = O_ID
                inner join documentcat on DOC_CATEGORY = DC_ID
                inner join unit on DOC_UNIT = UNIT_ID AND DOC_UNIT = ".$_SESSION['selectedunitid']."
                inner join priority on DOC_PRIORITY = P_ID
                inner join doc_status on DST_ID = DOC_STATUS
                inner join user on DOC_CREATEDBY = U_ID
                WHERE DOC_HIDDEN <> 'Y' AND DOC_UNIT IN (".$_SESSION['user_units'].") ) as DOCUMENTDETAILS
                LEFT OUTER JOIN USER ASSIGNED ON DOCUMENTDETAILS.DOC_ASSIGNEDTO = ASSIGNED.U_ID
                ORDER BY DOC_ID DESC"; 
	$result = $dbc->dbconn->query($sql);
	$rows = array();
	while ($cases = mysqli_fetch_assoc($result)){
	$cases['DOC_DESCRIPTION'] = $master->sneakpeak($cases['DOC_DESCRIPTION'],150);
	if($cases['P_ID'] == 4){
		$cases['DOC_SYSID'] = '<span class="glyphicon glyphicon-flag text-danger" title="High Priority"> </span> '.$cases['DOC_SYSID'];
	}
     if($cases['DST_NAME'] == 'CLOSED'){
        $cases['DATE_DIFF'] = "CLOSED";
     }
	 $cases['detailsURL'] = "<a href='dashboard.php?page=casedetails&id=".$cases['DOC_ID']."'>".'<span class="glyphicon glyphicon-open"> </span> '."OPEN</a>";
	 $rows[] = $cases;
	}
	$result->free();
	$dbc->dbconn->close();
    // unset($_SESSION['selectedunitid']);
	echo  json_encode($rows, JSON_HEX_QUOT | JSON_HEX_TAG);
?>