<?php
header('Content-Type: application/json');
require('../../_ex/connections.php');    
require('../../_ex/authentications.php');
require('../../_ex/master.php');
require('../../_ex/call.php');
$currentuser = new authentication;
 $maxtries = 1;

if ($_SESSION['callcentrestaff'] != 'Y'){
    die('UNAUTHORIZED ACCESS');
}

$call = new call;
$startcall = false;
$form = '';
//LOOK FOR OPEN CALLS
$call->clearoldcalls($_SESSION['userid']);
$activecall = $call->active_call($_SESSION['userid']);

if($activecall['ACTIVECOUNT'] == 0){
		
	while($startcall == false && $maxtries < 10){
		$startcall = $call->takecall($_SESSION['userid']);
		$maxtries++;
	}
	$activecall = $call->active_call($_SESSION['userid']);

}

$followUp = '';
if (isset($_GET['followup'])){
    $followUp = preg_replace( "/[^A-Za-z 0-9$]/","",$_GET['followup']);
}

if($followUp != ''){
	$selectedfollowup = $followUp;
}else{
	$selectedfollowup = $activecall['CR_REF'];
}
	
$masterdata = new master;
$form .= 	
	'
	<div id = "form" hidden>
		<div class="form-group col-lg-12">
		
			<div id = "autosaveindicator" class="form-group col-lg-12">
			<span class="glyphicon glyphicon-floppy-save text-success" title="High Priority"></span> <small>AUTO SAVING ENABLED</small><br/>
			</div>
			<div class="form-group col-lg-4">
			
			<label>TOKEN:</label>
			<h1>'.$activecall['CR_LOGID'].'</h1>
			<label>FOLLOW UP TOKEN:</label>
			<select  class="form-control" name="followup" id="followup" onchange="opencalldetails()">'.
					$masterdata->selectoptionsrequest_selected('CR_LOGID','CR_LOGID','call_records'," CR_LOGID <> '".$activecall['CR_LOGID']."' AND CR_SOLVED = 0 AND CR_ACTIVE <> 'Y' AND CR_CRCAT_ID <> 4 AND CR_REF IS NULL" ,$selectedfollowup,'Y')
			.'</select>

			</div>
			<div class="form-group col-lg-4 col-lg-offset-4 text-right">
			<label>Elapsed Time:</label>
			<h3><span id="hours">00</span>:<span id="minutes">00</span>:<span id="seconds">00</span></h3>
			</div>
		</div>
		<hr>
		<div class="form-group col-lg-12">
		
			<div class="form-group col-lg-4">
			<label>Caller Number <span id="callerinfo-refresh" class="glyphicon glyphicon-refresh custfinger" onclick="checkcallerinfo()"></span></label>
			<input type="text" id = "callernum" name = "callernum" class="form-control" value="'.$activecall['CR_TEL'].'">
			</div>
	
			<div class="form-group col-lg-8">
			<label>Full Name</label>
			<input type="text" id = "callername" name = "callername" class="form-control" value="'.$activecall['CR_CALLER'].'">
			</div>
			
			<div class="form-group col-lg-12">
			<label>Company Name</label>
			<input type="text" id = "companyname" name = "companyname" class="form-control" value="'.$activecall['CR_COMPANY'].'">
			</div>
			
			<div class="form-group col-lg-12">
			<label>Email</label>
			<input type="email" id = "calleremail" name = "calleremail" class="form-control" value="'.$activecall['CR_EMAIL'].'">
			</div>
			
			<div class="form-group col-lg-12">
				<label>Category</label>
				<select  class="form-control" name="doccat" id="doccat">'.
					$masterdata->selectoptionsrequest_selected('DC_ID','DC_NAME','documentcat','',$activecall['CR_DC_ID'])
				.'</select>
			</div>
			
			<div class="form-group col-lg-6">
				<label>From</label>
				<select  class="form-control" name="from" id="from">'.
					$masterdata->selectoptionsrequest_selected('O_ID','O_NAME','office','',$activecall['CR_O_ID'])
				.'</select>
			</div>
			
			<div class="form-group col-lg-6">
				<label>To</label>
				<select  class="form-control" name="to" id="to">'.
					$masterdata->selectoptionsrequest_selected('UNIT_ID','UNIT_NAME','unit','UNIT_ID NOT IN (33,39)',$activecall['CR_UNIT_ID'])
				.'</select>
			</div>
			<div class="form-group col-lg-12">
				<label>PV Number</label>
				<input type="text" id = "pvnumber" name = "pvnumber" class="form-control" value="'.$activecall['CR_PV'].'">
			</div>
			
			<div class="form-group col-lg-12">
				<label>Issue</label>
				<textarea id = "issue" name = "issue" class="form-control" required>'.$activecall['CR_ISSUE'].'</textarea>
			</div>
			
			<div class="form-group col-lg-12">
				<label>Response</label>
				<textarea id = "response" name = "response" class="form-control" required>'.$activecall['CR_RESPONSE'].'</textarea>
			</div>
			
			<div class="form-group col-lg-12">
				<label>Priority</label>
				<select  class="form-control" name="priority" id="priority">'.
					$masterdata->selectoptionsrequest_selected('P_ID','P_NAME','priority','',$activecall['CR_P_ID'])
				.'</select>
			</div>
			<div class="form-group col-lg-6">';
			
			if($activecall['CR_SOLVED'] == 1){
				$form .='<input type="checkbox" name="resolved" id="resolved" checked> MARK AS RESOLVED';
				
			}else{
				$form .='<input type="checkbox" name="resolved" id="resolved"> MARK AS RESOLVED';
				
			}
			
			
				
$form .= 	
	'</div>
		</div>
		<hr>
		<div class="form-group col-lg-12 text-center">
			<input class = "btn btn-danger btn-xs" onclick="savedata(201)" value="CALL PASSED">
			<input class = "btn btn-primary" onclick="savedata(200)" value="END CALL">
			<input class = "btn btn-danger btn-xs" onclick="savedata(202)" value="MISSED CALL">
		</div>
	</div>
	';

$resultset = array(
'form'=> $form,
'sec' => $activecall['ELAPSEDTIME'],
'resolved' => $activecall['CR_SOLVED']
);

echo  json_encode($resultset, JSON_HEX_QUOT | JSON_HEX_TAG);
?>