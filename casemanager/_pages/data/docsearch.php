<?php
require('../../_ex/connections.php');    
require('../../_ex/authentications.php');
require('../../_ex/master.php');
$currentuser = new authentication;
$master = new master;

if (isset($_GET['term'])){
    $searchterm = urlencode($_GET['term']);
}else{
	die('NO SEARCH TERM FOUND');
}
	$dbc = new dbconnection;
	$sql = "SELECT DOC_ID, DOC_SYSID, O_NAME, UNIT_NAME, DOC_REFNUMBER,DOC_DESCRIPTION, DOCUMENTDETAILS.U_NAME AS CREATEDBY, DATE(DOC_CREATEDON) AS `ENTERED DATE`, DST_NAME, ASSIGNED.U_NAME as ASSIGNED_USER, DOC_SENDER_TYPE, DOC_TXT1, DOC_TXT2
                FROM(
                SELECT DOC_SYSID, DOC_ID, O_NAME, UNIT_NAME, DOC_REFNUMBER, DOC_DESCRIPTION, DATE(DOC_DEADLINE) as DEADLINEDATE, DST_NAME, U_NAME, DOC_ASSIGNEDTO, DOC_CREATEDON, DOC_SENDER_TYPE, DOC_TXT1, DOC_TXT2
                FROM document 
                inner join office on DOC_OFFICE = O_ID
                left outer join unit on DOC_UNIT = UNIT_ID
                inner join doc_status on DST_ID = DOC_STATUS
                inner join user on DOC_CREATEDBY = U_ID
                WHERE DOC_HIDDEN <> 'Y' AND (";
	
	$fieldstocheck = array ('DOC_REFNUMBER','DOC_DESCRIPTION','O_NAME','DOC_TXT1','DOC_TXT2');
	$searchwords = explode("+",$searchterm);
	$searchwords[] = urldecode($searchterm);
	
	$firstrecord = TRUE;
	foreach ($fieldstocheck as $field){
		foreach($searchwords as $word){
			if ($firstrecord == TRUE){
				$firstrecord = FALSE;
				$sql .= $field." LIKE '%".mysqli_real_escape_string($dbc->dbconn,$word)."%' " ;
			}else{
				$sql .= "OR ".$field." LIKE '%".mysqli_real_escape_string($dbc->dbconn,$word)."%' " ;
			}
		}
	}			
	$sql .=	")
			) as DOCUMENTDETAILS
                LEFT OUTER JOIN USER ASSIGNED ON DOCUMENTDETAILS.DOC_ASSIGNEDTO = ASSIGNED.U_ID
                ORDER BY DOC_ID DESC";
	
	$result = $dbc->dbconn->query($sql);
	$rows = array();
	while ($cases = mysqli_fetch_assoc($result)){

		$cases["id"] = $cases['DOC_ID'];
		$cases["value"] = $master->sneakpeak($cases['DOC_DESCRIPTION'],50).'('.$cases['ASSIGNED_USER'].'-'.$cases['UNIT_NAME'].')';
		$cases["lable"] = $cases['DOC_REFNUMBER'];
		
	 $rows[] = $cases;
	}
	$result->free();
	$dbc->dbconn->close();
	echo  json_encode($rows, JSON_HEX_QUOT | JSON_HEX_TAG);

?>