<?php
require('../../_ex/connections.php');    
require('../../_ex/authentications.php');
require('../../_ex/master.php');
$master = new master;
if (isset($_GET['year'])){
    $selectedyear = preg_replace( "/[^0-9$]/","",$_GET['year']);
}else{
	$selectedyear = date('Y');
}
$currentuser = new authentication;

	$dbc = new dbconnection;
	$sql = "SELECT DOC_ID, DOC_SYSID, O_NAME, UNIT_NAME, DOC_REFNUMBER, P_NAME, DOCUMENTDETAILS.U_NAME AS CREATEDBY, DATE(DOC_CREATEDON) AS `ENTERED DATE`, DST_NAME, ASSIGNED.U_NAME as ASSIGNED_USER, P_ID, DOC_SENDER_TYPE, DOC_TXT1, DOC_TXT2
                FROM(
                SELECT DOC_SYSID, DOC_ID, O_NAME, DC_NAME, UNIT_NAME, DOC_REFNUMBER, DOC_DESCRIPTION, DATE(DOC_DEADLINE) as DEADLINEDATE, P_NAME, DST_NAME, U_NAME, DOC_ASSIGNEDTO, DOC_CREATEDON, P_ID, DOC_SENDER_TYPE, DOC_TXT1, DOC_TXT2
                FROM document 
                inner join office on DOC_OFFICE = O_ID
                inner join documentcat on DOC_CATEGORY = DC_ID
                left outer join unit on DOC_UNIT = UNIT_ID
                inner join priority on DOC_PRIORITY = P_ID
                inner join doc_status on DST_ID = DOC_STATUS
                inner join user on DOC_CREATEDBY = U_ID
                WHERE DOC_HIDDEN <> 'Y' AND YEAR(DOC_CREATEDON) = ".$selectedyear.") as DOCUMENTDETAILS
                LEFT OUTER JOIN USER ASSIGNED ON DOCUMENTDETAILS.DOC_ASSIGNEDTO = ASSIGNED.U_ID
                ORDER BY DOC_ID DESC";
	$result = $dbc->dbconn->query($sql);
	$rows = array();
	while ($cases = mysqli_fetch_assoc($result)){
	//$cases['DOC_DESCRIPTION'] = $master->sneakpeak($cases['DOC_DESCRIPTION'],150);
	if($cases['P_ID'] == 4){
		$cases['DOC_SYSID'] = '<span class="glyphicon glyphicon-flag text-danger" title="High Priority"> </span> '.$cases['DOC_SYSID'];
	}
	if ($cases['DOC_SENDER_TYPE'] == 0 ){
			$cases['SENDER'] = $cases['O_NAME'].'('.$cases['DOC_TXT1'].'-'.$cases['DOC_TXT2'].')';
	}else{
			$cases['SENDER'] = $cases['DOC_TXT1'].'-'.$cases['DOC_TXT2'];
	}
	
	$sql2 = "SELECT DR_DESC FROM doc_remarks WHERE DR_DOC_ID = ".$cases['DOC_ID'];
	$result2 = $dbc->dbconn->query($sql2);
	$rows2 = array();
	while ($attachments = mysqli_fetch_assoc($result2)){
		$cases['ATTACHMENTS'] .= '<p>'.$attachments['DR_DESC'].'</p><br/>';
	}
	
	 $rows[] = $cases;
	}
	$result->free();
	$dbc->dbconn->close();
	echo  json_encode($rows, JSON_HEX_QUOT | JSON_HEX_TAG);

?>