<?php
header('Content-Type: application/json');
require('../../_ex/connections.php');    
require('../../_ex/authentications.php');
require('../../_ex/master.php');
require('../../_ex/document.php');
require('../../_ex/call.php');
$currentuser = new authentication;
$callinfo = "";
$sendcasebutton = "";
$reftoken = "";
$CR_ID = 0;
$returnstate = false;
if ($_SESSION['callcentrestaff'] != 'Y'){
    die('UNAUTHORIZED ACCESS');
}
if (isset($_GET['id'])){
    $CR_ID = preg_replace( "/[^0-9$]/","",$_GET['id']);
}

$call = new call;
$details = $call->get_call_info($CR_ID);
if ($details['CR_ID'] != '' || $details['CR_ID'] != NULL ){
	$returnstate = true;
	
	if($details['CR_REF'] != NULL || $details['CR_REF'] != ''){
	
	$reftoken = '<em class="text-info">REF-TOKEN:<a href="#" onclick="calldetails('.$details['CR_REF_ID'].')">'.$details['CR_REF'].'</a></em>';
	}
	
	
	if ($details['CR_DOC_ID'] == NULL && ($details['CR_REF'] == NULL || $details['CR_REF'] == '')){
		$sendcasebutton = '<button id="sendcase'.$details['CR_ID'].'" type="button" class="btn btn-primary btn-xs" onclick="sendcase('.$details['CR_ID'].')"><small><span class="glyphicon glyphicon-share-alt"></span> SEND CASE</small></button>';
	}
	
		if ($details['CR_SOLVED'] == 0){
			if($details['CR_REF'] == NULL || $details['CR_REF'] == ''){
			$resolvedbutton = '<button id="markasresolved" type="button" class="btn btn-primary btn-xs" onclick="markasresolved('.$details['CR_ID'].',0)"><small><span class="glyphicon glyphicon-ok"></span> MARK AS RESOLVED</small></button>';
			}
			$editbutton = '<button id="editcall" type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myModal" onclick="editcalldetails('.$details['CR_ID'].')"><small><span class="glyphicon glyphicon-pencil"></span> EDIT</small></button>';
			$state="UNRESOLVED";
		}else{
			if($details['CR_REF'] == NULL || $details['CR_REF'] == ''){
			$resolvedbutton = '<button id="markasresolved" type="button" class="btn btn-primary btn-xs" onclick="markasresolved('.$details['CR_ID'].',1)"><small><span class="glyphicon glyphicon-ok"></span> MARK AS UNRESOLVED</small></button>';
			}
			$editbutton = '';
			$state="RESOLVED";
		}
	
	
	$class = '';
	if ($details['P_NAME'] == 'HIGH'){
		$class='class="glyphicon glyphicon-flag text-danger"';
	}
	
	if($details['CR_PV'] != NULL){
		$pv_number = "<em>SAP DOC No.</em><p>".$details['CR_PV']."</p>";
	}else{
		$pv_number = "";
	}
	
	if($details['CR_COMPANY'] != NULL){
		$companyname = "<h5>".$details['CR_COMPANY']."</h5>";
	}else{
		$companyname = "";
	}

	$callinfo .= 	
	'
		<div id="calldetails'.$details['CR_ID'].'" class="panel panel-default" hidden>
			<div class="col-lg-12 text-right custpad_top text-danger">
				<span class="glyphicon glyphicon-remove custfinger" onclick="closecalldetail('.$details['CR_ID'].')"></span>
			</div>
		
			<div class="panel-body">
				<div class="panel-heading">
					<h3 id="callheader'.$details['CR_ID'].'" class="text-info" >ISSUE REF:'.$details['CR_LOGID'].'</h3>
				</div>
				<div class="col-lg-6">
				'.$reftoken.'
				</div>
				<div class="col-lg-6 text-right">
					'.$resolvedbutton.'
					'.$sendcasebutton.'
					'.$editbutton.'
				</div>';
					if($details['CR_REF'] == NULL || $details['CR_REF'] == ''){
	$callinfo .= 
			'<hr>
				<div class="col-lg-6">
					<div class="col-lg-12">
						<p>From:</p>
						<h4>'.$details['CR_CALLER'].'</h4>
						<h5>'.$details['CR_TEL'].'</h5>
						'.$companyname.'
						<h5>'.$details['O_NAME'].'</h5>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="col-lg-12">
						<p>To:</p>
						<h4>'.$details['UNIT_NAME'].'</h4>
					</div>
				</div>';
	}
	$callinfo .= 	
	'			<hr>
				<div class="col-lg-12">
					<div class="col-lg-12">
						<em>Issue:</em>
						<p>'.$details['CR_ISSUE'].'</p>
						'.$pv_number.'
					</div>
				</div>
				<div class="col-lg-12">
					<div class="col-lg-12">
						<em>Response:</em>
						<p>'.$details['CR_RESPONSE'].'</p>
					</div>
				</div>';


	if($details['CR_REF'] == NULL || $details['CR_REF'] == ''){
	$followupcalls = $call->followup_calls($CR_ID,2);
	
	$callinfo .= 
			'<hr>	
				<div class="col-lg-12">
				<h5>Follow Up Calls</h5>

					'.$followupcalls.'
				</div>';
	}
	$callinfo .= 
		'<hr>
			<div class="col-lg-12 text-info">
			<small>
				<em>Date: '.$details['CR_ON'].' ('.$details['DURATION'].')</em>
				<em>&nbsp;&nbsp;&nbsp;&nbsp; State: '.ucwords(strtolower($state)).'</em>
				<em>&nbsp;&nbsp;&nbsp;&nbsp;<span '.$class.' ></span> '.ucwords(strtolower($details['P_NAME'])).' Priority</em>
			</small>
			</div>';
	if($details['CR_DOC_ID'] != NULL) {
	$document = new document;
	$documentinfo = $document->getdocumentinfo($details['CR_DOC_ID']);
	$callinfo .='
	<hr>
	<div class="col-lg-12 back0">
		<div class="col-lg-12">
			<div class="col-lg-8">
				<h4 class="text-danger">CASE: <a href="dashboard.php?page=casedetails&amp;id='.$details['CR_DOC_ID'].'" title="Case Details" target="_blank">'. $documentinfo['DOC_SYSID'].'</a><small><em> ('.ucwords(strtolower($documentinfo['DST_NAME'])).')</em></small></h4>
			</div>
		</div>
		
		<div class="col-lg-12">
			<div class="col-lg-12">
				<h5>Case Assigned To: '. ucwords(strtolower(preg_replace( "/[^a-z$]/"," ",$documentinfo['ASSIGNEDTO']))).'</h5>
			</div>
		</div>
		<hr>
		<div class="panel panel-default">
		  <div class="table-responsive">
			  <table class="table table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>Case Updates</th>
						<th>By</th>
						<th>On</th>
					</tr>
				</thead>
				<tbody>';
				$count = 1;
				$dbc = new dbconnection;
				$sql="SELECT DR_ID,DR_DESC, DR_FILE, DR_OUT_REF, U_NAME, DR_ON FROM doc_remarks inner join user ON DR_BY = U_ID WHERE DR_DOC_ID = ".$details['CR_DOC_ID']." ORDER BY DR_ON DESC";
				$result = $dbc->dbconn->query($sql);
				while ($updates = mysqli_fetch_assoc($result)){
	$callinfo .='<tr>
					<th scope="row">'.$count.'</th>';
					if($updates['DR_FILE'] == NULL || $updates['DR_FILE'] == '') {
		$callinfo.= '<td>'.$updates['DR_DESC'].'</td>';
					}else{
		$callinfo.= '<td><a href="filedownload.php?download='.$updates['DR_ID'].'">'.$updates['DR_DESC'].'</a></td>';
					}
						
					
	$callinfo .='	<td>'.$updates['U_NAME'].'</td>
					<td>'.$updates['DR_ON'].'</td>
				</tr>';
				$count++;
				}
				$result->free();
				$dbc->dbconn->close(); 
	$callinfo .='</tbody>
			
			  </table>
		  </div>
		</div>
		<hr>
	</div>
	';
	}			
				
	$callinfo .='</div>	
		</div>
		';
}
		
$resultset = array(
	'state'=>$returnstate,
	'callinfo'=> $callinfo
);
echo  json_encode($resultset, JSON_HEX_QUOT | JSON_HEX_TAG);
?>