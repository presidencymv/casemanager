<?php
header('Content-Type: application/json');
require('../../_ex/connections.php');    
require('../../_ex/authentications.php');
require('../../_ex/master.php');
require('../../_ex/document.php');
require('../../_ex/chancellery.php');
$currentuser = new authentication;
$returnstatus = "NOTSET";
if ($_SESSION['chancellerystaff'] != 'Y'){
    die('UNAUTHORIZED ACCESS');
}


if (isset($_GET['docid'])){
$docid = preg_replace( "/[^0-9$]/","",$_GET['docid']);
	if ($docid == ''){
	$returnstatus = "FAILED";
	}
}else{
	$returnstatus = "FAILED";
}

$chancellerydoc = new chancellery;
$docinfo = $chancellerydoc->getdocumentinfo_chancellery($docid);

if($docinfo['DOC_SENDER_TYPE'] == 0){
	$senderinfo = $docinfo['O_NAME'];
	
}else{
	
	$senderinfo = $docinfo['DOC_TXT1'].' - '.$docinfo['DOC_TXT2'];
}


$form = <<<FORMDETAILS
<div class="row">
<div class="col-lg-12">
<div class="panel panel-default">
	<div class="panel-body custom_text">
		<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="col-lg-4">
							<div class="col-lg-12">
								<p>From:</p>
								<h4>{$senderinfo}</h4>
							</div>
						
							<div class="col-lg-12">
								<p>To:</p>
								<h4>{$docinfo['UNIT_NAME']}</h4>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="col-lg-12">
								<p>Category:</p>
								<h4>{$docinfo['DC_NAME']}</h4>
							</div>
						</div>
						
						<div class="col-lg-4">
							<div class="col-lg-12">
								<p>Reference No:</p>
								<h4>{$docinfo['DOC_REFNUMBER']}</h4>
							</div>
						</div>
						<hr>
						<div class="col-lg-12">
							<div class="col-lg-12">
								<p>Case Description:</p>
								<h4>{$docinfo['DOC_DESCRIPTION']}</h4>
							</div>
						</div>
						<hr>
						<div class="col-lg-12 ">
							<div class="col-lg-12">
								
								<p>Incoming Files:</p>
								<div id="newattachments" name = "add_attachments" class="dropzone">
								</div>
								
								
								<div class="panel panel-default">
								  <div class="table-responsive">
									  <table class="table table-striped">
										<thead>
											<tr>
												<th>#</th>
												<th>CH-NUM</th>
												<th>Description</th>
												<th>File</th>
												<th>By</th>
												<th>On</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody id="case_data">
										 
										</tbody>
									  </table>
								  </div>
								</div>
							</div>
								 
						</div>
						
					</div>	
				</div>
			
		</div>
		
	</div>
</div>
</div>
</div><!--/.row-->
FORMDETAILS;
$resultset = array(
'returnstatus'=> $returnstatus,
'form'=> $form
);

echo  json_encode($resultset, JSON_HEX_QUOT | JSON_HEX_TAG);
?>


		