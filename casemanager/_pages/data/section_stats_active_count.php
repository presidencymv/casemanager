<?php
require('../../_ex/connections.php');    
require('../../_ex/authentications.php');
$currentuser = new authentication;
$dbc = new dbconnection;
if(isset($_POST['section']) && !empty($_POST['section'])){
    $unit_ID = preg_replace( "/[^0-9$]/","",$_POST['section']);
    if ($unit_ID == ''){
        unset($_SESSION['selectedunitid']);
        unset($_SESSION['selectedunitname']);
        unset($_SESSION['selectedunitactivecount']);
        unset($_SESSION['selectedunitclosedcount']);
        unset($_SESSION['selectedunittotalcount']);
        header('location: dashboard.php?page=sectionstats');
        die();
    }else{
        $_SESSION['selectedunitid'] = $unit_ID;
        $sql = "SELECT DOC_SYSID, O_NAME, DC_NAME, UNIT_NAME, DOC_REFNUMBER, DOC_DESCRIPTION, DEADLINEDATE,CONCAT(DATEDIFF(CURDATE(),DEADLINEDATE), ' DAYS') AS DATE_DIFF , P_NAME, DST_NAME, DOCUMENTDETAILS.U_NAME AS CREATEDBY, ASSIGNED.U_NAME AS ASSIGNEDTO, DOC_ID
                FROM(
                SELECT DOC_SYSID, DOC_ID, O_NAME, DC_NAME, UNIT_NAME, DOC_REFNUMBER, DOC_DESCRIPTION, DATE(DOC_DEADLINE) as DEADLINEDATE, P_NAME, DST_NAME, U_NAME, DOC_ASSIGNEDTO
                FROM document 
                inner join office on DOC_OFFICE = O_ID
                inner join documentcat on DOC_CATEGORY = DC_ID
                inner join unit on DOC_UNIT = UNIT_ID AND DOC_UNIT = ".$_SESSION['selectedunitid']."
                inner join priority on DOC_PRIORITY = P_ID
                inner join doc_status on DST_ID = DOC_STATUS
                inner join user on DOC_CREATEDBY = U_ID
                WHERE DOC_HIDDEN <> 'Y' AND DOC_UNIT IN (".$_SESSION['user_units'].") )as DOCUMENTDETAILS
                LEFT OUTER JOIN USER ASSIGNED ON DOCUMENTDETAILS.DOC_ASSIGNEDTO = ASSIGNED.U_ID
                ORDER BY DOC_ID DESC"; 
        $result = $dbc->dbconn->query($sql);
        $rows = array();
        $active_count = 0;
        $overdue_count = 0;
        $closed_count = 0;
        $total_count = 0;
        while ($cases = mysqli_fetch_assoc($result)){
            $rows[] = $cases;

            $total_count ++;

            if($cases['DST_NAME'] != 'CLOSED'){
                $active_count ++;
                $string_of_date_diff = $cases['DATE_DIFF'];
                if($string_of_date_diff[0] != '-'){
                    $overdue_count ++;
                }
            }else{
                $closed_count ++;
            }

        }
        $result->free();
        $dbc->dbconn->close();
        $_SESSION['selectedunitname'] = '';
        $_SESSION['selectedunitname'] = $rows[0]['UNIT_NAME'];
        $_SESSION['selectedunitactivecount'] = 0;
        $_SESSION['selectedunitactivecount'] = $active_count;
        $_SESSION['unitoverduecount'] = 0;
        $_SESSION['unitoverduecount'] = $overdue_count;
        $_SESSION['selectedunitclosedcount'] = 0;
        $_SESSION['selectedunitclosedcount'] = $closed_count;
        $_SESSION['selectedunittotalcount'] = 0;
        $_SESSION['selectedunittotalcount'] = $total_count;
        header('location: ../../dashboard.php?page=sectionstats');
        die();
    }
}else{
    unset($_SESSION['selectedunitname']);
    unset($_SESSION['selectedunitid']);
    unset($_SESSION['selectedunitactivecount']);
    unset($_SESSION['unitoverduecount']);
    unset($_SESSION['selectedunitclosedcount']);
    unset($_SESSION['selectedunittotalcount']);
    header('location: dashboard.php?page=sectionstats');
    die();
}
?>