<?php
require('../../_ex/connections.php');    
require('../../_ex/authentications.php');
$currentuser = new authentication;
require('../../_ex/master.php');
require('../../_ex/document.php');

$today = date("Y-m-d");        

if (isset($_GET['id'])){
$docid = preg_replace( "/[^0-9$]/","",$_GET['id']);
	if ($docid == ''){
	header('location: dashboard.php');
	die();
	}
}else{
	header('location: dashboard.php');
	die();
}

require($_SERVER['DOCUMENT_ROOT'].'/casemanager/_pages/subcomponents/case_auth_checkstatements.php');

if ($caseflag == FALSE){
	header('location: dashboard.php');
	die();
}


$count = 1;
$att_details = '';

$dbc = new dbconnection;
$sql="SELECT DR_ID, DR_DESC, DR_FILE, DR_FILE_UNIT, U_NAME, DR_ON, DR_OUT_REF,DR_BY,DOC_ASSIGNEDTO,DOC_UNIT,DOC_CREATEDBY,DOC_STATUS,DR_IOR_ID FROM doc_remarks inner join user ON DR_BY = U_ID  inner join document ON DOC_ID = DR_DOC_ID WHERE (DR_FILE <> '' OR DR_FILE <> NULL) AND DR_DOC_ID = ".$docid." ORDER BY DR_ON DESC";
$result = $dbc->dbconn->query($sql);

while ($updates = mysqli_fetch_assoc($result)){

$att_details .= '<tr>
				<th scope="row">'.$count.'</th>
				<td>'.$updates['DR_OUT_REF'].'</td>
				<td>'.$updates['DR_DESC'].'</td>';

$ext = explode('.', $updates['DR_FILE']);
$ext = strtolower(end($ext));


$allowed = array('pdf','png','jpg');

if(in_array($ext,$allowed)){
	$att_details .=	'<td><a href="filedownload.php?download='.$updates['DR_ID'].'">Download</a> | <a href="fileview.php?view='.$updates['DR_ID'].'" target="_blank" title="view">View</a></td>';

}else{
	$att_details .=	'<td><a href="filedownload.php?download='.$updates['DR_ID'].'">Download</a></td>';
}
			

$att_details .= '<td>'.$updates['U_NAME'].'</td>
				<td>'.$updates['DR_ON'].'</td>';

if ($currentuser->attachment_auth($updates['DR_FILE_UNIT'], $updates['DOC_UNIT'],$updates['DOC_ASSIGNEDTO'], $updates['DR_BY'], $updates['DOC_CREATEDBY'], $updates['DOC_STATUS']) == TRUE && $updates['DR_IOR_ID'] == NULL) {
	$att_details .= '<td>
						<div class="btn-group">
						  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Action <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu">
							<li><a href="dashboard.php?page=caseattachments&sf=up&id='.$docid.'&rec='.$updates['DR_ID'].'" >UPDATE</a></li>
							<li><a href="#" onclick = "sendcase('.$docid.','.$updates['DR_ID'].',100)">SENT OUT OF OFFICE</a></li>
							<li><a href="#" onclick = "sendcase('.$docid.','.$updates['DR_ID'].',200)">SENT INTERNALLY</a></li>
						  </ul>
						</div>
						</td>';
	
}else{
	$att_details .= '<td></td>';
}


$att_details .= '</tr>';

$count++;

}
$result->free();
$dbc->dbconn->close();

$details = array(
'att_details'=> $att_details
);
echo  json_encode($details, JSON_HEX_QUOT | JSON_HEX_TAG);
?>
