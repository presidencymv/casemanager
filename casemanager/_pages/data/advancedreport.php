<?php
if ($_SESSION['reports'] != 'Y' && $_SESSION['admins'] != 'Y' &&  $_SESSION['handler'] != 'Y' ){
    header('location: dashboard.php');
    die();
}

if(isset($_POST['sdate']) && isset($_POST['edate'])){
$count = 1;
$dbc = new dbconnection;

$source_con = '';
$sender_con = '';
$receiver_con = '';
	
	
if($_POST['source'] != ''){
	$source_con= " AND DOC_SRC = ".mysqli_real_escape_string($dbc->dbconn,$_POST['source']);
}

if($_POST['sender'] != ''){
	$sender_con= " AND DOC_OFFICE = ".mysqli_real_escape_string($dbc->dbconn,$_POST['sender']);
}

if($_POST['receiver'] != ''){
	$receiver_con= " AND DOC_UNIT = ".mysqli_real_escape_string($dbc->dbconn,$_POST['receiver']);
}
$selection1 = "";
if($_SESSION['admins'] != 'Y'){
$authorized_sections = $currentuser->myunits();
$selection1 = " AND DOC_UNIT IN (".$authorized_sections.")";
}

 if($_POST['or1'] == 'all'){
	$selection2= " AND ((DOC_STATUS <> 4) OR (DOC_STATUS = 4 AND DATE(DOC_STATUSON) BETWEEN '".mysqli_real_escape_string($dbc->dbconn,$_POST['sdate'])."' AND '".mysqli_real_escape_string($dbc->dbconn,$_POST['edate'])."' ))";
 }elseif($_POST['or1'] == 'pending'){
	$selection2= " AND DOC_STATUS <> 4";
 }elseif($_POST['or1'] == 'closed'){
	$selection2= " AND ((DOC_STATUS = 4 AND DATE(DOC_STATUSON) BETWEEN '".mysqli_real_escape_string($dbc->dbconn,$_POST['sdate'])."' AND '".mysqli_real_escape_string($dbc->dbconn,$_POST['edate'])."' ))";
 }else{
	$selection2= " AND ((DOC_STATUS <> 4) OR (DOC_STATUS = 4 AND DATE(DOC_STATUSON)  BETWEEN '".mysqli_real_escape_string($dbc->dbconn,$_POST['sdate'])."' AND '".mysqli_real_escape_string($dbc->dbconn,$_POST['edate'])."' ))";
 }
 


$sql="SELECT DOC_SYSID, O_NAME, DC_NAME, UNIT_NAME, DOC_REFNUMBER, DOC_DESCRIPTION, DEADLINEDATE,CONCAT(DATEDIFF(CURDATE(),DEADLINEDATE), ' DAYS') AS DATE_DIFF , P_NAME, DST_NAME, DOCUMENTDETAILS.U_NAME AS CREATEDBY, ASSIGNED.U_NAME AS ASSIGNEDTO, DOC_LASTUPDATE, DOC_CREATEDON
FROM(
SELECT DOC_SYSID, DOC_ID, O_NAME, DC_NAME, UNIT_NAME, DOC_REFNUMBER, DOC_DESCRIPTION, DATE(DOC_DEADLINE) as DEADLINEDATE, P_NAME, DST_NAME, U_NAME, DOC_ASSIGNEDTO, DOC_LASTUPDATE, DOC_CREATEDON
FROM document 
inner join office on DOC_OFFICE = O_ID
inner join documentcat on DOC_CATEGORY = DC_ID
left outer join unit on DOC_UNIT = UNIT_ID
inner join priority on DOC_PRIORITY = P_ID
inner join doc_status on DST_ID = DOC_STATUS
inner join user on DOC_CREATEDBY = U_ID
WHERE DOC_HIDDEN <> 'Y'".$selection1." AND (DATE(DOC_CREATEDON) BETWEEN '".mysqli_real_escape_string($dbc->dbconn,$_POST['sdate'])."' AND '".mysqli_real_escape_string($dbc->dbconn,$_POST['edate'])."')".$source_con.$sender_con.$receiver_con.$selection2."
AND ((DOC_STATUS <> 4) OR (DOC_STATUS = 4 AND DATE(DOC_STATUSON) BETWEEN '".mysqli_real_escape_string($dbc->dbconn,$_POST['sdate'])."' AND '".mysqli_real_escape_string($dbc->dbconn,$_POST['edate'])."' ))) as DOCUMENTDETAILS
LEFT OUTER JOIN USER ASSIGNED ON DOCUMENTDETAILS.DOC_ASSIGNEDTO = ASSIGNED.U_ID
ORDER BY DOC_ID";



 
$stmt =  $dbc->dbconn->stmt_init();
$stmt->prepare($sql);


$stmt->execute();
$result = $stmt->get_result();
while ($report = $result->fetch_array(MYSQLI_NUM))
{
echo '<tr>
		<th scope="row">'.$count.'</th>
		<td>'.$report[0].'</td>
		<td>'.$report[1].'</td>
		<td>'.$report[4].'</td>
		<td>'.$report[3].'</td>
		<td>'.strip_tags($report[5]).'</td>
		<td>'.$report[2].'</td>
		<td>'.$report[6].'</td>
		<td>'.$report[9].'</td>
		<td>'.$report[11].'</td>
		<td>'.$report[13].'</td>
		<td>'.$report[10].'</td>
		<td>'.$report[12].'</td>
	</tr>';
$count++;
}

$stmt->free_result();
$stmt->close();
$dbc->dbconn->close();
}
?>