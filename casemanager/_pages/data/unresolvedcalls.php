<?php
require('../../_ex/connections.php');    
require('../../_ex/authentications.php');
require('../../_ex/master.php');
require('../../_ex/call.php');

$currentuser = new authentication;
$call = new call;

if ($_SESSION['callcentrestaff'] == 'Y'){
	$dbc = new dbconnection;
	$sql = "SELECT CR_ID, CR_LOGID, CR_REF, CR_CALLER, CR_TEL, CR_ISSUE, CR_ON, CR_P_ID, O_NAME, UNIT_NAME, CR_DOC_ID, CR_SOLVED, CR_CRCAT_ID, CR_PV, U_NAME FROM call_records 
	LEFT OUTER JOIN office ON CR_O_ID = O_ID 
	INNER JOIN unit ON UNIT_ID = CR_UNIT_ID
	INNER JOIN user ON CR_BY = U_ID
	WHERE CR_SOLVED = 0 AND CR_ACTIVE = 'N' AND CR_CRCAT_ID != 4 AND CR_REF IS NULL ORDER BY CR_ID DESC";
	$result = $dbc->dbconn->query($sql);
	$rows = array();
	while ($calls = mysqli_fetch_assoc($result)){
	$calls['ISSUE'] = $call->sneakpeak($calls['CR_ISSUE'],150);
	
	
	if($calls['CR_P_ID'] == 4){
		$calls['CR_LOGID'] = '<span class="glyphicon glyphicon-flag text-danger" title="High Priority"> </span> '.$calls['CR_LOGID'].'<br/><button id="takecallbutton" type="button" class="btn btn-primary" onclick="takecall('."'".$calls['CR_LOGID']."'".')">TAKE CALL</button>';
	}else{
		$calls['CR_LOGID'] = $calls['CR_LOGID'].'<br/><button id="takecallbutton" type="button" class="btn btn-primary btn-xs" onclick="takecall('."'".$calls['CR_LOGID']."'".')"><small>FOLLOW-UP</small></button>';
	}
	

if($calls['CR_DOC_ID'] != NULL){
	//$callupdate = $call->numofcallcaseupdates($calls['CR_ID'],$dbc)." CALL CASE UPDATE(S)";
	//$callcasestatus = $call->callcasestatus($calls['CR_DOC_ID']);
	//$CurrentCaseStatusDesc = '<small class="text-info">STATUS: '.$callcasestatus['DST_NAME'].'</small>';
	$callupdate = '<span class="label label-success">CASE SENT TO SECTION</span>';
}else{
	$callupdate = '<span class="label label-warning">NO CASE CREATED YET</span>';
	//$CurrentCaseStatusDesc = "";
}


if($calls['CR_SOLVED'] != 1){
	$resolvedstate = "UNRESOLVED";
	$attention = '<h1><span class="glyphicon glyphicon-exclamation-sign text-danger"></h1>';
}else{
	$resolvedstate = "RESOLVED";
	$attention = '<h1><span class="glyphicon glyphicon-ok-sign text-success"></h1>';
}

if($calls['CR_PV'] != NULL){
	$pv_number = "<h5>SAP DOC No. : ".$calls['CR_PV']."</h5>";
}else{
	$pv_number = "";
}
	
	
$calls['calldetails'] = <<<EOL
<h5>{$calls['CR_CALLER']}</h5>
<h5>{$calls['CR_TEL']}</h5>
<small>FollowUp Calls</small><br/>
{$call->followup_calls($calls['CR_ID'],1,$dbc)}
EOL;

if($calls['CR_CRCAT_ID'] == 1 || $calls['CR_CRCAT_ID'] == 2){
$calls['issuedetails'] = <<<EOL
<small>TO:{$calls['UNIT_NAME']}</small><br/>
<h5>{$calls['CR_ISSUE']}</h5>
{$pv_number}
<hr>
<small>{$calls['O_NAME']}</small><br/>
<small>ON: {$calls['CR_ON']}</small><br/>
<small class="text-info"><em>{$callupdate}</em></small><br/>
{$CurrentCaseStatusDesc}
<hr>
<small>ATTENDED BY:</small><br/>
{$calls['U_NAME']}
<hr>
<small class="text-info"><em>CASE STATE:{$resolvedstate}</em></small>
{$attention}
EOL;
$calls['detailsURL'] = <<<EOL
<a class="custfinger" onclick="calldetails({$calls['CR_ID']})"><span class="glyphicon glyphicon-open"> </span> OPEN</a>
EOL;

}elseif($calls['CR_CRCAT_ID'] == 3){
$calls['issuedetails'] = '<h4 class="text-danger">MISSED CALL</h4>';
}elseif($calls['CR_CRCAT_ID'] == 4){
$calls['issuedetails'] = '<h4 class="text-danger">CALL CANCELLED DUE TO TIMEOUT</h4>';
}elseif($calls['CR_CRCAT_ID'] == 6){

$calls['issuedetails'] = <<<EOL
<small>TO:{$calls['UNIT_NAME']} (SMS MESSAGE)</small><br/>
<h5>{$calls['CR_ISSUE']}</h5>
{$pv_number}
<hr>
<small>{$calls['O_NAME']}</small><br/>
<small>ON: {$calls['CR_ON']}</small><br/>
<small class="text-info"><em>{$callupdate}</em></small><br/>
{$CurrentCaseStatusDesc}
<hr>
<small>ATTENDED BY:</small><br/>
{$calls['U_NAME']}
<hr>
<small class="text-info"><em>CASE STATE:{$resolvedstate}</em></small>
{$attention}
EOL;
$calls['detailsURL'] = <<<EOL
<a class="custfinger" onclick="calldetails({$calls['CR_ID']})"><span class="glyphicon glyphicon-open"> </span> OPEN</a>
EOL;
}



 $rows[] = $calls;
}
$result->free();
$dbc->dbconn->close();
echo  json_encode($rows, JSON_HEX_QUOT | JSON_HEX_TAG);
} 
?>