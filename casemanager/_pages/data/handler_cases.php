<?php
require('../../_ex/connections.php');    
require('../../_ex/authentications.php');
$currentuser = new authentication;
if ($_SESSION['counter'] == 'Y'){
	$dbc = new dbconnection;
	$sql="SELECT DOC_ID,DOC_SYSID, O_NAME, DC_NAME, DOC_UNIT, UNIT_NAME, IFNULL(DOC_REFNUMBER, 'NO REFERENCE') as REFNUMBER, DOC_DESCRIPTION, DEADLINEDATE, P_NAME, DST_ID, DST_NAME, DOC_STATUS, DOCUMENTDETAILS.U_NAME AS CREATEDBY, IFNULL(ASSIGNED.U_NAME,'NONE') AS ASSIGNEDTO,IFNULL(ASSIGNED.U_ID,'NONE') AS ASSIGNEDID
		FROM(
		SELECT DOC_SYSID, DOC_ID, O_NAME, DC_NAME, DOC_UNIT, UNIT_NAME, DOC_REFNUMBER, DOC_DESCRIPTION, DATE(DOC_DEADLINE) as DEADLINEDATE, P_NAME, DST_ID, DST_NAME, DOC_STATUS, U_NAME, DOC_ASSIGNEDTO
		FROM document 
		inner join office on DOC_OFFICE = O_ID
		inner join documentcat on DOC_CATEGORY = DC_ID
		left outer join unit on DOC_UNIT = UNIT_ID
		inner join priority on DOC_PRIORITY = P_ID
		inner join doc_status on DST_ID = DOC_STATUS
		inner join user on DOC_CREATEDBY = U_ID
		WHERE  DOC_HIDDEN = 'N' AND DOC_UNIT IS NULL ) as DOCUMENTDETAILS 
		LEFT OUTER JOIN USER ASSIGNED ON DOCUMENTDETAILS.DOC_ASSIGNEDTO = ASSIGNED.U_ID
		ORDER BY DOC_ID DESC";
	$result = $dbc->dbconn->query($sql);
	$rows = array();
	while ($cases = mysqli_fetch_assoc($result)){
	 $cases['detailsURL'] = "<a href='dashboard.php?page=casedetails&id=".$cases['DOC_ID']."'>".'<span class="glyphicon glyphicon-open"> </span> '."OPEN</a>";
	 $rows[] = $cases;
	}
	$result->free();
	$dbc->dbconn->close();
	echo  json_encode($rows, JSON_HEX_QUOT | JSON_HEX_TAG);
} 



?>