<?php
header('Content-Type: application/json');
require('../../_ex/connections.php');    
require('../../_ex/authentications.php');
require('../../_ex/master.php');
require('../../_ex/remark.php');
$currentuser = new authentication;
if ($_SESSION['chancellerystaff'] != 'Y'){
    die('UNAUTHORIZED ACCESS');
}


$docid = preg_replace( "/[^0-9$]/","",$_POST['docid']);
$recid = preg_replace( "/[^0-9$]/","",$_POST['recid']);

$remark = new remark;
$remarkinfo = $remark->remarkinfo($recid,$docid);

if($remarkinfo['DR_HARDCOPY'] == 1){
	$checked = "checked";
	
}else{
	$checked = "";
}

$selectedChar = $remarkinfo['DR_DCH_ID'];
$masterdata = new master;
$doccharlist = $masterdata->selectoptionsrequest_selected ('DCH_ID','DCH_NAME','doc_character','',$selectedChar,$emptyfield='N');

if($remarkinfo['DR_CREATION_DATE'] == NULL){
	$date = new DateTime(date('Y-m-d'));
}else{
	$date = new DateTime(date('Y-m-d'));
}

$form = <<<FORMDETAILS
<div class="row">
	<div class="col-lg-12">
		<div class="form-group col-lg-12">
				<h3 class="text-danger">FileName: {$remarkinfo['DR_DESC']}</h3>
		</div>
		<div class="form-group col-lg-12">
			<h4 class="text-info">General Information</h4>
			<div class="form-group col-lg-6">
					<label>Reference</label>
					<input type="text" id = "reference" name = "reference" class="form-control" value="{$remarkinfo['DR_OUT_REF']}">
			</div>
			<div class="form-group col-lg-6">
					<label>Pages</label>
					<input type="number" id = "pages" name = "pages" class="form-control" value="{$remarkinfo['DR_PAGES']}" min=0>
			</div>
			<div class="form-group col-lg-12">
					<label>Details</label>
					<textarea class="form-control" rows="3" id="details" name = "details">{$remarkinfo['DR_DETAILS']}</textarea>
			</div>
		
			<div class="form-group col-lg-12">
				<input type="checkbox" name="hardcopy" id="hardcopy" {$checked}> DOCUMENT HARD COPY AVAILABLE
			</div>
		</div>
		<div class="form-group col-lg-12">
			<h4 class="text-info">Author Information</h4>
			<div class="form-group col-lg-6">
				<label>Author Name</label>
				<input type="text" id = "authorname" name = "authorname" class="form-control" value="{$remarkinfo['DR_AUTHOR']}">
			</div>
			<div class="form-group col-lg-3">
				<label>Tel1</label>
				<input type="text" id = "authortel1" name = "authortel1" class="form-control" value="{$remarkinfo['DR_AUTHOR_TEL1']}">
			</div>
			<div class="form-group col-lg-3">
				<label>Tel2</label>
				<input type="text" id = "authortel2" name = "authortel2" class="form-control" value="{$remarkinfo['DR_AUTHOR_TEL2']}">
			</div>
			<div class="form-group col-lg-12">
				<label>Email</label>
				<input type="email" id = "authoremail" name = "authoremail" class="form-control" value="{$remarkinfo['DR_AUTHOR_EMAIL']}">
			</div>
		</div>

		<div class="form-group col-lg-12">
			<h4 class="text-info">Document Characteristics</h4>
			<div class="form-group col-lg-6">
				<label>Category</label>
				<select class="form-control" name="docchar" id="docchar">
				{$doccharlist}
				</select>
			</div>
			
			<div class="form-group col-lg-6">
				<label>Document Created On</label>
				<input class="form-control" type="date" name="createddate" id="createddate" value = "{$date->format('Y-m-d')}"/>
			</div>
		</div>
		
		
		<div class="form-group col-lg-12">
			<input id="submit-all" type="submit" title="submit" onclick="updateattributes()" value="Update">
		</div>
	</div>
</div>
FORMDETAILS;
$resultset = array(
'form'=> $form
);

echo  json_encode($resultset, JSON_HEX_QUOT | JSON_HEX_TAG);
?>