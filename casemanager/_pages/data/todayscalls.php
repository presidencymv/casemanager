<?php
//header('Content-Type: application/json');
require('../../_ex/connections.php');    
require('../../_ex/authentications.php');
$currentuser = new authentication;


if ($_SESSION['callcentrestaff'] != 'Y'){
    die('UNAUTHORIZED ACCESS');
}


/* <th>#</th>
<th>STATUS</th>
<th>TOKEN</th>
<th>TIME</th>
<th>DURATION</th>
<th>BY</th>
 */

$count = 1;
$att_details = '';
$dbc = new dbconnection;
$sql = "SELECT `CRCAT_NAME` as `STATUS` , `CR_LOGID` as `TOKEN` ,TIME(`CR_ON`) as `TIME`, TIMEDIFF(COALESCE(`CR_END_ON`,NOW()),`CR_ON`) as `DURATION`, `U_NAME` as `STAFF`, CR_SOLVED
FROM `call_records` 
INNER JOIN `call_records_cats` ON CR_CRCAT_ID = CRCAT_ID
INNER JOIN `user`ON `CR_BY` = `U_ID`
WHERE DATE(`CR_ON`) =  CURDATE() ORDER BY CR_ID";
$result = $dbc->dbconn->query($sql);

while ($updates = mysqli_fetch_assoc($result)){

if($updates['STATUS'] == 'ACTIVE'){
	$img = '<img class="greenlight" src="img/greenlight.gif" width="10" height="10"> ';
	
}else{
	$img = '';
	
}


$att_details .= '<tr>
				<th scope="row">'.$count.'</th>
				<td>'.$img.$updates['STATUS'].'</td>
				<td>'.$updates['TOKEN'].'</td>
				<td>'.$updates['TIME'].'</td>
				<td>'.$updates['DURATION'].'</td>
				<td>'.$updates['STAFF'].'</td>';
$att_details .= '</tr>';

$count++;

}
$result->free();
$dbc->dbconn->close();

		
$resultset = array(
	'details'=> $att_details
);
echo  json_encode($resultset, JSON_HEX_QUOT | JSON_HEX_TAG);
?>