<?php
header('Content-Type: application/json');
require('../../_ex/connections.php');    
require('../../_ex/authentications.php');
$currentuser = new authentication;
if ($_SESSION['callcentrestaff'] != 'Y'){
    die('UNAUTHORIZED ACCESS');
}
if (isset($_GET['id'])){
    $CR_ID = preg_replace( "/[^0-9$]/","",$_GET['id']);
}

$sql="SELECT CR_CALLER, CR_TEL, CR_EMAIL, CR_O_ID, CR_UNIT_ID, CR_DC_ID, CR_P_ID, CR_ISSUE, CR_PV, CR_COMPANY FROM call_records WHERE CR_ID = ".$CR_ID;
$dbc = new dbconnection;
$stmt =  $dbc->dbconn->stmt_init();
$stmt->prepare($sql);
$stmt->execute();
$stmt->bind_result($CR_CALLER,$CR_TEL,$CR_EMAIL,$CR_O_ID,$CR_UNIT_ID,$CR_DC_ID,$CR_P_ID,$CR_ISSUE,$CR_PV,$CR_COMPANY);
$stmt->fetch();
$stmt->free_result();
$stmt->close();
$dbc->dbconn->close();

$callinfo = Array( 
		'CR_CALLER'=> $CR_CALLER,
		'CR_TEL'=> $CR_TEL,
		'CR_EMAIL'=> $CR_EMAIL, 
		'CR_O_ID'=> $CR_O_ID,
		'CR_UNIT_ID'=> $CR_UNIT_ID,
		'CR_DC_ID'=> $CR_DC_ID,
		'CR_P_ID'=> $CR_P_ID,
		'CR_ISSUE'=> $CR_ISSUE,
		'CR_PV'=> $CR_PV,
		'CR_COMPANY'=> $CR_COMPANY
		);
echo  json_encode($callinfo, JSON_HEX_QUOT | JSON_HEX_TAG);
?>