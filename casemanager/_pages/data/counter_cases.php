<?php
require('../../_ex/connections.php');    
require('../../_ex/authentications.php');
require('../../_ex/master.php');
$master = new master;
$currentuser = new authentication;
$dbc = new dbconnection;


if($_SESSION['counter'] == 'Y'){
	
	if (in_array($dbc->CounterUnitID,explode(',',$_SESSION['user_units']))){
			//FOR COUNTER STAFFS
			if(in_array($dbc->CounterUnitID,explode(',',$_SESSION['handlerunits']))){
			//COUNTER ADMIN - SEES ALL CREATED BUT UNSENT CASES
			$sql="SELECT DOC_SYSID, DOC_ID, O_NAME, DC_NAME, DOC_UNIT, DOC_REFNUMBER, DOC_DESCRIPTION, DATE(DOC_DEADLINE) as DEADLINEDATE, P_NAME, DST_ID, DST_NAME, DOC_STATUS, U_NAME, DOC_REJECT,P_ID, DOC_SENDER_TYPE, DOC_TXT1, DOC_TXT2
			FROM document 
			left outer join office on DOC_OFFICE = O_ID
			inner join documentcat on DOC_CATEGORY = DC_ID
			inner join priority on DOC_PRIORITY = P_ID
			inner join doc_status on DST_ID = DOC_STATUS
			inner join user on DOC_CREATEDBY = U_ID
			WHERE  DOC_HIDDEN = 'N' AND DOC_UNIT IS NULL 
			ORDER BY DOC_ID DESC";
			$result = $dbc->dbconn->query($sql);
			$rows = array();
			while ($cases = mysqli_fetch_assoc($result)){
				if($cases['P_ID'] == 4){
					$cases['DOC_SYSID'] = '<span class="glyphicon glyphicon-flag text-danger" title="High Priority"> </span> '.$cases['DOC_SYSID'];
				}
				if ($cases['DOC_SENDER_TYPE'] == 0 ){
						$cases['SENDER'] = $cases['O_NAME'].'('.$cases['DOC_TXT1'].'-'.$cases['DOC_TXT2'].')';
				}else{
						$cases['SENDER'] = $cases['DOC_TXT1'].'-'.$cases['DOC_TXT2'];
				}
			$cases['DOC_DESCRIPTION'] = $master->sneakpeak($cases['DOC_DESCRIPTION'],150);
			 $cases['detailsURL'] = "<a href='dashboard.php?page=casedetails&id=".$cases['DOC_ID']."'>".'<span class="glyphicon glyphicon-open"> </span> '."OPEN</a>";
			 $rows[] = $cases;
			}
			$result->free();
			$dbc->dbconn->close();
			echo  json_encode($rows, JSON_HEX_QUOT | JSON_HEX_TAG);
			
			
			}else{
			//CREATOR
			$sql="SELECT DOC_SYSID, DOC_ID, O_NAME, DC_NAME, DOC_UNIT, DOC_REFNUMBER, DOC_DESCRIPTION, DATE(DOC_DEADLINE) as DEADLINEDATE, P_NAME, DST_ID, DST_NAME, DOC_STATUS, U_NAME, DOC_REJECT,P_ID, DOC_SENDER_TYPE, DOC_TXT1, DOC_TXT2
			FROM document 
			left outer join office on DOC_OFFICE = O_ID
			inner join documentcat on DOC_CATEGORY = DC_ID
			inner join priority on DOC_PRIORITY = P_ID
			inner join doc_status on DST_ID = DOC_STATUS
			inner join user on DOC_CREATEDBY = U_ID
			WHERE  DOC_HIDDEN = 'N' AND DOC_UNIT IS NULL
			AND DOC_CREATEDBY IN (".$_SESSION['counterstaffs'].")
			ORDER BY DOC_ID DESC";
	
			$result = $dbc->dbconn->query($sql);
			$rows = array();
			while ($cases = mysqli_fetch_assoc($result)){
			$cases['DOC_DESCRIPTION'] = $master->sneakpeak($cases['DOC_DESCRIPTION'],150);
				if($cases['P_ID'] == 4){
					$cases['DOC_SYSID'] = '<span class="glyphicon glyphicon-flag text-danger" title="High Priority"> </span> '.$cases['DOC_SYSID'];
				}
				if ($cases['DOC_SENDER_TYPE'] == 0 ){
						$cases['SENDER'] = $cases['O_NAME'].'('.$cases['DOC_TXT1'].'-'.$cases['DOC_TXT2'].')';
				}else{
						$cases['SENDER'] = $cases['DOC_TXT1'].'-'.$cases['DOC_TXT2'];
				}
				$cases['detailsURL'] = "<a href='dashboard.php?page=casedetails&id=".$cases['DOC_ID']."'>".'<span class="glyphicon glyphicon-open"> </span> '."OPEN</a>";
				$rows[] = $cases;
			}
			$result->free();
			$dbc->dbconn->close();
			echo  json_encode($rows, JSON_HEX_QUOT | JSON_HEX_TAG);	
			
			
			
			}
	}else{
		$sql="SELECT DOC_SYSID, DOC_ID, O_NAME, DC_NAME, DOC_UNIT, DOC_REFNUMBER, DOC_DESCRIPTION, DATE(DOC_DEADLINE) as DEADLINEDATE, P_NAME, DST_ID, DST_NAME, DOC_STATUS, U_NAME, DOC_REJECT,P_ID, DOC_SENDER_TYPE, DOC_TXT1, DOC_TXT2
			FROM document 
			left outer join office on DOC_OFFICE = O_ID
			inner join documentcat on DOC_CATEGORY = DC_ID
			inner join priority on DOC_PRIORITY = P_ID
			inner join doc_status on DST_ID = DOC_STATUS
			inner join user on DOC_CREATEDBY = U_ID
			WHERE  DOC_HIDDEN = 'N' AND DOC_UNIT IS NULL
			AND DOC_CREATEDBY = ".$_SESSION['userid']."
			ORDER BY DOC_ID DESC";
		$result = $dbc->dbconn->query($sql);
		$rows = array();
		while ($cases = mysqli_fetch_assoc($result)){
		$cases['DOC_DESCRIPTION'] = $master->sneakpeak($cases['DOC_DESCRIPTION'],150);
			if($cases['P_ID'] == 4){
				$cases['DOC_SYSID'] = '<span class="glyphicon glyphicon-flag text-danger" title="High Priority"> </span> '.$cases['DOC_SYSID'];
			}
					if ($cases['DOC_SENDER_TYPE'] == 0 ){
					$cases['SENDER'] = $cases['O_NAME'].'('.$cases['DOC_TXT1'].'-'.$cases['DOC_TXT2'].')';
			}else{
					$cases['SENDER'] = $cases['DOC_TXT1'].'-'.$cases['DOC_TXT2'];
			}
			$cases['detailsURL'] = "<a href='dashboard.php?page=casedetails&id=".$cases['DOC_ID']."'>".'<span class="glyphicon glyphicon-open"> </span> '."OPEN</a>";
			$rows[] = $cases;
		}
		$result->free();
		$dbc->dbconn->close();
		echo  json_encode($rows, JSON_HEX_QUOT | JSON_HEX_TAG);	
	}

	
}

?>