<?php
require('../../_ex/connections.php');    
require('../../_ex/authentications.php');
$currentuser = new authentication;
if ( $_SESSION['admins'] != 'Y'){
	header('location: dashboard.php');
	die();
}
$dbc = new dbconnection;
$sql="SELECT U_ID, U_NAME, U_ACTIVE,U_COUNTER as CASECREATION,U_ADMIN,U_REPORTS,U_SENDALL,U_CREATEDBY,U_TIMESTAMP, U_PHONE FROM user WHERE U_TYPE <> 'local' ORDER BY U_NAME";
$result = $dbc->dbconn->query($sql);
$rows = array();
 $count = 1;
while ($options = mysqli_fetch_assoc($result)){
	if ($options['U_ACTIVE'] == 1){
		
		$options['A_STATUS'] = 'Activated';
	}else{
		$options['A_STATUS'] = 'Deactivated';
		
	}
	$options['COUNT'] = $count;
	$options['URL'] = '<a href="dashboard.php?page=manageuser&sf=up&rec='.$options['U_ID'].'">'.$options['U_NAME'].'</a>';
	$rows[] = $options;
	$count++;
}
$result->free();
$dbc->dbconn->close();
echo  json_encode($rows, JSON_HEX_QUOT | JSON_HEX_TAG);
?>
