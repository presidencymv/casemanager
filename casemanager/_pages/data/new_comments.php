<?php
require('../../_ex/connections.php');    
require('../../_ex/authentications.php');
require('../../_ex/master.php');
require('../../_ex/document.php');

$currentuser = new authentication;    
require ('../subcomponents/case_auth.php');


if (isset($_GET['rnum'])){
$rnum = preg_replace( "/[^0-9$]/","",$_GET['rnum']);


$dbc = new dbconnection;
$sql="SELECT CC_ID,CC_TEXT, U_NAME, CC_BY, CC_ON FROM case_comments INNER JOIN `user` ON CC_BY = U_ID WHERE CC_DOC_ID = ".$docid." AND CC_ID > ".$rnum." ORDER BY CC_ID";
$result = $dbc->dbconn->query($sql);
$rows = array();
while ($options = mysqli_fetch_assoc($result)){
	if($_SESSION['userid'] == $options['CC_BY']){
		$options['commentitem'] = '<li class="list-group-item"><div class="alert alert-success" ><p><b><u>'.$options['U_NAME'] .'</u></b> ('.$options['CC_ON'] .'):</p><p class="alert-link">'.$options['CC_TEXT'] .'</p></div></li>';
	}else{
		$options['commentitem'] = '<li class="list-group-item"><div class="alert alert-warning" ><p><b><u>'.$options['U_NAME'] .'</u></b> ('.$options['CC_ON'] .'):</p><p class="alert-link">'.$options['CC_TEXT'] .'</p></div></li>';
	}
	
	$rows[] = $options;
}
$result->free();
$dbc->dbconn->close();
echo  json_encode($rows, JSON_HEX_QUOT | JSON_HEX_TAG);

}else{
	die();
}
?>