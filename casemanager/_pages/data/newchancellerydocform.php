<?php
header('Content-Type: application/json');
require('../../_ex/connections.php');    
require('../../_ex/authentications.php');
require('../../_ex/master.php');
$currentuser = new authentication;
if ($_SESSION['chancellerystaff'] != 'Y'){
    die('UNAUTHORIZED ACCESS');
}

$masterdata = new master;
$source = $masterdata->selectoptionsrequest('DSrc_ID','DSrc_Name','doc_source','DSrc_ID IN (1,2,3,4,7,8)');
$sender = $masterdata->selectoptionsrequest('O_ID','O_NAME','office','');
$receiver = $masterdata->selectoptionsrequest('UNIT_ID','UNIT_NAME','unit','UNIT_ID NOT IN (33,39)');
$type = $masterdata->selectoptionsrequest('DC_ID','DC_NAME','documentcat','');
$priority = $masterdata->selectoptionsrequest('P_ID','P_NAME','priority','');

$date = new DateTime(date('Y-m-d'));
$dayOfWeek = date('l');
$plus2days = array('Tuesday','Wednesday','Thursday','Friday');
$plus1days = array('Staturday');

if (in_array($dayOfWeek, $plus2days)){
	$date->modify('+5 day');  
}elseif(in_array($dayOfWeek, $plus1days)) {
	$date->modify('+4 day');  
}else{
	$date->modify('+3 day');
}
$today = date('Y-m-d');
$selecteddate = $date->format('Y-m-d');
$form = <<<FORMDETAILS
<div class="row">
		<div class="col-lg-12">
			<div class="form-group col-lg-4">
				<label>Source</label>
				<select required class="form-control" name="source" id="source">
				$source
				</select>
			</div>
			  
			 <div class="form-group col-lg-12">
				<div class="radio">
					<label>
						<input type="radio" name="or1" id="optionsRadios1" value="0" checked="" onclick="showgomform()">GOM
					</label>
					<label>
						<input type="radio" name="or1" id="optionsRadios2" value="1" onclick="showprivateform()">Private
					</label>
				</div>
			</div>
			
			<div id="gom">
				<div class="form-group col-lg-4">
					<label>From:</label>
					<select required class="form-control" name="sender" id="sender">
						$sender
					</select>
				</div>
				<div class="form-group col-lg-4">
					<label>Sent By (Optional)</label>
					<input type="text" id = "byname" name = "byname" class="form-control">
				</div>
				<div class="form-group col-lg-4">
					<label>Designation (Optional)</label>
					<input type="text" id = "designation" name = "designation" class="form-control">
				</div>
			</div>
			<div id="private">
				<div class="form-group col-lg-6">
					<label>Name / Company</label>
					<input type="text" id = "namecompany" name = "namecompany" class="form-control">
				</div>
				<div class="form-group col-lg-6">
					<label>NIC (Optional)</label>
					<input type="text" id = "nic" name = "nic" class="form-control">
				</div>
			</div>
			<div class="form-group col-lg-4">
				<label>To</label>
				<select required class="form-control" name="receiver" id="receiver">
					$receiver
				</select>
			</div>
			
			<div class="form-group col-lg-4">
				<label>Type</label>
				<select required class="form-control" name="type" id="type">
				$type
				</select>
			</div>
			<div class="form-group col-lg-4">
				<label>Reference Number</label>
				<input type="text" id = "reference" name = "reference" class="form-control">
			</div>
			<div class="form-group col-lg-4">
				<label>Description</label>
				<textarea id = "description" name = "description" class="form-control" required></textarea>
			</div>
			<div class="form-group col-lg-4">
				<label>Deadline</label>
				<input id="deadline" class="form-control" type="date" name="deadline" min="$today" value ="$selecteddate" required/>
			</div>
			<div class="form-group col-lg-4">
				<label>Priority</label>
				<select  class="form-control" name="priority" id="priority">
				$priority
				</select>
			</div>
			<div class="form-group col-lg-12">
				<button onclick="clearattachments()">Clear File Attachments</button>
				<div id="dropnewfiles" name = "add_attachments" class="dropzone">
				</div>
			</div>
		
			
			
			<div class="form-group col-lg-12">
				<input type="checkbox" name="exreply" id="exreply" checked> REPLY EXPECTED
			</div>
			<div class="form-group col-lg-12">
				<input type="checkbox" name="hardcopy" id="hardcopy"> HARD COPY
			</div>
			<div class="form-group col-lg-12">
				<input id="submit-all" type="submit" title="submit" onclick="addnewdoc()" value="REGISTER">
			</div>
		</div>
</div>
FORMDETAILS;
$resultset = array(
'form'=> $form
);

echo  json_encode($resultset, JSON_HEX_QUOT | JSON_HEX_TAG);
?>