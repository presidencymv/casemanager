<?php
header('Content-Type: application/json');
require('../../_ex/connections.php');    
require('../../_ex/authentications.php');
require('../../_ex/master.php');
require('../../_ex/sms.php');
$currentuser = new authentication;


if ($_SESSION['callcentrestaff'] != 'Y'){
    die('UNAUTHORIZED ACCESS');
}
if (isset($_GET['id'])){
    $SMS_R_ID = preg_replace( "/[^0-9$]/","",$_GET['id']);
}

$sms = new sms;
$details = $sms->getSMSinfo($SMS_R_ID);
$count = 1;
$form = 	
	'
	<div id = "form" class="row">
		<div class="form-group col-lg-12">
			<label>SMS ID</label>
			<input type="text" id = "efsms_id" name = "efsms_id" class="form-control" readonly="" value="'.$details['SMS_R_ID'].'">
			<input type="hidden" id = "ef_id" name = "ef_id" class="form-control" readonly="" value="'.$sms->formid($details['SMS_R_ID']).'" >
		</div>
		<hr>
		<div class="form-group col-lg-12">
			<div class="form-group col-lg-4">
			<label>Caller Number</label>
			<input type="text" id = "efcallernum" name = "efcallernum" class="form-control" value="'.$details['SMS_R_SENDER_NUMBER'].'">
			</div>
	
			<div class="form-group col-lg-8">
			<label>Full Name</label>
			<input type="text" id = "efcallername" name = "efcallername" class="form-control" value="'.$details['SMS_R_SENDER_NAME'].'">
			</div>
			<div class="form-group col-lg-12">
			<label>Company Name</label>
			<input type="text" id = "companyname" name = "companyname" class="form-control">
			</div>
			<div class="form-group col-lg-12">
			<label>Email</label>
			<input type="email" id = "efcalleremail" name = "efcalleremail" class="form-control">
			</div>
			
			<div class="form-group col-lg-12">
				<label>Category</label>
				<select  class="form-control" name="efdoccat" id="efdoccat">'.
					$sms->selectoptionsrequest_selected('DC_ID','DC_NAME','documentcat','',0)
				.'</select>
			</div>
			
			<div class="form-group col-lg-6">
				<label>From</label>
				<select  class="form-control" name="effrom" id="effrom">'.
					$sms->selectoptionsrequest_selected('O_ID','O_NAME','office','',966)
				.'</select>
			</div>
			
			<div class="form-group col-lg-6">
				<label>To</label>
				<select  class="form-control" name="efto" id="efto">'.
					$sms->selectoptionsrequest_selected('UNIT_ID','UNIT_NAME','unit','',0)
				.'</select>
			</div>
			<div class="form-group col-lg-12">
				<label>PV Number</label>
				<input type="text" id = "pvnumber" name = "pvnumber" class="form-control">
			</div>
			<div class="form-group col-lg-12">
				<label>Issue</label>
				<textarea id = "efissue" name = "efissue" class="form-control" required>'.$details['SMS_R_MESSAGE'].'</textarea>
			</div>
			
			<div class="form-group col-lg-12">
				<label>Response</label>
				<select  class="form-control" name="efresponse" id="efresponse">'.
					$sms->selectoptionsrequest_selected('SMS_S_R_DESC','SMS_S_R_DESC','sms_standard_response','',0)
				.'</select>
				<!--<textarea id = "efresponse" name = "efresponse" class="form-control">NO RESPONSE YET</textarea>--!>
			</div>
			
			<div class="form-group col-lg-12">
				<label>Priority</label>
				<select  class="form-control" name="efpriority" id="efpriority">'.
					$sms->selectoptionsrequest_selected('P_ID','P_NAME','priority','',0)
				.'</select>
			</div>
		</div>
		<hr>
		<div class="form-group col-lg-12 text-center">
			<input class = "btn btn-primary" onclick="createSMSRecord('.$SMS_R_ID.')" value="REGISTER SMS">
		</div>
	</div>
	';

$resultset = array(
'selectedcount'=>$count,
'form'=> $form
);

echo  json_encode($resultset, JSON_HEX_QUOT | JSON_HEX_TAG);
?>