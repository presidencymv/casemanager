<?php
header('Content-Type: application/json');
require('../../_ex/connections.php');    
require('../../_ex/authentications.php');
require('../../_ex/master.php');
require('../../_ex/call.php');
$currentuser = new authentication;
$CR_ID = 0;
if ($_SESSION['callcentrestaff'] != 'Y'){
    die('UNAUTHORIZED ACCESS');
}
if (isset($_GET['ref'])){
    $REF_ID = preg_replace( "/[^A-Z0-9$]/","",$_GET['ref']);
}

$call = new call;
$CR_ID = $call->get_CR_ID($REF_ID);
		
$resultset = array(
	'cr'=> $CR_ID
);
echo  json_encode($resultset, JSON_HEX_QUOT | JSON_HEX_TAG);
?>