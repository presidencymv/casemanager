<?php
require('../../_ex/connections.php');    
require('../../_ex/authentications.php');
require('../../_ex/master.php');
$master = new master;
if (isset($_GET['year'])){
    $selectedyear = preg_replace( "/[^0-9$]/","",$_GET['year']);
}
$currentuser = new authentication;
$loopcount = 1;
	$dbc = new dbconnection;
	$sql = "SELECT DOC_SYSID, O_NAME, DC_NAME, UNIT_NAME, DOC_REFNUMBER, DOC_DESCRIPTION, DEADLINEDATE,CONCAT(DATEDIFF(CURDATE(),DEADLINEDATE), ' DAYS') AS DATE_DIFF , P_NAME, DST_NAME, DOCUMENTDETAILS.U_NAME AS CREATEDBY, ASSIGNED.U_NAME AS ASSIGNEDTO, DOC_ID, P_ID, DOC_SENDER_TYPE, DOC_TXT1, DOC_TXT2,DOC_NAME
                FROM(
                SELECT DOC_SYSID, DOC_ID, O_NAME, DC_NAME, UNIT_NAME, DOC_REFNUMBER, DOC_DESCRIPTION, DATE(DOC_DEADLINE) as DEADLINEDATE, P_NAME, DST_NAME, U_NAME, DOC_ASSIGNEDTO, P_ID, DOC_SENDER_TYPE, DOC_TXT1, DOC_TXT2,DOC_NAME
                FROM document 
                left outer join office on DOC_OFFICE = O_ID
                inner join documentcat on DOC_CATEGORY = DC_ID
                inner join unit on DOC_UNIT = UNIT_ID
                inner join priority on DOC_PRIORITY = P_ID
                inner join doc_status on DST_ID = DOC_STATUS
                inner join user on DOC_CREATEDBY = U_ID
                WHERE DOC_HIDDEN <> 'Y' AND DOC_STATUS <> 4 AND DOC_ASSIGNEDTO = ".$_SESSION['userid']." AND YEAR(DOC_CREATEDON) = ".$selectedyear.") as DOCUMENTDETAILS
                LEFT OUTER JOIN USER ASSIGNED ON DOCUMENTDETAILS.DOC_ASSIGNEDTO = ASSIGNED.U_ID
                ORDER BY DOC_ID DESC"; 
	$result = $dbc->dbconn->query($sql);
	$rows = array();
	while ($cases = mysqli_fetch_assoc($result)){
	$cases['DOC_DESCRIPTION'] = $master->sneakpeak($cases['DOC_DESCRIPTION'],150);
	if($cases['P_ID'] == 4){
		$cases['DOC_SYSID'] = '<span class="glyphicon glyphicon-flag text-danger" title="High Priority"> </span> '.$cases['DOC_SYSID'];
	}
	 $cases['detailsURL'] = "<a href='dashboard.php?page=casedetails&id=".$cases['DOC_ID']."'>".'<span class="glyphicon glyphicon-open"> </span> '."OPEN</a>";
	 $cases['chkbox'] = '<input class="doc_chkboxes" type="checkbox" name="docs[]" value="'.$cases['DOC_ID'].'">';
	 	if ($cases['DOC_SENDER_TYPE'] == 0 ){
			$cases['SENDER'] = $cases['O_NAME'].'('.$cases['DOC_TXT1'].'-'.$cases['DOC_TXT2'].')';
	}else{
			$cases['SENDER'] = $cases['DOC_TXT1'].'-'.$cases['DOC_TXT2'];
	}
	 $rows[] = $cases;
	}
	$result->free();
	$dbc->dbconn->close();
	echo  json_encode($rows, JSON_HEX_QUOT | JSON_HEX_TAG);

?>