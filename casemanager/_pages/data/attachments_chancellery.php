<?php
header('Content-Type: application/json');
require('../../_ex/connections.php');    
require('../../_ex/authentications.php');
$currentuser = new authentication;
require('../../_ex/master.php');
require('../../_ex/document.php');

if ($_SESSION['chancellerystaff'] != 'Y'){
   die("NOT AUTHORIZED");
}


$today = date("Y-m-d");        

if (isset($_GET['id'])){
$docid = preg_replace( "/[^0-9$]/","",$_GET['id']);
	if ($docid == ''){
	header('location: dashboard.php');
	die();
	}
}else{
	header('location: dashboard.php');
	die();
}

$count = 1;
$att_details = '';
$dbc = new dbconnection;
$sql="
SELECT DR_ID, DR_DESC, DR_FILE, DR_FILE_UNIT, U_NAME, DR_ON, DR_OUT_REF,DR_BY,IOR_CHAN_NUM, DR_HARDCOPY
FROM doc_remarks 
inner join user ON DR_BY = U_ID  
inner join in_out_registery ON DR_IOR_ID = IOR_ID
WHERE (DR_FILE <> '' OR DR_FILE <> NULL) 
AND DR_DOC_ID = ".$docid." 
AND DR_IOR_ID IS NOT NULL 
ORDER BY DR_ON DESC";
$result = $dbc->dbconn->query($sql);

while ($updates = mysqli_fetch_assoc($result)){
if ($updates['DR_HARDCOPY'] == 1){
	
	$chancellery_num = $updates['IOR_CHAN_NUM'];
}else{
	$chancellery_num = "-";
	
}


$att_details .= '<tr>
				<th scope="row">'.$count.'</th>
				<td>'.$chancellery_num.'</td>

				<td>'.$updates['DR_OUT_REF'].'</td>
				<td><a href="filedownload.php?download='.$updates['DR_ID'].'">'.$updates['DR_DESC'].'</a></td>
				<td>'.$updates['U_NAME'].'</td>
				<td>'.$updates['DR_ON'].'</td>';

if  ($_SESSION['counter'] == 'Y') {
	//$att_details .= '<td><a href="dashboard.php?page=caseattachments&sf=up&id='.$docid.'&rec='.$updates['DR_ID'].'" data-toggle="modal" data-target="#myModal" >UPDATE</a></td>';
	$att_details .= '<td><button data-toggle="modal" data-target="#myModal" onclick="updatefileattributes('.$docid.','.$updates['DR_ID'].')">UPDATE</button></td>';
}


$att_details .= '</tr>';

$count++;

}
$result->free();
$dbc->dbconn->close();

$details = array(
'att_details'=> $att_details
);
echo  json_encode($details, JSON_HEX_QUOT | JSON_HEX_TAG);
?>
