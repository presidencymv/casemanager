<?php
$userpreferences = $currentuser->getuserpreferences($_SESSION['userid']);
$userpreferences = json_decode($userpreferences, true);
?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">                   
                <div class="row">
                        <ol class="breadcrumb">
                                <li><a href="dashboard.php?page=home"><span class="glyphicon glyphicon-home"></span></a></li>
                                <li class="active">Notification Settings</li>
                        </ol>
                </div><!--/.row-->
                <div class="row">
                        <div class="col-lg-12">
                                <?php include('_pages/subcomponents/msgs.php'); ?>
                        </div>
                </div>
                
                <div class="row">
                        <div class="col-lg-12">
                                <h1 class="page-header">Notification Settings</h1>
                        </div>
                </div><!--/.row-->
                <div class="row">
                        <div class="col-lg-12">
                <form action="managepreferences.php?ex=100&sid=<?php echo $_SESSION['userid'] ?>" name = "savepreferences" enctype="multipart/form-data" method="post" onsubmit='return confirm("Save?")'>
                        <h3>Receive Notifications For</h3>
                        <hr>
                        <div class="radio">
                                <label for="onall">
                                        <input type="radio" class="allon" name="all" id="onall">All Yes
                                </label>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <label for="offall">
                                        <input type="radio" class="alloff" name="all" id="offall">All No
                                </label>
                        </div>
                        <div class="form-group">
                                <div class="table-responsive">
                                        <table class="table table-hover">
                                                <thead>
                                                        <tr>
                                                                <th class="col-lg-2">Option</th>
                                                                <th class="col-lg-8">Description</th>
                                                        </tr>
                                                </thead>
                                                <tbody>
                                                        <?php if($_SESSION['handler'] == 'Y'){ ?>
                                                        <tr>
                                                                <td>
                                                                        <label>Case Routed/Forwarded to Section</label>
                                                                        <div class="radio">
                                                                                <label for="caseRouted1">
                                                                                        <input type="radio" class="on" name="caseRouted" id="caseRouted1" value="1" <?php if($userpreferences['caseRouted']){echo "checked";} ?>>Yes
                                                                                </label>
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <label for="caseRouted0">
                                                                                        <input type="radio" class="off" name="caseRouted" id="caseRouted0" value="0" <?php if(!$userpreferences['caseRouted']){echo "checked";} ?>>No
                                                                                </label>
                                                                        </div>
                                                                </td>
                                                                <td>
                                                                        <p>Would you like to receive notifications when a case is routed/forwarded to your section?</p>
                                                                </td>
                                                        </tr>
                                                        <?php } ?>
                                                        <tr>
                                                                <td>
                                                                        <label>Case Assigned to Me</label>
                                                                        <div class="radio">
                                                                                <label for="caseAssigned1">
                                                                                        <input type="radio" class="on" name="caseAssigned" id="caseAssigned1" value="1" <?php if($userpreferences['caseAssigned']){echo "checked";} ?>>Yes
                                                                                </label>
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <label for="caseAssigned0">
                                                                                        <input type="radio" class="off" name="caseAssigned" id="caseAssigned0" value="0" <?php if(!$userpreferences['caseAssigned']){echo "checked";} ?>>No
                                                                                </label>
                                                                        </div>
                                                                </td>
                                                                <td>
                                                                        <p>Would you like to receive notifications when a case is assigned to you?</p>
                                                                </td>
                                                        </tr>
                                                        <tr>
                                                                <td>
                                                                        <label>Case Shared with Me</label>
                                                                        <div class="radio">
                                                                                <label for="caseSharedWithMe1">
                                                                                        <input type="radio" class="on" name="caseSharedWithMe" id="caseSharedWithMe1" value="1" <?php if($userpreferences['caseSharedWithMe']){echo "checked";} ?>>Yes
                                                                                </label>
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <label for="caseSharedWithMe0">
                                                                                        <input type="radio" class="off" name="caseSharedWithMe" id="caseSharedWithMe0" value="0" <?php if(!$userpreferences['caseSharedWithMe']){echo "checked";} ?>>No
                                                                                </label>
                                                                        </div>
                                                                </td>
                                                                <td>
                                                                        <p>Would you like to receive notifications when a case is shared with you?</p>
                                                                </td>
                                                        </tr>
                                                        <?php if($_SESSION['handler'] == 'Y'){ ?>
                                                        <tr>
                                                                <td>
                                                                        <label>Case in My Section Being Shared</label>
                                                                         <div class="radio">
                                                                                <label for="caseSharedSection1">
                                                                                        <input type="radio" class="on" name="caseSharedSection" id="caseSharedSection1" value="1" <?php if($userpreferences['caseSharedSection']){echo "checked";} ?>>Yes
                                                                                </label>
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <label for="caseSharedSection0">
                                                                                        <input type="radio" class="off" name="caseSharedSection" id="caseSharedSection0" value="0" <?php if(!$userpreferences['caseSharedSection']){echo "checked";} ?>>No
                                                                                </label>
                                                                        </div>
                                                                </td>
                                                                <td>
                                                                        <p>Would you like to receive notifications when a case in your section is shared with a staff?</p>
                                                                </td>
                                                        </tr>
                                                        <?php } ?>
                                                        <tr>
                                                                <td>
                                                                        <label>Case Assigned to Me Being Shared</label>
                                                                        <div class="radio">
                                                                                <label for="caseSharedAssigned1">
                                                                                        <input type="radio" class="on" name="caseSharedAssigned" id="caseSharedAssigned1" value="1" <?php if($userpreferences['caseSharedAssigned']){echo "checked";} ?>>Yes
                                                                                </label>
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <label for="caseSharedAssigned0">
                                                                                        <input type="radio" class="off" name="caseSharedAssigned" id="caseSharedAssigned0" value="0" <?php if(!$userpreferences['caseSharedAssigned']){echo "checked";} ?>>No
                                                                                </label>
                                                                        </div>
                                                                </td>
                                                                <td>
                                                                        <p>Would you like to receive notifications when a case assigned to you is shared with another staff?</p>
                                                                </td>
                                                        </tr>
                                                        <tr>
                                                                <td>
                                                                        <label>Case Shared With Me Being Shared</label>
                                                                        <div class="radio">
                                                                                <label for="caseSharedShared1">
                                                                                        <input type="radio" class="on" name="caseSharedShared" id="caseSharedShared1" value="1" <?php if($userpreferences['caseSharedShared']){echo "checked";} ?>>Yes
                                                                                </label>
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <label for="caseSharedShared0">
                                                                                        <input type="radio" class="off" name="caseSharedShared" id="caseSharedShared0" value="0" <?php if(!$userpreferences['caseSharedShared']){echo "checked";} ?>>No
                                                                                </label>
                                                                        </div>
                                                                </td>
                                                                <td>
                                                                        <p>Would you like to receive notifications when a case shared with you is shared with another staff?</p>
                                                                </td>
                                                        </tr>
                                                        <?php if($_SESSION['handler'] == 'Y'){ ?>
                                                        <tr>
                                                                <td>
                                                                        <label>Comment to Case in My Section</label>
                                                                        <div class="radio">
                                                                                <label for="caseCommentSection1">
                                                                                        <input type="radio" class="on" name="caseCommentSection" id="caseCommentSection1" value="1" <?php if($userpreferences['caseCommentSection']){echo "checked";} ?>>Yes
                                                                                </label>
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <label for="caseCommentSection0">
                                                                                        <input type="radio" class="off" name="caseCommentSection" id="caseCommentSection0" value="0" <?php if(!$userpreferences['caseCommentSection']){echo "checked";} ?>>No
                                                                                </label>
                                                                        </div>
                                                                </td>
                                                                <td>
                                                                        <p>Would you like to receive notifications when someone comments on a case in your section?</p>
                                                                </td>
                                                        </tr>
                                                        <?php } ?>
                                                        <tr>
                                                                <td>
                                                                        <label>Comment to Case Assigned to Me</label>
                                                                        <div class="radio">
                                                                                <label for="caseCommentAssigned1">
                                                                                        <input type="radio" class="on" name="caseCommentAssigned" id="caseCommentAssigned1" value="1" <?php if($userpreferences['caseCommentAssigned']){echo "checked";} ?>>Yes
                                                                                </label>
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <label for="caseCommentAssigned0">
                                                                                        <input type="radio" class="off" name="caseCommentAssigned" id="caseCommentAssigned0" value="0" <?php if(!$userpreferences['caseCommentAssigned']){echo "checked";} ?>>No
                                                                                </label>
                                                                        </div>
                                                                </td>
                                                                <td>
                                                                        <p>Would you like to receive notifications when someone comments on a case assigned to you?</p>
                                                                </td>
                                                        </tr>
                                                        <tr>
                                                                <td>
                                                                        <label>Comment to Case Shared with Me</label>
                                                                        <div class="radio">
                                                                                <label for="caseCommentShared1">
                                                                                        <input type="radio" class="on" name="caseCommentShared" id="caseCommentShared1" value="1" <?php if($userpreferences['caseCommentShared']){echo "checked";} ?>>Yes
                                                                                </label>
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <label for="caseCommentShared0">
                                                                                        <input type="radio" class="off" name="caseCommentShared" id="caseCommentShared0" value="0" <?php if(!$userpreferences['caseCommentShared']){echo "checked";} ?>>No
                                                                                </label>
                                                                        </div>
                                                                </td>
                                                                <td>
                                                                        <p>Would you like to receive notifications when someone comments on a case shared with you?</p>
                                                                </td>
                                                        </tr>
                                                        <?php if($_SESSION['handler'] == 'Y'){ ?>
                                                        <tr>
                                                                <td>
                                                                        <label>Updates/Instructions to Case in My Section</label>
                                                                        <div class="radio">
                                                                                <label for="caseUpdateSection1">
                                                                                        <input type="radio" class="on" name="caseUpdateSection" id="caseUpdateSection1" value="1" <?php if($userpreferences['caseUpdateSection']){echo "checked";} ?>>Yes
                                                                                </label>
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <label for="caseUpdateSection0">
                                                                                        <input type="radio" class="off" name="caseUpdateSection" id="caseUpdateSection0" value="0" <?php if(!$userpreferences['caseUpdateSection']){echo "checked";} ?>>No
                                                                                </label>
                                                                        </div>
                                                                </td>
                                                                <td>
                                                                        <p>Would you like to receive notifications when there are new updates/instructions on a case in your section?</p>
                                                                </td>
                                                        </tr>
                                                        <?php } ?>
                                                        <tr>
                                                                <td>
                                                                        <label>Updates/Instructions on Case Assigned to Me</label>
                                                                        <div class="radio">
                                                                                <label for="caseUpdateAssigned1">
                                                                                        <input type="radio" class="on" name="caseUpdateAssigned" id="caseUpdateAssigned1" value="1" <?php if($userpreferences['caseUpdateAssigned']){echo "checked";} ?>>Yes
                                                                                </label>
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <label for="caseUpdateAssigned0">
                                                                                        <input type="radio" class="off" name="caseUpdateAssigned" id="caseUpdateAssigned0" value="0" <?php if(!$userpreferences['caseUpdateAssigned']){echo "checked";} ?>>No
                                                                                </label>
                                                                        </div>
                                                                </td>
                                                                <td>
                                                                        <p>Would you like to receive notifications when there are new updates/instructions on a case assigned to you?</p>
                                                                </td>
                                                        </tr>
                                                        <tr>
                                                                <td>
                                                                        <label>Updates/Instructions on Case Shared with Me</label>
                                                                        <div class="radio">
                                                                                <label for="caseUpdateShared1">
                                                                                        <input type="radio" class="on" name="caseUpdateShared" id="caseUpdateShared1" value="1" <?php if($userpreferences['caseUpdateShared']){echo "checked";} ?>>Yes
                                                                                </label>
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <label for="caseUpdateShared0">
                                                                                        <input type="radio" class="off" name="caseUpdateShared" id="caseUpdateShared0" value="0" <?php if(!$userpreferences['caseUpdateShared']){echo "checked";} ?>>No
                                                                                </label>
                                                                        </div>
                                                                </td>
                                                                <td>
                                                                        <p>Would you like to receive notifications when there are new updates/instructions on a case shared with you?</p>
                                                                </td>
                                                        </tr>
                                                        <?php if($_SESSION['handler'] == 'Y'){ ?>
                                                        <tr>
                                                                <td>
                                                                        <label>New Attachment to a Case in My Section</label>
                                                                        <div class="radio">
                                                                                <label for="caseNewFileSection1">
                                                                                        <input type="radio" class="on" name="caseNewFileSection" id="caseNewFileSection1" value="1" <?php if($userpreferences['caseNewFileSection']){echo "checked";} ?>>Yes
                                                                                </label>
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <label for="caseNewFileSection0">
                                                                                        <input type="radio" class="off" name="caseNewFileSection" id="caseNewFileSection0" value="0" <?php if(!$userpreferences['caseNewFileSection']){echo "checked";} ?>>No
                                                                                </label>
                                                                        </div>
                                                                </td>
                                                                <td>
                                                                        <p>Would you like to receive notifications when new attachment is available on a case in your section?</p>
                                                                </td>
                                                        </tr>
                                                        <?php } ?>
                                                        <tr>
                                                                <td>
                                                                        <label>New Attachment to a Case Assigned to Me</label>
                                                                        <div class="radio">
                                                                                <label for="caseNewFileAssigned1">
                                                                                        <input type="radio" class="on" name="caseNewFileAssigned" id="caseNewFileAssigned1" value="1" <?php if($userpreferences['caseNewFileAssigned']){echo "checked";} ?>>Yes
                                                                                </label>
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <label for="caseNewFileAssigned0">
                                                                                        <input type="radio" class="off" name="caseNewFileAssigned" id="caseNewFileAssigned0" value="0" <?php if(!$userpreferences['caseNewFileAssigned']){echo "checked";} ?>>No
                                                                                </label>
                                                                        </div>
                                                                </td>
                                                                <td>
                                                                        <p>Would you like to receive notifications when new attachment is available on a case assigned to you?</p>
                                                                </td>
                                                        </tr>
                                                        <tr>
                                                                <td>
                                                                        <label>New Attachment to a Case Shared with Me</label>
                                                                        <div class="radio">
                                                                                <label for="caseNewFileShared1">
                                                                                        <input type="radio" class="on" name="caseNewFileShared" id="caseNewFileShared1" value="1" <?php if($userpreferences['caseNewFileShared']){echo "checked";} ?>>Yes
                                                                                </label>
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <label for="caseNewFileShared0">
                                                                                        <input type="radio" class="off" name="caseNewFileShared" id="caseNewFileShared0" value="0" <?php if(!$userpreferences['caseNewFileShared']){echo "checked";} ?>>No
                                                                                </label>
                                                                        </div>
                                                                </td>
                                                                <td>
                                                                        <p>Would you like to receive notifications when new attachment is available on a case shared with you?</p>
                                                                </td>
                                                        </tr>
                                                        <?php if($_SESSION['handler'] == 'Y'){ ?>
                                                        <tr>
                                                                <td>
                                                                        <label>Attachment Updated in a Case in My Section</label>
                                                                        <div class="radio">
                                                                                <label for="caseFileUpdatedSection1">
                                                                                        <input type="radio" class="on" name="caseFileUpdatedSection" id="caseFileUpdatedSection1" value="1" <?php if($userpreferences['caseFileUpdatedSection']){echo "checked";} ?>>Yes
                                                                                </label>
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <label for="caseFileUpdatedSection0">
                                                                                        <input type="radio" class="off" name="caseFileUpdatedSection" id="caseFileUpdatedSection0" value="0" <?php if(!$userpreferences['caseFileUpdatedSection']){echo "checked";} ?>>No
                                                                                </label>
                                                                        </div>
                                                                </td>
                                                                <td>
                                                                        <p>Would you like to receive notifications when an attachment is updated/modified on a case in your section?</p>
                                                                </td>
                                                        </tr>
                                                        <?php } ?>
                                                        <tr>
                                                                <td>
                                                                        <label>Attachment Updated in a Case Assigned to Me</label>
                                                                        <div class="radio">
                                                                                <label for="caseFileUpdatedAssigned1">
                                                                                        <input type="radio" class="on" name="caseFileUpdatedAssigned" id="caseFileUpdatedAssigned1" value="1" <?php if($userpreferences['caseFileUpdatedAssigned']){echo "checked";} ?>>Yes
                                                                                </label>
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <label for="caseFileUpdatedAssigned0">
                                                                                        <input type="radio" class="off" name="caseFileUpdatedAssigned" id="caseFileUpdatedAssigned0" value="0" <?php if(!$userpreferences['caseFileUpdatedAssigned']){echo "checked";} ?>>No
                                                                                </label>
                                                                        </div>
                                                                </td>
                                                                <td>
                                                                        <p>Would you like to receive notifications when an attachment is updated/modified on a case assigned to you?</p>
                                                                </td>
                                                        </tr>
                                                        <tr>
                                                                <td>
                                                                        <label>Attachment Updated in a Case Shared with Me</label>
                                                                        <div class="radio">
                                                                                <label for="caseFileUpdatedShared1">
                                                                                        <input type="radio" class="on" name="caseFileUpdatedShared" id="caseFileUpdatedShared1" value="1" <?php if($userpreferences['caseFileUpdatedShared']){echo "checked";} ?>>Yes
                                                                                </label>
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <label for="caseFileUpdatedShared0">
                                                                                        <input type="radio" class="off" name="caseFileUpdatedShared" id="caseFileUpdatedShared0" value="0" <?php if(!$userpreferences['caseFileUpdatedShared']){echo "checked";} ?>>No
                                                                                </label>
                                                                        </div>
                                                                </td>
                                                                <td>
                                                                        <p>Would you like to receive notifications when an attachment is updated/modified on a case shared with you?</p>
                                                                </td>
                                                        </tr>
                                                        <?php if($_SESSION['handler'] == 'Y'){ ?>
                                                        <tr>
                                                                <td>
                                                                        <label>Edit Case in a Case in My Section</label>
                                                                        <div class="radio">
                                                                                <label for="caseEditSection1">
                                                                                        <input type="radio" class="on" name="caseEditSection" id="caseEditSection1" value="1" <?php if($userpreferences['caseEditSection']){echo "checked";} ?>>Yes
                                                                                </label>
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <label for="caseEditSection0">
                                                                                        <input type="radio" class="off" name="caseEditSection" id="caseEditSection0" value="0" <?php if(!$userpreferences['caseEditSection']){echo "checked";} ?>>No
                                                                                </label>
                                                                        </div>
                                                                </td>
                                                                <td>
                                                                        <p>Would you like to receive notifications when a case in your section is edited?</p>
                                                                </td>
                                                        </tr>
                                                        <?php } ?>
                                                        <tr>
                                                                <td>
                                                                        <label>Edit Case in a Case Assigned to Me</label>
                                                                        <div class="radio">
                                                                                <label for="caseEditAssigned1">
                                                                                        <input type="radio" class="on" name="caseEditAssigned" id="caseEditAssigned1" value="1" <?php if($userpreferences['caseEditAssigned']){echo "checked";} ?>>Yes
                                                                                </label>
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <label for="caseEditAssigned0">
                                                                                        <input type="radio" class="off" name="caseEditAssigned" id="caseEditAssigned0" value="0" <?php if(!$userpreferences['caseEditAssigned']){echo "checked";} ?>>No
                                                                                </label>
                                                                        </div>
                                                                </td>
                                                                <td>
                                                                        <p>Would you like to receive notifications when a case assigned to you is edited?</p>
                                                                </td>
                                                        </tr>
                                                        <tr>
                                                                <td>
                                                                        <label>Edit Case in a Case Shared with Me</label>
                                                                        <div class="radio">
                                                                                <label for="caseEditShared1">
                                                                                        <input type="radio" class="on" name="caseEditShared" id="caseEditShared1" value="1" <?php if($userpreferences['caseEditShared']){echo "checked";} ?>>Yes
                                                                                </label>
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <label for="caseEditShared0">
                                                                                        <input type="radio" class="off" name="caseEditShared" id="caseEditShared0" value="0" <?php if(!$userpreferences['caseEditShared']){echo "checked";} ?>>No
                                                                                </label>
                                                                        </div>
                                                                </td>
                                                                <td>
                                                                        <p>Would you like to receive notifications when a case shared with you is edited?</p>
                                                                </td>
                                                        </tr>
                                                        <?php if($_SESSION['handler'] == 'Y'){ ?>
                                                        <tr>
                                                                <td>
                                                                        <label>Case Closed in My Section</label>
                                                                        <div class="radio">
                                                                                <label for="caseClosedSection1">
                                                                                        <input type="radio" class="on" name="caseClosedSection" id="caseClosedSection1" value="1" <?php if($userpreferences['caseClosedSection']){echo "checked";} ?>>Yes
                                                                                </label>
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <label for="caseClosedSection0">
                                                                                        <input type="radio" class="off" name="caseClosedSection" id="caseClosedSection0" value="0" <?php if(!$userpreferences['caseClosedSection']){echo "checked";} ?>>No
                                                                                </label>
                                                                        </div>
                                                                </td>
                                                                <td>
                                                                        <p>Would you like to receive notifications when a case in your section is closed?</p>
                                                                </td>
                                                        </tr>
                                                        <?php } ?>
                                                        <tr>
                                                                <td>
                                                                        <label>Case Closed in a Case Shared with Me</label>
                                                                        <div class="radio">
                                                                                <label for="caseClosedShared1">
                                                                                        <input type="radio" class="on" name="caseClosedShared" id="caseClosedShared1" value="1" <?php if($userpreferences['caseClosedShared']){echo "checked";} ?>>Yes
                                                                                </label>
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <label for="caseClosedShared0">
                                                                                        <input type="radio" class="off" name="caseClosedShared" id="caseClosedShared0" value="0" <?php if(!$userpreferences['caseClosedShared']){echo "checked";} ?>>No
                                                                                </label>
                                                                        </div>
                                                                </td>
                                                                <td>
                                                                        <p>Would you like to receive notifications when a case shared with you is closed?</p>
                                                                </td>
                                                        </tr>
                                                        <?php if($_SESSION['handler'] == 'Y'){ ?>
                                                        <tr>
                                                                <td>
                                                                        <label>Case Reopened in My Section</label>
                                                                        <div class="radio">
                                                                                <label for="caseReopenedSection1">
                                                                                        <input type="radio" class="on" name="caseReopenedSection" id="caseReopenedSection1" value="1" <?php if($userpreferences['caseReopenedSection']){echo "checked";} ?>>Yes
                                                                                </label>
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <label for="caseReopenedSection0">
                                                                                        <input type="radio" class="off" name="caseReopenedSection" id="caseReopenedSection0" value="0" <?php if(!$userpreferences['caseReopenedSection']){echo "checked";} ?>>No
                                                                                </label>
                                                                        </div>
                                                                </td>
                                                                <td>
                                                                        <p>Would you like to receive notifications when a case in your section is reopened?</p>
                                                                </td>
                                                        </tr>
                                                        <?php } ?>
                                                        <tr>
                                                                <td>
                                                                        <label>Case Reopened in a Case Shared with Me</label>
                                                                        <div class="radio">
                                                                                <label for="caseReopenedShared1">
                                                                                        <input type="radio" class="on" name="caseReopenedShared" id="caseReopenedShared1" value="1" <?php if($userpreferences['caseReopenedShared']){echo "checked";} ?>>Yes
                                                                                </label>
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <label for="caseReopenedShared0">
                                                                                        <input type="radio" class="off" name="caseReopenedShared" id="caseReopenedShared0" value="0" <?php if(!$userpreferences['caseReopenedShared']){echo "checked";} ?>>No
                                                                                </label>
                                                                        </div>
                                                                </td>
                                                                <td>
                                                                        <p>Would you like to receive notifications when a case shared with you is reopened?</p>
                                                                </td>
                                                        </tr>
                                                        <tr>
                                                                <td>
                                                                        <label>Case Rejected</label>
                                                                        <div class="radio">
                                                                                <label for="caseRejected1">
                                                                                        <input type="radio" class="on" name="caseRejected" id="caseRejected1" value="1" <?php if($userpreferences['caseRejected']){echo "checked";} ?>>Yes
                                                                                </label>
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <label for="caseRejected0">
                                                                                        <input type="radio" class="off" name="caseRejected" id="caseRejected0" value="0" <?php if(!$userpreferences['caseRejected']){echo "checked";} ?>>No
                                                                                </label>
                                                                        </div>
                                                                </td>
                                                                <td>
                                                                        <p>Would you like to receive notifications when a case you created/forwarded is rejected?</p>
                                                                </td>
                                                        </tr>
                                                </tbody>
                                        </table>
                                </div>
                                        </div>
                                        <hr>
                                        <div class="form-group col-lg-12">
                                                <input type="submit" title="submit" value="SAVE">
                                        </div>
                                </form>
                                <script type="text/javascript">
                                        $(function(){
                                            $("input.allon").change(function(){
                                                $("input.on").prop("checked", true);
                                            });
                                            
                                            $("input.alloff").change(function(){
                                                $("input.off").prop("checked", true);
                                            });
                                            
                                            $("input.on, input.off").change(function(){
                                                $("input.allon").prop("checked",
                                                    ($("input.on").length == $("input.on:checked").length));
                                                $("input.alloff").prop("checked",
                                                    ($("input.off").length == $("input.off:checked").length));
                                            });
                                        });
                                </script>
                        </div>
                </div><!--/.row-->
</div>  <!--/.main-->