<?php
require('_ex/units.php');
//REDIRECT INVALID USERS
if ($_SESSION['ministersdirectivestaff'] != 'Y'){
    header('location: dashboard.php');
    die();
}
?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="dashboard.php?page=home"><span class="glyphicon glyphicon-home"></span></a></li>
				<li class="active">Minister's Directives</li>
			</ol>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12">
				<?php include('_pages/subcomponents/msgs.php'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Minister's Directive</h1>
			</div>
		</div><!--/.row-->
		<div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Create New Directive</a></div>
                    <div id="collapse1" class="panel-collapse collapse in">
				    <div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
										<form action="case_creation.php?ex=102" name = "createcaseform" enctype="multipart/form-data" method="post" onsubmit="return submission();">
											<div class="form-group">
												<div class="form-group">
													<div class="form-group col-lg-4">
														<label>Assign to</label>
														<select class="form-control" name="member" id="member">
															<?php include('data/md_unitmembers.php'); ?>
														</select>
													</div>
												</div>
												<div class="form-group col-lg-3">
													<label>Type</label>
													<select required class="form-control" name="type" id="type">
														<option value="28">Letters</option>
														<option value="47">Minister's Directive</option>
													</select>
												</div>
												<div class="form-group col-lg-6">
													<label>Case Name</label>
													<textarea id = "casename" name = "casename" class="form-control" required></textarea>
												</div>
												<div class="form-group col-lg-6">
													<label>Divisions</label>
													<textarea id = "divs" name = "divs" class="form-control" required></textarea>
												</div>
												<div class="form-group col-lg-6">
													<label>Cord. Divisions/Sections</label>
													<textarea id = "cdivs" name = "cdivs" class="form-control"></textarea>
												</div>
												<div class="form-group col-lg-6">
													<label>Cord. Staffs</label>
													<select  class="form-control" name="cstaffs[]" id="cstaffs" multiple>
													<?php
														$masterdata = new master;
														echo $masterdata->selectoptionsrequest('U_ID','U_NAME','user','');
													?>
													</select>
												</div>
												<div class="form-group col-lg-12">
													<label>Directive</label>
													<textarea id = "description" name = "description" class="form-control" required></textarea>
												</div>
												<script>
													CKEDITOR.replace( 'description' );
												</script>
												<div class="form-group col-lg-4">
													<label>Deadline</label>
													<input class="form-control" type="date" name="deadline" min="<?php echo date('Y-m-d');?>" value ="<?php
													$date = new DateTime(date('Y-m-d'));
													$dayOfWeek = date('l');
													$plus2days = array('Tuesday','Wednesday','Thursday','Friday');
													$plus1days = array('Staturday');

													if (in_array($dayOfWeek, $plus2days)){
														$date->modify('+5 day');  
													}elseif(in_array($dayOfWeek, $plus1days)) {
														$date->modify('+4 day');  
													}else{
														$date->modify('+3 day');
													}
													echo $date->format('Y-m-d');
													?>" required/>
												</div>
												<div class="form-group col-lg-4">
													<label>Priority</label>
													<select  class="form-control" name="priority" id="priority"></select>
												</div>
												<div class="form-group col-lg-12">
													<input type="checkbox" name="sms" id="sms"> SEND SMS NOTIFICATION
												</div>
											</div>
											<hr>
											<div class="form-group col-lg-12">
											<input type="submit" title="submit" value="CREATE DIRECTIVE">
											</div>
										</form>
							</div>
						</div><!--/.row-->
					</div>
					</div><!--END OF COLLAPSGROUP-->

					<div class="panel panel-default">
						<div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Directive Cases</a></div>
							<div id="collapse3" class="panel-collapse collapse in">
							<div class="panel-body">
								<table data-toggle="table" data-url="_pages/data/ministersdirectives.php"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="u_id" data-sort-order="desc">
									<thead>
									<tr>
										<th data-field="DOC_SYSID"  data-sortable="true">SYSID:</th>
										<th data-field="DOC_REFNUMBER"  data-sortable="true">REFERENCE:</th>
										<th data-field="DOC_NAME"  data-sortable="true">CASE NAME:</th>
										<th data-field="DOC_DESCRIPTION"  data-sortable="true">DIRECTIVE:</th>
										<th data-field="ASSIGNED"  data-sortable="true">ASSIGNED:</th>
										<th data-field="DOC_DIVISIONS"  data-sortable="true">DIVISIONS:</th>
										<th data-field="CStaffs"  data-sortable="true">CORD. STAFFS:</th>
										<th data-field="DOC_C_DIVISIONS"  data-sortable="true">CORD. DIVISIONS:</th>
										<th data-field="DEADLINEDATE"  data-sortable="true">DEADLINE:</th>
										<th data-field="DST_NAME"  data-sortable="true">STATUS:</th>
										<th data-field="LUpdate"  data-sortable="true">UPDATES:</th>
										<th data-field="detailsURL"  data-sortable="true">ACTION:</th>
									</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
			</div>
					
</div>	<!--/.main-->
<script type="text/javascript">

//type Options
/*         $.ajax({
            url: '_pages/data/type_options.php',
            dataType: 'json',
            success: function (data) {
                var options = data;
                document.getElementById("type").innerHTML += '<option value=""></option>';
                for (var option in options) {
                    document.getElementById("type").innerHTML += '<option value="' + options[option]. DC_ID + '">' + options[option].DC_NAME + '</option>';
                }
            }
        }); */
//priority Options
        $.ajax({
            url: '_pages/data/priority_options.php',
            dataType: 'json',
            success: function (data) {
                var options = data;
                for (var option in options) {
                    document.getElementById("priority").innerHTML += '<option value="' + options[option]. P_ID + '">' + options[option].P_NAME + '</option>';
                }
            }
        });
		
		
        function submission() {
            if (confirm("Create Case and Continue?")) {
				 for (instance in CKEDITOR.instances) {
					CKEDITOR.instances[instance].updateElement();
					}
			                //DEFAULT
                var inputs = ["source", "sender", "type", "description", "deadline", "priority"];
                
				var arrayLength = inputs.length;

                for (var i = 0; i < arrayLength; i++) {
					var x = document.getElementById(inputs[i]).value;
                    if (x == null || x == "") {
                        alert("Please fill the field: " + inputs[i]);
                        document.getElementById(inputs[i]).focus();
                        return false;
						break;
                    }
                }
				
				return true;
            } else {
                return false;
            }
        }
</script>
