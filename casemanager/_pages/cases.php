<?php
//REDIRECT INVALID USERS
if ($_SESSION['counter'] != 'Y'){
    header('location: dashboard.php');
    die();
}
?>
<div class="col-sm-9 col-sm-offset-3 col-lg-11 col-lg-offset-2 main">		
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="dashboard.php?page=home"><span class="glyphicon glyphicon-home"></span></a></li>
				<li class="active">Cases</li>
			</ol>
		</div><!--/.row-->
		<div class="row">
			<?php include('_pages/subcomponents/yearpagination.php');?>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header"><span class="glyphicon glyphicon-folder-close small"></span> New / Rejected Cases</h1>
			</div>
		</div><!--/.row-->
		<div class="row">
			<div class="col-xs-12 col-md-12 col-md-10">
                <div class="panel panel-default">
					<div class="panel-body custom_text">
						<table data-toggle="table" data-url="_pages/data/counter_cases.php"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="u_id" data-sort-order="desc">
							<thead>
							<tr>
								<th data-field="DOC_SYSID"  data-sortable="true">SYSID:</th>
								<th data-field="SENDER"  data-sortable="true">SENDER:</th>
								<th data-field="DOC_REFNUMBER"  data-sortable="true">REFERENCE:</th>
								<th data-field="DOC_DESCRIPTION"  data-sortable="true">DESCRIPTION:</th>
								<th data-field="DC_NAME"  data-sortable="true">CATEGORY:</th>
								<th data-field="DEADLINEDATE"  data-sortable="true">DEADLINE:</th>
								<th data-field="DST_NAME"  data-sortable="true">STATUS:</th>
								<th data-field="DOC_REJECT"  data-sortable="true">REJECTED:</th>
								<th data-field="detailsURL"  data-sortable="true">ACTION:</th>
							</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->

	</div>	<!--/.main-->