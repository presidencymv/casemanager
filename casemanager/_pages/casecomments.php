<?php
require ('_pages/subcomponents/case_auth.php');
require('_ex/contact.php');
?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">	
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="dashboard.php?page=home"><span class="glyphicon glyphicon-home"></span></a></li>
				<li><a href="dashboard.php?page=cases">Cases</a></li>
				<li class="active"> Case: <?php echo $documentinfo['DOC_SYSID'] ?></li>
			</ol>
		</div><!--/.row-->
		<?php
		include('_pages/subcomponents/pageheader.php');
		?>
		<?php
        include('subcomponents/submenu.php');
        ?>
		<div class="row">
			<div class="col-lg-12">
				<?php include('_pages/subcomponents/msgs.php'); ?>
			</div>
		</div>
		
	
		<div class="row">
			<div class="col-lg-8">
			   <div class="panel panel-default">
				    <div class="panel-body">
					    <div class="col-lg-12">
                            <div class="col-lg-12">
                                <h3>Comments</h3>
			                </div>
							<div class="col-lg-12">
								<?php 	
								if($assignedoptions == TRUE || $handleroptions == TRUE || $counteroptions == TRUE || $sharedoptions == TRUE){
                                include ('_pages/subforms/add_comments.php');
								}
								?>
			                </div>
							
							<div class="col-lg-12">
							<ul id="comments" class="list-group">
	
							</ul>
								
							</div>
							
						</div>
				    </div>	
				</div>
			</div>
		</div><!--/.row-->
		
</div>	<!--/.main-->
<script type="text/javascript">
		var runnumber;
        $.ajax({
            url: '_pages/data/initial_comments.php?id=<?php echo $docid;?>',
            dataType: 'json',
            success: function (data) {
                var comments = data;
                for (var comment in comments) {
					runnumber = comments[comment].CC_ID;
					$("#comments").prepend (comments[comment].commentitem);
					
                }
            }
        });
		
		function getnewcomments(){
			   $.ajax({
					url: '_pages/data/new_comments.php?id=<?php echo $docid;?>&rnum='+runnumber,
					dataType: 'json',
					success: function (data) {
						var comments = data;
						for (var comment in comments) {
							runnumber = comments[comment].CC_ID;
							$("#comments").prepend (comments[comment].commentitem);
							
						}
					}
				});

		}
		
		setInterval( "getnewcomments()", 5000 );

</script>
<?php
$update_type = 'casecommentview';
include ('_pages/subcomponents/alert_seen.php');
?>