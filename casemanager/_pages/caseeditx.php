<?php
require ('_pages/subcomponents/case_auth.php');
?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="dashboard.php?page=home"><span class="glyphicon glyphicon-home"></span></a></li>
				<li><a href="dashboard.php?page=cases">Cases</a></li>
				<li class="active"> Case: <?php echo $documentinfo['DOC_SYSID'] ?></li>
			</ol>
		</div><!--/.row-->
		<?php
		include('_pages/subcomponents/pageheader.php');
		?>
		<?php
        include('subcomponents/submenu.php');
        ?>
		<div class="row">
			<div class="col-lg-12">
				<?php include('_pages/subcomponents/msgs.php'); ?>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
					<div class="panel panel-default">
				    <div class="panel-body">
					    <form action="case_update.php?ex=100&id=<?php echo $docid;?>" name = "createcaseform" enctype="multipart/form-data" method="post" onsubmit="return submission();">
                            <div class="form-group">
                                <div class="form-group col-lg-4">
									<label>Source</label>
									<select  class="form-control" name="source" id="source"></select>
								</div>
								<div class="form-group col-lg-4">
									<label>Sender</label>
									<select  class="form-control" name="sender" id="sender"></select>
								</div>
								<div class="form-group col-lg-4">
									<label>Type</label>
									<select  class="form-control" name="type" id="type"></select>
								</div>
								<div class="form-group col-lg-4">
								    <label>Case Reference</label>
                                    <input type="text" id = "reference" name = "reference" class="form-control" value ="<?php echo $documentinfo['DOC_REFNUMBER'] ?>" required>
								</div>
                                <div class="form-group col-lg-12">
								    <label>Case Description</label>
                                    <textarea id = "description" name = "description" class="form-control" required><?php echo $documentinfo['DOC_DESCRIPTION'] ?></textarea>
								</div>
                                <div class="form-group col-lg-4">
								    <label>Deadline</label>
									<input class="form-control" type="date" name="deadline" value ="<?php $date = new DateTime($documentinfo['DEADLINEDATE']); echo $date->format('Y-m-d');;?>" required/>
								</div>
								 <div class="form-group col-lg-4">
									<label>Priority</label>
									<select  class="form-control" name="priority" id="priority"></select>
								</div>
							</div>
                            <hr>
                            <div class="form-group col-lg-12">
                            <input type="submit" title="submit" value="SAVE CHANGES">
                            </div>
                        </form>
						</div>	
				</div>
			</div>
		</div><!--/.row-->
</div>	<!--/.main-->
<script type="text/javascript">
//source Options
        $.ajax({
            url: '_pages/data/source_options.php',
            dataType: 'json',
            success: function (data) {
                var options = data;
				var selectedvalue = '<?php echo $documentinfo['DSrc_ID'];?>';
                for (var option in options) {
					if( options[option].DSrc_ID == selectedvalue){
						document.getElementById("source").innerHTML += '<option value="' + options[option].DSrc_ID + '" selected>' + options[option].DSrc_Name + '</option>';
					}else{
                        document.getElementById("source").innerHTML += '<option value="' + options[option].DSrc_ID + '">' + options[option].DSrc_Name + '</option>';
                    }
                }
            }
        });
//sender Options
        $.ajax({
            url: '_pages/data/sender_options.php',
            dataType: 'json',
            success: function (data) {
                var options = data;
				var selectedvalue = '<?php echo $documentinfo['O_ID'];?>';
                for (var option in options) {
				if( options[option].O_ID == selectedvalue){
				
					document.getElementById("sender").innerHTML += '<option value="' + options[option].O_ID + '" selected>' + options[option].O_NAME + '</option>';
					}else{
                    document.getElementById("sender").innerHTML += '<option value="' + options[option].O_ID + '">' + options[option].O_NAME + '</option>';
					}
                }
            }
        });
//type Options
        $.ajax({
            url: '_pages/data/type_options.php',
            dataType: 'json',
            success: function (data) {
                var options = data;
				var selectedvalue = '<?php echo $documentinfo['DC_ID'];?>';
                for (var option in options) {
				if( options[option].DC_ID == selectedvalue){
				
					document.getElementById("type").innerHTML += '<option value="' + options[option].DC_ID + '" selected>' + options[option].DC_NAME + '</option>';
					}else{
                    document.getElementById("type").innerHTML += '<option value="' + options[option].DC_ID + '">' + options[option].DC_NAME + '</option>';
					}
                }
            }
        });
//priority Options
        $.ajax({
            url: '_pages/data/priority_options.php',
            dataType: 'json',
            success: function (data) {
                var options = data;
				var selectedvalue = '<?php echo $documentinfo['P_ID'];?>';
                for (var option in options) {
					if (options[option]. P_ID != 1){
					if( options[option].P_ID == selectedvalue){
						document.getElementById("priority").innerHTML += '<option value="' + options[option].P_ID + '" selected>' + options[option].P_NAME + '</option>';
					}else{
						document.getElementById("priority").innerHTML += '<option value="' + options[option].P_ID + '">' + options[option].P_NAME + '</option>';
					}
					
					} 
                }
            }
        });
		
		
        function submission() {
            if (confirm("Save Changes?")) {
			                //DEFAULT
                var inputs = ["source", "sender", "type", "reference", "description", "deadline", "priority"];
                
				var arrayLength = inputs.length;

                for (var i = 0; i < arrayLength; i++) {
					var x = document.getElementById(inputs[i]).value;
                    if (x == null || x == "") {
                        alert("Please fill the field: " + inputs[i]);
                        document.getElementById(inputs[i]).focus();
                        return false;
						break;
                    }
                }
				
				return true;
            } else {
                return false;
            }
        }
</script>

