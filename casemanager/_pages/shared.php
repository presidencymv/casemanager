<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="dashboard.php?page=home"><span class="glyphicon glyphicon-home"></span></a></li>
				<li class="active">Shared Cases</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Shared Cases</li>
			</div>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12" style =" min-width:1200px">
                <div class="panel panel-default">
					<div class="panel-body custom_text">
						<?php
						$scope = 'shared';
						include('_pages/subcomponents/case_archiveyears.php'); 
						?>
						
						<?php
						if ($archresults['result'] == TRUE):
						?>
                        <hr>
						<table data-toggle="table" data-url="_pages/data/assigned_shared_cases.php?year=<?php echo $selectedyear ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="u_id" data-sort-order="desc">
							<thead>
							<tr>
								<th data-field="DOC_SYSID"  data-sortable="true">SYSID:</th>
								<th data-field="SENDER"  data-sortable="true">SENDER:</th>
								<th data-field="UNIT_NAME"  data-sortable="true">RECEIVER:</th>
								<th data-field="DOC_REFNUMBER"  data-sortable="true">REFERENCE:</th>
								<th data-field="DOC_NAME"  data-sortable="true">CASE:</th>
								<th data-field="DOC_DESCRIPTION"  data-sortable="true">DESCRIPTION:</th>
								<th data-field="DC_NAME"  data-sortable="true">CATEGORY:</th>
								<th data-field="DEADLINEDATE"  data-sortable="true">DEADLINE:</th>
								<th data-field="overdue"  data-sortable="true">OVERDUE:</th>
								<th data-field="ASSIGNEDTO"  data-sortable="true">ASSIGNED:</th>
								<th data-field="DST_NAME"  data-sortable="true">STATUS:</th>
								<th data-field="detailsURL"  data-sortable="true">ACTION:</th>
							</tr>
							</thead>
						</table>
						<?php
						endif;
						?>
					</div>
				</div>
			</div>
		</div><!--/.row-->
</div>	<!--/.main-->