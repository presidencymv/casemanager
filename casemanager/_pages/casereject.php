<?php
require ('_pages/subcomponents/case_auth.php');
?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="dashboard.php?page=home"><span class="glyphicon glyphicon-home"></span></a></li>
				<li><a href="dashboard.php?page=cases">Cases</a></li>
				<li class="active"> Case: <?php echo $documentinfo['DOC_SYSID'] ?></li>
			</ol>
		</div><!--/.row-->
		<?php
		include('_pages/subcomponents/pageheader.php');
		?>
		<?php
        include('subcomponents/submenu.php');
        ?>
		<div class="row">
			<div class="col-lg-12">
				<?php include('_pages/subcomponents/msgs.php'); ?>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-8">
				<div class="panel panel-default">
				    <div class="panel-body">
					    <form action="case_reject.php?ex=100&id=<?php echo $docid;?>" name = "createcaseform" enctype="multipart/form-data" method="post" onsubmit='return confirm("Reject Case?")'>
                            <div class="form-group">
                                <div class="form-group col-lg-12">
									<label>REASON</label>
									<textarea id = "reason" name = "reason" class="form-control" required></textarea>
								</div>
							</div>
                            <div class="form-group col-lg-12">
                            <input class = "btn btn-primary" type="submit" title="submit" value="REJECT CASE">
                            </div>
                        </form>
					</div>	
				</div>
			</div>
		</div><!--/.row-->

</div>	<!--/.main-->
<script type="text/javascript">
</script>
