<?php
if ($_SESSION['chancellerystaff'] != 'Y'){
    header('location: dashboard.php');
    die();
}

?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="dashboard.php?page=home"><span class="glyphicon glyphicon-home"></span></a></li>
				<li class="active">Chancellery Dashboard</li>
			</ol>
		</div><!--/.row-->
	
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Chancellery Dashboard</li>
			</div>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12">
				<!-- Modal -->
				<div class="modal fade custmcmbackground" id="myModal"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
				  <div class="modal-dialog custdialog" role="document">
					<div class="modal-content custlightbackground">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close" >
						  <span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="myModalLabel"></h4>
					  </div>
					  <div class="modal-body">
						<div id="formloader">
							
						</div>
					  </div>
					</div>
				  </div>
				</div>
			</div>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
                <div class="panel panel-default">
					<div class="panel-body custom_text">
					<div class="col-lg-12">
						
						<button id="newdocbtn" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="newdoc()"><span class="glyphicon glyphicon-plus"></span> New Incoming Document</button>
					
					</div>
						
					</div>
				</div>
			</div>
		</div><!--/.row-->

		<div id="detailsform">
		
		</div>
		
		
		<div class="row">
			<div class="col-lg-12">
                <div class="panel panel-default">
					<div class="panel-body custom_text">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-body custom_text">
							<?php
								$scope = 'chancellery';
								include('_pages/subcomponents/case_archiveyears.php'); 
							?>
							<hr>
							<?php
								if ($archresults['result'] == TRUE):
							?>
						
							<table id="chancellery_active" data-toggle="table" data-url="_pages/data/chancelleryactive.php?year=<?php echo $selectedyear ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="u_id" data-sort-order="desc">
								<thead>
									<tr>
										<th data-field="DOC_SYSID"  data-sortable="true">SYSID:</th>
										<th data-field="SENDER"  data-sortable="true">SENDER:</th>
										<th data-field="UNIT_NAME"  data-sortable="true">RECEIVER:</th>
										<th data-field="DOC_REFNUMBER"  data-sortable="true">REFERENCE:</th>
										<th data-field="DOC_DESCRIPTION"  data-sortable="true">DESCRIPTION:</th>
										<th data-field="DC_NAME"  data-sortable="true">CATEGORY:</th>
										<th data-field="DEADLINEDATE"  data-sortable="true">DEADLINE:</th>
										<th data-field="DATE_DIFF_NEW"  data-sortable="true">OVERDUE:</th>
										<th data-field="DST_NAME"  data-sortable="true">STATUS:</th>
										<th data-field="detailsURL"  data-sortable="true">ACTION:</th>
									</tr>
								</thead>
							</table>
							<?php
								endif;
							?>
							</div>
						</div>
					</div>
						
					</div>
				</div>
			</div>
		</div><!--/.row-->
<script src="js/chancellery.js"></script>
</div>	<!--/.main-->