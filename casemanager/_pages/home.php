<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="dashboard.php?page=home"><span class="glyphicon glyphicon-home"></span></a></li>
				<li class="active">My Active Cases</li>
			</ol>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12">
				<?php include('_pages/subcomponents/msgs.php'); ?>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">My Active Cases</li>
			</div>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12" style =" min-width:1200px">
                <div class="panel panel-default">
					<div class="panel-body custom_text">
						<?php
						$scope = 'my_active';
						include('_pages/subcomponents/case_archiveyears.php'); 
						?>
						<hr>
						<?php
						if ($archresults['result'] == TRUE):
						?>
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getselections()">
						  Work Handover
						</button>
						
						<hr>
						<input id="selectall" type="checkbox" name="selectall" onClick="selectall(this.checked)"> SELECT

						<table id="myassignedtable" data-toggle="table" data-url="_pages/data/assigned_active_cases.php?year=<?php echo $selectedyear ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="u_id" data-sort-order="desc">
							<thead>
							<tr>
							
								<th data-field="chkbox"  data-sortable="false"></th>
								<th data-field="DOC_SYSID"  data-sortable="true">SYSID:</th>
								<th data-field="SENDER"  data-sortable="true">SENDER:</th>
								<th data-field="UNIT_NAME"  data-sortable="true">RECEIVER:</th>
								<th data-field="DOC_REFNUMBER"  data-sortable="true">REFERENCE:</th>
								<th data-field="DOC_NAME"  data-sortable="true">CASE:</th>
								<th data-field="DOC_DESCRIPTION"  data-sortable="true">DESCRIPTION:</th>
								<th data-field="DC_NAME"  data-sortable="true">CATEGORY:</th>
								<th data-field="DEADLINEDATE"  data-sortable="true">DEADLINE:</th>
								<th data-field="DATE_DIFF"  data-sortable="true">OVERDUE:</th>
								<th data-field="ASSIGNEDTO"  data-sortable="true">ASSIGNED:</th>
								<th data-field="DST_NAME"  data-sortable="true">STATUS:</th>
								<th data-field="detailsURL"  data-sortable="true">ACTION:</th>
							</tr>
							</thead>
						</table>
						<?php
						endif;
						?>
					</div>
				</div>
			</div>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<!-- Modal -->
				<div class="modal fade custmcmbackground" id="myModal"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close" >
						  <span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="myModalLabel">WORK HANDOVER</h4>
					  </div>
					  <div class="modal-body">
						<div id="spinner" class="text-center">
						</div>
						<div id="list">
						
						</div>
					  </div>
					</div>
				  </div>
				</div>
			</div>
		</div><!--/.row-->
</div>	<!--/.main-->


<script>
	var $table = $('#myassignedtable');
	var $totalitems;
function getselections(){
var items = [];
$("input[name='docs[]']:checked").each(
	function(){
	items.push($(this).val());
	}
);
$.post('_pages/data/handoverforms.php',
{
	dataset : items.join(',')
},

function(data, status){
	document.getElementById("spinner").style.height='0px';	
	document.getElementById("spinner").style.width='0px';	
	if(data.selectedcount == 0){
		document.getElementById("list").innerHTML='<h4 class="text-center text-info">Please Select A Case :) </h4>';
		setTimeout(function(){
         $('#myModal').modal('hide');
		}, 1000);
	}else{
		document.getElementById("list").innerHTML=data.forms;
		totalitems = data.selectedcount;
	}
		
});
}

function committransfer(formid){
	$.post('case_handover.php?ex=100&id='+formid,
	{
		member : $("#member"+formid).val()
	},
	function(data){
		if(data.commitstatus == 'SUCCESS'){
			$("#cont"+ formid).animate({
				height: '90px'
				});
			document.getElementById("form"+formid).innerHTML='<h4 id="canceltext' + formid + '" class="text-center text-success">' + data.commitresult + '</h4>';
			$("#cont"+ formid).fadeOut( 500 );
			totalitems = totalitems - 1;
			close_modal_if_empty(1500);
		}else{
			$("#cont"+ formid).animate({
				height: '90px'
				});
			document.getElementById("form"+formid).innerHTML='<h4 id="canceltext' + formid + '" class="text-center text-danger">' + data.commitresult + '</h4>';
			$("#canceltext"+ formid).effect( "pulsate", 800 );	
		}
		
			
	});
}


function canceltransfer(formid){
	$("#cont"+ formid).animate({
				height: '90px'
				});
	document.getElementById("form"+formid).innerHTML='<h4 id="canceltext' + formid + '" class="text-center text-danger">CASE HANDOVER CANCELLED</h4>';
	$("#canceltext"+ formid).effect( "pulsate", 300 );
	$("#cont"+ formid).fadeOut( 1000 );
	totalitems = totalitems - 1;
	close_modal_if_empty(2000);
}


function refreshtable(){
	document.getElementById("list").innerHTML="";	
	document.getElementById("spinner").style.height='100px';	
	document.getElementById("spinner").style.width='100px';
	location.reload();
}

function selectall(state){
	if(state == true){
		$('.doc_chkboxes').prop('checked',true); // true
	}
	else
	{
		$('.doc_chkboxes').prop('checked',false); // false
	}
}
$('#myModal').on('hidden.bs.modal', function () {
 refreshtable();
})

function close_modal_if_empty(timeinterval){
	if(totalitems == 0){
		setTimeout(function(){
         $('#myModal').modal('hide');
		}, timeinterval);
	}
}

</script>