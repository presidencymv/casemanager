<?php 
if ( $_SESSION['admins'] != 'Y'){
	header('location: dashboard.php');
	die();
}
?>	

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="dashboard.php?page=home"><span class="glyphicon glyphicon-home"></span></a></li>
				<li class="active">Manage Users</li>
			</ol>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12">
				<?php include('_pages/subcomponents/msgs.php'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Manage Users</h1>
			</div>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12">
			   <div class="panel panel-default">
				    <div class="panel-body">
					    <div class="col-lg-12">
							<div class="col-lg-12">
								<?php 	
								 if (isset($_GET['sf'])){
                                    $sf = preg_replace( "/[^a-z$]/","",$_GET['sf']);
                                    if ($sf == ''){
                                        include ('_pages/subforms/add_user.php');
                                    }else{
                                        include ('_pages/subforms/update_user.php');
                                    }
                                }else{
                                   include ('_pages/subforms/add_user.php');
                                }
								?>
			                </div>
						</div>
				    </div>	
				</div>
			</div>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12" style =" min-width:1200px">
                <div class="panel panel-default">
					<div class="panel-body custom_text">
						<table data-toggle="table" data-url="_pages/data/full_users.php"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="u_id" data-sort-order="desc">
							<thead>
							<tr>
								<th data-field="COUNT"  data-sortable="true">#</th>
								<th data-field="URL"  data-sortable="true">USERNAME:</th>
								<th data-field="U_PHONE"  data-sortable="true">PHONE:</th>
								<th data-field="CASECREATION"  data-sortable="true">CASECREATION:</th>
								<th data-field="U_ADMIN"  data-sortable="true">ADMINISTATION:</th>
								<th data-field="U_REPORTS"  data-sortable="true">REPORTS:</th>
								<th data-field="U_SENDALL"  data-sortable="true">SENDALL:</th>
								<th data-field="A_STATUS"  data-sortable="true">USER STATUS:</th>
								<th data-field="U_TIMESTAMP"  data-sortable="true">LASTUPDATED ON:</th>
							</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->
	
</div>	<!--/.main-->
<script type="text/javascript">

</script>
