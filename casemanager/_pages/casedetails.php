<?php
require ('_pages/subcomponents/case_auth.php');
require('_ex/contact.php');
if($documentinfo['DOC_CR_DOC'] == 1){
require('_ex/call.php');
$call = new call;
$CR_ID = $call->get_crid($documentinfo['DOC_ID']);
$call_details = $call->get_call_info($CR_ID);
}

?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="dashboard.php?page=home"><span class="glyphicon glyphicon-home"></span></a></li>
				<li><a href="dashboard.php?page=cases">Cases</a></li>
				<li class="active"> Case: <?php echo $documentinfo['DOC_SYSID'] ?></li>
			</ol>
		</div><!--/.row-->
		<?php
		include('_pages/subcomponents/pageheader.php');
		?>
		<?php
        include('_pages/subcomponents/submenu.php');
        ?>
		<div class="row">
			<div class="col-lg-12">
				<?php include('_pages/subcomponents/msgs.php'); ?>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-8">
				<?php if ($documentinfo['DOC_NAME'] != '') :?>
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="col-lg-12">
								<div class="col-lg-12">
									<p>CASE NAME::</p>
									<h4><?php echo $documentinfo['DOC_NAME'] ?></h4>
								</div>
							</div>

						
						</div>	
					</div>
				<?php endif?>
				<div class="panel panel-default">
				    <div class="panel-body">
					    <div class="col-lg-4">
                            <div class="col-lg-12">
                                <p>From:</p>
								<?php if ($documentinfo['DOC_SENDER_TYPE'] == 0) :?>
								<h4><?php echo $documentinfo['O_NAME']; ?></h4>
									<?php if ($documentinfo['DOC_TXT1'] != "") :?>
									<p>
										<?php echo $documentinfo['DOC_TXT1']; ?>
									<?php endif?>
									<?php if ($documentinfo['DOC_TXT2'] != "") :?>
										<?php echo " / ".$documentinfo['DOC_TXT2']; ?>
									<?php endif?>
									</p>
								<?php else:?>
								<h4><?php echo $documentinfo['DOC_TXT1']; ?></h4>
									<?php if ($documentinfo['DOC_TXT2'] != "") :?>
										<p>(<?php echo $documentinfo['DOC_TXT2']; ?>)</p>
									<?php endif?>
								<?php endif?>
			                </div>
							<div class="col-lg-12">
                                <p>To:</p>
								<h4><?php echo $documentinfo['UNIT_NAME'] ?></h4>
			                </div>
						</div>
						
						<div class="col-lg-4">
					
							<div class="col-lg-12">
                                <p>Case Priority:</p>
								<h4><?php echo $documentinfo['P_NAME'] ?></h4>
			                </div>
							<div class="col-lg-12">
                                <p>Category:</p>
								<h4><?php echo $documentinfo['DC_NAME'] ?></h4>
			                </div>
						</div>
						
						<div class="col-lg-4">
                            <div class="col-lg-12">
                                <p>Reference No:</p>
								<h4><?php echo $documentinfo['DOC_REFNUMBER'] ?></h4>
			                </div>
						</div>
				    </div>	
				</div>
				
				<?php
				if($documentinfo['DOC_CR_DOC'] == 1 && ($call_details['CR_PV'] != "" || $call_details['CR_COMPANY'] != ""  )):
				?>
				<div class="panel panel-default">
				    <div class="panel-body">
						
					    <div class="col-lg-12">
                                <p>Additional Info:</p>
								<h4>SAP DOC No.: <?php echo $call_details['CR_PV'] ?></h4>
								<h4>Company Name: <?php echo $call_details['CR_COMPANY'] ?></h4>
								
						</div>
				    </div>	
				</div>
				<?php
				endif;
				?>
				
				<?php
				if($documentinfo['DOC_CR_DOC'] == 1 ):
				?>
				<div class="panel panel-default">
				    <div class="panel-body">
					    <div class="col-lg-12">
							<?php
							include ('_pages/subcomponents/casecontacts.php');
							?>
						</div>
				    </div>	
				</div>
				<?php
				endif;
				?>
						
				
				<div class="panel panel-default">
				    <div class="panel-body">
				
					    <div class="col-lg-12">
                                <p>Case Description:</p>
								<h4><?php echo $documentinfo['DOC_DESCRIPTION'] ?></h4>
						<?php
						if($documentinfo['DOC_CR_DOC'] == 1):
						$followupcalls = $call->followup_calls($CR_ID,2);
						?>
						<hr>
						<p>Follow Up Calls:</p>
						<?php echo $followupcalls; ?>
						<?php
						endif;
						?>
						</div>
						<hr>
						<div class="col-lg-12 ">
							<?php
							if((($assignedoptions == TRUE) || ($handleroptions == TRUE) || ($counteroptions == TRUE)) && $documentinfo['DOC_STATUS'] != 4 && $editoptions == TRUE):
							include ('_pages/subforms/add_caseattachments.php');
							endif;
							include ('_pages/subcomponents/caseattachments.php');
							?>	 
						</div>

				    </div>	
				</div>
				
				<div class="panel panel-default">
				    <div class="panel-body">
					    <div class="col-lg-12">
                            <div class="col-lg-12">
                                <h3>Updates / Instructions</h3>
			                </div>
						
							<div class="col-lg-12">
								<?php 	
								if(($assignedoptions == TRUE || $handleroptions == TRUE || $counteroptions == TRUE) && $documentinfo['DOC_STATUS'] != 4 && $editoptions == TRUE){
									include ('_pages/subforms/add_caseupdates.php');
								}
								echo '<hr>';
								include ('_pages/subcomponents/caseupdates.php');
								?>
			                </div>
						</div>
				    </div>	
				</div>
			</div>
			<div class="col-lg-4">
				<?php if ( $assignedoptions == TRUE){ ?>
				<div class="panel panel-default">
				    <div class="panel-body">
						<div class="col-lg-12">
							<?php include ('_pages/subforms/statuschange.php');?>
						</div>
				    </div>	
				</div>
				<?php }?>
				<div class="panel panel-default">
				    <div class="panel-body">
						<div class="col-lg-12">
						<?php if ($documentinfo['DST_NAME'] != 'CLOSED'): ?>
							<p>Today:</p>
							<h2 class = "text-default"><?php echo $today;  ?></h2>
						<?php else: ?>
							<p>Closed:</p>
							<h2 class = "text-default"><?php echo $documentinfo['DOC_STATUSON'];  ?></h2>
						<?php endif; ?>
						</div>
						<div class="col-lg-12">
							<p>Deadline:</p>
							<?php 
							if ($overduestatus == TRUE):
							?>
							<h2 class = "text-danger"><?php echo $documentinfo['DEADLINEDATE'] ?></h2>
							<?php else:?>
							<h2><?php echo $documentinfo['DEADLINEDATE'] ?></h2>
							<?php 
							endif;
							?>
						</div>
						<div class="col-lg-12">
							<p>Case Handled By:</p>
							<h4><?php echo $documentinfo['ASSIGNEDTO'] ?></h4>
						</div>
						<div class="col-lg-12">
							<p>Status:</p>
							<h4><?php echo $documentinfo['DST_NAME'] ?></h4>
						</div>
				    </div>	
				</div>
					
				<?php if($documentinfo['UNIT_NAME'] != ''){ ?>
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="col-lg-12">
							<?php include ('_pages/subforms/follow_case.php'); ?>
						</div>
					</div>
				</div>
				<?php } ?>
				
				<?php if($documentinfo['DOC_UNIT'] == 25){ ?>
				<div class="panel panel-default">
				<div class="panel-body">
				<div class="col-lg-12 ">
					<?php
					if((($assignedoptions == TRUE) || ($handleroptions == TRUE) || ($counteroptions == TRUE)) && $documentinfo['DOC_STATUS'] != 4 && $editoptions == TRUE && $documentinfo['DOC_UNIT'] == 25):
						include ('_pages/subforms/add_amount.php');
					endif;
					
					include ('_pages/subcomponents/amount_details.php');
	
					?>	 
				</div>
				</div>
				</div>
				<?php } ?>
				
			</div>
			
		</div><!--/.row-->
</div>	<!--/.main-->
<script type="text/javascript">
</script>
<?php
$update_type = 'casedetailsview';
include ('_pages/subcomponents/alert_seen.php');
?>