<?php
if ($_SESSION['counter'] != 'Y' ){
	header('location: dashboard.php');
	die();
}
require ('_pages/subcomponents/case_auth.php');
if ($counteroptions != TRUE && $editoptions != TRUE){
	if($documentinfo['UNIT_NAME'] != NULL){
		header('location: dashboard.php');
		die();	
	}
}

?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="dashboard.php?page=home"><span class="glyphicon glyphicon-home"></span></a></li>
				<li><a href="dashboard.php?page=cases">Cases</a></li>
				<li class="active"> Case: <?php echo $documentinfo['DOC_SYSID'] ?></li>
			</ol>
		</div><!--/.row-->
		<?php
		include('_pages/subcomponents/pageheader.php');
		?>
		<?php
        include('subcomponents/submenu.php');
        ?>
		<div class="row">
			<div class="col-lg-12">
				<?php include('_pages/subcomponents/msgs.php'); ?>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
				    <div class="panel-body">
					    <form action="case_send.php?ex=100&id=<?php echo $docid;?>" name = "createcaseform" enctype="multipart/form-data" method="post" onsubmit='return confirm("Send Case?")'>
                            <div class="form-group">
                                <div class="form-group col-lg-4">
									<label>Section/Unit</label>
									<select  class="form-control" name="section" id="section"></select>
								</div>
							</div>
                            <hr>
                            <div class="form-group col-lg-12">
                            <input type="submit" title="submit" value="SEND CASE">
                            </div>
                        </form>
					</div>	
				</div>
			</div>
		</div><!--/.row-->
</div>	<!--/.main-->
<script type="text/javascript">
//source Options
        $.ajax({
            url: '_pages/data/unit_options_for_case_routing.php',
            dataType: 'json',
            success: function (data) {
                var options = data;
                for (var option in options) {
                        document.getElementById("section").innerHTML += '<option value="' + options[option].UNIT_ID + '">' + options[option].UNIT_NAME + '</option>';
                }
            }
        });


</script>
