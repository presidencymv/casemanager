<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">                   
                <div class="row">
                        <ol class="breadcrumb">
                                <li><a href="dashboard.php?page=home"><span class="glyphicon glyphicon-home"></span></a></li>
                                <li class="active">Help</li>
                        </ol>
                </div><!--/.row-->                
                <div class="row">
                        <div class="col-lg-12">
                                <h1 class="page-header">Help</h1>
                        </div>
                </div><!--/.row-->
                <div class="row">
                        <div class="col-lg-12">
                                <h4>If you need help with any of the features or want to know how to use MOFT CASE MANAGER, browse through the below FAQs.</h4>
                                <br>
                                <h4>To search the FAQs, type the keyword you are trying to find help on.</h4>
                                <h4>For example: If you want to know what is "My Active Cases", type "my active cases". Or if you want help on notifications, type "notification"</h4>
                        </div>
                </div><!--/.row-->
                <div class="row">
                        <div class="col-lg-12">
                                <h1 class="page-header">FAQs</h1>
                        </div>
                </div><!--/.row-->
                <div class="row">
                        <div class="col-lg-12">
                                <form action="" class="styled live-search" method="post">
                                        <fieldset class="">
                                                <p>Search:
                                                        <input type="text" class="text-input" id="filter" value="" />
                                                        <span id="filter-count"></span>
                                                </p>
                                        </fieldset>
                                </form>
                                <link href="css/jquery-ui.min.css" rel="stylesheet">
                                <script src='js/jquery-ui.min.js'></script>
                        	<script>
                                        $(function() {
                                                $( "#accordion" ).accordion({
                                                        heightStyle: "content"
                                                });
                                        });
                                </script>
                                <div id="accordion" class="helplist">
                                        <li>
                                                <h3>How do I create a case?</h3>
                                                <div>
                                                        <p>Click "New Case" on the left pane or click <a href="dashboard.php?page=newcase" target="_blank">here</a> to start creating a case.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>What is "Source" in Create New Case</h3>
                                                <div>
                                                        <p>"Source" means from where the case has come from. What is the source/origin of that case/document?</p>
                                                        <p>For example: you received a letter through GEMS, then the source is GEMS. Or, if you received an email and you want to create a case for that, then the source is Email.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>What is "Sender" in Create New Case</h3>
                                                <div>
                                                        <p>"Sender" means the office the case has come from.</p>
                                                        <p>For example: you received a letter from Ministry of Education, then the Sender is Ministry of Education.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>What should I do if the sender office is not in the Sender list?</h3>
                                                <div>
                                                        <p>When creating a case, if the office you want to select from the Sender list is missing, please call Admin section and request to add that office.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>When creating a new case, what is "Type"?</h3>
                                                <div>
                                                        <p>"Type" means what is this case regarding? This case is related to what issue?</p>
                                                        <p>For Example: is this case regarding staff hiring?</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>When creating a new case, what is "Case Reference"?</h3>
                                                <div>
                                                        <p>"Case Reference" is the reference number of the document.</p>
                                                        <p>If there is no reference number, you can either give your own reference number or leave it blank.</p>
                                                        <p>If you leave it blank, a reference number will be generated automatically for you when you save the case.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>When creating a new case, what is "Case Description"?</h3>
                                                <div>
                                                        <p>"Case Description" means a brief explanation of what this case is about.</p>
                                                        <p>For example: if a document has a subject line or regarding what issue it is, then this will be good as Case Description.</p>
                                                        <p>Here you may type in Dhivehi by changing your PC language to Dhivehi.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>When creating a new case, what is "Deadline"?</h3>
                                                <div>
                                                        <p>"Deadline" means the date on which this case has to be completed.</p>
                                                        <p>By default, it is working 3 days.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>When creating a new case, what is "Priority"?</h3>
                                                <div>
                                                        <p>"Priority" means the importance/urgency of this case.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>When creating a new case, what is "DEFAULT" Priority?</h3>
                                                <div>
                                                        <p>"DEFAULT" Priority means the priority given to the selected Sender (office).</p>
                                                        <p>Each office will have a default priority. So if you select default priority when creating a case, then the system will assign the default priority of that office to the case.</p>
                                                        <p>For example: President's Office has a default priority of MEDIUM. If you select DEFAULT priority for a letter from President's Office, then that case will have a Priority of MEDIUM.</p>
                                                        <p>If you are unsure about the default priority of a sender, you can select LOW or MEDIUM or HIGH depending on the urgency of the case.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>How do I save a case?</h3>
                                                <div>
                                                        <p>After you have filled in all the details of the case in "Create New Case" page, click "CREATE CASE" to save the case details.</p>
                                                        <p>Click "OK" when you are prompted whether to "Create Case and Continue".</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>How can I upload case attachment?</h3>
                                                <div>
                                                        <p>1) First open the case.</p>
                                                        <p>2) Click "Attachments" on the top sub-menu as indicated in below picture.</p>
                                                        <p>3) Enter the Description of the attachment.</p>
                                                        <p>4) Click "Browse" to select the file to upload.</p>
                                                        <p>5) Click "SAVE" and you are done.</p>
                                                        <p><img src="img/attachments_submenu.jpg" alt="attachments_submenu"></p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>What kind of files can I attach?</h3>
                                                <div>
                                                        <p>You can attach the following file types:</p>
                                                        <p>pdf, zip, Excel(xls, xlsx), Word(doc, docx), Audio(wav, mp3), Email(msg), Image(jpg, jpeg, gif, png, tif)</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>The file type I want to attach is not supported! What should I do?</h3>
                                                <div>
                                                        <p>Please contact ICT staff.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>How many files can I attach/upload?</h3>
                                                <div>
                                                        <p>There is no limit on the number of files you can attach. But you can save one file at a time.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>Can I update the attachment?</h3>
                                                <div>
                                                        <p>Yes you can.</p>
                                                        <p>You can update the Attachment Description and or the file.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>How can I update the Attachment Description only?</h3>
                                                <div>
                                                        <p>1) First open the case.</p>
                                                        <p>2) Click "Attachments" on the top sub-menu.</p>
                                                        <p>3) From the list of attached files, click "UPDATE" under the "Action" column for the attachment you want to update, as shown in below picture.</p>
                                                        <p><img src="img/update_attachment.jpg" alt="update_attachment"></p>
                                                        <p>4) Enter the new Description as shown in below picture.</p>
                                                        <p><img src="img/update_attachment_description.jpg" alt="update_attachment_description"></p>
                                                        <p>5) Click "UPDATE" and you are done.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>How can I update the Attachment File only?</h3>
                                                <div>
                                                        <p>1) First open the case.</p>
                                                        <p>2) Click "Attachments" on the top sub-menu.</p>
                                                        <p>3) From the list of attached files, click "UPDATE" under the "Action" column for the attachment you want to update, as shown in below picture.</p>
                                                        <p><img src="img/update_attachment.jpg" alt="update_attachment"></p>
                                                        <p>4) Click "Browse" to select the new file as shown in below picture</p>
                                                        <p><img src="img/update_attachment_file.jpg" alt="update_attachment_file"></p>
                                                        <p>5) Click "UPDATE" and you are done.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>Can I update the Attachment Description and Attachment File at the same time?</h3>
                                                <div>
                                                        <p>Yes you can.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>How can I update the Attachment Description and Attachment File at the same time?</h3>
                                                <div>
                                                        <p>1) First open the case.</p>
                                                        <p>2) Click "Attachments" on the top sub-menu.</p>
                                                        <p>3) From the list of attached files, click "UPDATE" under the "Action" column for the attachment you want to update, as shown in below picture.</p>
                                                        <p><img src="img/update_attachment.jpg" alt="update_attachment"></p>
                                                        <p>4) Enter the new Description as shown in below picture.</p>
                                                        <p><img src="img/update_attachment_description.jpg" alt="update_attachment_description"></p>
                                                        <p>5) Click "Browse" to select the new file.</p>
                                                        <p>6) Click "UPDATE" and you are done.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>How can I remove an Attachment?</h3>
                                                <div>
                                                        <p>1) First open the case.</p>
                                                        <p>2) Click "Attachments" on the top sub-menu.</p>
                                                        <p>3) From the list of attached files, click "UPDATE" under the "Action" column for the attachment you want to update, as shown in below picture.</p>
                                                        <p><img src="img/update_attachment.jpg" alt="update_attachment"></p>
                                                        <p>4) Click "REMOVE ATTACHMENT" as shown in below picture.</p>
                                                        <p><img src="img/update_attachment_remove.jpg" alt="update_attachment_remove"></p>
                                                        <p>5) Click "OK" when you are prompted "Are you sure you want to delete this post?"</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>Who can update/remove an attachment?</h3>
                                                <div>
                                                        <p>1) The user who uploaded the attachment.</p>
                                                        <p>2) The section heads of the section from which the attachment was uploaded.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>Who can view/download the attachment?</h3>
                                                <div>
                                                        <p>1) The case assigned staff.</p>
                                                        <p>2) The section heads.</p>
                                                        <p>3) The staff with whom case is shared.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>Who can add an attachment?</h3>
                                                <div>
                                                        <p>1) Any staff when creating a new case, before the case is routed.</p>
                                                        <p>2) The case assigned staff.</p>
                                                        <p>3) The section heads of the case routed section.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>What is "Contacts" on the top sub-menu?</h3>
                                                <div>
                                                        <p>When you open a case, you can see "Contacts" on the sub-menu item as shown in below picture.</p>
                                                        <p><img src="img/case_contacts.jpg" alt="case_contacts"></p>
                                                        <p>"Contacts" refer to specific people we may have to contact regarding this case.</p>
                                                        <p>For example: you received a letter from Ministry of Education. You have created a case for that letter. And you often contact certain specific officials from that ministry regarding this case. It would be good to save their contact details so you or other staff can quickly look this up.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>How can I add a contact to a case?</h3>
                                                <div>
                                                        <p>1) Open the case.</p>
                                                        <p>2) Click "Contacts" on the top sub-menu as shown in below picture.</p>
                                                        <p><img src="img/case_contacts.jpg" alt="case_contacts"></p>
                                                        <p>3) Enter the contact details. Full Name and Tel are required fields. You may leave other fields blank</p>
                                                        <p>4) Click "ADD CONTACT"</p>
                                                        <p>5) Click "OK" when you are prompted "Save?". That's it!</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>Who can add a contact?</h3>
                                                <div>
                                                        <p>1) The staff who created the case before it is routed.</p>
                                                        <p>2) The case assigned staff.</p>
                                                        <p>3) The section heads of the case routed section.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>Who can add a comment to a case?</h3>
                                                <div>
                                                        <p>The staff who created the case before it is routed.</p>
                                                        <p>The case assigned staff.</p>
                                                        <p>3) The section heads of the case routed section.</p>
                                                        <p>The staff with whom the case is shared.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>What is "History" on the top sub-menu?</h3>
                                                <div>
                                                        <p>"History" shows the case history/timeline/log.</p>
                                                        <p>It shows things like when was the case created, who created it etc.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>How do I route/send a new case to a section?</h3>
                                                <div>
                                                        <p>After you create a case and upload the attachments, you must route it to a section. To do this, follow the below steps:</p>
                                                        <p>1) Click "Send Case" on the top sub-menu as shown in below picture.</p>
                                                        <p><img src="img/case_send.jpg" alt="case_send"></p>
                                                        <p>2) Select the appropriate section from the "Section/Unit" list.</p>
                                                        <p>3) Click "SEND CASE".</p>
                                                        <p>4) Click "OK" when you are prompted to "Send Case?". That's it!</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>How do I assign a case to staff?</h3>
                                                <div>
                                                        <p>1) Go to "Unit Active Cases" on the left-pane as shown in below pitcure.</p>
                                                        <p><img src="img/unit_active_cases.jpg" alt="unit_active_cases"></p>
                                                        <p>2) Click "OPEN" under the Action column to select the case you want to assign to staff.</p>
                                                        <p>3) Click "Asign Case" on the top sub-menu as shown in below picture.</p>
                                                        <p><img src="img/case_assign.jpg" alt="case_assign"></p>
                                                        <p>4) Select the staff to assign the case from the Section Members list.</p>
                                                        <p>5) Click "ASSIGN CASE".</p>
                                                        <p>6) Click "OK" when you are prompted to "Send Case?". That's it!</p>

                                                </div>
                                        </li>
                                        <li>
                                                <h3>Who can add Updates/Instructions to a case?</h3>
                                                <div>
                                                        <p>1) The staff who created the case before it is routed.</p>
                                                        <p>2) The case assigned staff.</p>
                                                        <p>3) The section heads of the case routed section.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>Who can view Updates/Instructions in a case?</h3>
                                                <div>
                                                        <p>1) The staff who created the case before it is routed.</p>
                                                        <p>2) The case assigned staff.</p>
                                                        <p>3) The section heads of the case routed section.</p>
                                                        <p>4) The staff with whom the case is shared.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>When should I "Reject Case"?</h3>
                                                <div>
                                                        <p>Section head can reject a case when a case that does not belong to thier section is routed to them.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>What happens when I "Reject Case"?</h3>
                                                <div>
                                                        <p>The case will go back to the staff who created and routed it.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>How do I "Reject Case"?</h3>
                                                <div>
                                                        <p>1) Open the case.</p>
                                                        <p>2) Click "Reject Case" on the top sub-menu as shown in below picture.</p>
                                                        <p><img src="img/case_reject.jpg" alt="case_reject"></p>
                                                        <p>3) Enter the REASON for rejecting the case.</p>
                                                        <p>4) Click "REJECT CASE"</p>
                                                        <p>5) Click "OK" when you are prompted "Reject Case?". That's it!</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>How do I "Share Case"?</h3>
                                                <div>
                                                        <p>1) Open the case.</p>
                                                        <p>2) Click "Share Case" on the top sub-menu as shown in below picture.</p>
                                                        <p><img src="img/case_share.jpg" alt="case_share"></p>
                                                        <p>3) Select the staff with whom you want to share the case.</p>
                                                        <p>4) Click "SHARE CASE"</p>
                                                        <p>5) Click "OK" when you are prompted "Send Case?". That's it!</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>What happens when I "Share Case"?</h3>
                                                <div>
                                                        <p>Access to view the case will be given to the staff with whom the case was shared.</p>
                                                        <p>The staff with whom the case was shared can view the case info, attachments, contacts, make comments and view case history.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>Can I share the case with more than one staff?</h3>
                                                <div>
                                                        <p>Yes, you can share the case with as many staff as you want.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>How do I stop sharing a case? How do I remove share?</h3>
                                                <div>
                                                        <p>1) Open the case.</p>
                                                        <p>2) Click "Share Case" on the top sub-menu as shown in below picture.</p>
                                                        <p><img src="img/case_share_remove.jpg" alt="case_share_remove"></p>
                                                        <p>3) Click "REMOVE" for the staff you want to stop sharing the case with. That's it!</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>What is "Forward Case"?</h3>
                                                <div>
                                                        <p>"Forward Case" enables you to send/forward a case to another section after you have completed the work required on the case from you section.</p>
                                                        <p>For example: There is a case that is handled by your section. But after completing some work on the case, it has to be sent to another section. So you will forward the case to that section. That section will do their work on the case. If required, that section can forward it back to your section after their work is done.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>How do I "Forward Case"?</h3>
                                                <div>
                                                        <p>1) Open the case.</p>
                                                        <p>2) Click "Forward Case" on the top sub-menu as shown in below picture.</p>
                                                        <p><img src="img/case_forward.jpg" alt="case_forward"></p>
                                                        <p>3) Select the Section/Unit you want to forward the case.</p>
                                                        <p>4) Click "FORWARD CASE"</p>
                                                        <p>5) Click "OK" when you are prompted "Send Case?". That's it!</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>I have forwarded a case and I no longer have access to that case! What should I do?</h3>
                                                <div>
                                                        <p>When you forward a case, it will be sent to the forwarded section. Therefore, you will not have access to it!</p>
                                                        <p>You can either share the case with yourself before forwarding it or request the section head (of the section you forwarded the case to) to share it with you.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>Can I forward a forwarded case?</h3>
                                                <div>
                                                        <p>Yes, you can!</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>What is "Edit Case"?</h3>
                                                <div>
                                                        <p>"Edit Case" enables you to edit the case details.</p>
                                                        <p>For example: the deadline for the case is not correct. You can edit the case to update the deadline.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>How do I "Edit Case"?</h3>
                                                <div>
                                                        <p>1) Open the case.</p>
                                                        <p>2) Click "Edit Case" on the top sub-menu as shown in below picture.</p>
                                                        <p><img src="img/case_edit.jpg" alt="case_edit"></p>
                                                        <p>3) Enter the correct details as required.</p>
                                                        <p>4) Click "SAVE CHANGES"</p>
                                                        <p>5) Click "OK" when you are prompted "Save Changes?". That's it!</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>What is "My Active Cases"?</h3>
                                                <div>
                                                        <p>It shows you a list of cases assigned to you that are ongoing.</p>
                                                        <p>The number in the round bracket shows the total count of cases assigned to you that are ongoing.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>What is "My Closed Cases"?</h3>
                                                <div>
                                                        <p>It shows you a list of cases assigned to you that has been closed.</p>
                                                        <p>The number in the round bracket shows the total count of cases assigned to you that has been closed.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>What is "Shared Cases"?</h3>
                                                <div>
                                                        <p>It shows you a list of cases shared with you.</p>
                                                        <p>The number in the round bracket shows the total count of cases shared with you.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>What is "Status Check"?</h3>
                                                <div>
                                                        <p>It shows you a list of cases in your section that has been assigned to a staff.</p>
                                                        <p>It enables you to search for a case handled by a staff in your section and see its status.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>What is "Main Directory"?</h3>
                                                <div>
                                                        <p>It shows you a list of all the cases.</p>
                                                        <p>It enables you to search for a case handled and see whihc section (RECEIVER) is handling it.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>What is "Unit Active Cases"?</h3>
                                                <div>
                                                        <p>It shows you a list of ongoing/open cases in your section.</p>
                                                        <p>The number in the round bracket shows the total count of ongoing cases in your section.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>What is "Unit Closed Cases"?</h3>
                                                <div>
                                                        <p>It shows you a list of closed cases in your section.</p>
                                                        <p>The number in the round bracket shows the total count of closed cases in your section.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>What is "Unit Stats"?</h3>
                                                <div>
                                                        <p>It allows you to see statistics of a section and also staff.</p>
                                                        <p>Stats inlcude how many active cases, how many overdue cases, how many closed cases etc.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>What is "Reports"?</h3>
                                                <div>
                                                        <p>It allows you generate reports and export those reports to an Excel file.</p>
                                                        <p>Basic Report allows you to generate report for a given date. For example: on Report Date enter 2015/12/10, select PENDING and click "Generate Report" in order to get all the pending cases on 10 Dec 2015.</p>
                                                        <p>Advanced Report allows you to generate reports based on more detailed selections like a date range, from a particular office, for a particular section etc.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>What is "New Case"?</h3>
                                                <div>
                                                        <p>It is where you go to create a new case.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>What is "New / Rejected Cases"?</h3>
                                                <div>
                                                        <p>It shows you a list of the following two:</p>
                                                        <p>The number in the round bracket shows the total count of the below two types of cases.</p>
                                                        <p>1) Cases you have created, but not yet routed.</p>
                                                        <p>2) Cases you have created/forwarded, but has been rejected.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>What is "NOTIFICATIONS"?</h3>
                                                <div>
                                                        <p>It shows you your 5 most recent unread notifications and a link to see a list of all unread notifications if it is more than 5.</p>
                                                        <p>The red circle/oval shows you the count of unread notifications.</p>
                                                        <p>If you have no unread notifications, the red circle/oval will not be visible.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>How can I turn on/off notifications? How can I enable/disable notifications?</h3>
                                                <div>
                                                        <p>1) Click your username on the top right corner as shown in below picture:</p>
                                                        <p><img src="img/username.jpg" alt="username"></p>
                                                        <p>2) Click "Notification Settings" from the dropdown as shown in below picture:</p>
                                                        <p><img src="img/notification_settings.jpg" alt="notification_settings"></p>
                                                        <p>3) You can select "All Yes" if you want to turn on all notifications. Or you can select "All No" if you want to turn off all notifications. Or you can turn on or off notifications for particular option by selecting Yes or No.</p>
                                                        <p>4) Click "SAVE".</p>
                                                        <p>5) Click "OK" when you are prompted "Save?". That's it!</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>What is "FOLLOW"?</h3>
                                                <div>
                                                        <p>It enables you to get notifications for updates on that particular case, regardless of your notification settings.</p>
                                                        <p>If you are a section head, when you open a case (Case Info page), you will see a button labelled "FOLLOW" as shown in below picture:</p>
                                                        <p><img src="img/case_follow.jpg" alt="case_follow"></p>
                                                        <p>If you are following a case, then you will get notifications for that case, regardless of your notification settings.</p>
                                                        <p>Example: you may have turned off notifications for "Comment to Case in My Section" because you do not wish to get notification whenever someone comments on a case in your section. But you want to receive notification a specific case in your section. Then you should follow that case!</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>How do I "FOLLOW" a case?</h3>
                                                <div>
                                                        <p>Simply click the "FOLLOW" button on the "Case Info" page. When prompted "Save?", click "OK". That's it!</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>What is the main use of "FOLLOW"?</h3>
                                                <div>
                                                        <p>Imagine you are a section head. There are other section heads in your section as well. You do not wish to get notification when a case is routed to your section, or when something happens to a case in your section like case being shared, comment on case etc.</p>
                                                        <br>
                                                        <p>But then there is this one case you want to be up to date with. You want to know what is happening to that case. Well, just "FOLLOW" the case. You will get notifications whenever there is an update on that case!</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>Who can "FOLLOW" a case?</h3>
                                                <div>
                                                        <p>Only section heads can "FOLLOW" a case.</p>
                                                        <p>And only cases in your section. You cannot follow a case that is not in your section, even if you are a section head.</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>How do I log out?</h3>
                                                <div>
                                                        <p>1) Click your username on the top right hand corner.</p>
                                                        <p>2) From the dropdown list, click "Logout". That's it!</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>Describe to me in one sentence how Case Manager works? What is MOFT CASE MANAGER?</h3>
                                                <div>
                                                        <p>You create a case, upload attachments, route it to appropriate section, section head assigns case to staff, staff updates the case as updates happen, staff closes the case when it is finished!</p>
                                                </div>
                                        </li>
                                        <li>
                                                <h3>What else can I do with Case Manager?</h3>
                                                <div>
                                                        <p>You can:
                                                                <p>1) Add case contacts</p>
                                                                <p>2) Comment on case</p>
                                                                <p>3) See case history</p>
                                                                <p>4) Get notified when a case is routed to your section or assigned to you or shared with you or case updated</p>
                                                                <p>5) Quickly check the status of a case</p>
                                                                <p>6) Generate daily and advanced reports</p>
                                                        </p>
                                                </div>
                                        </li>
                                </div>
                        </div>
                </div><!--/.row-->
                <div class="row">
                        <div class="col-lg-12">
                                <p></p>
                        </div>
                </div><!--/.row-->
</div>  <!--/.main-->
<script>
$(document).ready(function(){
    $("#filter").keyup(function(){
 
        // Retrieve the input field text and reset the count to zero
        var filter = $(this).val(), count = 0;
 
        // Loop through the comment list
        $(".helplist li").each(function(){
 
            // If the list item does not contain the text phrase fade it out
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).fadeOut();
 
            // Show the list item if the phrase matches and increase the count by 1
            } else {
                $(this).show();
                count++;
            }
        });
 
        // Update the count
        var numberItems = count;
        $("#filter-count").text("Number of results: "+count);
    });
});
</script>