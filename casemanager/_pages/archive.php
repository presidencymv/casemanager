<?php
if ($_SESSION['chancellerystaff'] != 'Y'){
    header('location: dashboard.php');
    die();
}
require('_ex/chancellery.php');

?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="dashboard.php?page=home"><span class="glyphicon glyphicon-home"></span></a></li>
				<li class="active">Chancellery Archive</li>
			</ol>
		</div><!--/.row-->
		<?php
        include('_pages/subcomponents/chancellery_submenu.php');
        ?>
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Chancellery Archive</li>
			</div>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12" style =" min-width:1200px">
                <div class="panel panel-default">
					<div class="panel-body custom_text">
						<?php
						$scope = 'chancelleryarchive';
						include('_pages/subcomponents/case_archiveyears.php'); 
						?>
						<hr>
						<?php
						if ($archresults['result'] == TRUE):
						?>
						<table data-toggle="table" data-url="_pages/data/chancelleryarchive.php?year=<?php echo $selectedyear ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="u_id" data-sort-order="desc">
							<thead>
							<tr>
								<th data-field="DOC_SYSID"  data-sortable="true">SYSID:</th>
								<th data-field="DOC_REFNUMBER" data-sortable="true">REFERENCE:</th>
								<th data-field="O_NAME"  data-sortable="true">FROM:</th>
								<th data-field="UNIT_NAME"  data-sortable="true">TO:</th>
								<th data-field="DST_NAME"  data-sortable="true">STATUS:</th>
								<th data-field="ASSIGNED_USER"  data-sortable="true">ASSIGNED TO:</th>
								<th data-field="CREATEDBY"  data-sortable="true">ENTERED BY:</th>
								<th data-field="ENTERED DATE"  data-sortable="true">RECEIVED ON:</th>
							</tr>
							</thead>
						</table>
						<?php
						endif;
						?>
					</div>
				</div>
			</div>
		</div><!--/.row-->
</div>	<!--/.main-->