<div class="row">
    <?php
        $pages = array('callcentre'=>'inactive','callrecords'=>'inactive');
        $pages[$page] = 'active';
    ?>
    <ul class="nav nav-pills">
        <li role="presentation" <?php echo 'class = "'.$pages['callcentre'].'"'; ?>><a href="dashboard.php?page=callcentre" title="Home">Call Logging</a></li>
		<li role="presentation" <?php echo 'class = "'.$pages['callrecords'].'"'; ?>><a href="dashboard.php?page=callrecords" title="Records">Records</a></li>
	</ul>
</div><!--/.row-->

<hr>