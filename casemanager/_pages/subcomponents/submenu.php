<div class="row">
    <?php
        $pages = array('casedetails'=>'inactive','caserelateds'=>'inactive','caseattachments'=>'inactive','casecomments'=>'inactive','casehistory'=>'inactive','casecontacts'=>'inactive','caseshare'=>'inactive','caseassign'=>'inactive','casereject'=>'inactive','caseedit'=>'inactive','casesend'=>'inactive','caseforward'=>'inactive');
        $pages[$page] = 'active';
    ?>
    <ul class="nav nav-pills">
        <li role="presentation" <?php echo 'class = "'.$pages['casedetails'].'"'; ?>><a href="dashboard.php?page=casedetails&id=<?php echo $docid;?>" title="Case Details">Case Info</a></li>
	
	<?php
	if($assignedoptions == TRUE || $handleroptions == TRUE):
	?>
        <li role="presentation" <?php echo 'class = "'.$pages['caserelateds'].'"'; ?>><a href="dashboard.php?page=caserelateds&id=<?php echo $docid;?>" title="Related Cases">Related Cases</a></li>
	<?php
	endif;
	?>	
		
		<li role="presentation" <?php echo 'class = "'.$pages['caseattachments'].'"'; ?>><a href="dashboard.php?page=caseattachments&id=<?php echo $docid;?>" title="Attachments"><span class="glyphicon glyphicon-paperclip"></span> Attachments</a></li>
		<li role="presentation" <?php echo 'class = "'.$pages['casecontacts'].'"'; ?>><a href="dashboard.php?page=casecontacts&id=<?php echo $docid;?>" title="Contacts"><span class="glyphicon glyphicon-phone"></span> Contacts</a></li>
		<li role="presentation" <?php echo 'class = "'.$pages['casecomments'].'"'; ?>><a href="dashboard.php?page=casecomments&id=<?php echo $docid;?>" title="Comments"><span class="glyphicon glyphicon-comment"></span> Comments</a></li>
        <li role="presentation" <?php echo 'class = "'.$pages['casehistory'].'"'; ?>><a href="dashboard.php?page=casehistory&id=<?php echo $docid;?>" title="History"><span class="glyphicon glyphicon-time"></span> History</a></li>
    <?php 
	if (($counteroptions == TRUE || $_SESSION['admins'] == 'Y' )){
		if($documentinfo['UNIT_NAME'] == NULL){
			echo '<li role="presentation" class = "'.$pages['casesend'].'"><a href="dashboard.php?page=casesend&id='.$docid.'" title="SendToSection"><span class="glyphicon glyphicon-share-alt"></span> Send Case</a></li>';
		}
	}
	
	if ($handleroptions == TRUE){
		
		if(($documentinfo['UNIT_NAME'] != '' && $documentinfo['DST_NAME'] != 'CLOSED') && $editoptions == TRUE){
			echo '<li role="presentation" class = "'.$pages['casereject'].'"><a href="dashboard.php?page=casereject&id='.$docid.'" title="Reject Case"><span class="glyphicon glyphicon-remove"></span> Reject Case</a></li>';
		}
		
		if($documentinfo['UNIT_NAME'] != '' && $editoptions == TRUE){
			echo '<li role="presentation" class = "'.$pages['caseassign'].'"><a href="dashboard.php?page=caseassign&id='.$docid.'" title="Assign Case"><span class="glyphicon glyphicon-user"></span> Assign Case</a></li>';
			echo '<li role="presentation" class = "'.$pages['caseforward'].'"><a href="dashboard.php?page=caseforward&id='.$docid.'" title="Forward Case"><span class="glyphicon glyphicon-share"></span> Forward Case</a></li>';
		}
		
	}
	if (($handleroptions == TRUE || $assignedoptions == TRUE) && $editoptions == TRUE){
		if($documentinfo['UNIT_NAME'] != ''){
			echo '<li role="presentation" class = "'.$pages['caseshare'].'"><a href="dashboard.php?page=caseshare&id='.$docid.'" title="Share Case"><span class="glyphicon glyphicon-share"></span> Share Case</a></li>';
		}
	}
	
	
	if (($assignedoptions == TRUE || ($counteroptions == TRUE && $documentinfo['UNIT_NAME'] == NULL ) || $handleroptions == TRUE) && $editoptions == TRUE && $documentinfo['DOC_CR_DOC'] != 1 ){
		if ($documentinfo['DST_NAME'] != 'CLOSED' ){
			echo '<li role="presentation" class = "'.$pages['caseedit'].'"><a href="dashboard.php?page=caseedit&id='.$docid.'" title="Edit Case Details"><span class="glyphicon glyphicon-edit"></span> Edit Case</a></li>';	
		}	
	}
	?>
	
	</ul>
	
	
</div><!--/.row-->

<hr>