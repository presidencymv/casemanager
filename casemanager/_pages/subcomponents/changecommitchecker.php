<?php
switch ($case_change){

	case 'statuschange':
		if ($assignedoptions != TRUE){
			header('location: dashboard.php');
			die();
		}
	break;
	
	case 'casefollow':
		if ($handleroptions != TRUE){
			header('location: dashboard.php');
			die();
		}
	break;
	
	case 'caserelateds':
		if ($assignedoptions != TRUE && $handleroptions != TRUE){
			header('location: dashboard.php');
			die();
		}
	break;
	
	case 'caseremarks':
		if(($assignedoptions != TRUE && $handleroptions != TRUE && $counteroptions != TRUE) || $documentinfo['DOC_STATUS'] == 4 || $editoptions != TRUE){
			header('location: dashboard.php');
			die();
		}
	break;
	
	case 'casecontacts':
		if(($assignedoptions != TRUE && $handleroptions != TRUE && $counteroptions != TRUE) || $documentinfo['DOC_STATUS'] == 4 || $editoptions != TRUE){
			header('location: dashboard.php');
			die();
		}
	break;

	case 'casehandover':
		if ($assignedoptions != TRUE){
			header('location: dashboard.php');
			die();
		}
	break;
	
		
	case 'caseassign':
		if ($_SESSION['handler'] != 'Y' ){
			header('location: dashboard.php');
			die();
		}

		if($documentinfo['UNIT_NAME'] == '' || $handleroptions != TRUE){
			header('location: dashboard.php');
			die();
		}
	break;
	
	
	case 'casecomments':
		if($assignedoptions != TRUE && $handleroptions != TRUE && $counteroptions != TRUE && $sharedoptions != TRUE){
			header('location: dashboard.php');
			die();
		}
	break;

	case 'quickassign':
		//HANDLED IN MAIN PAGE
	break;

	case 'casereject':
		if ($handleroptions != TRUE || $editoptions != TRUE){
			header('location: dashboard.php');
			die();
		}

		if($documentinfo['UNIT_NAME'] == '' || $documentinfo['DST_NAME'] == 'CLOSED' ){
			header('location: dashboard.php');
			die();
		}
	break;

	case 'casesend':
		if ($counteroptions != TRUE && $editoptions != TRUE){
			if($documentinfo['UNIT_NAME'] != NULL){
				header('location: dashboard.php');
				die();	
			}
		}
	
	break;

	case 'caseforward':
		if ($_SESSION['handler'] != 'Y' ){
			header('location: dashboard.php');
			die();
		}

		if($documentinfo['UNIT_NAME'] == '' || $handleroptions != TRUE){
			header('location: dashboard.php');
			die();
		}
	break;
	
	
	case 'caseshare':
		if ($assignedoptions != TRUE &&  $handleroptions != TRUE && $editoptions != TRUE){
			header('location: dashboard.php');
			die();
		}
	break;
	case 'caseupdate':
		if ($documentinfo['DST_ID'] == 4 || $editoptions == FALSE || $documentinfo['DOC_CR_DOC'] == 1){
		header('location: dashboard.php');
		die();
	}
	break;
	
	default:
		die('PLEASE SET PAGE AUTHORIZATION');
	break;
}
?>