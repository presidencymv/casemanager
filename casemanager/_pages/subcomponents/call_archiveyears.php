<?php
$call = new call;
$archresults = $call->archiveyears();
if ($archresults['result'] == TRUE){
    $archyears = $archresults['years_array'];

    foreach ( $archyears as $archyear){
        $cleanedarchyears[strval($archyear)] = 'inactive';
    }

    if (isset($_GET['year'])){
        $selectedyear = strval(preg_replace( "/[^0-9$]/","",$_GET['year']));
        if(!in_array($selectedyear, $archyears)){
           $selectedyear = strval($archyears[0]);
        }
    }else{
        $selectedyear = strval($archyears[0]);
    }

    $cleanedarchyears[$selectedyear] = 'active';

    $counter = 0;
    $arch_menu = '<ul class="nav nav-pills">';
    foreach( $cleanedarchyears as $record){
       $arch_menu .= '<li role="presentation" class = "'.$record.'" ><a href="dashboard.php?page='.$archresults['page'].'&year='.strval($archyears[$counter]).'" title="'.strval($archyears[$counter]).'">'.strval($archyears[$counter]).'</a></li>';
       $counter ++;
    }
    $arch_menu .= '</ul>';

    echo '
    <div class="row">
        '.$arch_menu.'
    </div><!--/.row-->

    ';
}else{
    echo 'NO RECORDS FOUND...';
}

?>