<div class="panel panel-default">

	<button class="btn btn-info"><span class="glyphicon glyphicon-save"></span> Export to Excel</button>

  <div class="table-responsive">
	  <table class="table table-striped" id="exportreport">
		<thead>
			<tr>
				<th>#</th>
				<th>Sender</th>
				<th>Reference</th>
				<th>Letter Information</th>
				<th>Description</th>
				<th>Amount</th>
				<th>Updates</th>
				<th>Priority</th>
			</tr>
		</thead>
		<tbody>
		<?php include('_pages/data/budgetexreport.php');?>
		</tbody>
	  </table>
  </div>
</div>
<script>
    $("button").click(function(){
      $("#exportreport").table2excel({
        name: "DAILY REPORT"
      }); 
    });
</script>