<?php

$documentinfo = $document->getdocumentinfo($docid);
if($documentinfo['DST_NAME'] == 'ASSIGNED' && $documentinfo['ASSIGNEDTO'] == $_SESSION['username']){
	$values= array($docid,3);
	if($document->docstatus($values) == TRUE){
		$document->notification($docid,'Case Seen By Assigned Staff: '.$_SESSION['username'].' and Status Set To ON GOING By '.$_SESSION['username'],12);
		$documentinfo['DST_NAME'] = 'ON GOING';
	}
}

//this check is added to prevent case forwarding, if its a new case which has not been routed, 
//the flow must be, create case, route to a unit, then only can it be forwarded from there on
if($documentinfo['UNIT_NAME'] == '' && $documentinfo['DST_NAME'] == 'NEW' && $page == 'caseforward'){
	header('location: dashboard.php');
	die();
}


//CHECKING OVERDUE STATUS
$overduestatus = false;
if ($documentinfo['DST_ID'] == 4){
	if ($documentinfo['DEADLINEDATE'] <  $documentinfo['DOC_STATUSON']){
		$overduestatus = true;
	}
}else{
	
		if ($documentinfo['DEADLINEDATE'] <  $today){
		$overduestatus = true;
		}
}


?>