<div class="panel panel-default">
  <div class="table-responsive">
	  <table class="table table-striped">
		<thead>
			<tr>
				<th>#</th>
				<th>Reference</th>
				<th>File Name</th>
				<th>Action</th>
				<th>By</th>
				<th>On</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody id="case_data">
		 
		</tbody>
	  </table>
  </div>
</div>


<script>
function loadcasedetails(){
		$.ajax({
		url: '_pages/data/attachments.php?id=<?php echo $docid ?>',
		dataType: 'json',
		success: function (data) {
			document.getElementById("case_data").innerHTML=data.att_details;	
		}
		});		
}

$(document).ready(
	loadcasedetails()	

)
</script>