<?php   
$awardedinfo = $selectedproject->getawardedinfo($p_id);
?>
<div class="panel panel-warning">
    <div class="panel-heading"><span class="glyphicon glyphicon-star"></span>Awarded Party</div>
	<div class="panel-body">
            <div class="col-lg-6">
                <label>Name:</label>
                <p><?php echo $awardedinfo['CP_NAME']?></p>
                <label>Address:</label>
                <p><?php echo $awardedinfo['CP_ADDRESS']?></p>
			</div>
            <div class="col-lg-6">
                <label>Amount:</label>
                <p><?php echo $awardedinfo['C_NAME'].' '.$awardedinfo['AW_CONTRACTAMT']?></p>
                <label>Duration:</label>
                <p><?php echo $awardedinfo['AW_DURATION']?></p>
			</div>
            <div class="col-lg-12">
                <label>Awarded On:</label>
                <p><?php echo $awardedinfo['AWARDED_DATE']?></p>
			</div>
	</div>	
</div>
