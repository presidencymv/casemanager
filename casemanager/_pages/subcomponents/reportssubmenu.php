<div class="row">
    <?php
        $pages = array('basicreport'=>'inactive','advancedreport'=>'inactive');
        $pages[$page] = 'active';
    ?>
    <ul class="nav nav-pills">
        <li role="presentation" <?php echo 'class = "'.$pages['basicreport'].'"'; ?>><a href="dashboard.php?page=basicreport" title="basic report">Basic Report</a></li>
        <li role="presentation" <?php echo 'class = "'.$pages['advancedreport'].'"'; ?>><a href="dashboard.php?page=advancedreport" title="advanced report">Advanced Report</a></li>	
	</ul>
</div><!--/.row-->
