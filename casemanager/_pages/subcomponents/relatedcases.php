<div class="panel panel-default">
  <div class="table-responsive">
	  <table class="table table-striped">
		<thead>
			<tr>
				<th>#</th>
				<th>Reference</th>
				<th>Description</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
		<?php include('_pages/data/relatedcases.php');?>
		</tbody>
	
	  </table>
  </div>
</div>