<?php

//REDIRECT INVALID USERS
$document = new document;
$assignedoptions = FALSE;
$handleroptions = FALSE;
$adminoptions = FALSE;
$sharedoptions = FALSE;
$counteroptions = FALSE;
$editoptions = FALSE;
$caseflag = FALSE;


if ($_SESSION['admins'] == 'Y'){
	$adminoptions = TRUE;
	$caseflag = TRUE;
	$editoptions = TRUE;
}

if ($document->authorizeddoc_assigned($docid) == TRUE){
	$assignedoptions = TRUE;
	$caseflag = TRUE;
	$editoptions = TRUE;
}

if ($_SESSION['handler'] == 'Y'){
	if ($document->authorizeddoc($docid) == TRUE ){
		$handleroptions = TRUE;
		$caseflag = TRUE;
		$editoptions = TRUE;
	}
}

if (($document->authorizeddoc_shared($docid) == TRUE )|| ($document->callcentrecase($docid) == TRUE && $_SESSION['callcentrestaff'] == 'Y') ){
	$sharedoptions = TRUE;
	$caseflag = TRUE;
}

if ($_SESSION['counter'] == 'Y' ){
	if ($document->authorizeddoc_counter($docid) == TRUE){
		$counteroptions = TRUE;
				
		if ($document->counterchangeauth($docid) == TRUE){
			$editoptions = TRUE;
		}
		
		$caseflag = TRUE;
	}
}


if ($document->callcentrecase($docid) == TRUE && in_array($page,array('caseedit')) == TRUE){
	$caseflag = FALSE;
}

?>