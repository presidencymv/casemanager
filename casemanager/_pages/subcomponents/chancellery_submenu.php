<div class="row">
    <?php
        $pages = array('chancellery'=>'inactive','archive'=>'inactive');
        $pages[$page] = 'active';
    ?>
    <ul class="nav nav-pills">
        <li role="presentation" <?php echo 'class = "'.$pages['chancellery'].'"'; ?>><a href="dashboard.php?page=chancellery" title="chancellery">Dashboard</a></li>
		<li role="presentation" <?php echo 'class = "'.$pages['archive'].'"'; ?>><a href="dashboard.php?page=archive" title="archive">Archive</a></li>
	</ul>
</div><!--/.row-->

<hr>