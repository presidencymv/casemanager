<?php
if (isset($_GET['msg'])){
	$msg = preg_replace( "/[^0-9$]/","",$_GET['msg']);
    if ($msg != ''){
		switch ($msg){
			case 1:
				echo '
				<div class="row">
					<div class="alert bg-success">
						<button type="button" data-dismiss="alert" aria-hidden="true" class="close"><span class="glyphicon glyphicon-remove"></button></span>Successful.
					</div>
				</div>
				';			
			break;
			
			case 2:
				echo '
				<div class="row">
					<div class="alert bg-warning">
						<button type="button" data-dismiss="alert" aria-hidden="true" class="close"><span class="glyphicon glyphicon-remove"></button>
						<span class="glyphicon glyphicon-warning-sign"></span>Something went wrong, please try again.
					</div>
				</div>
				';			
			break;
			
			case 3:
				echo '
				<div class="row">
					<div class="alert bg-danger">
						<button type="button" data-dismiss="alert" aria-hidden="true" class="close"><span class="glyphicon glyphicon-remove"></button>
						<span class="glyphicon glyphicon-exclamation-sign"></span>Failed.
					</div>
				</div>
				';			
			break;

			case 4:
				echo '
				<div class="row">
					<div class="alert bg-warning">
						<button type="button" data-dismiss="alert" aria-hidden="true" class="close"><span class="glyphicon glyphicon-remove"></button>
						<span class="glyphicon glyphicon-warning-sign"></span>You do not have permission to view this case anymore. This maybe because case sharing has been removed from you, case has been forwarded and or case has been rejected.
					</div>
				</div>
				';			
			break;
		}
	}
}
?>