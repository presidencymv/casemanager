<div class="panel panel-default">

	<button class="btn btn-info"><span class="glyphicon glyphicon-save"></span> Export to Excel</button>

  <div class="table-responsive">
	  <table class="table table-striped" id="exportreport">
		<thead>
			<tr>
				<th>#</th>
				<th>SysID</th>
				<th>Sender</th>
				<th>Reference</th>
				<th>Receiver</th>
				<th>Description</th>
				<th>Category</th>
				<th>Deadline</th>
				<th>Current Status</th>
				<th>Assigned To</th>
				<th>Created On</th>
				<th>Created By</th>
				<th>StatusUpdateOn</th>
			</tr>
		</thead>
		<tbody>
		<?php include('_pages/data/advancedreport.php');?>
		</tbody>
	  </table>
  </div>
</div>
<script>
    $("button").click(function(){
      $("#exportreport").table2excel({
        name: "DAILY REPORT"
      }); 
    });
</script>