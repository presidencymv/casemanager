<div class="panel panel-default">
  <div class="table-responsive">
	  <table class="table table-striped">
		<thead>
			<tr>
				<th>#</th>
				<th>Name</th>
				<th>Designation</th>
				<th>Address</th>
				<th>Tel</th>
				<th>Fax</th>
				<th>E-Mail</th>
			</tr>
		</thead>
		<tbody>
		<?php include('_pages/data/contacts.php');?>
		</tbody>
	
	  </table>
  </div>
</div>