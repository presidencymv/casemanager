<?php
if (isset($_GET['rec'])){
  $rec_id = preg_replace( "/[^0-9$]/","",$_GET['rec']);
	if ($rec_id == ''){
	header('location: dashboard.php');
	die();
	}
}else{
	header('location: dashboard.php');
	die();
}
$athmnt = new remark;
$attachment_info = $athmnt->remarkinfo($rec_id,$docid);

//this is to prevent changing attachment by unauthorized users
//authorized users are the following
//the user who uploaded the attachment, section admin for attachments by that section user


if ($currentuser->attachment_auth($attachment_info['DR_FILE_UNIT'], $attachment_info['DOC_UNIT'],$attachment_info['DOC_ASSIGNEDTO'], $attachment_info['DR_BY'], $attachment_info['DOC_CREATEDBY'],$attachment_info['DOC_STATUS']) == FALSE || $attachment_info['DR_IOR_ID'] != NULL) {
	die('ACCESS DENIED');
}
?>
<form action="case_remarks.php?ex=103&id=<?php echo $docid;?>&rec=<?php echo $rec_id;?>" name = "add_attachments" enctype="multipart/form-data" method="post" onsubmit='return confirm("Update?")'>
	<div class="form-group col-lg-4">
		<label>Out-Going Reference(Optional)</label>
		<input type="text" id = "reference" name = "reference" class="form-control" value="<?php echo $attachment_info['DR_OUT_REF'] ?>">
	</div>
	
	<div class="form-group col-lg-12">
		 <label>Description</label>
		 <textarea name="remark_description" class="form-control" required><?php echo $attachment_info['DR_DESC'] ?></textarea>
	</div>

	<div class="form-group col-lg-12">
	<label>Select File (Supported file types - pdf, zip, Excel(xls, xlsx), Word(doc, docx), Audio(wav, mp3), Email(msg), Image(jpg, jpeg, gif, png, tif))</label>
	<input class = "btn btn-default" name="userfile" type="file" id="userfile" size="26" maxlength="150" />
	</div>

	<div class="form-group col-lg-12">
	<input class = "btn btn-primary" type="submit" title="submit" value="UPDATE">
	</div>
</form>
<hr>
<a class = "btn btn-danger" href="case_remarks.php?ex=104&id=<?php echo $docid ?>&rec=<?php echo $rec_id ?>" target="_top" onclick="return confirm('Are you sure you want to delete this case file attachment?')"><span class="glyphicon glyphicon-remove"></span> REMOVE ATTACHMENT</a>
