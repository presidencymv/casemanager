<?php
if (isset($_GET['rec'])){
$rec_id = preg_replace( "/[^0-9$]/","",$_GET['rec']);
	if ($rec_id == ''){
	header('location: dashboard.php');
	die();
	}
}else{
	header('location: dashboard.php');
	die();
}
$contact = new contact;
$contact_info = $contact->contact_details($docid,$rec_id);
if($contact_info['CC_ID']==''){
	header('location: dashboard.php');
	die();
}
?>    
<div class="panel panel-default">
	<div class="panel-heading">Update Contact</div>
	<div class="panel-body">
		<form action="case_contacts.php?ex=101&id=<?php echo $docid?>" name = "update_contacts" enctype="multipart/form-data" method="post" onsubmit='return confirm("Update?")'>
			<div class="form-group col-lg-6">
				<label>CONTACT RECORD</label>
				<input type="text" id = "mod_contact_id" name = "mod_contact_id" class="form-control" required readonly="" value="<?php echo $contact_info['CC_ID'];?>">
			</div>
			<div class="form-group col-lg-6">
				<label>Full Name</label>
				<input type="text" id = "mod_fullname" name = "mod_fullname" class="form-control" required value="<?php echo $contact_info['CC_NAME'];?>">
			</div>
			<div class="form-group col-lg-6">
				<label>Designation</label>
				 <input type="text" id = "mod_designation" name = "mod_designation" class="form-control" value="<?php echo $contact_info['CC_DESIG'];?>">
			</div>
			<div class="form-group col-lg-6">
				<label>Address</label>
				 <input type="text" id = "mod_address" name = "mod_address" class="form-control" value="<?php echo $contact_info['CC_ADDRESS'];?>">
			</div>
			<div class="form-group col-lg-6">
				<label>Email</label>
				 <input type="email" id = "mod_email" name = "mod_email" class="form-control" value="<?php echo $contact_info['CC_EMAIL'];?>">
			</div>
			<div class="form-group col-lg-6">
				<label>Tel</label>
				 <input type="text" id = "mod_telephone1" name = "mod_telephone1" class="form-control" required value="<?php echo $contact_info['CC_TEL'];?>">
			</div>
			<div class="form-group col-lg-6">
				<label>Fax</label>
				 <input type="text" id = "mod_telephone2" name = "mod_telephone2" class="form-control" value="<?php echo $contact_info['CC_FAX'];?>">
			</div>
		   
			<div class="form-group col-lg-12">
			<input class = "btn btn-primary" type="submit" title="submit" value="UPDATE CONTACT">
			</div>
		</form>
	</div>	
</div>