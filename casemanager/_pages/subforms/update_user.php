<?php
if (isset($_GET['rec'])){
$rec_id = preg_replace( "/[^0-9$]/","",$_GET['rec']);
	if ($rec_id == ''){
	header('location: dashboard.php');
	die();
	}
}else{
	header('location: dashboard.php');
	die();
}


$user_info = $currentuser->userinfo($rec_id);
if($user_info['U_ID']==''){
	header('location: dashboard.php');
	die();
}
?>
<form action="manageusers.php?ex=101&id=<?php echo $user_info['U_ID'] ?>" name = "add_users" enctype="multipart/form-data" method="post" onsubmit='return confirm("Save?")'>
	<div class="form-group col-lg-2">
		<label>User Name</label>
		 <input type="text" id = "uname" name = "uname" class="form-control" value = '<?php echo $user_info['U_NAME'] ?>' required>
	</div>
	<div class="form-group col-lg-2">
		<label>Phone</label>
		 <input type="text" id = "uphone" name = "uphone" class="form-control" value = '<?php echo $user_info['U_PHONE'] ?>' required>
	</div>
	<div class="form-group col-lg-2">
		<label>Case Creation Activities</label>
		<select class="form-control" name="counter" id="counter">
		<option value='N' <?php if( $user_info['U_COUNTER'] == 'N'){ echo 'selected';}?>> N </option>
		<option value='Y' <?php if( $user_info['U_COUNTER'] == 'Y'){ echo 'selected';}?>> Y </option>
		</select>
	</div>
	<div class="form-group col-lg-2">
		<label>System Administration</label>
		<select class="form-control" name="admin" id="admin">
		<option value='N' <?php if( $user_info['U_ADMIN'] == 'N'){ echo 'selected';}?>> N </option>
		<option value='Y' <?php if( $user_info['U_ADMIN'] == 'Y'){ echo 'selected';}?>> Y </option>
		</select>
	</div>
	<div class="form-group col-lg-2">
		<label>Report Generation</label>
		<select class="form-control" name="reports" id="reports">
		<option value='N' <?php if( $user_info['U_REPORTS'] == 'N'){ echo 'selected';}?>> N </option>
		<option value='Y' <?php if( $user_info['U_REPORTS'] == 'Y'){ echo 'selected';}?>> Y </option>
		</select>
	</div>
	<div class="form-group col-lg-2">
		<label>Send All</label>
		<select class="form-control" name="sendall" id="sendall">
		<option value='N' <?php if( $user_info['U_SENDALL'] == 'N'){ echo 'selected';}?>> N </option>
		<option value='Y' <?php if( $user_info['U_SENDALL'] == 'Y'){ echo 'selected';}?>> Y </option>
		</select>
	</div>
	<div class="form-group col-lg-12">
	<input class = "btn btn-primary" type="submit" title="submit" value="UPDATE USER">
	</div>
</form>
<hr>
<?php if ($user_info['U_ACTIVE'] == 1 ):?>
	<a class = "btn btn-danger" href="manageusers.php?ex=102&id=<?php echo $user_info['U_ID'] ?>&status=0" target="_top" onclick="return confirm('Are you sure you want to deactivate the user?')"><span class="glyphicon glyphicon-remove"></span> DEACTIVATE USER</a>
<?php else: ?>
	<a class = "btn btn-success" href="manageusers.php?ex=102&id=<?php echo $user_info['U_ID'] ?>&status=1" target="_top" onclick="return confirm('Are you sure you want to activate the user?')"><span class="glyphicon glyphicon-check"></span> ACTIVATE USER</a>
<?php endif; ?>