<?php
require('_ex/follower.php');
$follower = new follower;
$userisfollowingcase = $follower->isfollowing($docid);
?>
<?php if($handleroptions){ ?>
<form action="case_follow.php?ex=<?php if(!$userisfollowingcase){echo '100';}else{echo '101';} ?>&id=<?php echo $docid;?>" name="casefollow" enctype="multipart/form-data" method="post" onsubmit='return confirm("Save?")'>
	<input type="hidden" name="docid" value="<?php echo $docid;?>">
	<div class="form-group col-lg-12">
		<input class="<?php if(!$userisfollowingcase){echo 'btn';} ?>" type="submit" title="<?php if($userisfollowingcase){echo 'Stop following this case'; }else{echo 'Follow this case';} ?>" value="<?php if($userisfollowingcase){echo 'UNFOLLOW'; }else{echo 'FOLLOW';} ?>">
	</div>
</form>
<?php } ?>
<div class="form-group col-lg-12">
	<select class="form-control" name="followerlist" id="followerlist">
		<option>Who is following this case?</option>
		<?php
			$casefollowers = $follower->getdocfollowers($docid);
			if(!$casefollowers){
				echo '<option disabled>None</option>';
			}else{
				foreach($casefollowers as $follower){
					echo '<option disabled>'.$follower['U_NAME'].'</option>';
				}
			}
		?>
	</select>
</div>