<form action="managesenders.php?ex=100" name = "add_contacts" enctype="multipart/form-data" method="post" onsubmit='return confirm("Save?")'>
	<div class="form-group col-lg-6">
		 <label>AGA</label>
		<select  class="form-control" name="aga" id="aga">
		<option></option>
		</select>
	</div>
	<div class="form-group col-lg-3">
		<label>Business Area</label>
		 <input type="text" id = "barea" name = "barea" class="form-control">
	</div>
	<div class="form-group col-lg-3">
		 <label>Default Priority</label>
		<select  class="form-control" name="priority" id="priority"></select>
	</div>
	<div class="form-group col-lg-12">
		<label>Sender Name</label>
		 <input type="text" id = "name" name = "name" class="form-control">
	</div>
	<div class="form-group col-lg-12">
		<label>Address</label>
		 <input type="text" id = "address" name = "address" class="form-control">
	</div>

	<div class="form-group col-lg-12">
	<input class = "btn btn-primary" type="submit" title="submit" value="ADD SENDER">
	</div>
</form>
<script type="text/javascript">
//priority Options
$.ajax({
	url: '_pages/data/priority_options.php',
	dataType: 'json',
	success: function (data) {
		var options = data;
		for (var option in options) {
			if(options[option]. P_ID != 1){
				document.getElementById("priority").innerHTML += '<option value="' + options[option]. P_ID + '">' + options[option].P_NAME + '</option>';
			}
		}
	}
});

//AGA Options
$.ajax({
	url: '_pages/data/aga_options.php',
	dataType: 'json',
	success: function (data) {
		var options = data;
		for (var option in options) {
				document.getElementById("aga").innerHTML += '<option value="' + options[option]. AGA_ID + '">' + options[option].AGA_NAME + '</option>';
		}
	}
});
</script>