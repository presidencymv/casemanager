<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<form action="dashboard.php?page=advancedreport" name = "generatereport" method="post">
					    <div class="form-group col-lg-2">
							<label>Start Date</label>
							<input class="form-control" type="date" name="sdate" required/>
						</div>
						<div class="form-group col-lg-2">
							<label>End Date</label>
							<input class="form-control" type="date" name="edate" required/>
						</div>
						<hr>
						<div class="form-group col-lg-4">
							<label>Source</label>
							<select class="form-control" name="source" id="source"></select>
						</div>
						<div class="form-group col-lg-4">
							<label>Sender</label>
							<select class="form-control" name="sender" id="sender">
							<option></option>
							<?php
							$masterdata = new master;
							echo $masterdata->selectoptionsrequest('O_ID','O_NAME','office','');
							
							?>
							</select>
						</div>
						<div class="form-group col-lg-4">
							<label>Receiver</label>
							<select class="form-control" name="receiver" id="receiver">
							<option></option>
							<?php
							echo $masterdata->selectoptionsrequest('UNIT_ID','UNIT_NAME','unit','');
							?>
							</select>
						</div>
						<hr>
						<div class="form-group col-lg-12">
							<label>Selection</label>
							<div class="radio">
								<label>
									<input type="radio" name="or1" id="optionsRadios1" value="all" checked="">ALL
								</label>
								<label>
									<input type="radio" name="or1" id="optionsRadios2" value="pending" >PENDING
								</label>
								<label>
									<input type="radio" name="or1" id="optionsRadios3" value="closed" >CLOSED
								</label>
							</div>
						</div>
					<div class="form-group col-lg-12">
					<input class = "btn btn-primary" type="submit" title="submit" value="Generate Report">
					</div>
				</form>
			</div>	
		</div>
	</div>
</div><!--/.row-->
<script type="text/javascript">
//source Options
        $.ajax({
            url: '_pages/data/source_options.php',
            dataType: 'json',
            success: function (data) {
                var options = data;
                document.getElementById("source").innerHTML += '<option value=""></option>';
                for (var option in options) {
                    document.getElementById("source").innerHTML += '<option value="' + options[option].DSrc_ID + '">' + options[option].DSrc_Name + '</option>';
                }
            }
        });

</script>