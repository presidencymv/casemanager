<?php
if (isset($_GET['rec'])){
$rec_id = preg_replace( "/[^0-9$]/","",$_GET['rec']);
	if ($rec_id == ''){
	header('location: dashboard.php');
	die();
	}
}else{
	header('location: dashboard.php');
	die();
}

$sender = new office;
$sender_info = $sender->officeuinfo($rec_id);
if($sender_info['O_ID']==''){
	header('location: dashboard.php');
	die();
}
?>

<form action="managesenders.php?ex=101&id=<?php echo $rec_id ?>" name = "add_contacts" enctype="multipart/form-data" method="post" onsubmit='return confirm("Save?")'>
	<div class="form-group col-lg-6">
		 <label>AGA</label>
		<select  class="form-control" name="aga" id="aga">
		<option></option>
		</select>
	</div>
	<div class="form-group col-lg-3">
		<label>Business Area</label>
		 <input type="text" id = "barea" name = "barea" class="form-control" value="<?php echo $sender_info['O_BA'] ?>">
	</div>
	<div class="form-group col-lg-3">
		 <label>Default Priority</label>
		<select  class="form-control" name="priority" id="priority"></select>
	</div>
	<div class="form-group col-lg-12">
		<label>Sender Name</label>
		 <input type="text" id = "name" name = "name" class="form-control" value="<?php echo $sender_info['O_NAME'] ?>">
	</div>
	<div class="form-group col-lg-12">
		<label>Address</label>
		 <input type="text" id = "address" name = "address" class="form-control" value="<?php echo $sender_info['O_ADDRESS'] ?>">
	</div>

	<div class="form-group col-lg-12">
	<input class = "btn btn-primary" type="submit" title="submit" value="UPDATE SENDER">
	</div>
</form>


<script type="text/javascript">
//priority Options
$.ajax({
	url: '_pages/data/priority_options.php',
	dataType: 'json',
	success: function (data) {
		var options = data;
		var selectedvalue = '<?php echo $sender_info['P_ID'];?>';
		for (var option in options) {
			if(options[option]. P_ID != 1){
				if( options[option].P_ID == selectedvalue){
					document.getElementById("priority").innerHTML += '<option value="' + options[option].P_ID + '" selected>' + options[option].P_NAME + '</option>';
				}else{
					document.getElementById("priority").innerHTML += '<option value="' + options[option].P_ID + '">' + options[option].P_NAME + '</option>';
				}
			}
		}
	}
});

//AGA Options
$.ajax({
	url: '_pages/data/aga_options.php',
	dataType: 'json',
	success: function (data) {
		var options = data;
		var selectedvalue = '<?php echo $sender_info['AGA_ID'];?>';
		for (var option in options) {
				if( options[option].AGA_ID == selectedvalue){
					document.getElementById("aga").innerHTML += '<option value="' + options[option].AGA_ID + '" selected>' + options[option].AGA_NAME + '</option>';
				}else{
				document.getElementById("aga").innerHTML += '<option value="' + options[option]. AGA_ID + '">' + options[option].AGA_NAME + '</option>';
				}
		}
	}
});
</script>