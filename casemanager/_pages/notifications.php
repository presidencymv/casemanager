<?php
$dbc = new dbconnection;
$sql="SELECT A_ID, A_U_ID, U_NAME, A_DOC_ID, DOC_REFNUMBER, A_TYPE, A_TYPE_DESC, AD_DESC, A_CREATEDON FROM alerts LEFT OUTER JOIN alert_description ON A_TYPE_DESC = AD_ID LEFT OUTER JOIN user ON A_CREATEDBY = U_ID LEFT OUTER JOIN document ON A_DOC_ID = DOC_ID WHERE A_SEEN = 'N' AND A_U_ID = ".$_SESSION['userid']." ORDER BY A_CREATEDON DESC";
$result = $dbc->dbconn->query($sql);
$unreadalertlist = array();
$count = 0;
while ($options = mysqli_fetch_assoc($result)){
	
	$link = 'case_alerts.php?ex=100&nid='.$options['A_ID'].'&sid='.$options['A_U_ID'].'&did='.$options['A_DOC_ID'].'&tid='.$options['A_TYPE_DESC'];

	$createdtime = DateTime::createFromFormat('Y-m-d H:i:s', $options['A_CREATEDON']);
	
	$unreadalertlist[$count]['commentitem'] = '<a href="'.$link.'"><div><div><h5><strong>'.$options['AD_DESC'].'</strong></h5><p>Case No:'.$options['DOC_REFNUMBER'].'</p><p class="small">By:'.$options['U_NAME'].'</p><p class="small text-muted"><i class="glyphicon glyphicon-time"></i> '.$createdtime->format('d  M Y @ H:i A').'</p></div></div></a>';

	$count++;

}
if(empty($unreadalertlist)){
	$unreadalertlist[$count]['commentitem'] = '<div class="media"><div class="media-body"><h5 class="media-heading"><strong>No New Notifications</strong></h5><p></p><p class="small"></p><p class="small text-muted"></p></div></div>';
}
$result->free();
$dbc->dbconn->close();
?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">                   
                <div class="row">
                        <ol class="breadcrumb">
                                <li><a href="dashboard.php?page=home"><span class="glyphicon glyphicon-home"></span></a></li>
                                <li class="active">My Notifications</li>
                        </ol>
                </div><!--/.row-->                
                <div class="row">
                        <div class="col-lg-12">
                                <h1 class="page-header">Notifications</h1>
                        </div>
                </div><!--/.row-->
                <div class="row">
                        <div class="col-lg-6">
                        	<table class="table table-hover">
                        		<tbody>
                        			<?php foreach($unreadalertlist as $item){
                        			?>
                        				<tr>
                        					<td>
                        						<?php echo $item['commentitem']; ?>
                        					</td>
                        				</tr>
                        			<?php } ?>
                        		</tbody>
                        	</table>
                        </div>
                </div><!--/.row-->
</div>  <!--/.main-->