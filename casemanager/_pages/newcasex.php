<?php
//REDIRECT INVALID USERS
if ($_SESSION['counter'] != 'Y'){
    header('location: dashboard.php');
    die();
}
?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="dashboard.php?page=home"><span class="glyphicon glyphicon-home"></span></a></li>
				<li class="active">New Case</li>
			</ol>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12">
				<?php include('_pages/subcomponents/msgs.php'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Create New Case</h1>
			</div>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12">
					    <form action="case_creation.php?ex=101" name = "createcaseform" enctype="multipart/form-data" method="post" onsubmit="return submission();">
                            <div class="form-group">
                                <div class="form-group col-lg-4">
									<label>Source</label>
									<select required class="form-control" name="source" id="source"></select>
								</div>
								<div class="form-group col-lg-4">
									<label>Sender</label>
									<select required class="form-control" name="sender" id="sender">
									<?php
									$masterdata = new master;
									echo $masterdata->selectoptionsrequest('O_ID','O_NAME','office','');
									?>
									</select>
								</div>
								<div class="form-group col-lg-4">
									<label>Type</label>
									<select required class="form-control" name="type" id="type"></select>
								</div>
								<div class="form-group col-lg-4">
								    <label>Case Reference</label>
                                    <input type="text" id = "reference" name = "reference" class="form-control">
								</div>
                                <div class="form-group col-lg-12">
								    <label>Case Description</label>
                                    <textarea id = "description" name = "description" class="form-control" required></textarea>
								</div>
                                <div class="form-group col-lg-4">
								    <label>Deadline</label>
									<input class="form-control" type="date" name="deadline" min="<?php echo date('Y-m-d');?>" value ="<?php
                                    $date = new DateTime(date('Y-m-d'));
									$dayOfWeek = date('l');
									$plus2days = array('Tuesday','Wednesday','Thursday','Friday');
									$plus1days = array('Staturday');

									if (in_array($dayOfWeek, $plus2days)){
										$date->modify('+5 day');  
									}elseif(in_array($dayOfWeek, $plus1days)) {
										$date->modify('+4 day');  
									}else{
										$date->modify('+3 day');
									}
									echo $date->format('Y-m-d');
									?>" required/>
								</div>
								 <div class="form-group col-lg-4">
									<label>Priority</label>
									<select  class="form-control" name="priority" id="priority"></select>
								</div>
							</div>
                            <hr>
                            <div class="form-group col-lg-12">
                            <input type="submit" title="submit" value="CREATE CASE">
                            </div>
                        </form>
			</div>
		</div><!--/.row-->
</div>	<!--/.main-->
<script type="text/javascript">
//source Options
        $.ajax({
            url: '_pages/data/source_options.php',
            dataType: 'json',
            success: function (data) {
                var options = data;
                document.getElementById("source").innerHTML += '<option value=""></option>';
                for (var option in options) {
                    document.getElementById("source").innerHTML += '<option value="' + options[option].DSrc_ID + '">' + options[option].DSrc_Name + '</option>';
                }
            }
        });

//type Options
        $.ajax({
            url: '_pages/data/type_options.php',
            dataType: 'json',
            success: function (data) {
                var options = data;
                document.getElementById("type").innerHTML += '<option value=""></option>';
                for (var option in options) {
                    document.getElementById("type").innerHTML += '<option value="' + options[option]. DC_ID + '">' + options[option].DC_NAME + '</option>';
                }
            }
        });
//priority Options
        $.ajax({
            url: '_pages/data/priority_options.php',
            dataType: 'json',
            success: function (data) {
                var options = data;
                for (var option in options) {
                    document.getElementById("priority").innerHTML += '<option value="' + options[option]. P_ID + '">' + options[option].P_NAME + '</option>';
                }
            }
        });
		
		
        function submission() {
            if (confirm("Create Case and Continue?")) {
			                //DEFAULT
                var inputs = ["source", "sender", "type", "description", "deadline", "priority"];
                
				var arrayLength = inputs.length;

                for (var i = 0; i < arrayLength; i++) {
					var x = document.getElementById(inputs[i]).value;
                    if (x == null || x == "") {
                        alert("Please fill the field: " + inputs[i]);
                        document.getElementById(inputs[i]).focus();
                        return false;
						break;
                    }
                }
				
				return true;
            } else {
                return false;
            }
        }
</script>
