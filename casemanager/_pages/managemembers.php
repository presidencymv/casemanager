<?php 
if ( $_SESSION['admins'] != 'Y'){
	header('location: dashboard.php');
	die();
}
 if (isset($_GET['unit'])){
	$selectedunit = preg_replace( "/[^0-9$]/","",$_GET['unit']);
}else{
	header('location: dashboard.php');
	die();
}
require('_ex/units.php');

$unit = new unit;

$unit_info = $unit->unitinfo($selectedunit);
?>	

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="dashboard.php?page=home"><span class="glyphicon glyphicon-home"></span></a></li>
				<li><a href="dashboard.php?page=manageunit">Manage Units</a></li>
				<li class="active"><?php echo $unit_info['UNIT_NAME']; ?></li>
			</ol>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12">
				<?php include('_pages/subcomponents/msgs.php'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header"><?php echo $unit_info['UNIT_NAME']; ?></h1>
			</div>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12">
			   <div class="panel panel-default">
				    <div class="panel-body">
					    <div class="col-lg-12">
							<div class="col-lg-12">
								<?php 	
								 if (isset($_GET['sf'])){
                                    $sf = preg_replace( "/[^a-z$]/","",$_GET['sf']);
                                    if ($sf == ''){
                                        include ('_pages/subforms/add_member.php');
                                    }else{
                                        include ('_pages/subforms/update_member.php');
                                    }
                                }else{
                                   include ('_pages/subforms/add_member.php');
                                }
								?>
			                </div>
						</div>
				    </div>	
				</div>
			</div>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12" style =" min-width:1200px">
                <div class="panel panel-default">
					<div class="panel-body custom_text">
						<table data-toggle="table" data-url="_pages/data/full_members.php?unit=<?php echo $selectedunit;?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="u_id" data-sort-order="desc">
							<thead>
							<tr>
								<th data-field="COUNT"  data-sortable="true">#</th>
								<th data-field="U_NAME"  data-sortable="true">USER NAME</th>
								<th data-field="UNIT_NAME"  data-sortable="true">UNIT NAME</th>
								<th data-field="um_handler"  data-sortable="true">UNIT ADMIN</th>
								<th data-field="A_STATUS"  data-sortable="true">USER STATUS:</th>
								<th data-field="URL"  data-sortable="true">ACTION:</th>
							</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->
	
</div>	<!--/.main-->
<script type="text/javascript">

</script>
