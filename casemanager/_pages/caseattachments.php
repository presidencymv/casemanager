<?php
require ('_pages/subcomponents/case_auth.php');
require('_ex/remark.php');
?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="dashboard.php?page=home"><span class="glyphicon glyphicon-home"></span></a></li>
				<li><a href="dashboard.php?page=cases">Cases</a></li>
				<li class="active"> Case: <?php echo $documentinfo['DOC_SYSID'] ?></li>
			</ol>
		</div><!--/.row-->
		<?php
		include('_pages/subcomponents/pageheader.php');
		?>
		<?php
        include('subcomponents/submenu.php');
        ?>
		<div class="row">
			<div class="col-lg-12">
				<?php include('_pages/subcomponents/msgs.php'); ?>
			</div>
		</div>
		

		<div class="row">
			<div class="col-lg-12">
			   <div class="panel panel-default">
				    <div class="panel-body">
					    <div class="col-lg-12">
                            <div class="col-lg-12">
                                <h3>Attachments</h3>
			                </div>
							<div class="col-lg-12">
								<?php
								if(($assignedoptions == TRUE || $handleroptions == TRUE || $counteroptions == TRUE) && $documentinfo['DOC_STATUS'] != 4 && $editoptions == TRUE){

									if (isset($_GET['sf'])){
											 $sf = preg_replace( "/[^a-z$]/","",$_GET['sf']);
											 if ($sf == ''){
													 include ('_pages/subforms/add_caseattachments.php');
											 }else{
											 		include ('_pages/subforms/update_caseattachments.php');
											 }
									 }else{
											include ('_pages/subforms/add_caseattachments.php');
									 }




								}

								?>
			                </div>
							<?php
							include ('_pages/subcomponents/caseattachments.php');
							?>
						</div>
				    </div>
				</div>
			</div>
		</div><!--/.row-->

</div>	<!--/.main-->
<script type="text/javascript">
</script>
<?php
$update_type = 'caseattachmentview';
include ('_pages/subcomponents/alert_seen.php');
?>