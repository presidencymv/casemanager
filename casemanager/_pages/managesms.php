<?php
header('Content-Type: application/json');

require('_ex/connections.php');    
require('_ex/authentications.php');
require('_ex/master.php');
require('_ex/sms.php');

$commitresult = "GOOD";


$currentuser = new authentication;


if ($_SESSION['callcentrestaff'] != 'Y'){
    $commitresult = 'NO RIGHTS';
}else{
	if (isset($_POST['st'])){
	$ex = preg_replace( "/[^0-9$]/","",$_POST['st']);
		if ($ex == ''){
		$commitresult = 'Missing EX';
		}
	}else{
		$commitresult = 'Missing EX';
	}
}


$sms = new sms;

if($commitresult == "GOOD"){
	switch($ex){
			case '101':
			//DELETE SMS
				$values= array(
				$_SESSION['userid'],
				trim($_POST['id'])
				);
										
				$fieldlist = array ("userid","record");
				$case = array('n','n');
				
				$returnresult = $sms->invalidchar2($values,$fieldlist,$case);
				
				if ($returnresult['STATUS'] == 'PASS'){
					if($sms->deletesms($values) == TRUE){
						$commitresult = "SMS DELETED";
					}else{
						$commitresult = "FAILED";
					}
				}else{
					$commitresult = $returnresult['FAILED'];
				}
			
			break;
			case '102':
			//MERGE SMS
				$values= array(
				$_SESSION['userid'],
				trim($_POST['id'])
				);					
				$fieldlist = array ("userid","record");
				$case = array('n','n');
				
				$returnresult = $sms->invalidchar2($values,$fieldlist,$case);
				
				if ($returnresult['STATUS'] == 'PASS'){
					if($sms->mergesms($values) == TRUE){
						$commitresult = "MERGE COMPLETED";
					}else{
						$commitresult = "FAILED";
					}
				}else{
					$commitresult = $returnresult['FAILED'];
				}
			break;
		}
}
$resultset = array(
'commitresult'=> $commitresult
);

echo  json_encode($resultset, JSON_HEX_QUOT | JSON_HEX_TAG);
?>