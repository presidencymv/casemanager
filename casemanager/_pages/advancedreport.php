<?php
//REDIRECT INVALID USERS
if ($_SESSION['reports'] != 'Y' && $_SESSION['admins'] != 'Y' &&  $_SESSION['handler'] != 'Y' ){
    header('location: dashboard.php?error=1');
    die();
}
?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="dashboard.php?page=home"><span class="glyphicon glyphicon-home"></span></a></li>
				<li>Reports</li>
				<li class="active">Advanced Report</li>
			</ol>
		</div><!--/.row-->
		<?php
        include('subcomponents/reportssubmenu.php');
        ?>
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Advanced Report</li>
			</div>
		</div><!--/.row-->
		<?php include('_pages/subforms/advancedreport_form.php'); ?>
		<?php include('_pages/subcomponents/advancedreport.php'); ?>
</div>	<!--/.main-->