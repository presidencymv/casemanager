<?php
if ($_SESSION['callcentrestaff'] != 'Y'){
    header('location: dashboard.php');
    die();
}
require('_ex/call.php');
?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="dashboard.php?page=callcentre"><span class="glyphicon glyphicon-home"></span></a></li>
				<li>Call Centre</li>
			</ol>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">CALL CENTRE</li>
			</div>
		</div><!--/.row-->
		<?php
        include('_pages/subcomponents/callcentre_submenu.php');
        ?>
		<div class="row">
			<div class="col-lg-12">
				<?php include('_pages/subcomponents/msgs.php'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<!-- Modal -->
				<div class="modal fade" id="myModal"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close" >
						  <span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="myModalLabel">UPDATE CALL</h4>
					  </div>
					  <div class="modal-body">
						<div id="spinner" class="text-center">
						</div>
						<div id="editform">
						
						</div>
					  </div>
					</div>
				  </div>
				</div>
			</div>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-6">
				<div class="panel panel-default">
				    <div class="panel-body">
							<div class="col-lg-12">
								<?php
									include('_pages/subcomponents/call_archiveyears.php'); 
								?>
								<hr>
							</div>
							
						<?php
						if ($archresults['result'] == TRUE):
						?>
					
						<table id="callstable"  data-toggle="table" data-url="_pages/data/calls.php?year=<?php echo $selectedyear ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="u_id" data-sort-order="desc">
							<thead>
							<tr>
								<th data-field="CR_LOGID"  data-sortable="true">#:</th>
								<th data-field="CR_CALLER"  data-sortable="true">Caller</th>
								<th data-field="CR_TEL"  data-sortable="true">Telephone</th>
								<th data-field="callstate"  data-sortable="true">Status</th>
								<th data-field="CR_ISSUE"  data-sortable="true">Issue:</th>
								<th data-field="CR_RESPONSE"  data-sortable="true">Response:</th>
								<th data-field="U_NAME"  data-sortable="true">Attended By:</th>
								<th data-field="detailsURL"  data-sortable="true">Action:</th>
							</tr>
							</thead>
						</table>
						<?php
						endif;
						?>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div id="calldetails">
				</div>
			</div>
		</div><!--/.row-->
</div>	<!--/.main-->
<script src="js/callcentre.js"></script>