<?php
require ('_pages/subcomponents/case_auth.php');
?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="dashboard.php?page=home"><span class="glyphicon glyphicon-home"></span></a></li>
				<li><a href="dashboard.php?page=cases">Cases</a></li>
				<li class="active"> Case: <?php echo $documentinfo['DOC_SYSID'] ?></li>
			</ol>
		</div><!--/.row-->
		<?php
		include('_pages/subcomponents/pageheader.php');
		?>
		<?php
        include('subcomponents/submenu.php');
        ?>
		<div class="row">
			<div class="col-lg-12">
				<?php include('_pages/subcomponents/msgs.php'); ?>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-8">
				<div class="panel panel-default">
				    <div class="panel-body">
					    <form action="case_share.php?ex=100&id=<?php echo $docid;?>" name = "share_case" enctype="multipart/form-data" method="post" onsubmit='return confirm("Send Case?")'>
                            <div class="form-group">
                                <div class="form-group col-lg-6">
									<label>Users</label>
									<select class="form-control" name="user" id="user">
									<?php
									$masterdata = new master;
									echo $masterdata->selectoptionsrequest('U_ID','U_NAME','user','');
									?>
									</select>
								</div>
							</div>
                            <div class="form-group col-lg-12">
                            <input class = "btn btn-primary" type="submit" title="submit" value="SHARE CASE">
                            </div>
                        </form>
					</div>	
				</div>
			</div>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12">
                <div class="panel panel-default">
					<div class="panel-body custom_text">
						<table data-toggle="table" data-url="_pages/data/shared_users.php?id=<?php echo $docid;?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="u_id" data-sort-order="desc">
							<thead>
							<tr>
								<th data-field="COUNT"  data-sortable="true">#</th>
								<th data-field="U_NAME"  data-sortable="true">USER NAME</th>
								<th data-field="URL"  data-sortable="true">REMOVE</th>
							</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->
</div>	<!--/.main-->

<?php
$update_type = 'caseshareview';
include ('_pages/subcomponents/alert_seen.php');
?>