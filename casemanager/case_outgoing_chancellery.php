<?php
header('Content-Type: application/json');
require('_ex/connections.php');
require('_ex/authentications.php');
require('_ex/master.php');
require('_ex/document.php');
require('_ex/chancellery.php');
require('_ex/remark.php');
require('_ex/alert.php');
$currentuser = new authentication;
$commitresult = "GOOD";

$case_change = 'caseremarks';

require ('_pages/subcomponents/case_change_auth.php');



if (isset($_GET['ex'])){
$ex = preg_replace( "/[^0-9$]/","",$_GET['ex']);
	if ($ex == ''){
	$commitresult = 'Missing EX OR DOC ID';
	}
}else{
	$commitresult = 'Missing EX OR DOC ID';
}

if (isset($_GET['rec'])){
$rec = preg_replace( "/[^0-9$]/","",$_GET['rec']);
	if ($rec == ''){
	$commitresult = 'Missing EX OR DOC ID';
	}
}else{
	$commitresult = 'Missing EX OR DOC ID';
}


$values= array($docid,$rec);
$fieldlist = array ('DocumentID','recordid');
$case = array('n','n');
$remark = new remark;

if($commitresult == "GOOD"){

	if ($remark->invalidchar($values,$fieldlist,$case,'on')){
		$chancellery = new chancellery;
		if($ex == 100){

			$chanid = $chancellery->getchanid($docid,0,$_SESSION['userid']);	

		}else{
			$chanid = $chancellery->getchanid($docid,2,$_SESSION['userid']);	
		}
		
		if ($remark->sendout($values,$chanid) == TRUE){
			$document->notification($docid,'New Update :'.$_POST['remark_description'].' By:'.$_SESSION['username'],15);
			$newalert = new alert;
			$newalert->create_alert($docid,'Case Updated');
			$commitresult = 'Success';
		}else{
			$commitresult = 'Failed';
		}

}



}

$resultset = array(
'commitresult'=> $commitresult
);

echo  json_encode($resultset, JSON_HEX_QUOT | JSON_HEX_TAG);
?>
