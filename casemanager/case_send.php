<?php
require('_ex/connections.php');    
require('_ex/authentications.php');
require('_ex/master.php');
require('_ex/document.php');
require('_ex/units.php');
require('_ex/alert.php');
$currentuser = new authentication;

if ($_SESSION['counter'] != 'Y' ){
	header('location: dashboard.php');
	die();
}
$case_change = 'casesend';

require ('_pages/subcomponents/case_change_auth.php');


if (isset($_GET['ex'])){
$ex = preg_replace( "/[^0-9$]/","",$_GET['ex']);
	if ($ex == ''){
	header('location: dashboard.php');
	die();
	}
}else{
	header('location: dashboard.php');
	die();
}

switch($ex){
        case '100':
			$values= array($docid,trim($_POST['section']));
			$fieldlist = array ("document","section");
			$case = array('n','n');
			
			if ($document->invalidchar($values,$fieldlist,$case,'on')){
				if ($document->routedocument($values) == TRUE){
					$unit = new unit;
					$unitname = $unit->getunitname($_POST['section']);
					$document->notification($docid,'Case Sent To Section '.$unitname.' By '.$_SESSION['username'],12);
					$newalert = new alert;
					$newalert->create_alert($docid,'Case Routed',$_POST['section']);
					header('location: dashboard.php?page=cases');
					die();
				}else{
					header('location: dashboard.php?page=caseedit&msg=3&id='.$docid);
					die();
				}
			}else{
				
				die('Oh no!');
			}
        break;

        case '101':
			$values= array($docid,trim($_POST['section']));
			$fieldlist = array ("document","section");
			$case = array('n','n');
			
			if ($document->invalidchar($values,$fieldlist,$case,'on')){
				if ($document->routedocument($values, $_SESSION['userid']) == TRUE){
					$unit = new unit;
					$unitname = $unit->getunitname($_POST['section']);
					$document->notification($docid,'Case Forwarded To Section '.$unitname.' By '.$_SESSION['username'],12);
					$newalert = new alert;
					$newalert->create_alert($docid,'Case Routed',$_POST['section']);
					die('Oh no!');
					header('location: dashboard.php?page=cases');
					die();
				}else{
					die('Oh no!');
					header('location: dashboard.php?page=caseedit&msg=3&id='.$docid);
					die();
				}
			}else{
				
				die('Oh no!');
			}
        break;	
    }
	
header('location: dashboard.php?page=caseedit&msg=2&id='.$docid);
die();	
?>