<?php
require('_ex/connections.php');    
require('_ex/authentications.php');
require('_ex/master.php');
require('_ex/units.php');
$currentuser = new authentication;
if ( $_SESSION['admins'] != 'Y'){
	header('location: dashboard.php');
	die();
}

if (isset($_GET['ex'])){
$ex = preg_replace( "/[^0-9$]/","",$_GET['ex']);
	if ($ex == ''){
	header('location: dashboard.php');
	die();
	}
}else{
	header('location: dashboard.php');
	die();
}

switch($ex){
        case '100':
			$values= array(trim($_POST['unitname']));
			$fieldlist = array ('Unit Name');
			$case = array('else');
			$unit = new unit;
			if ($unit->invalidchar($values,$fieldlist,$case,'on')){
				if ($unit->createunit($values) == TRUE){
					header('location: dashboard.php?page=manageunit');
					die();
				}else{
					header('location: dashboard.php?page=manageunit&msg=3');
					die();
				}
			}
        break;
		case '101':
			if  (isset($_GET['id'])){
				$id = preg_replace( "/[^0-9$]/","",$_GET['id']);
				if ($id == ''){
					header('location: dashboard.php');
					die();
				}
			}else{
				header('location: dashboard.php');
				die();
			}
			
			$dbc = new dbconnection;
			if(in_array($id,$dbc->noupdateSections)){
				header('location: dashboard.php');
				die();
			}
			
			
			$values= array($id,trim($_POST['unitname']));
			$fieldlist = array ('ID','Unit Name');
			$case = array('n','else');
			$unit = new unit;
			if ($unit->invalidchar($values,$fieldlist,$case,'on')){
				if ($unit->updateunit($values) == TRUE){
					header('location: dashboard.php?page=manageunit');
					die();
				}else{
					header('location: dashboard.php?page=manageunit&msg=3');
					die();
				}
			}
        break;
		case '102':
			if (isset($_GET['unit'])){
			$selectedunit = preg_replace( "/[^0-9$]/","",$_GET['unit']);
				if ($selectedunit == ''){
				header('location: dashboard.php');
				die();
				}
			}else{
				header('location: dashboard.php');
				die();
			}
			$values= array($selectedunit,trim($_POST['user']),trim($_POST['unitadmin']));
			$fieldlist = array ('Unit Name','User Name','Unit Admin');
			$case = array('n','n','t');
			$unit = new unit;
			if ($unit->invalidchar($values,$fieldlist,$case,'on')){
				if ($unit->createunitmember($values) == TRUE){
					header('location: dashboard.php?page=managemembers&msg=1&unit='.$selectedunit);
					die();
				}else{
					header('location: dashboard.php?page=managemembers&msg=3&unit='.$selectedunit);
					die();
				}
			}
        break;
		case '103':
			if (isset($_GET['id'])){
			$id = preg_replace( "/[^0-9$]/","",$_GET['id']);
				if ($id == ''){
				header('location: dashboard.php');
				die();
				}
			}else{
				header('location: dashboard.php');
				die();
			}
			
			if (isset($_GET['unit'])){
			$selectedunit = preg_replace( "/[^0-9$]/","",$_GET['unit']);
				if ($selectedunit == ''){
				header('location: dashboard.php');
				die();
				}
			}else{
				header('location: dashboard.php');
				die();
			}

			$values= array($id,$selectedunit,trim($_POST['user']),trim($_POST['unitadmin']));
			$fieldlist = array ('ID','Unit Name','User Name','Unit Admin');
			$case = array('n','n','n','t');
			$unit = new unit;
			if ($unit->invalidchar($values,$fieldlist,$case,'on')){
				if ($unit->updatemember($values) == TRUE){
					header('location: dashboard.php?page=managemembers&msg=1&unit='.$selectedunit);
					die();
				}else{
					header('location: dashboard.php?page=managemembers&msg=3&unit='.$selectedunit);
					die();
				}
			}
        break;
		
			case '104':
			if (isset($_GET['id'])){
			$id = preg_replace( "/[^0-9$]/","",$_GET['id']);
				if ($id == ''){
				header('location: dashboard.php');
				die();
				}
			}else{
				header('location: dashboard.php');
				die();
			}
			
			if (isset($_GET['unit'])){
			$selectedunit = preg_replace( "/[^0-9$]/","",$_GET['unit']);
				if ($selectedunit == ''){
				header('location: dashboard.php');
				die();
				}
			}else{
				header('location: dashboard.php');
				die();
			}
			
				$unit = new unit;
			if ($unit->removemember($id) == TRUE){
				header('location: dashboard.php?page=managemembers&msg=1&unit='.$selectedunit);
				die();
			}else{
				header('location: dashboard.php?page=managemembers&msg=3&unit='.$selectedunit);
				die();
			}
			
        break;
    }
header('location: dashboard.php?page=manageunit&msg=2');
die();	
?>