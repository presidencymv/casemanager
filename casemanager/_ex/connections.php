<?php
error_reporting(0);
class dbconnection {
    private $DB_HOST = '127.0.0.1';
    private $DB_USER = 'root';
    private $DB_PASSWORD = '';
    private $DB_SCHEMA = 'casemanager';
	public  $CounterUnitID = 1;//Replace this value if the counter unit ID changes.
	public  $CallCentreUnitID = 2;//Replace this value if the CallCentre unit ID changes.
    public  $MinistersDirectiveID = 3;
	public 	$noupdateSections = array(1,2,3);
    public  $dbconn;

    public function __construct(){
        $this->dbconn = new mysqli($this->DB_HOST,$this->DB_USER,$this->DB_PASSWORD,$this->DB_SCHEMA);
        if($this->dbconn->connect_errno > 0){
            die('Connection Failed!');
        }else{
            mysqli_set_charset( $this->dbconn,"utf8");
        }
    }
}
?>
