<?php
class unit extends master{

	public function getunitname($unitid){
		$sql = "SELECT UNIT_NAME from unit WHERE UNIT_ID = ".$unitid;
        $dbc = new dbconnection;
        $stmt =  $dbc->dbconn->stmt_init();
        $stmt->prepare($sql);
        $stmt->execute();
        $stmt->bind_result($UNITNAME);
        $stmt->fetch();
        $stmt->free_result();
        $stmt->close();
        $dbc->dbconn->close();
		return $UNITNAME;
	}

 public function getunitmembers($unitid){
      $dbc = new dbconnection;
      $counter=0;
	  $members = '';
      $sql = "select UM_U_ID from unit_member where UM_UNIT_ID = ".$unitid;
      $result = $dbc->dbconn->query($sql);
        while ($rows = mysqli_fetch_assoc($result)){
             if ($counter == 0){
             $members .= $rows['UM_U_ID'];
             $counter = 1;
             }else{
             $members .= ','.$rows['UM_U_ID'];    
             }
          }
    $result->free();
    $dbc->dbconn->close();
    return $members;
}
    public function createunitmember($values){
        $dbc = new dbconnection;
        $stmt = $dbc->dbconn->prepare("INSERT INTO unit_member (`um_unit_id`, `um_u_id`, `um_handler`, `um_added_by`) VALUES (?,?,?,?)");
        $stmt->bind_param('ddsd', $um_unit_id, $um_u_id, $um_handler, $um_added_by);
       
        $um_unit_id=$values[0];
        $um_u_id=$values[1];
		
		if ($values[2] != 'Y'){
			$um_handler='N';
		}else{
			$um_handler=$values[2];	
		}
        
        $um_added_by=$_SESSION['userid'];
        $stmt->execute();
		
        if ($stmt->errno) {
            $stmt->close();
			$dbc->dbconn->close();
			return FALSE; 
            
        }
        else {
			$stmt->close();
            $dbc->dbconn->close();
            return TRUE;
        }
        
    }

    public function mymember($recid){
        $dbc = new dbconnection;
          $sql="select * from unit_member WHERE UM_ID = ".$recid." AND UM_UNIT_ID IN ('". $_SESSION['handlerunits'] ."')"; 
          $result = $dbc->dbconn->query($sql);
          if (mysqli_num_rows($result) == 1){
             $result->free();
             $dbc->dbconn->close();
             return TRUE;
          }else{
             $result->free();
             $dbc->dbconn->close();
             return FALSE; 
          }
    }

    public function removemember($recid){
        $dbc = new dbconnection;
        $stmt = $dbc->dbconn->prepare("DELETE FROM unit_member WHERE UM_ID = ?");
        $stmt->bind_param('d', $UM_ID);
        $UM_ID = $recid;

        $stmt->execute();
        if ($stmt->errno) {
            $dbc->dbconn->close();
            return FALSE;
        }else{
            if ($dbc->dbconn->affected_rows > 0){
                $dbc->dbconn->close();
                return TRUE;
            }else{
                $dbc->dbconn->close();
                return FALSE;
            }
        }
	}
	
	public function updatemember($values){
        $dbc = new dbconnection;
        $stmt = $dbc->dbconn->prepare("UPDATE unit_member SET um_unit_id = ?,um_u_id = ?,um_handler = ?,um_added_by = ?  WHERE um_id = ".$values[0]);
        $stmt->bind_param('ddsd', $um_unit_id, $um_u_id, $um_handler, $um_added_by);

        $um_unit_id=$values[1];
        $um_u_id=$values[2];
		
		if ($values[3] != 'Y'){
			$um_handler='N';
		}else{
			$um_handler=$values[3];	
		}
        
        $um_added_by=$_SESSION['userid'];
		
        $stmt->execute();
        if ($stmt->errno) {
            $stmt->close();
            $dbc->dbconn->close();
            return FALSE;
        }else{
            $stmt->close();
            $dbc->dbconn->close();
            return TRUE;
        }
    }
	
	
	public function memberinfo($ID){
		$sql=" SELECT um_id, um_unit_id, um_u_id, um_handler FROM unit_member WHERE um_id = ".$ID;		 
		$dbc = new dbconnection;
		$stmt =  $dbc->dbconn->stmt_init();
		$stmt->prepare($sql);
		$stmt->execute();
		$stmt->bind_result($um_id,$um_unit_id,$um_u_id,$um_handler);
		$stmt->fetch();
		$stmt->free_result();
		$stmt->close();
		$dbc->dbconn->close();
		$memberinfo = Array(
		'um_id'=> $um_id,
		'um_unit_id'=> $um_unit_id,
		'um_u_id'=> $um_u_id,
		'um_handler'=> $um_handler
		);
		return $memberinfo; 
    }
	
	
	
	public function createunit($values){
        $dbc = new dbconnection;
        $stmt = $dbc->dbconn->prepare("INSERT INTO unit (`UNIT_NAME`, `UNIT_CREATEDBY`) VALUES (?, ?)");
        $stmt->bind_param('sd', $UNIT_NAME, $UNIT_CREATEDBY);

        $UNIT_NAME = $values[0];
        $UNIT_CREATEDBY = $_SESSION['userid'];
        
        $stmt->execute();
        if ($stmt->errno) {
			$stmt->close();
			$dbc->dbconn->close();
			RETURN FALSE; 
        }
        else {
           	$stmt->close();
			$dbc->dbconn->close();
			RETURN TRUE; 
        }
       
    }

    public function updateunit($values){
        $dbc = new dbconnection;
        $stmt = $dbc->dbconn->prepare("UPDATE unit SET UNIT_NAME = ?, UNIT_CREATEDBY = ? WHERE UNIT_ID = ".$values[0]);
        $stmt->bind_param('sd', $UNIT_NAME, $UNIT_CREATEDBY);

        $UNIT_NAME = $values[1];
        $UNIT_CREATEDBY = $_SESSION['userid'];
        $stmt->execute();
        if ($stmt->errno) {
            $stmt->close();
            $dbc->dbconn->close();
            return FALSE;
        }else{
            $stmt->close();
            $dbc->dbconn->close();
            return TRUE;
        }
    }
	
	
	
	
	public function unitinfo($ID){
		$sql=" SELECT UNIT_ID, UNIT_NAME FROM unit WHERE UNIT_ID = ".$ID;		 
		$dbc = new dbconnection;
		$stmt =  $dbc->dbconn->stmt_init();
		$stmt->prepare($sql);
		$stmt->execute();
		$stmt->bind_result($UNIT_ID,$UNIT_NAME);
		$stmt->fetch();
		$stmt->free_result();
		$stmt->close();
		$dbc->dbconn->close();
		$unitinfo = Array(
		'UNIT_ID'=> $UNIT_ID,
		'UNIT_NAME'=> $UNIT_NAME
		);
		return $unitinfo; 
    }

    public function getuserunits($userID){
        $dbc = new dbconnection;
        $counter = 0;
        $units = '';
        $sql = "select um_unit_id from unit_member where um_u_id = ".$userID;
        $result = $dbc->dbconn->query($sql);
        while ($rows = mysqli_fetch_assoc($result)){
            if ($counter == 0){
                $units .= $rows['um_unit_id'];
                $counter = 1;
            }else{
                $units .= ','.$rows['um_unit_id'];    
            }
        }
        $result->free();
        $dbc->dbconn->close();
        return $units;
    }

}
?>