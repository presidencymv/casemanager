<?php

class chancellery extends document{
	

    public function addtoregistery($exreply,$docid,$userid,$inout){
	
        $dbc = new dbconnection;
        $stmt = $dbc->dbconn->prepare("INSERT INTO in_out_registery (`IOR_CHAN_NUM`, `IOR_EXPECTED_REPLY`,`IOR_DOC_ID`,`IOR_BY`) VALUES (?, ?, ?, ?)");
        $stmt->bind_param('sddd', $IOR_CHAN_NUM ,$IOR_EXPECTED_REPLY, $IOR_DOC_ID ,$IOR_BY);
        $IOR_CHAN_NUM = $this->generate_chancellery_id($inout);
		$IOR_EXPECTED_REPLY = $exreply;
		$IOR_DOC_ID = $docid;
		$IOR_BY = $userid;
        $stmt->execute();
		
        if ($stmt->errno) {
			$stmt->close();
			$dbc->dbconn->close();
			return FALSE;
        }
        else {
			$stmt->close();
			$dbc->dbconn->close();
			return TRUE;
        }
		
    }
	
	public function getregidid($docid){
		$dbc = new dbconnection;
        $sql="select IOR_ID from in_out_registery WHERE IOR_DOC_ID =".$docid." AND IOR_CHAN_NUM LIKE '%IN%' ORDER BY IOR_ID DESC LIMIT 1" ;
        $result = $dbc->dbconn->query($sql);
        $IOR = mysqli_fetch_assoc($result);
		return $IOR['IOR_ID'];
	}
	
	public function getchanid($docid,$inout,$userid){
	
			if($inout == 1){
				$type = 'IN';
			}else if($inout == 0){
				$type = 'OUT';
			}else{
				$type = 'FT';
			}
	
			
			$dbc = new dbconnection;
       
			$result = $dbc->dbconn->query($sql);
			$IOR = mysqli_fetch_assoc($result);
			$dbc = new dbconnection;
			$sql="select IOR_ID from in_out_registery WHERE IOR_DOC_ID =".$docid." AND IOR_CHAN_NUM LIKE '%".$type."%' ORDER BY IOR_ID DESC LIMIT 1" ;
			$result =  $dbc->dbconn->query($sql);
			if($result){
			   if (mysqli_num_rows($result) == 0){
					$this->addtoregistery(0,$docid,$userid,$inout);
					$sql="select IOR_ID from in_out_registery WHERE IOR_DOC_ID =".$docid." AND IOR_CHAN_NUM LIKE '%".$type."%' ORDER BY IOR_ID DESC LIMIT 1" ;
					$result =  $dbc->dbconn->query($sql);
					$IOR = mysqli_fetch_assoc($result);
					$result->free();
					$dbc->dbconn->close();
				}else{
					$IOR = mysqli_fetch_assoc($result);
					$result->free();
					$dbc->dbconn->close();
				}
			}
			return $IOR['IOR_ID'];
	}
	
	
	private function generate_chancellery_id($inout){
	    $dbc = new dbconnection;
		if($inout == 1){
			$type = 'IN';
		}else if($inout == 0){
			$type = 'OUT';
		}else{
			$type = 'FT';
		}
        $sql = "SELECT CNC_SEQ, CONCAT('CH/', CNC_YEAR, '/', CNC_MONTH, '/".$type."/', CNC_SEQ) as NUMBER FROM chan_num_ctrl WHERE CNC_MONTH = month(curdate()) AND CNC_YEAR = year(curdate()) AND CNC_IO = ".$inout; 

        //CREATE NUMBER RANGE IF IT DOES NOT EXIST
        if ($this->recordexists($sql) == false){
            $ins_stmt = $dbc->dbconn->prepare("INSERT INTO chan_num_ctrl (`CNC_MONTH`, `CNC_YEAR`, `CNC_IO`, `CNC_SEQ`) VALUES ( month(curdate()), year(curdate()), ?, ?)");
            $ins_stmt->bind_param('dd', $CNC_IO, $CNC_SEQ);
			$CNC_IO = $inout;
            $CNC_SEQ = 1;
            $ins_stmt->execute();
            $ins_stmt->close();
        }
		
		//GET NUMBER
        $sel_stmt =  $dbc->dbconn->stmt_init();
        $sel_stmt->prepare($sql);
        $sel_stmt->execute();
        $sel_stmt->bind_result($RUNNING_NUMBER, $DISPLAYNUMBER);
        $sel_stmt->fetch();
        $sel_stmt->free_result();
        $sel_stmt->close();
                
        //ADD NUMBER
        $newnumber = $RUNNING_NUMBER + 1;
        $up_stmt = $dbc->dbconn->prepare("UPDATE chan_num_ctrl SET CNC_SEQ = ? WHERE CNC_MONTH = month(curdate()) AND CNC_YEAR = year(curdate()) AND CNC_IO = ".$inout);
        $up_stmt->bind_param('d', $newnumber);
        $up_stmt->execute();
        $up_stmt->close();

        //CLOSE CONNECTION
        $dbc->dbconn->close();

        //RETURN NUMBER
        return  $DISPLAYNUMBER;
	}
	

}
?>