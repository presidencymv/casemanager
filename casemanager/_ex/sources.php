<?php

class source extends master{

    public function createsource($values){
        $dbc = new dbconnection;
        $stmt = $dbc->dbconn->prepare("INSERT INTO doc_source (`DSrc_Name`) VALUES (?)");
        $stmt->bind_param('s', $DSrc_Name);
		
        $DSrc_Name = $values[0];

        $stmt->execute();
        if ($stmt->errno) {
			$stmt->close();
			$dbc->dbconn->close();
			RETURN FALSE; 
        }
        else {
           	$stmt->close();
			$dbc->dbconn->close();
			RETURN TRUE; 
        }
       
    }

    public function updatesource($values){
        $dbc = new dbconnection;
        $stmt = $dbc->dbconn->prepare("UPDATE doc_source SET DSrc_Name = ? WHERE DSrc_ID = ".$values[0]);
        $stmt->bind_param('s', $DSrc_Name);
        $DSrc_Name = $values[1];
        $stmt->execute();
        if ($stmt->errno) {
            $stmt->close();
            $dbc->dbconn->close();
            return FALSE;
        }else{
            $stmt->close();
            $dbc->dbconn->close();
            return TRUE;
        }
    }
	
	 public function sourceinfo($ID){
            $sql=" SELECT DSrc_ID, DSrc_Name FROM doc_source WHERE DSrc_ID = ".$ID;		 
            $dbc = new dbconnection;
            $stmt =  $dbc->dbconn->stmt_init();
            $stmt->prepare($sql);
            $stmt->execute();
            $stmt->bind_result($DSrc_ID,$DSrc_Name);
            $stmt->fetch();
            $stmt->free_result();
            $stmt->close();
            $dbc->dbconn->close();
            $sourceinfo = Array(
            'DSrc_ID'=> $DSrc_ID,
			'DSrc_Name'=> $DSrc_Name
            );
            return $sourceinfo; 
    }

}

?>
