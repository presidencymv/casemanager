<?php

class remark extends master{

        public function createremark($values, $docunitid = null){
        $dbc = new dbconnection;
        $stmt = $dbc->dbconn->prepare("INSERT INTO doc_remarks (`DR_DOC_ID`, `DR_DESC`, `DR_FILE`, `DR_FILE_UNIT`,`DR_OUT_REF`, `DR_BY`) VALUES (?, ?, ?, ?, ?, ?)");
        $stmt->bind_param('dssdsd', $DR_DOC_ID, $DR_DESC, $DR_FILE, $DR_FILE_UNIT,$DR_OUT_REF, $DR_BY);

        $DR_DOC_ID=$values[0];
        $DR_DESC=$values[1];
        $DR_FILE=$values[2];
		$DR_OUT_REF=$values[3];
        $DR_FILE_UNIT=$docunitid;
        $DR_BY=$_SESSION['userid'];
        $stmt->execute();
        if ($stmt->errno) {
			$stmt->close();
			$dbc->dbconn->close();
			RETURN FALSE;
        }
        else {
			$stmt->close();
			$dbc->dbconn->close();
            RETURN TRUE;
        }

    }
	
	public function createremark_chancellery($values, $docunitid = null, $CHANNUM, $hc){
		
        $dbc = new dbconnection;
        $stmt = $dbc->dbconn->prepare("INSERT INTO doc_remarks (`DR_DOC_ID`, `DR_DESC`, `DR_FILE`, `DR_FILE_UNIT`,`DR_OUT_REF`, `DR_BY`, `DR_IOR_ID`, `DR_HARDCOPY`) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
        $stmt->bind_param('dssdsddd', $DR_DOC_ID, $DR_DESC, $DR_FILE, $DR_FILE_UNIT,$DR_OUT_REF, $DR_BY, $DR_IOR_ID, $DR_HARDCOPY);

        $DR_DOC_ID=$values[0];
        $DR_DESC=$values[1];
        $DR_FILE=$values[2];
		$DR_OUT_REF=$values[3];
        $DR_FILE_UNIT=$docunitid;
        $DR_BY=$_SESSION['userid'];
		$DR_IOR_ID=$CHANNUM;
		$DR_HARDCOPY=$hc;
		
        $stmt->execute();
        if ($stmt->errno) {
			$stmt->close();
			$dbc->dbconn->close();
			RETURN FALSE;
        }
        else {
			$stmt->close();
			$dbc->dbconn->close();
            RETURN TRUE;
        }

    }
	

        public function generatedocname(){
        $dbc = new dbconnection;
        $sql="select DR_ID,DR_DOC_ID, DR_FILE,DR_ON from doc_remarks WHERE DR_BY =".$_SESSION['userid']." ORDER BY DR_ID DESC LIMIT 1" ;
        $result = $dbc->dbconn->query($sql);
        $docid = mysqli_fetch_assoc($result);
        //$ext = substr($docid['DR_FILE'], strpos($docid['DR_FILE'],'.'), strlen($docid['DR_FILE'])-1);
        $ext = explode('.', $docid['DR_FILE']);
        $ext = strtolower(end($ext));

        $newfilename = $docid['DR_DOC_ID'].md5(mt_rand().$docid['DR_ON'].$docid['DR_ID']).'.'.$ext;

        $sql = "UPDATE doc_remarks SET DR_FILE = '".$newfilename."' WHERE DR_ID = ".$docid['DR_ID']." AND DR_DOC_ID = ".$docid['DR_DOC_ID'];
        $execute = $dbc->dbconn->query($sql);

        $result->free();
        $dbc->dbconn->close();
        return $newfilename;
        }

        public function remarkinfo($remarkid,$docid){
           $sql="select DR_ID, DR_DOC_ID, DR_OUT_REF, DR_DESC, DR_FILE, DR_FILE_UNIT, DR_BY, DR_ON, DOC_ASSIGNEDTO, DOC_UNIT, DOC_CREATEDBY, DOC_STATUS, DR_IOR_ID, DR_PAGES, DR_HARDCOPY,DR_DETAILS,DR_AUTHOR,DR_AUTHOR_TEL1,DR_AUTHOR_TEL2,DR_AUTHOR_EMAIL,DR_CREATION_DATE,DR_DCH_ID from doc_remarks Inner Join document ON DR_DOC_ID = DOC_ID WHERE DR_ID =".$remarkid." AND DR_DOC_ID = ".$docid ;
           $dbc = new dbconnection;
           $stmt =  $dbc->dbconn->stmt_init();
           $stmt->prepare($sql);
           $stmt->execute();
           $stmt->bind_result($DR_ID,$DR_DOC_ID,$DR_OUT_REF,$DR_DESC,$DR_FILE,$DR_FILE_UNIT,$DR_BY,$DR_ON,$DOC_ASSIGNEDTO,$DOC_UNIT,$DOC_CREATEDBY,$DOC_STATUS,$DR_IOR_ID,$DR_PAGES,$DR_HARDCOPY,$DR_DETAILS,$DR_AUTHOR,$DR_AUTHOR_TEL1,$DR_AUTHOR_TEL2,$DR_AUTHOR_EMAIL,$DR_CREATION_DATE,$DR_DCH_ID);
           $stmt->fetch();
           $stmt->free_result();
           $stmt->close();
           $dbc->dbconn->close();
		//DR_AUTHOR,DR_AUTHOR_TEL1,DR_AUTHOR_TEL2,DR_AUTHOR_EMAIL,DR_CREATION_DATE,DR_DCH_ID
           $remarkinfo = Array(
           'DR_ID'=> $DR_ID,
           'DR_DOC_ID'=> $DR_DOC_ID,
		   'DR_OUT_REF'=> $DR_OUT_REF,
           'DR_DESC'=> $DR_DESC,
           'DR_FILE'=> $DR_FILE,
           'DR_FILE_UNIT'=> $DR_FILE_UNIT,
           'DR_BY'=> $DR_BY,
           'DR_ON'=> $DR_ON,
		   'DOC_ASSIGNEDTO'=>$DOC_ASSIGNEDTO,
		   'DOC_UNIT'=>$DOC_UNIT,
		   'DOC_CREATEDBY'=>$DOC_CREATEDBY,
		   'DOC_STATUS'=>$DOC_STATUS,
		   'DR_IOR_ID'=>$DR_IOR_ID,
		   'DR_PAGES'=>$DR_PAGES,
		   'DR_HARDCOPY'=>$DR_HARDCOPY,
		   'DR_DETAILS'=>$DR_DETAILS,
		   'DR_AUTHOR'=>$DR_AUTHOR,
		   'DR_AUTHOR_TEL1'=>$DR_AUTHOR_TEL1,
		   'DR_AUTHOR_TEL2'=>$DR_AUTHOR_TEL2,
		   'DR_AUTHOR_EMAIL'=>$DR_AUTHOR_EMAIL,
		   'DR_CREATION_DATE'=>$DR_CREATION_DATE,
		   'DR_DCH_ID'=>$DR_DCH_ID
           );
           return  $remarkinfo;

        }

        public function updateremark($values, $newname = null){
          $dbc = new dbconnection;

          if($newname != '' && $newname != null){
            $stmt = $dbc->dbconn->prepare("UPDATE doc_remarks SET `DR_DESC` = ?, `DR_FILE` = ?, `DR_BY` = ?, `DR_OUT_REF` = ? WHERE DR_ID = ? AND DR_DOC_ID = ?");
            $stmt->bind_param('ssdsdd', $DR_DESC, $DR_FILE, $DR_BY, $DR_OUT_REF, $DR_ID, $DR_DOC_ID);

            $DR_ID=$values[0];
            $DR_DOC_ID = $values[1];
            $DR_DESC=$values[2];
            $DR_FILE=$newname;
			$DR_OUT_REF =$values[3];
            $DR_BY=$_SESSION['userid'];
          }else{
            $stmt = $dbc->dbconn->prepare("UPDATE doc_remarks SET `DR_DESC` = ?, `DR_BY` = ?,`DR_OUT_REF` = ? WHERE DR_ID = ? AND DR_DOC_ID = ?");
            $stmt->bind_param('sdsdd', $DR_DESC, $DR_BY,$DR_OUT_REF, $DR_ID, $DR_DOC_ID);
            $DR_ID=$values[0];
            $DR_DOC_ID = $values[1];
            $DR_DESC=$values[2];
			$DR_OUT_REF =$values[3];
            $DR_BY=$_SESSION['userid'];
          }

          

          $stmt->execute();
          if ($stmt->errno) {
           $stmt->close();
           $dbc->dbconn->close();
           RETURN FALSE;
          }
          else {
           $stmt->close();
           $dbc->dbconn->close();
           RETURN TRUE;
          }

        }

      	public function delete_remarks($DR_ID, $DOC_ID){
      		$dbc = new dbconnection;
      		$rem_info = $this->remarkinfo($DR_ID, $DOC_ID);

      		//if(unlink('files/attachments/'.$rem_info['DR_FILE'])){
				
				
			if(rename('files/attachments/'.$rem_info['DR_FILE'], 'files/RECBIN/'.$rem_info['DR_FILE'])){

      			$sql = "DELETE FROM doc_remarks WHERE DR_ID = ".$DR_ID;
      			$execute = $dbc->dbconn->query($sql);
      			if($dbc->dbconn->affected_rows){
      				return TRUE;
      			}else{
      				return FALSE;
      			}
      		}else{
      			return FALSE;
      		}
      		$stmt->close();
      		}
			
		public function sendout($values, $chanid){
			$dbc = new dbconnection;
			$stmt = $dbc->dbconn->prepare("UPDATE doc_remarks SET `DR_IOR_ID` = ? WHERE DR_DOC_ID = ? AND DR_ID = ? AND DR_IOR_ID IS NULL");
			$stmt->bind_param('ddd', $DR_IOR_ID, $DR_DOC_ID,$DR_ID);

			$DR_IOR_ID=$chanid;
			$DR_DOC_ID = $values[0];
			$DR_ID=$values[1];

			$stmt->execute();
			if ($stmt->errno) {
				$stmt->close();
				$dbc->dbconn->close();
				RETURN FALSE;
			}
			else {
				$stmt->close();
				$dbc->dbconn->close();
				RETURN TRUE;
			}
		
		}
		
		public function maintain_attributes($values){
	
			$dbc = new dbconnection;
			$stmt = $dbc->dbconn->prepare("UPDATE doc_remarks SET `DR_OUT_REF` = ?,`DR_DETAILS` = ? ,`DR_PAGES` = ? ,`DR_HARDCOPY` = ?,`DR_AUTHOR` = ?,`DR_AUTHOR_TEL1` = ? ,`DR_AUTHOR_TEL2` = ?,`DR_AUTHOR_EMAIL` = ?,`DR_CREATION_DATE` = ? ,`DR_DCH_ID` = ?  WHERE DR_DOC_ID = ? AND DR_ID = ?");
			$stmt->bind_param('ssddsssssddd', $DR_OUT_REF, $DR_DETAILS, $DR_PAGES, $DR_HARDCOPY, $DR_AUTHOR, $DR_AUTHOR_TEL1, $DR_AUTHOR_TEL2, $DR_AUTHOR_EMAIL, $DR_CREATION_DATE, $DR_DCH_ID, $DR_DOC_ID, $DR_ID);

			$DR_OUT_REF = $values[4];
			$DR_DETAILS = $values[5];
			$DR_PAGES = $values[3];
			$DR_HARDCOPY = $values[2];
			$DR_AUTHOR = $values[6];
			$DR_AUTHOR_TEL1 = $values[7];
			$DR_AUTHOR_TEL2 = $values[8];
			$DR_AUTHOR_EMAIL = $values[9];
			$DR_CREATION_DATE = $values[11];
			$DR_DCH_ID = $values[10];
			$DR_DOC_ID = $values[0];
			$DR_ID = $values[1];

			$stmt->execute();
			if ($stmt->errno) {
				$stmt->close();
				$dbc->dbconn->close();
				RETURN FALSE;
			}
			else {
				$stmt->close();
				$dbc->dbconn->close();
				RETURN TRUE;
			}
		
		}
		
}
?>
