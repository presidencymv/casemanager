<?php
class contact extends master{

	public function contact_details($docid,$contact_id){
            $sql="SELECT CC_ID, CC_NAME, CC_DESIG, CC_ADDRESS, CC_TEL, CC_FAX, CC_EMAIL FROM case_contact inner join user ON CC_BY = U_ID WHERE CC_ID = ".$contact_id." AND CC_DOC_ID = ".$docid;
            $dbc = new dbconnection;
            $stmt =  $dbc->dbconn->stmt_init();
            $stmt->prepare($sql);
            $stmt->execute();
            $stmt->bind_result($CC_ID,$CC_NAME,$CC_DESIG,$CC_ADDRESS,$CC_TEL,$CC_FAX,$CC_EMAIL);
            $stmt->fetch();
            $stmt->free_result();
            $stmt->close();
            $dbc->dbconn->close();
            $contact_info = Array(
            'CC_ID'=> $CC_ID,
            'CC_NAME'=> $CC_NAME,
            'CC_DESIG'=> $CC_DESIG, 
            'CC_ADDRESS'=> $CC_ADDRESS,
            'CC_TEL'=> $CC_TEL,
            'CC_FAX'=> $CC_FAX, 
            'CC_EMAIL'=> $CC_EMAIL
            );
            return $contact_info; 
        }
		
        public function addcontact($values,$CRTYPE=0){
            $dbc = new dbconnection;
            $stmt = $dbc->dbconn->prepare("INSERT INTO case_contact (`CC_DOC_ID`, `CC_NAME`, `CC_DESIG`,`CC_ADDRESS`,`CC_TEL`,`CC_FAX`,`CC_EMAIL`,`CC_BY`,`CC_CR_CONTACT`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
            $stmt->bind_param('dssssssdd', $CC_DOC_ID, $CC_NAME, $CC_DESIG, $CC_ADDRESS, $CC_TEL, $CC_FAX, $CC_EMAIL, $CC_BY, $CC_CR_CONTACT);
            $CC_DOC_ID = $values[0];
            $CC_NAME = $values[1];
            $CC_DESIG = $values[2];
            $CC_ADDRESS = $values[3];
            $CC_EMAIL = $values[4];
            $CC_TEL = $values[5];
            $CC_FAX = $values[6];
			$CC_BY =$_SESSION['userid'];
			$CC_CR_CONTACT = $CRTYPE;
			$stmt->execute();
			if ($stmt->errno) {
				$stmt->close();
				$dbc->dbconn->close();
				return FALSE;
			}else{
				$stmt->close();
				$dbc->dbconn->close();
				return TRUE;
			}
        }

        public function updatecontact($values){
            $dbc = new dbconnection;
            $up_stmt = $dbc->dbconn->prepare("UPDATE case_contact SET CC_NAME = ?,CC_DESIG = ?,CC_ADDRESS = ?,CC_TEL = ?,CC_FAX = ?,CC_EMAIL = ?, CC_BY = ? WHERE  CC_DOC_ID = ".$values[0]." AND CC_ID = ".$values[1]);
            $up_stmt->bind_param('ssssssd', $CC_NAME, $CC_DESIG, $CC_ADDRESS, $CC_TEL, $CC_FAX, $CC_EMAIL, $CC_BY );
			$CC_NAME = $values[2];
            $CC_DESIG = $values[3];
            $CC_ADDRESS = $values[4];
            $CC_EMAIL = $values[5];
            $CC_TEL = $values[6];
            $CC_FAX = $values[7];
			$CC_BY =$_SESSION['userid'];
            $up_stmt->execute();
			if ($up_stmt->errno) {
				$up_stmt->close();
				$dbc->dbconn->close();
				return FALSE;
			}else{
				$up_stmt->close();
				$dbc->dbconn->close();
				return TRUE;
			}
        }
		
		
        public function updatedefault_callcontact($values){
            $dbc = new dbconnection;
            $up_stmt = $dbc->dbconn->prepare("UPDATE case_contact SET CC_NAME = ?, CC_ADDRESS = ?, CC_TEL = ?, CC_EMAIL = ?, CC_BY = ? WHERE  CC_DOC_ID = ? AND CC_CR_CONTACT = 1");
            $up_stmt->bind_param('ssssdd', $CC_NAME, $CC_ADDRESS, $CC_TEL, $CC_EMAIL, $CC_BY, $CC_DOC_ID );
			
			$CC_NAME = $values[2];
            $CC_ADDRESS = $values[3];
            $CC_TEL = $values[4];
            $CC_EMAIL = $values[5];
            $CC_BY = $values[0];
            $CC_DOC_ID = $values[1];
			
            $up_stmt->execute();

			if ($up_stmt->errno) {
				$up_stmt->close();
				$dbc->dbconn->close();
				return FALSE;
			}else{
				$up_stmt->close();
				$dbc->dbconn->close();
				return TRUE;
			}
        }

        public function valid_contact($CC_DOC_ID,$CC_ID){  
          $dbc = new dbconnection;
          $sql="SELECT * FROM case_contact WHERE CC_CR_CONTACT = 0 AND CC_DOC_ID = ".$CC_DOC_ID." AND CC_ID = ".$CC_ID;
          $result = $dbc->dbconn->query($sql);
          if (mysqli_num_rows($result) == 1){
            $dbc->dbconn->close();
            return TRUE;   
          }else{
            $dbc->dbconn->close();
            return FALSE;   
          }
        }
		
		


}
	
?>