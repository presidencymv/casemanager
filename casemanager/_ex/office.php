<?php

class office extends master{

    public function createoffice($values){
        $dbc = new dbconnection;
        $stmt = $dbc->dbconn->prepare("INSERT INTO office (`O_AGA`, `O_BA`,`O_NAME`, `O_ADDRESS`, `O_DEFAULTPRIO`, `O_CREATEDBY`) VALUES (?, ?, ?, ?, ?, ?)");
        $stmt->bind_param('ddssdd', $O_AGA, $O_BA, $O_NAME, $O_ADDRESS, $O_DEFAULTPRIO, $O_CREATEDBY);
        
		if ($values[0] == ''){
			$O_AGA = NULL;
		}else{
			$O_AGA = $values[0];
		}
        
		if ($values[1] == ''){
			$O_BA = NULL;
		}else{
			$O_BA = $values[2];
		}
        
        $O_NAME = $values[2];
		
        if ($values[3] == ''){
			$O_ADDRESS = NULL;
		}else{
			$O_ADDRESS = $values[3];
		}
		
        $O_DEFAULTPRIO = $values[4];
        $O_CREATEDBY = $_SESSION['userid'];
        
        $stmt->execute();
        if ($stmt->errno) {
			$stmt->close();
			$dbc->dbconn->close();
			Return FALSE;
        }
        else {
			$stmt->close();
			$dbc->dbconn->close();
			Return TRUE;
        }
        
    }

    public function updateoffice($values){
        $dbc = new dbconnection;
        $stmt = $dbc->dbconn->prepare("UPDATE office SET O_AGA = ?, O_BA = ?, O_NAME = ?, O_ADDRESS = ?, O_DEFAULTPRIO = ?, O_CREATEDBY = ? WHERE O_ID = ".$values[0]);
        $stmt->bind_param('ddssdd', $O_AGA, $O_BA, $O_NAME, $O_ADDRESS, $O_DEFAULTPRIO, $O_CREATEDBY);

        if ($values[1] == ''){
			$O_AGA = NULL;
		}else{
			$O_AGA = $values[1];
		}
        
		if ($values[2] == ''){
			$O_BA = NULL;
		}else{
			$O_BA = $values[2];
		}
        
        $O_NAME = $values[3];
		
        if ($values[4] == ''){
			$O_ADDRESS = NULL;
		}else{
			$O_ADDRESS = $values[4];
		}
        $O_DEFAULTPRIO = $values[5];
        $O_CREATEDBY = $_SESSION['userid'];
        $stmt->execute();
        if ($stmt->errno) {
            $stmt->close();
            $dbc->dbconn->close();
            return FALSE;
        }else{
		
			if ($dbc->dbconn->affected_rows > 0){
                $stmt->close();
				$dbc->dbconn->close();
				return TRUE;
            }else{
                $stmt->close();
				$dbc->dbconn->close();
				return FALSE;
            }
		
			
            
        }
    }
	
	 public function officeuinfo($O_ID){
            $sql=" SELECT
					 O_ID,
					 AGA_ID,
					 O_BA,
					 O_NAME,
					 O_ADDRESS,
					 P_ID
					 FROM office
					 LEFT OUTER JOIN aga ON O_AGA = AGA_ID
					 INNER JOIN priority ON O_DEFAULTPRIO = P_ID WHERE O_ID = ".$O_ID;
					 
            $dbc = new dbconnection;
            $stmt =  $dbc->dbconn->stmt_init();
            $stmt->prepare($sql);
            $stmt->execute();
            $stmt->bind_result($O_ID,$AGA_ID,$O_BA,$O_NAME,$O_ADDRESS,$P_ID);
            $stmt->fetch();
            $stmt->free_result();
            $stmt->close();
            $dbc->dbconn->close();
            $officeuinfo = Array(
            'O_ID'=> $O_ID,
            'AGA_ID'=> $AGA_ID,
            'O_BA'=> $O_BA, 
            'O_NAME'=> $O_NAME,
            'O_ADDRESS'=> $O_ADDRESS,
            'P_ID'=> $P_ID
            );
            return $officeuinfo; 
    }


}

?>
