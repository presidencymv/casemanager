<?php
class alert{

	public function create_alert($document_id, $a_type, $section_id = null, $user = null){

		if($a_type == 'Case Routed'){

  			//create an alert for each admin in the routed section
			$this->case_routed_alert($document_id, $a_type, $section_id);

		}elseif($a_type == 'Case Assigned'){

    		//create an alert for the assigned staff
			$this->case_assigned_alert($document_id, $a_type, $user);

		}elseif($a_type == 'Case Shared'){

    		//create an alert for the staff with whom the case was shared with, all admins in the section/unit, assigned staff and shared staffs
			$this->case_shared_alert($document_id, $a_type, $user);

		}elseif($a_type == 'Case Comment'){

    		//create an alert for all admins in the section/unit, assigned staff and shared staffs
			$this->case_comment_alert($document_id, $a_type);

		}elseif($a_type == 'Case Updated'){

    		//create an alert for all admins in the section/unit, assigned staff and shared staffs
			$this->case_update_alert($document_id, $a_type);

		}elseif($a_type == 'Case New Attachment'){

    		//create an alert for all admins in the section/unit, assigned staff and shared staffs
			$this->case_attachment_alert($document_id, $a_type);

		}elseif($a_type == 'Case Attachments Updated'){

    		//create an alert for all admins in the section/unit, assigned staff and shared staffs
			$this->case_attachment_updated_alert($document_id, $a_type);

		}elseif($a_type == 'Case Details Edited'){

    		//create an alert for all admins in the section/unit, assigned staff and shared staffs
			$this->case_details_edited_alert($document_id, $a_type);

		}elseif($a_type == 'Case Closed'){

    		//create an alert for all admins in the section/unit, and shared staffs
			$this->case_status_change_alert($document_id, $a_type);

		}elseif($a_type == 'Case Reopened'){

    		//create an alert for all admins in the section/unit, and shared staffs
			$this->case_status_change_alert($document_id, $a_type);

		}elseif($a_type == 'Case Rejected'){

      		//create an alert for the staff who created the case
			$this->case_rejected_alert($document_id, $a_type);

		}
	}




	private function case_rejected_alert($document_id, $a_type){

		$dbc = new dbconnection;

		//get the staff id of the one who created the case. 
		//staff id will be used to create an alert for that staff
		$sql3="select DOC_CREATEDBY from document WHERE DOC_ID =".$document_id." LIMIT 1";
		$result3 = $dbc->dbconn->query($sql3);
		$docid = mysqli_fetch_assoc($result3);
		$doc_created_staffid = $docid['DOC_CREATEDBY'];
		$result3->free();
		$dbc->dbconn->close();

		//1) Case Rejected
		//check if not the case is closed by the created staff
		if($doc_created_staffid != $_SESSION['userid']){
			$this->assigned_alert_create($doc_created_staffid, 'caseRejected', $document_id, $a_type, 26);
		}

	}




	private function case_status_change_alert($document_id, $a_type){
  	//There are a possible of 2 alerts that maybe created when a case status is changed. They are
  	// 1) Case Closed/Reopened in My Section 	2) Case Closed/Reopened in a Case Shared with Me
  	// lets create one after the other

    //get the section/unit id and assigned staff id. 
    //section/unit id will be used to get a list of all admins in that section
    //assigned staff id will be used to create an alert for that staff
		$dbc = new dbconnection;
		$sql3="select DOC_UNIT, DOC_ASSIGNEDTO from document WHERE DOC_ID =".$document_id." LIMIT 1";
		$result3 = $dbc->dbconn->query($sql3);
		$docid = mysqli_fetch_assoc($result3);
		$sec_id = $docid['DOC_UNIT'];
		$doc_assigned_staffid = $docid['DOC_ASSIGNEDTO'];
		$result3->free();
		$dbc->dbconn->close();

    //Note:currently, only the assigned staff can change the status. (that is set status to ongoing or close)
    //in the future if others are allowed to change the status, the below methods to create alerts will not be correct

    //set the values based on whether it is case closed or case reopened
		if($a_type == 'Case Closed'){
			$key1 = 'caseClosedSection';
			$type_desc = 22;
			$key2 = 'caseClosedShared';
			$type_desc2 = 23;
		}else{
			$key1 = 'caseReopenedSection';
			$type_desc = 24;
			$key2 = 'caseReopenedShared';
			$type_desc2 = 25;
		}

    //1) Case Closed/Reopened in My Section
    //check if the case is assigned to a section
		if($sec_id != null){
    	//get list of all section heads in the routed section
			$sectionheads = $this->get_sectionheads_preference($sec_id);
	  	//for each section head, if his notification setting is turned on for Case Closed in My Section, create an alert
			foreach($sectionheads as $items){
	  		//only if this section head is not the logged in user (say you are the head and you commented, you do not need to alert yourself)
				if($items['um_u_id'] != $_SESSION['userid']){
					$preferences = json_decode($items['U_PREFERENCES'],true);
					if($preferences[$key1]){
						$this->save_alert($items['um_u_id'], $document_id, $a_type, $type_desc);
					}else{
	  				//check if this section head is following this case
						$dbc = new dbconnection;
						$sql = "select DF_DOC_ID, DF_U_ID from doc_follow where DF_DOC_ID = ".$document_id." and DF_U_ID =".$items['um_u_id'];
						$result = $dbc->dbconn->query($sql);
						if(mysqli_num_rows($result) == 1){
							$this->save_alert($items['um_u_id'], $document_id, $a_type, $type_desc);
						}
						$result->free();
						$dbc->dbconn->close();
					}
				}
			}
		}

    //2) Case Closed/Reopened in a Case Shared with Me
    //get list of all staffs with whom case has already been shared
		$sharedstaffpreference = $this->get_sharedstaff_preference($document_id);
    //for each staff with whom has case already been shared, if his notification setting is turned on for $key1, create an alert
		foreach($sharedstaffpreference as $items){
    	//only if this shared staff is not the logged in user (say you are the shared user and you commented, you do not need to alert yourself)
			if($items['DS_U_ID'] != $_SESSION['userid']){
				$preferences = json_decode($items['U_PREFERENCES'],true);
				if($preferences[$key2]){
					$flag = true;
    			//only if, shared staff is not a section head of the doc routed section and he has turned on notifications for caseClosedSection
					$dbc = new dbconnection;
					$sql = "select um_id from unit_member where um_handler = 'Y' and um_u_id = ".$items['DS_U_ID']." and um_unit_id =".$sec_id;
					$result = $dbc->dbconn->query($sql);
					if(mysqli_num_rows($result) == 1){
						if($preferences[$key1]){
							$flag = false;
						}else{
							$result->free();
    					//check if this section head is following this case
							$sql = "select DF_ID from doc_follow where DF_DOC_ID = ".$document_id." and DF_U_ID =".$items['DS_U_ID'];
							$result = $dbc->dbconn->query($sql);
							if(mysqli_num_rows($result) == 1){
								$flag = false;
							}
						}
					}
					$result->free();
					$dbc->dbconn->close();

					if($flag == true){
						$this->save_alert($items['DS_U_ID'], $document_id, $a_type, $type_desc2);    					
					}
				}
			}
		}
	}




	private function case_details_edited_alert($document_id, $a_type){
  	//There are a possible of 3 alerts that maybe created when a case details is edited. They are
  	// 1) Edit Case in a Case Assigned to Me 	2) Edit Case in a Case in My Section 	3) Edit Case in a Case Shared with Me
  	// lets create one after the other


    //get the section/unit id and assigned staff id. 
    //section/unit id will be used to get a list of all admins in that section
    //assigned staff id will be used to create an alert for that staff
		$dbc = new dbconnection;
		$sql3="select DOC_UNIT, DOC_ASSIGNEDTO from document WHERE DOC_ID =".$document_id." LIMIT 1" ;
		$result3 = $dbc->dbconn->query($sql3);
		$docid = mysqli_fetch_assoc($result3);
		$sec_id = $docid['DOC_UNIT'];
		$doc_assigned_staffid = $docid['DOC_ASSIGNEDTO'];
		$result3->free();

    //1) Edit Case in a Case Assigned to Me
    //check if the case is assigned to a staff and Attachment is not made by the assigned staff
		if($doc_assigned_staffid != null && $doc_assigned_staffid != $_SESSION['userid']){
			$this->assigned_alert_create($doc_assigned_staffid, 'caseEditAssigned', $document_id, $a_type, 20);
		}



    //2) Edit Case in a Case in My Section
    //check if the case is assigned to a section
		if($sec_id != null){
			$this->section_alert_create($sec_id, 'caseEditSection', $doc_assigned_staffid, 'caseEditAssigned', $document_id, $a_type, 19);
		}

    //3) Edit Case in a Case Shared with Me
		$this->shared_alert_create($document_id, 'caseEditShared', $doc_assigned_staffid, 'caseEditAssigned', $sec_id, 'caseEditSection', $a_type, 21);
	}




	private function case_attachment_updated_alert($document_id, $a_type){
  	//There are a possible of 3 alerts that maybe created when a case attachment is updated. They are
  	// 1) Attachment Updated in a Case Assigned to Me 	2) Attachment Updated in a Case in My Section 	3) Attachment Updated in a Case Shared with Me
  	// lets create one after the other


    //get the section/unit id and assigned staff id. 
    //section/unit id will be used to get a list of all admins in that section
    //assigned staff id will be used to create an alert for that staff
		$dbc = new dbconnection;
		$sql3="select DOC_UNIT, DOC_ASSIGNEDTO from document WHERE DOC_ID =".$document_id." LIMIT 1" ;
		$result3 = $dbc->dbconn->query($sql3);
		$docid = mysqli_fetch_assoc($result3);
		$sec_id = $docid['DOC_UNIT'];
		$doc_assigned_staffid = $docid['DOC_ASSIGNEDTO'];
		$result3->free();

    //1) Attachment Updated in a Case Assigned to Me
    //check if the case is assigned to a staff and Attachment is not made by the assigned staff
		if($doc_assigned_staffid != null && $doc_assigned_staffid != $_SESSION['userid']){
			$this->assigned_alert_create($doc_assigned_staffid, 'caseFileUpdatedAssigned', $document_id, $a_type, 17);
		}



    //2) Attachment Updated in a Case in My Section
    //check if the case is assigned to a section
		if($sec_id != null){
			$this->section_alert_create($sec_id, 'caseFileUpdatedSection', $doc_assigned_staffid, 'caseFileUpdatedAssigned', $document_id, $a_type, 16);
		}

    //3) Attachment Updated in a Case Shared with Me
		$this->shared_alert_create($document_id, 'caseFileUpdatedShared', $doc_assigned_staffid, 'caseFileUpdatedAssigned', $sec_id, 'caseFileUpdatedSection', $a_type, 18);
	}




	private function case_attachment_alert($document_id, $a_type){
  	//There are a possible of 3 alerts that maybe created when attachment is uploaded. They are
  	// 1) New Attachment to a Case Assigned to Me 	2) New Attachment to a Case in My Section 	3) New Attachment to a Case Shared with Me
  	// lets create one after the other


    //get the section/unit id and assigned staff id. 
    //section/unit id will be used to get a list of all admins in that section
    //assigned staff id will be used to create an alert for that staff
		$dbc = new dbconnection;
		$sql3="select DOC_UNIT, DOC_ASSIGNEDTO from document WHERE DOC_ID =".$document_id." LIMIT 1" ;
		$result3 = $dbc->dbconn->query($sql3);
		$docid = mysqli_fetch_assoc($result3);
		$sec_id = $docid['DOC_UNIT'];
		$doc_assigned_staffid = $docid['DOC_ASSIGNEDTO'];
		$result3->free();

    //1) New Attachment to a Case Assigned to Me
    //check if the case is assigned to a staff and Attachment is not made by the assigned staff
		if($doc_assigned_staffid != null && $doc_assigned_staffid != $_SESSION['userid']){
			$this->assigned_alert_create($doc_assigned_staffid, 'caseNewFileAssigned', $document_id, $a_type, 14);
		}



    //2) New Attachment to a Case in My Section
    //check if the case is assigned to a section
		if($sec_id != null){
			$this->section_alert_create($sec_id, 'caseNewFileSection', $doc_assigned_staffid, 'caseNewFileAssigned', $document_id, $a_type, 13);
		}

    //3) New Attachment to a Case Shared with Me
		$this->shared_alert_create($document_id, 'caseNewFileShared', $doc_assigned_staffid, 'caseNewFileAssigned', $sec_id, 'caseNewFileSection', $a_type, 15);
	}




	private function case_update_alert($document_id, $a_type){
  	//There are a possible of 3 alerts that maybe created when a case is updated. They are
  	// 1) Updates/Instructions on Case Assigned to Me 	2) CUpdates/Instructions to Case in My Section 	3) Updates/Instructions on Case Shared with Me
  	// lets create one after the other


    //get the section/unit id and assigned staff id. 
    //section/unit id will be used to get a list of all admins in that section
    //assigned staff id will be used to create an alert for that staff
		$dbc = new dbconnection;
		$sql3="select DOC_UNIT, DOC_ASSIGNEDTO from document WHERE DOC_ID =".$document_id." LIMIT 1" ;
		$result3 = $dbc->dbconn->query($sql3);
		$docid = mysqli_fetch_assoc($result3);
		$sec_id = $docid['DOC_UNIT'];
		$doc_assigned_staffid = $docid['DOC_ASSIGNEDTO'];
		$result3->free();
		$dbc->dbconn->close();

    //1) Updates/Instructions on Case Assigned to Me
    //check if the case is assigned to a staff and Updates/Instructions is not made by the assigned staff
		if($doc_assigned_staffid != null && $doc_assigned_staffid != $_SESSION['userid']){
			$this->assigned_alert_create($doc_assigned_staffid, 'caseUpdateAssigned', $document_id, $a_type, 11);
		}


    //2) Updates/Instructions to Case in My Section
    //check if the case is assigned to a section
		if($sec_id != null){
			$this->section_alert_create($sec_id, 'caseUpdateSection', $doc_assigned_staffid, 'caseUpdateAssigned', $document_id, $a_type, 10);
		}



    //3) Updates/Instructions on Case Shared with Me
		$this->shared_alert_create($document_id, 'caseUpdateShared', $doc_assigned_staffid, 'caseUpdateAssigned', $sec_id, 'caseUpdateSection', $a_type, 12);
	}




	private function case_comment_alert($document_id, $a_type){
  	//There are a possible of 3 alerts that maybe created when a case is commented on. They are
  	// 1) Comment to Case Assigned to Me 		2) Comment to Case in My Section 		3) Comment to Case Shared with Me
  	// lets create one after the other


    //get the section/unit id and assigned staff id. 
    //section/unit id will be used to get a list of all admins in that section
    //assigned staff id will be used to create an alert for that staff
		$dbc = new dbconnection;
		$sql3="select DOC_UNIT, DOC_ASSIGNEDTO from document WHERE DOC_ID =".$document_id." LIMIT 1";
		$result3 = $dbc->dbconn->query($sql3);
		$docid = mysqli_fetch_assoc($result3);
		$sec_id = $docid['DOC_UNIT'];
		$doc_assigned_staffid = $docid['DOC_ASSIGNEDTO'];
		$result3->free();
		$dbc->dbconn->close();


    //1) Comment to Case Assigned to Me
    //check if the case is assigned to a staff and comment is not made by the assigned staff
		if($doc_assigned_staffid != null && $doc_assigned_staffid != $_SESSION['userid']){
			$this->assigned_alert_create($doc_assigned_staffid, 'caseCommentAssigned', $document_id, $a_type, 8);
		}


    //2) Comment to Case in My Section
    //check if the case is assigned to a section
		if($sec_id != null){
			$this->section_alert_create($sec_id, 'caseCommentSection', $doc_assigned_staffid, 'caseCommentAssigned', $document_id, $a_type, 7);
		}


    //3) Comment to Case Shared with Me
		$this->shared_alert_create($document_id, 'caseCommentShared', $doc_assigned_staffid, 'caseCommentAssigned', $sec_id, 'caseCommentSection', $a_type, 9);
	}




	private function section_alert_create($sec_id, $key1, $doc_assigned_staffid, $key2, $document_id, $a_type, $a_type_desc){
  	//get list of all section heads in the routed section
		$sectionheads = $this->get_sectionheads_preference($sec_id);
  	//for each section head, if his notification setting is turned on for $key1, create an alert
		foreach($sectionheads as $items){
  		//only if this section head is not the logged in user (say you are the head and you commented, you do not need to alert yourself)
			if($items['um_u_id'] != $_SESSION['userid']){
				$preferences = json_decode($items['U_PREFERENCES'],true);
				$flag = false;
				if($preferences[$key1]){
					$flag = true;
  				
				}else{
  				//check if this section head is following this case
					$dbc = new dbconnection;
					$sql = "select DF_DOC_ID, DF_U_ID from doc_follow where DF_DOC_ID = ".$document_id." and DF_U_ID =".$items['um_u_id'];
					$result = $dbc->dbconn->query($sql);
					if(mysqli_num_rows($result) == 1){
						$flag = true;
					}
					$result->free();
					$dbc->dbconn->close();
				}
			//only if, section head is not the assigned staff and he has turned on notifications for $key2 (if it is, an alert was created in assigned_alert_create())
				if($items['um_u_id'] == $doc_assigned_staffid && $preferences[$key2]){
					$flag = false;
				}
				
				if($flag == true){
					$this->save_alert($items['um_u_id'], $document_id, $a_type, $a_type_desc);
				}
			}
		}
	}




	private function assigned_alert_create($doc_assigned_staffid, $key1, $document_id, $a_type, $a_type_desc){
  	//get assigned staff notification preferences
		$preference = $this->get_user_preference($doc_assigned_staffid);
  	//if assigned staff has notification turned on for $key1, create an alert entry
		if($preference[$key1]){
			$this->save_alert($doc_assigned_staffid, $document_id, $a_type, $a_type_desc);
		}
	}




	private function shared_alert_create($document_id, $key1, $doc_assigned_staffid, $key2, $sec_id, $key3, $a_type, $a_type_desc){
  	//get list of all staffs with whom case has already been shared
		$sharedstaffpreference = $this->get_sharedstaff_preference($document_id);
    //for each staff with whom has case already been shared, if his notification setting is turned on for $key1, create an alert
		foreach($sharedstaffpreference as $items){
    	//only if this shared staff is not the logged in user (say you are the shared user and you commented, you do not need to alert yourself)
			if($items['DS_U_ID'] != $_SESSION['userid']){
				$preferences = json_decode($items['U_PREFERENCES'],true);
				if($preferences[$key1]){
					$flag = true;
    			//only if, shared staff is not the assigned staff and he has turned on notifications for $key2 (if it is, an alert was created in assigned_alert_create())
					if($items['DS_U_ID'] == $doc_assigned_staffid && $preferences[$key2]){
						$flag = false;
					}
    			//only if, shared staff is not a section head of the doc routed section and he has turned on notifications for $key3 (if it is, an alert was created in section_alert_create())
					if($sec_id){
						$dbc = new dbconnection;
						$sql = "select um_id from unit_member where um_handler = 'Y' and um_u_id = ".$items['DS_U_ID']." and um_unit_id =".$sec_id;
						$result = $dbc->dbconn->query($sql);
						if(mysqli_num_rows($result) == 1){
							if($preferences[$key3]){
								$flag = false;
							}else{
								$result->free();
	    					//check if this section head is following this case
								$sql = "select DF_ID from doc_follow where DF_DOC_ID = ".$document_id." and DF_U_ID =".$items['DS_U_ID'];
								$result = $dbc->dbconn->query($sql);
								if(mysqli_num_rows($result) == 1){
									$flag = false;
								}
							}
						}
						$result->free();
						$dbc->dbconn->close();
					}

					if($flag == true){
						$this->save_alert($items['DS_U_ID'], $document_id, $a_type, $a_type_desc);    					
					}
				}
			}
		}
	}




	private function case_shared_alert($document_id, $a_type, $user){
  	//There are a possible of 4 alerts that maybe created when a case is shared. They are
  	// 1) Case Shared with Me
  	// 2) Case Assigned to Me Being Shared
  	// 3) Case in My Section Being Shared
  	// 4) Case Shared With Me Being Shared
  	// lets create one after the other


  	//1) Case Shared with Me
  	//alert for the user with whom the case was shared
  	//check if the user is not sharing the case with himself
		if($_SESSION['userid'] != $user){
  		//get user notification preferences
			$preference = $this->get_user_preference($user);
	    //if assigned staff has notification turned on for Case Shared with Me, create an alert entry
			if($preference['caseSharedWithMe']){
				$this->save_alert($user, $document_id, $a_type, 3);
			}
		}

    //get the section/unit id and assigned staff id. 
    //section/unit id will be used to get a list of all admins in that section
    //assigned staff id will be used to create an alert for that staff
		$dbc = new dbconnection;
		$sql3="select DOC_UNIT, DOC_ASSIGNEDTO from document WHERE DOC_ID =".$document_id." LIMIT 1";
		$result3 = $dbc->dbconn->query($sql3);
		$docid = mysqli_fetch_assoc($result3);
		$sec_id = $docid['DOC_UNIT'];
		$doc_assigned_staffid = $docid['DOC_ASSIGNEDTO'];
		$result3->free();
		$dbc->dbconn->close();


    //2) Case Assigned to Me Being Shared
    //check if the case is assigned to a staff
		if($doc_assigned_staffid != null){
    	//check that the case is not being shared by the assigned staff
			if($doc_assigned_staffid != $_SESSION['userid']){
    		//get assigned staff notification preferences
				$preference = $this->get_user_preference($doc_assigned_staffid);
    		//if assigned staff has notification turned on for Case Assigned to Me Being Shared, create an alert entry
				if($preference['caseSharedAssigned']){
					$flag = true;
    			//only if the assigned staff is not the user with whom case is shared and he has notifications on for Case Shared with Me (if it was, alert was created in first case)
					if($doc_assigned_staffid == $user && $preference['caseSharedWithMe']){
						$flag = false;
					}
					if($flag == true){
						$this->save_alert($doc_assigned_staffid, $document_id, $a_type, 5);
					}
				}
			}
		}


    //3) Case in My Section Being Shared
    //check if the case is assigned to a section
		if($sec_id != null){
    	//get list of all section heads in the routed section
			$sectionheads = $this->get_sectionheads_preference($sec_id);

    	//for each section head, if his notification setting is turned on for Case in My Section Being Shared, create an alert
			foreach($sectionheads as $items){
    		//only if this section head is not the one who is sharing it (say you are the head and you shared it, you do not need to alert yourself)
				if($items['um_u_id'] != $_SESSION['userid']){
					$preferences = json_decode($items['U_PREFERENCES'],true);
					if($preferences['caseSharedSection']){
						$flag = true;
	    			//only if, this section head is not the user with whom the case is shared and he has turned on notifications for Case Shared with Me (if it was, alert was created in first case)
						if($items['um_u_id'] == $user && $preferences['caseSharedWithMe']){
							$flag = false;
						}
	    			//only if, section head is not the assigned staff and he has turned on notifications for Case Assigned to Me Being Shared (if it is, an alert was created in the second case)
						if($items['um_u_id'] == $doc_assigned_staffid && $preferences['caseSharedAssigned']){
							$flag = false;
						}
						if($flag == true){
							$this->save_alert($items['um_u_id'], $document_id, $a_type, 4);    					
						}
					}else{
	    			//check if this section head is following this case
						$dbc = new dbconnection;
						$sql = "select DF_DOC_ID, DF_U_ID from doc_follow where DF_DOC_ID = ".$document_id." and DF_U_ID =".$items['um_u_id'];
						$result = $dbc->dbconn->query($sql);
						if(mysqli_num_rows($result) == 1){
							$this->save_alert($items['um_u_id'], $document_id, $a_type, 4);
						}
						$result->free();
						$dbc->dbconn->close();
					}
				}
			}
		}


    //4) Case Shared With Me Being Shared
    //get list of all staffs with whom case has already been shared
		$sharedstaffpreference = $this->get_sharedstaff_preference($document_id);
    //for each staff with whom has case already been shared, if his notification setting is turned on for Case Shared With Me Being Shared, create an alert
		foreach($sharedstaffpreference as $items){
    	//only if the shared staff is not the one who shared the case
			if($items['DS_U_ID'] != $_SESSION['userid']){
				$preferences = json_decode($items['U_PREFERENCES'],true);
				if($preferences['caseSharedShared']){
					$flag = true;
    			//check if an alert has been created for this user from the above 3 types. if so no need to repeat alert
    			//type 1 caseSharedWithMe
					if($items['DS_U_ID'] == $user && $preferences['caseSharedWithMe']){
						$flag = false;
					}
    			//type 2 caseSharedAssigned
					if($items['DS_U_ID'] == $doc_assigned_staffid && $preferences['caseSharedAssigned']){
						$flag = false;
					}
    			//type 3 caseSharedSection
					$dbc = new dbconnection;
					$sql = "select um_id from unit_member where um_handler = 'Y' and um_u_id = ".$items['DS_U_ID']." and um_unit_id =".$sec_id;
					$result = $dbc->dbconn->query($sql);
					if(mysqli_num_rows($result) == 1){
						if($preferences['caseSharedSection']){
							$flag = false;
						}else{
							$result->free();
    					//check if this section head is following this case
							$sql = "select DF_ID from doc_follow where DF_DOC_ID = ".$document_id." and DF_U_ID =".$items['DS_U_ID'];
							$result = $dbc->dbconn->query($sql);
							if(mysqli_num_rows($result) == 1){
								$flag = false;
							}
						}
					}
					$result->free();
					$dbc->dbconn->close();

					if($flag == true){
						$this->save_alert($items['DS_U_ID'], $document_id, $a_type, 6);    					
					}
				}
			}
		}
	}




	private function case_routed_alert($document_id, $a_type, $section_id){
    //get list of all section heads in the routed section
		$sectionheads = $this->get_sectionheads_preference($section_id);
    //for each section head, if his notification setting is turned on for Case Routed/Forwarded to Section, create an alert
		foreach($sectionheads as $items){
    	//only if this section head is not the one who is routed it (say you are the head and you shared it, you do not need to alert yourself)
			if($items['um_u_id'] != $_SESSION['userid']){
				$preferences = json_decode($items['U_PREFERENCES'],true);
				if($preferences['caseRouted']){
					$this->save_alert($items['um_u_id'], $document_id, $a_type, 1);
				}
			}
		}
	}




	private function case_assigned_alert($document_id, $a_type, $user){
    //if there is an unread alert if previously the case was assigned to a staff, delete that alert
		$this->remove_alert($document_id, $a_type);
    //check if the user is assigning the case to himself
		if($_SESSION['userid'] != $user){
    	//get user notification preferences
			$preference = $this->get_user_preference($user);
	    //if assigned staff has notification turned on for Case Assigned to Me, create an alert entry
			if($preference['caseAssigned']){
				$this->save_alert($user, $document_id, $a_type, 2);
			}
		}

	}




	private function save_alert($user, $document_id, $a_type, $tpye_desc){
		$dbc = new dbconnection;
		$ins_stmt = $dbc->dbconn->prepare("INSERT INTO alerts (`A_U_ID`, `A_DOC_ID`,`A_TYPE`,`A_TYPE_DESC`,`A_CREATEDBY`) VALUES ( ?, ?, ?, ?, ?)");
		$ins_stmt->bind_param('ddsdd', $A_U_ID, $A_DOC_ID, $A_TYPE, $A_TYPE_DESC, $A_CREATEDBY);
		$A_U_ID = $user;
		$A_DOC_ID = $document_id;
		$A_TYPE = $a_type;
		$A_TYPE_DESC = $tpye_desc;
		$A_CREATEDBY = $_SESSION['userid'];
		$ins_stmt->execute();
		$ins_stmt->close();
		$dbc->dbconn->close();
	}




	private function get_sharedstaff_preference($document_id){
		$dbc = new dbconnection;
		$sql = "SELECT DS_U_ID, U_PREFERENCES FROM doc_share LEFT OUTER JOIN user ON DS_U_ID = U_ID WHERE DS_DOC_ID =".$document_id;
		$result = $dbc->dbconn->query($sql);
		$sharedstaff = array();
		while($rows = mysqli_fetch_assoc($result)){
			$sharedstaff[] = $rows;
		}
		$result->free();
		$dbc->dbconn->close();
		return $sharedstaff;
	}




	private function get_sectionheads_preference($section_id){
		$dbc = new dbconnection;
		$sql = "SELECT um_u_id, U_PREFERENCES FROM unit_member LEFT OUTER JOIN user ON um_u_id = U_ID WHERE um_handler = 'Y' AND um_unit_id =".$section_id;
		$result = $dbc->dbconn->query($sql);
		$sectionheads = array();
		while($rows = mysqli_fetch_assoc($result)){
			$sectionheads[] = $rows;
		}
		$result->free();
		$dbc->dbconn->close();
		return $sectionheads;
	}




	private function get_user_preference($user){
		$dbc = new dbconnection;

  	//get assigned staff notification preferences
		$sql = "SELECT U_PREFERENCES FROM user WHERE U_ID =".$user;
		$stmt =  $dbc->dbconn->stmt_init();
		$stmt->prepare($sql);
		$stmt->execute();
		$stmt->bind_result($U_PREFERENCES);
		$stmt->fetch();
		$stmt->free_result();
		$stmt->close();
		$dbc->dbconn->close();
		$settings = json_decode($U_PREFERENCES,true);
		return $settings;
	}




	public function markalertasread($nid, $user_id, $document_id){

		$dbc = new dbconnection;
		$stmt = $dbc->dbconn->prepare('UPDATE `alerts` SET `A_SEEN` = "Y", `A_SEENON` = NOW() WHERE `A_SEEN` = "N" AND `A_ID` = ? AND `A_U_ID` = ? AND `A_DOC_ID` = ?');
		$stmt->bind_param('ddd', $A_ID, $A_U_ID, $A_DOC_ID);
		$A_ID = $nid;
		$A_U_ID = $user_id;
		$A_DOC_ID = $document_id;
		$stmt->execute();
		$stmt->close();
		$dbc->dbconn->close();

	}




	public function alert_entry_seen($document_id, $u_type){

		$dbc = new dbconnection;

		//type casedetail = "1,2,3,6,10,11,12,19,20,21,22,23,24,25,26";
		//type caseshare = "4,5";
		//type casecomment = "7,8,9";
		//type caseattachment = "13,14,15,16,17,18";


		if($u_type == 'casedetailsview'){

			$stmt = $dbc->dbconn->prepare('UPDATE `alerts` SET `A_SEEN` = "Y", `A_SEENON` = NOW() WHERE `A_SEEN` = "N" AND `A_U_ID` = ? AND `A_DOC_ID` = ? AND `A_TYPE_DESC` IN (1,2,3,6,10,11,12,19,20,21,22,23,24,25,26)');

		}elseif($u_type == 'caseshareview'){

			$stmt = $dbc->dbconn->prepare('UPDATE `alerts` SET `A_SEEN` = "Y", `A_SEENON` = NOW() WHERE `A_SEEN` = "N" AND `A_U_ID` = ? AND `A_DOC_ID` = ? AND `A_TYPE_DESC` IN (4,5)');

		}elseif($u_type == 'casecommentview'){

			$stmt = $dbc->dbconn->prepare('UPDATE `alerts` SET `A_SEEN` = "Y", `A_SEENON` = NOW() WHERE `A_SEEN` = "N" AND `A_U_ID` = ? AND `A_DOC_ID` = ? AND `A_TYPE_DESC` IN (7,8,9)');

		}elseif($u_type == 'caseattachmentview'){

			$stmt = $dbc->dbconn->prepare('UPDATE `alerts` SET `A_SEEN` = "Y", `A_SEENON` = NOW() WHERE `A_SEEN` = "N" AND `A_U_ID` = ? AND `A_DOC_ID` = ? AND `A_TYPE_DESC` IN (13,14,15,16,17,18)');

		}

		$stmt->bind_param('dd', $A_U_ID, $A_DOC_ID);
		$A_U_ID = $_SESSION['userid'];
		$A_DOC_ID = $document_id;
		$stmt->execute();
		$stmt->close();
		$dbc->dbconn->close();

	}




	public function remove_alert($document_id, $a_type){

		$dbc = new dbconnection;
		$stmt = $dbc->dbconn->prepare("DELETE FROM alerts WHERE A_SEEN = 'N' AND A_TYPE = ? AND A_DOC_ID = ?");
		$stmt->bind_param('sd', $A_TYPE, $A_DOC_ID);
		$A_TYPE = $a_type;
		$A_DOC_ID = $document_id;
		$stmt->execute();
		$stmt->close();
		$dbc->dbconn->close();

	}




	public function countuserunseenalerts($userid){
		$sql = "SELECT COUNT(A_ID) as userunseenalertcount FROM alerts WHERE A_SEEN = 'N' AND A_U_ID = ".$userid;
        $dbc = new dbconnection;
        $stmt =  $dbc->dbconn->stmt_init();
        $stmt->prepare($sql);
        $stmt->execute();
        $stmt->bind_result($userunseenalertcount);
        $stmt->fetch();
        $stmt->free_result();
        $stmt->close();
        $dbc->dbconn->close();
        return $userunseenalertcount;
	}




	public function get_latest_unread_alerts(){
		$dbc = new dbconnection;
		$sql="SELECT A_ID, A_U_ID, A_DOC_ID, A_TYPE, A_TYPE_DESC, AD_DESC, A_CREATEDON FROM alerts LEFT OUTER JOIN alert_description ON A_TYPE_DESC = AD_ID WHERE A_SEEN = 'N' AND A_U_ID = ".$_SESSION['userid']." ORDER BY A_CREATEDON DESC LIMIT 5";
		$result = $dbc->dbconn->query($sql);
		$unseenalerts = array();
		while($rows = mysqli_fetch_assoc($result)){
			$unseenalerts[] = $rows;
		}
		$result->free();
		$dbc->dbconn->close();
		return $unseenalerts;
	}




}
?>