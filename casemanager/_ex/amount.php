<?php
class amount extends master{

		public function amount_details($docid,$amount_id){
            $sql="SELECT CSA_ID, CSA_DOC_ID, CSA_DESC, CSA_AMOUNT FROM case_special_amount inner join user ON CSA_BY = U_ID WHERE CSA_ID = ".$amount_id." AND CSA_DOC_ID = ".$docid;
            $dbc = new dbconnection;
            $stmt =  $dbc->dbconn->stmt_init();
            $stmt->prepare($sql);
            $stmt->execute();
            $stmt->bind_result($CSA_ID,$CSA_DOC_ID,$CSA_DESC,$CSA_AMOUNT);
            $stmt->fetch();
            $stmt->free_result();
            $stmt->close();
            $dbc->dbconn->close();
            $amount_info = Array(
            'CSA_ID'=> $CSA_ID,
            'CSA_DOC_ID'=> $CSA_DOC_ID,
            'CSA_DESC'=> $CSA_DESC, 
            'CSA_AMOUNT'=> $CSA_AMOUNT
            );
            return $amount_info; 
        }
		
        public function addamount($values){
            $dbc = new dbconnection;
            $stmt = $dbc->dbconn->prepare("INSERT INTO case_special_amount (`CSA_DOC_ID`, `CSA_DESC`, `CSA_AMOUNT`, `CSA_BY`) VALUES (?, ?, ?, ?)");
            $stmt->bind_param('dssd', $CSA_DOC_ID, $CSA_DESC, $CSA_AMOUNT,$CSA_BY);
            $CSA_DOC_ID = $values[0];
            $CSA_DESC = $values[1];
            $CSA_AMOUNT = $values[2];
			$CSA_BY =$_SESSION['userid'];
			$stmt->execute();
			if ($stmt->errno) {
				$stmt->close();
				$dbc->dbconn->close();
				return FALSE;
			}else{
				$stmt->close();
				$dbc->dbconn->close();
				return TRUE;
			}
        }

        public function updateamount($values){
            $dbc = new dbconnection;
            $up_stmt = $dbc->dbconn->prepare("UPDATE case_special_amount SET CSA_DESC = ?,CSA_AMOUNT = ?,CSA_BY = ? WHERE  CSA_DOC_ID = ".$values[0]." AND CSA_ID = ".$values[1]);
            $up_stmt->bind_param('ssd', $CSA_DESC, $CSA_AMOUNT, $CSA_BY );
			$CSA_DESC = $values[2];
            $CSA_AMOUNT = $values[3];
			$CSA_BY =$_SESSION['userid'];
            $up_stmt->execute();
			if ($up_stmt->errno) {
				$up_stmt->close();
				$dbc->dbconn->close();
				return FALSE;
			}else{
				$up_stmt->close();
				$dbc->dbconn->close();
				return TRUE;
			}
        }
		
		public function deleteamount($docid,$rec){
            $dbc = new dbconnection;
			$sql = "DELETE FROM case_special_amount WHERE CSA_DOC_ID = ".$docid." AND CSA_ID = ".$rec;
			$execute = $dbc->dbconn->query($sql);
			if($dbc->dbconn->affected_rows){
				return TRUE;
			}else{
				return FALSE;
			}
        }
		

}
	
?>