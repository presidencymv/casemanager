<?php
require('users.php');
require('proxy.php');
class authentication extends user{
    private $salt = 'jwcv89024j0gj234jg249uyg92jsdasd4g';

    public function __construct(){
		
        session_start();
		ob_start();
        if (isset($_SESSION['sesskey'])){
		
            if ($this->checksessionstatus()){
                $this->updatesession();
                $this->user_id = $_SESSION['userid'];
                $this->user_name = $_SESSION['username'];
            }else{
                header('location: index.php?user=relogin&missingsesskey');
                die();   
            }
        }else{
            header('location: index.php?user=login');
            die();
        }
    }

    public function changepassword($newpassword){
             
             $dbc = new dbconnection;
             $saltynewpassword = md5($newpassword.sha1($this->salt));
             $sql = "UPDATE Users SET U_PASSWORD = '".$saltynewpassword."' WHERE U_ID = ".$this->user_id;
             if ($dbc->dbconn->query($sql)){
                $dbc->dbconn->close();
               header('location: index.php?user=relogin');
               die();      
             }
    }

    public function checkcurrentpassword($password){
            $dbc = new dbconnection;
            $sql = "SELECT * FROM users WHERE U_PASSWORD = '".md5($password.sha1($this->salt))."' AND U_NAME = '".$this->user_name."' AND U_ID = ".$this->user_id ;
            $result = $dbc->dbconn->query($sql);
            if (mysqli_num_rows($result) == 1){
                $result->free();
                $dbc->dbconn->close();
                return TRUE;
            }else{
                $result->free();
                $dbc->dbconn->close();
                   echo '<div data-alert class="alert-box">
                            Incorrect Password!!!
                            <a href="#" class="close">&times;</a>
                        </div>';
                return FALSE;
            }
    }

    private function checksessionstatus(){
      
		$sessionkey = md5($_SESSION['userid'] .$_SESSION['username'].$_SESSION['counter'].$_SESSION['reports'].$_SESSION['sendall'].$_SESSION['handler'].$_SESSION['handlerunits'].$_SESSION['user_units'].$_SESSION['counterstaffs'].$_SESSION['callcentrestaff'].$_SESSION['ministersdirectivestaff'].$_SESSION['admins'].'-'. $today = date("Ymd"));
		
		if($_SESSION['sesskey'] == $sessionkey){
				$dbc = new dbconnection;
				$sql = "SELECT sess_id AS cresult FROM sess WHERE (sess_key = '".$_SESSION['sesskey']."') AND (sess_timestamp > (NOW()- INTERVAL 60 MINUTE))";
				$result=$dbc->dbconn->query($sql);
				if(mysqli_num_rows($result) == 1) {
					$result->free();
					$dbc->dbconn->close();
					return TRUE;
				}else{
					$result->free();
					$dbc->dbconn->close();
					return FALSE;
				}
		}else{
				return FALSE;
		}
      
		
    }

    private function updatesession(){
       $dbc = new dbconnection;
       $sql="UPDATE sess SET sess_timestamp = NOW() WHERE sess_key = '".$_SESSION['sesskey']."' AND sess_user ='".$_SESSION['username']."'";
	   $result=$dbc->dbconn->query($sql);
       $dbc->dbconn->close();
    }
	
	public function createuser($values){
		$dbc = new dbconnection;
        $stmt = $dbc->dbconn->prepare("INSERT INTO user (`U_NAME`, `U_COUNTER`, `U_ADMIN`, `U_REPORTS`, `U_SENDALL`, `U_PREFERENCES`, `U_CREATEDBY`, `U_PHONE`) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
        $stmt->bind_param('ssssssdd', $U_NAME, $U_COUNTER, $U_ADMIN, $U_REPORTS, $U_SENDALL, $U_PREFERENCES, $U_CREATEDBY, $U_PHONE);
		
		$U_NAME = $values[0];
		$U_COUNTER = $values[1];
		$U_ADMIN = $values[2];
		$U_REPORTS = $values[3];
        $U_SENDALL = $values[4];
        $U_PHONE = $values[5];
		$U_PREFERENCES = '{"caseRouted": 1,"caseAssigned": 1,"caseSharedWithMe": 1,"caseSharedSection": 1,"caseSharedAssigned": 1,"caseSharedShared": 1,"caseCommentSection": 1,"caseCommentAssigned": 1,"caseCommentShared": 1,"caseUpdateSection": 1,"caseUpdateAssigned": 1,"caseUpdateShared": 1,"caseNewFileSection": 1,"caseNewFileAssigned": 1,"caseNewFileShared": 1,"caseFileUpdatedSection": 1,"caseFileUpdatedAssigned": 1,"caseFileUpdatedShared": 1,"caseEditSection": 1,"caseEditAssigned": 1,"caseEditShared": 1,"caseClosedSection": 1,"caseClosedShared": 1,"caseReopenedSection": 1,"caseReopenedShared": 1,"caseRejected": 1}';
    $U_CREATEDBY = $_SESSION['userid'];
        
        $stmt->execute();
        if ($stmt->errno) {
			$stmt->close();
			$dbc->dbconn->close();
			RETURN FALSE; 
        }
        else {
           	$stmt->close();
			$dbc->dbconn->close();
			RETURN TRUE; 
        }
	}
	
	public function updateuser($values){
		$dbc = new dbconnection;
        $stmt = $dbc->dbconn->prepare("UPDATE user SET U_NAME = ?, U_COUNTER = ?, U_ADMIN = ?, U_REPORTS = ?,U_SENDALL = ?, U_CREATEDBY = ?, U_PHONE = ? WHERE U_ID = ".$values[0]);
        $stmt->bind_param('sssssdd', $U_NAME, $U_COUNTER, $U_ADMIN, $U_REPORTS, $U_SENDALL, $U_CREATEDBY, $U_PHONE);

        $U_NAME = $values[1];
		$U_COUNTER = $values[2];
		$U_ADMIN = $values[3];
		$U_REPORTS = $values[4];
        $U_SENDALL = $values[5];
        $U_PHONE = $values[6];
		$U_CREATEDBY = $_SESSION['userid'];
		
        $stmt->execute();
        if ($stmt->errno) {
            $stmt->close();
            $dbc->dbconn->close();
            return FALSE;
        }else{
            $stmt->close();
            $dbc->dbconn->close();
            return TRUE;
        }
	
	}
	
		public function activate_deactivate($ID,$STATUS){
		$dbc = new dbconnection;
        $stmt = $dbc->dbconn->prepare("UPDATE user SET U_ACTIVE = ? WHERE U_ID = ".$ID);
        $stmt->bind_param('d', $STATUS);
		
        $stmt->execute();
        if ($stmt->errno) {
            $stmt->close();
            $dbc->dbconn->close();
            return FALSE;
        }else{
            $stmt->close();
            $dbc->dbconn->close();
            return TRUE;
        }
	}
	
	public function userinfo($ID){
		$sql=" SELECT U_ID, U_NAME, U_COUNTER, U_ADMIN, U_REPORTS, U_SENDALL, U_ACTIVE, U_PHONE FROM user WHERE U_ID = ".$ID;		 
		$dbc = new dbconnection;
		$stmt =  $dbc->dbconn->stmt_init();
		$stmt->prepare($sql);
		$stmt->execute();
		$stmt->bind_result($U_ID,$U_NAME,$U_COUNTER,$U_ADMIN,$U_REPORTS,$U_SENDALL, $U_ACTIVE, $U_PHONE);
		$stmt->fetch();
		$stmt->free_result();
		$stmt->close();
		$dbc->dbconn->close();
		$unitinfo = Array(
		'U_ID'=> $U_ID,
		'U_NAME'=> $U_NAME,
		'U_COUNTER'=> $U_COUNTER,
		'U_ADMIN'=> $U_ADMIN,
		'U_REPORTS'=> $U_REPORTS,
		'U_SENDALL'=> $U_SENDALL,
		'U_ACTIVE'=>$U_ACTIVE,
		'U_PHONE'=>$U_PHONE
		);
		return $unitinfo; 
	}
	
	public function myunits(){
		$myunits = '';
        $dbc = new dbconnection;
        $sql="select count(um_id) as myunits from unit_member WHERE um_u_id =".$this->user_id ;
        $result = $dbc->dbconn->query($sql);
        $count = mysqli_fetch_assoc($result);
        
        if ($count['myunits']>0){

          $counter=0;
          $sql2 = "select um_unit_id from unit_member where um_u_id =".$this->user_id;
          $result2 = $dbc->dbconn->query($sql2);
          while ($rows = mysqli_fetch_assoc($result2)){
             if ($counter == 0){
				$myunits .= $rows['um_unit_id'];
				$counter = 1;
             }else{
				$myunits .= ','.$rows['um_unit_id'];    
             }
          }
          $result2->free();
        }else{
          $myunits = 'NONE';  
        }
        $result->free();
        $dbc->dbconn->close();
		
		return $myunits;
    }

  public function updateuserpreferences($values, $userid){

    $dbc = new dbconnection;

    $stmt = $dbc->dbconn->prepare("UPDATE user SET U_PREFERENCES = ? WHERE U_ID = ".$userid);
    $stmt->bind_param('s', $U_PREFERENCES);

    $U_PREFERENCES = $values;
    

    $stmt->execute();
    if ($stmt->errno) {
      $stmt->close();
      $dbc->dbconn->close();
      return FALSE;
    }else{
      $stmt->close();
      $dbc->dbconn->close();
      return TRUE;
    }
  
  }

  public function getuserpreferences($ID){
    $sql=" SELECT U_PREFERENCES FROM user WHERE U_ID = ".$ID;     
    $dbc = new dbconnection;
    $stmt =  $dbc->dbconn->stmt_init();
    $stmt->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($U_PREFERENCES);
    $stmt->fetch();
    $stmt->free_result();
    $stmt->close();
    $dbc->dbconn->close();
    $userpreferences = $U_PREFERENCES;
    return $userpreferences; 
  }
  
  public function attachment_auth($DR_UNIT_ID, $DOC_UNIT_ID, $ASSIGNED_ID, $CREATED_ID, $DOC_CREATED_ID, $DOC_STATUS){

	if($DOC_STATUS != 4){
		 if ($DOC_UNIT_ID != NULL){
	  
			if (in_array($DOC_UNIT_ID, explode(',' , $_SESSION['handlerunits']))){
				RETURN TRUE;
			}
			
			if ($ASSIGNED_ID == $_SESSION['userid']){
				RETURN TRUE;
			}

		  }else{
			
				if (in_array($DR_UNIT_ID, explode(',' , $_SESSION['handlerunits']))){
					RETURN TRUE;
				}


				if ($DOC_CREATED_ID == $_SESSION['userid'] || $CREATED_ID == $_SESSION['userid']){
					RETURN TRUE;
				}
				
				if ($DOC_CREATED_ID == $_SESSION['userid'] || $CREATED_ID == $_SESSION['userid']){
					RETURN TRUE;
				}
			
		  }
	
	}
	RETURN FALSE;
  

  }

}
?>