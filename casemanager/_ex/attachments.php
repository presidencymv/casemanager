<?php
class fileattachment{
    private $fileattachment;
    private $allowed_filetypes = array('zip','pdf','xls','xlsx','doc','docx','wav','mp3','msg','jpg','jpeg','gif','png','tif','tiff'); // These will be the types of file that will pass the validation.
    private $max_filesize = 20485760; // Maximum filesize in BYTES (20MB).
    private $upload_path = 'files/attachments/'; // The place the files will be uploaded to (currently a 'files' directory).
    private $replacecharacters = array ("'","--",";");
    private $filename;
    private $ext;
	private $attach_type;

    public function __construct($userfile){
        $this->fileattachment = $userfile;
		$this->attach_type = $type;
		$this->filename = $userfile['name'];
		$this->ext = strtolower(pathinfo($this->filename, PATHINFO_EXTENSION));
        //$this->ext = explode('.', $this->filename);
        //$this->ext = strtolower(end($this->ext));
    }

    public function checkfileattachment(){
    $flag = TRUE;

		if(!in_array($this->ext,$this->allowed_filetypes)){
			$flag = FALSE;
			die('Wrong file format!');
	    }

		if(filesize($this->fileattachment['size']) > $this->max_filesize){
			$flag = FALSE;
			die('File size is too large. Max 20MB');
	    }
	
		if(!is_writable($this->upload_path)){
			$flag = FALSE;
			die('unwritable');
		}
		if ($this->filename != str_replace($this->replacecharacters,"",$this->filename))
		{
			$flag = FALSE;
			die('File name contains characters not allowed (apostrophe/hyphen/semicolon)');
		}
		return $flag;
	}
 public function uploadfile(){

		if(move_uploaded_file($this->fileattachment['tmp_name'],$this->upload_path . $this->filename)){
		  return TRUE;            
		}else{
		  return FALSE;  
		}
	
 }

 public function getfilename(){
     return $this->filename;  
 }
 public function renamefile($newname){
     rename($this->upload_path . $this->filename,$this->upload_path . $newname);
 }

}
?>