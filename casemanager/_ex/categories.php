<?php

class issuecatagories extends master{

    public function createcategory($values){
        $dbc = new dbconnection;
        $stmt = $dbc->dbconn->prepare("INSERT INTO documentcat (`DC_NAME`, `DC_CREATEDBY`) VALUES (?, ?)");
        $stmt->bind_param('sd', $DC_NAME, $DC_CREATEDBY);

        $DC_NAME = $values[0];
        $DC_CREATEDBY = $_SESSION['userid'];
        
        $stmt->execute();
        if ($stmt->errno) {
			$stmt->close();
			$dbc->dbconn->close();
			RETURN FALSE; 
        }
        else {
           	$stmt->close();
			$dbc->dbconn->close();
			RETURN TRUE; 
        }
       
    }

    public function updatecategory($values){
        $dbc = new dbconnection;
        $stmt = $dbc->dbconn->prepare("UPDATE documentcat SET DC_NAME = ?, DC_CREATEDBY = ? WHERE DC_ID = ".$values[0]);
        $stmt->bind_param('sd', $DC_NAME, $DC_CREATEDBY);

        $DC_NAME = $values[1];
        $DC_CREATEDBY = $_SESSION['userid'];
        $stmt->execute();
        if ($stmt->errno) {
            $stmt->close();
            $dbc->dbconn->close();
            return FALSE;
        }else{
            $stmt->close();
            $dbc->dbconn->close();
            return TRUE;
        }
    }
	
	 public function categoryinfo($ID){
            $sql=" SELECT DC_ID, DC_NAME FROM documentcat WHERE DC_ID = ".$ID;		 
            $dbc = new dbconnection;
            $stmt =  $dbc->dbconn->stmt_init();
            $stmt->prepare($sql);
            $stmt->execute();
            $stmt->bind_result($DC_ID,$DC_NAME);
            $stmt->fetch();
            $stmt->free_result();
            $stmt->close();
            $dbc->dbconn->close();
            $categoryinfo = Array(
            'DC_ID'=> $DC_ID,
			'DC_NAME'=> $DC_NAME
            );
            return $categoryinfo; 
    }

}

?>
