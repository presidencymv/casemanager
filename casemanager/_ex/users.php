<?php
class user {
  private $userexist = 0;
  private $user_id;
  private $user_name;
  private $user_counter;
  private $user_admin;
  private $user_reports;
	private $user_sendall;
  private $user_handler;
  private $user_handlerunits = "";
	private $user_units;
	private $othercounterstaffs;
	private $callcentrestaff = 'N';
  private $chancellerystaff = 'N';
  private $ministersdirectivestaff = 'N';
  private $salt = 'ajsdh8439r8ewnf238rhdqd43tg35tg1982yhdf';

    public function getuserinfo($username, $userpassword){
          
          $dbc = new dbconnection;
          

          $sql="select * from user WHERE U_ACTIVE = 1 AND U_TYPE = 'local' AND U_NAME = '".mysqli_real_escape_string($dbc->dbconn,$username)."' AND U_PASSWORD = '".md5($userpassword.sha1($this->salt))."'";
          $result = $dbc->dbconn->query($sql);
          if (mysqli_num_rows($result) == 1){
            $user = mysqli_fetch_assoc($result);
            $this->user_id = $user['U_ID']; 
            $this->user_name = $user['U_NAME'];
            $this->user_counter = $user['U_COUNTER'];
            $this->user_admin = $user['U_ADMIN'];
            $this->user_reports = $user['U_REPORTS'];
			      $this->user_sendall = $user['U_SENDALL'];
            $this->userexist = 1;
            $this->checkifhandler();
			      $this->userunits();
            
            if (in_array($dbc->CounterUnitID,explode(',',$this->user_units))){
              $this->othercounterstaffs = $this->allcounterstaffs();
              $this->chancellerystaff = 'Y';
            }
            
            if (in_array($dbc->CallCentreUnitID,explode(',',$this->user_units))){
              $this->callcentrestaff = 'Y';
            }
            
            if (in_array($dbc->MinistersDirectiveID,explode(',',$this->user_handlerunits))){
              $this->ministersdirectivestaff = 'Y';
            }


             $this->createsession();
             $result->free();
             $dbc->dbconn->close();
             return TRUE;
          }else{
             $dbc->dbconn->close();
             return FALSE; 
          }


    }

	
    public function getuserinfoad($username){
          
          $dbc = new dbconnection;
          $sql="select * from user WHERE U_ACTIVE = 1 AND U_TYPE = 'ad' AND U_NAME = '".$username."'";
          $result = $dbc->dbconn->query($sql);
          if (mysqli_num_rows($result) == 1){
            $user = mysqli_fetch_assoc($result);
            $this->user_id = $user['U_ID']; 
            $this->user_name = $user['U_NAME'];
            $this->user_counter = $user['U_COUNTER'];
            $this->user_admin = $user['U_ADMIN'];
            $this->user_reports = $user['U_REPORTS'];
			      $this->user_sendall = $user['U_SENDALL'];
            $this->userexist = 1;
            $this->checkifhandler();
			      $this->userunits();
			 
            if (in_array($dbc->CounterUnitID,explode(',',$this->user_units))){
              $this->othercounterstaffs = $this->allcounterstaffs();
              $this->chancellerystaff = 'Y';
            }
            
            if (in_array($dbc->CallCentreUnitID,explode(',',$this->user_units))){
              $this->callcentrestaff = 'Y';
            }
            
            if (in_array($dbc->MinistersDirectiveID,explode(',',$this->user_handlerunits))){
                $this->ministersdirectivestaff = 'Y';
            }

			 
             $this->createsession();
             $result->free();
             $dbc->dbconn->close();
             return TRUE;
          }else{
             $dbc->dbconn->close();
             return FALSE; 
          }


    }

    private function checkifhandler(){
        $dbc = new dbconnection;
        $sql="select count(um_id) as handler from unit_member WHERE um_handler = 'Y' and um_u_id =".$this->user_id ;
        $result = $dbc->dbconn->query($sql);
        $count = mysqli_fetch_assoc($result);
        
        if ($count['handler']>0){
          $this->user_handler = 'Y';
          $counter=0;
          
          $sql2 = "select um_unit_id from unit_member where  um_handler = 'Y' and um_u_id =".$this->user_id;
          $result2 = $dbc->dbconn->query($sql2);
          while ($rows = mysqli_fetch_assoc($result2)){

             if ($counter == 0){
             $this->user_handlerunits .= $rows['um_unit_id'];
             $counter = 1;
             }else{
              $this->user_handlerunits .= ','.$rows['um_unit_id'];    
             }

          }
          $result2->free();
        }else{
          $this->user_handler = 'N';  
        }
           $result->free();
           $dbc->dbconn->close();
		   return true;
    }
	
	// private function checkifcallcentrestaff(){
  //       $dbc = new dbconnection;
  //       $sql="select count(um_id) as handler from unit_member WHERE um_handler = 'Y' and um_u_id =".$this->user_id ;
  //       $result = $dbc->dbconn->query($sql);
  //       $count = mysqli_fetch_assoc($result);
        
  //       if ($count['handler']>0){
  //         $this->user_handler = 'Y';
  //         $counter=0;
          
  //         $sql2 = "select um_unit_id from unit_member where  um_handler = 'Y' and um_u_id =".$this->user_id;
  //         $result2 = $dbc->dbconn->query($sql2);
  //         while ($rows = mysqli_fetch_assoc($result2)){

  //            if ($counter == 0){
  //            $this->user_handlerunits .= $rows['um_unit_id'];
  //            $counter = 1;
  //            }else{
  //             $this->user_handlerunits .= ','.$rows['um_unit_id'];    
  //            }

  //         }
  //         $result2->free();
  //       }else{
  //         $this->user_handler = 'N';  
  //       }
  //          $result->free();
  //          $dbc->dbconn->close();
	// 	   return true;
  //   }
	
	
	private function allcounterstaffs(){
		  $dbc = new dbconnection;
		  $allcounterstaffs = '';
          $counter=0;
          $sql = "select u_id from user inner join unit_member ON u_id = um_u_id where  um_unit_id = ".$dbc->CounterUnitID;
          $result = $dbc->dbconn->query($sql);
          while ($rows = mysqli_fetch_assoc($result)){
             if ($counter == 0){
				$allcounterstaffs .= $rows['u_id'];
				$counter = 1;
             }else{
				$allcounterstaffs .= ','.$rows['u_id'];    
             }
          }
          $result->free();
          $dbc->dbconn->close();
		  return $allcounterstaffs;
	}
	
	
    private function createsession(){
      $dbc = new dbconnection;
      session_start();
      session_regenerate_id(true);

      $_SESSION['userid'] = $this->user_id;
      $_SESSION['username'] = $this->user_name;
      $_SESSION['counter'] = $this->user_counter;
      $_SESSION['admins'] = $this->user_admin;
      $_SESSION['reports'] = $this->user_reports;
      $_SESSION['sendall'] = $this->user_sendall;
      $_SESSION['handler'] = $this->user_handler;
      $_SESSION['handlerunits'] = $this->user_handlerunits;
      $_SESSION['user_units'] = $this->user_units;
      $_SESSION['counterstaffs'] = $this->othercounterstaffs;
      $_SESSION['callcentrestaff'] = $this->callcentrestaff;
      $_SESSION['chancellerystaff'] = $this->chancellerystaff;
      $_SESSION['ministersdirectivestaff'] = $this->ministersdirectivestaff;
      $_SESSION['sesskey'] =  md5($_SESSION['userid'] .$_SESSION['username'].$_SESSION['counter'].$_SESSION['reports'].$_SESSION['sendall'].$_SESSION['handler'].$_SESSION['handlerunits'].$_SESSION['user_units'].$_SESSION['counterstaffs'].$_SESSION['callcentrestaff'].$_SESSION['ministersdirectivestaff'].$_SESSION['admins'].'-'. $today = date("Ymd")); 

      $sql = "DELETE FROM sess WHERE sess_user = '".$this->user_name."'";
      $execute = $dbc->dbconn->query($sql);

      $sql = "INSERT INTO sess (`sess_key`, `sess_user`) VALUES ('".$_SESSION['sesskey']."','".$this->user_name."')";
      $execute = $dbc->dbconn->query($sql);

      session_write_close();
      $dbc->dbconn->close();
      return true;
    }


  public function getusername($id){
    $sql = "SELECT U_NAME FROM user WHERE U_ID = ".$id;
           
    $dbc = new dbconnection;
    $stmt =  $dbc->dbconn->stmt_init();
    $stmt->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($U_NAME);
    $stmt->fetch();
    $stmt->free_result();
    $stmt->close();
    $dbc->dbconn->close();
    $userinfo = Array(
      'USERNAME'=> $U_NAME, 
    );
    return  $userinfo;
  }


  public function getcontactnumber($id){
    $sql = "SELECT U_PHONE FROM user WHERE U_ID = ".$id;
           
    $dbc = new dbconnection;
    $stmt =  $dbc->dbconn->stmt_init();
    $stmt->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($U_PHONE);
    $stmt->fetch();
    $stmt->free_result();
    $stmt->close();
    $dbc->dbconn->close();
    $userinfo = Array(
      'PHONE'=> $U_PHONE, 
    );
    return  $userinfo;
  }
	
public function userunits(){
	$sql = "SELECT um_unit_id from unit_member WHERE um_u_id = ".$this->user_id;
	$dbc = new dbconnection;
	$result = $dbc->dbconn->query($sql);
	$counter=0;
	while ($rows = mysqli_fetch_assoc($result)){
	 if ($counter == 0){
	 $this->user_units .= $rows['um_unit_id'];
	 $counter = 1;
	 }else{
	  $this->user_units .= ','.$rows['um_unit_id'];    
	 }
	}
	$result->free();
	return  $userinfo;
  }
	
}
   
?>