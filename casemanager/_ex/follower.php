<?php
class follower extends master{
	public function createfollower($values){
		$dbc = new dbconnection;
		$stmt = $dbc->dbconn->prepare("INSERT INTO doc_follow (`DF_DOC_ID`, `DF_U_ID`) VALUES (?, ?)");
		$stmt->bind_param('dd', $DF_DOC_ID, $DF_U_ID);

		$DF_DOC_ID=$values[0];
		$DF_U_ID=$_SESSION['userid'];
		$stmt->execute();
		if($stmt->errno){
			$stmt->close();
			$dbc->dbconn->close();
			RETURN FALSE;
		}else{
			$stmt->close();
			$dbc->dbconn->close();
			RETURN TRUE;
		}
	}

	public function isfollowing($docid){
		$dbc = new dbconnection;
		$sql="select * from doc_follow WHERE DF_DOC_ID = ".$docid." AND DF_U_ID = ".$_SESSION['userid']; 
		$result = $dbc->dbconn->query($sql);
		if(mysqli_num_rows($result) == 1){
			$result->free();
			$dbc->dbconn->close();
			return TRUE;
		}else{
			$result->free();
			$dbc->dbconn->close();
			return FALSE;
		}
	}

	public function stopfollowing($values){
		$dbc = new dbconnection;
		$stmt = $dbc->dbconn->prepare("DELETE FROM doc_follow WHERE DF_DOC_ID = ? AND DF_U_ID = ?");
		$stmt->bind_param('dd', $DF_DOC_ID, $DF_U_ID);
		$DF_DOC_ID = $values[0];
		$DF_U_ID = $_SESSION['userid'];
		$stmt->execute();
		if($stmt->errno){
			$dbc->dbconn->close();
			return FALSE;
		}else{
			if($dbc->dbconn->affected_rows > 0){
				$dbc->dbconn->close();
				return TRUE;
			}else{
				$dbc->dbconn->close();
				return FALSE;
			}
		}
	}

	public function getdocfollowers($docid){
		$dbc = new dbconnection;
		$sql = "SELECT U_NAME FROM doc_follow LEFT OUTER JOIN user ON DF_U_ID = U_ID WHERE DF_DOC_ID = ".$docid;
		$result = $dbc->dbconn->query($sql);
		$docfollowers = array();
		while($rows = mysqli_fetch_assoc($result)){
			$docfollowers[] = $rows;
		}
		$result->free();
		$dbc->dbconn->close();
		return $docfollowers;
	}
}
?>