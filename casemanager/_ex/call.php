<?php
class call extends master{
	
	public $markedlist;
	
    public function takecall($userid){
		$checkactive = $this->active_call($userid);

		
		if ($checkactive['ACTIVECOUNT'] == 0){
			$dbc = new dbconnection;
			$stmt = $dbc->dbconn->prepare("INSERT INTO call_records (`CR_LOGID`, `CR_BY`) VALUES (?, ?)");
			$stmt->bind_param('sd', $CR_LOGID ,$CR_BY);
			$CR_LOGID = $this->generate_logid();
			$CR_BY = $userid;
			$stmt->execute();
			
			if ($stmt->errno) {
				$stmt->close();
				$dbc->dbconn->close();
				return FALSE;
			}
			else {
				$stmt->close();
				$dbc->dbconn->close();
				return TRUE;
			}
		}else{
			return TRUE;
		}
   
    }
	
	
	private function generate_logid(){
	
		date_default_timezone_set("Indian/Maldives");
        $sql = "SELECT count(CR_ID)+1 as CALLSEQ FROM call_records WHERE DATE(`CR_ON`) = CURDATE()";
        $dbc = new dbconnection;
        $stmt =  $dbc->dbconn->stmt_init();
        $stmt->prepare($sql);
        $stmt->execute();
        $stmt->bind_result($CALLSEQ);
        $stmt->fetch();
        $stmt->free_result();
        $stmt->close();
        $dbc->dbconn->close();
		$today =  date("Y/m/d");
	
		$year = array(
		'1'=>'A',
		'2'=>'B',
		'3'=>'C',
		'4'=>'D',
		'5'=>'E',
		'6'=>'F',
		'7'=>'G',
		'8'=>'H',
		'9'=>'I',
		'0'=>'J');
		
		$yearchars =str_split(date("y"));
		
		$month = array('01'=>'G','02'=>'H','03'=>'I','04'=>'J','05'=>'K','06'=>'L','07'=>'M','08'=>'P','09'=>'Q','10'=>'R','11'=>'S','T'=>'S');
		
		$newlogid = $year[$yearchars[0]].$year[$yearchars[1]].$month[date("m")].date("d").$CALLSEQ;
		
        return $newlogid;
    }
	
	public function clearoldcalls($userid){
		    $dbc = new dbconnection;
            $up_stmt = $dbc->dbconn->prepare("UPDATE call_records SET CR_ACTIVE = 'N',CR_CRCAT_ID = '4' WHERE CR_ACTIVE = 'Y' AND CR_BY = ".$userid." AND date(CR_ON) < CURDATE()");
            $up_stmt->execute();
			if ($up_stmt->errno) {
				$up_stmt->close();
				$dbc->dbconn->close();
				return FALSE;
			}else{
				$up_stmt->close();
				$dbc->dbconn->close();
				return TRUE;
			}
	}
	
	
	public function savecall($values,$CR_SMS_R_ID_VAL = NULL){

		$activecalldetails = $this->active_call($values[0]);
		
		if($activecalldetails['ACTIVECOUNT'] == 1) {
			
			$dbc = new dbconnection;
			$up_stmt = $dbc->dbconn->prepare("UPDATE call_records SET CR_REF = ?, CR_TEL = ?, CR_CALLER = ?, CR_EMAIL = ?, CR_O_ID = ?, CR_UNIT_ID = ?, CR_DC_ID = ?, CR_P_ID = ?, CR_ISSUE = ?, CR_RESPONSE = ?, CR_CRCAT_ID = ?, CR_LASTUPDATEBY = ?, CR_SOLVED = ?, CR_ACTIVE = ?, CR_PV = ?, CR_COMPANY = ?, CR_SMS_R_ID = ? WHERE  CR_BY = ? AND CR_LOGID = ? AND CR_ID = ?");
			$up_stmt->bind_param('dsssddddssdddsssddsd', $CR_REF, $CR_TEL, $CR_CALLER, $CR_EMAIL, $CR_O_ID, $CR_UNIT_ID, $CR_DC_ID, $CR_P_ID, $CR_ISSUE, $CR_RESPONSE, $CR_CRCAT_ID, $CR_LASTUPDATEBY, $CR_SOLVED, $CR_ACTIVE, $CR_PV, $CR_COMPANY, $CR_SMS_R_ID, $CR_BY, $CR_LOGID, $CR_ID);

			$CR_REF 			= $this->get_CR_ID($values[1]);
			$CR_TEL				= $values[2];
			$CR_CALLER			= strtoupper($values[3]);
			$CR_EMAIL			= $values[4];
			$CR_O_ID			= $values[5];
			$CR_UNIT_ID			= $values[6];
			$CR_DC_ID			= $values[7];
			
			
			if ($values[8] == 1) {
				$CR_P_ID = $this->getdefaultpriority($CR_O_ID);
			}else{
				$CR_P_ID = $values[8];
			}
			

			$CR_ISSUE			= $values[9];
			$CR_RESPONSE		= $values[10];
			$CR_CRCAT_ID		= $values[12];
			$CR_LASTUPDATEBY	= $values[0];
			$CR_SOLVED			= $values[11];
			
			if ($CR_CRCAT_ID == 5) {
				$CR_ACTIVE = 'Y';
				
			}else{
				$CR_ACTIVE = 'N';
				
			}
			
			$CR_BY				= $values[0];
			$CR_LOGID			= $activecalldetails['CR_LOGID'];
			$CR_ID				= $activecalldetails['CR_ID'];
			$CR_PV				= $values[13];
			$CR_COMPANY			= strtoupper($values[14]);
			
			$CR_SMS_R_ID		= $CR_SMS_R_ID_VAL;
			
			if($CR_SMS_R_ID_VAL != NULL){
				$HEADER = "CALL CENTER";
				
				$MESSAGE = " ".$CR_RESPONSE." CASE REF : ".$CR_LOGID;
				
				$this->autoReplySMS($CR_TEL,$HEADER,$MESSAGE);
			}
			
			$up_stmt->execute();
			if ($up_stmt->errno){
				$up_stmt->close();
				$dbc->dbconn->close();
				return FALSE;
			}else{
				$up_stmt->close();
				$dbc->dbconn->close();
				if ($CR_CRCAT_ID != 5) {
					$this->endcall($CR_ID);
				}
				return TRUE;
			}

		}else{
			return FALSE;
		}
 
	}
	
	public function followup_calls($CR_ID,$format=1,$dbc = NULL){
		if ($dbc == NULL){
			$dbc = new dbconnection;	
		}
		  
		  $sql = "SELECT CR_ID, CR_LOGID, CR_CALLER, CR_TEL, CR_ISSUE, CR_RESPONSE, CR_ON FROM call_records WHERE CR_REF = ".$CR_ID." ORDER BY CR_ON DESC";
          $result = $dbc->dbconn->query($sql);
          $followupcalls = '';
		  $counter = 0;
          if (mysqli_num_rows($result) > 0){
            while ($rows = mysqli_fetch_assoc($result)){
				if($format == 1){
					$followupcalls .= '<small><a href="#" onclick="calldetails('.$rows['CR_ID'].')">'.$rows['CR_TEL'].' ON '.$rows['CR_ON'].'</a></small></br>';
				}else{
					if($counter > 0){
						//$followupcalls .= '<hr>';
					}
					$followupcalls .= '<div class="col-lg-12">
											<h4 class="text-info" >'.$rows['CR_TEL'].'('.$rows['CR_ON'].')</h4>
												<div class="col-lg-12">
													<div class="col-lg-12">
														<em>Message:</em>
														<p>'.$rows['CR_ISSUE'].'</p>
													</div>
												</div>
												<div class="col-lg-12">
													<div class="col-lg-12">
														<em>Response:</em>
														<p>'.$rows['CR_RESPONSE'].'</p>
													</div>
												</div>
										</div>';
				}
				$counter++;
            }
             $result->free();
			 if ($dbc == NULL){
				$dbc->dbconn->close();	
			}
             
             return $followupcalls;
          }else{
             $result->free();
              if ($dbc == NULL){
				$dbc->dbconn->close();	
			}
			 $followupcalls = "<small>NO FOLLOWUP CALLS YET</small>";
			 return $followupcalls;
          }
	
	}
	
	public function main_call($CR_ID){
		
		return $this->recordexists('SELECT CR_ID FROM call_records WHERE CR_REF = '.$CR_ID);
		
	}
	public function updatecall($values){
		
		$formid = $this->formid($values[11]);
		
		if($formid == $values[12]) {
			$dbc = new dbconnection;
			$up_stmt = $dbc->dbconn->prepare("UPDATE call_records SET CR_REF = ?, CR_TEL = ?, CR_CALLER = ?, CR_EMAIL = ?, CR_O_ID = ?, CR_UNIT_ID = ?, CR_DC_ID = ?, CR_P_ID = ?, CR_ISSUE = ?, CR_RESPONSE = ?, CR_LASTUPDATEBY = ?, CR_PV = ?, CR_COMPANY = ? WHERE CR_ID = ?");
			$up_stmt->bind_param('dsssddddssdssd', $CR_REF, $CR_TEL, $CR_CALLER, $CR_EMAIL, $CR_O_ID, $CR_UNIT_ID, $CR_DC_ID, $CR_P_ID, $CR_ISSUE, $CR_RESPONSE, $CR_LASTUPDATEBY, $CR_PV, $CR_COMPANY, $CR_ID);

			$CR_REF 			= $this->get_CR_ID($values[1]);
			$CR_TEL				= $values[2];
			$CR_CALLER			= $values[3];
			$CR_EMAIL			= $values[4];
			$CR_O_ID			= $values[5];
			$CR_UNIT_ID			= $values[6];
			$CR_DC_ID			= $values[7];
			
			if ($values[8] == 1) {
				$CR_P_ID = $this->getdefaultpriority($CR_O_ID);
			}else{
				$CR_P_ID = $values[8];
			}
			
			$CR_ISSUE			= $values[9];
			$CR_RESPONSE		= $values[10];
			$CR_LASTUPDATEBY	= $values[0];
			$CR_PV				= $values[13];
			$CR_COMPANY			= $values[14];
			$CR_ID				= $values[11];
			
			
			$up_stmt->execute();
			
			if ($up_stmt->errno){
				$up_stmt->close();
				$dbc->dbconn->close();
				return FALSE;
			}else{
				$up_stmt->close();
				$dbc->dbconn->close();		
				return TRUE;
			}

		}else{
			return FALSE;
		}
 
	}
	
	
	private function endcall($ID){
			$dbc = new dbconnection;
			$up_stmt = $dbc->dbconn->prepare("UPDATE call_records SET CR_END_ON = NOW() WHERE CR_ID = ?");
			$up_stmt->bind_param('d', $ID);
			$up_stmt->execute();
			$up_stmt->close();
			$dbc->dbconn->close();
	}
	
	public function setdocument($ID,$DOC_ID){
			$dbc = new dbconnection;
			
			
			$up_stmt = $dbc->dbconn->prepare("UPDATE call_records SET CR_DOC_ID = ? WHERE CR_ID = ?");
			$up_stmt->bind_param('dd', $DOC_ID,$ID);
			$up_stmt->execute();

			
			$up_stmt2 = $dbc->dbconn->prepare("UPDATE document SET DOC_CR_DOC = 1 WHERE DOC_ID = ?");
			$up_stmt2->bind_param('d', $DOC_ID);
			$up_stmt2->execute();
			
			
			
			if ($up_stmt->errno || $up_stmt2->errno) {
				$up_stmt->close();
				$up_stmt2->close();
				$dbc->dbconn->close();
				return FALSE;
			}else{
				$up_stmt->close();
				$up_stmt2->close();
				$dbc->dbconn->close();
				return TRUE;
			}
			
	}
	
	public function active_call($userid){	 
        $sql = "SELECT count(CR_ID) as `ACTIVECOUNT`,CR_ID,CR_LOGID,CR_REF,CR_CALLER,CR_TEL,CR_EMAIL,CR_O_ID,CR_UNIT_ID,CR_DC_ID,CR_P_ID,CR_ISSUE,CR_RESPONSE,TIME_TO_SEC(TIMEDIFF(NOW(),CR_ON)) as `ELAPSEDTIME`,CR_SOLVED, CR_PV, CR_COMPANY FROM call_records WHERE CR_ACTIVE = 'Y' AND CR_BY = ".$userid." LIMIT 1";
		$dbc = new dbconnection;
        $stmt =  $dbc->dbconn->stmt_init();
        $stmt->prepare($sql);
        $stmt->execute();
        $stmt->bind_result($ACTIVECOUNT,$CR_ID,$CR_LOGID,$CR_REF,$CR_CALLER,$CR_TEL,$CR_EMAIL,$CR_O_ID,$CR_UNIT_ID,$CR_DC_ID,$CR_P_ID,$CR_ISSUE,$CR_RESPONSE,$ELAPSEDTIME,$CR_SOLVED,$CR_PV,$CR_COMPANY);
        $stmt->fetch();
        $stmt->free_result();
        $stmt->close();
        $dbc->dbconn->close();
		
		
		if($CR_REF){
		$REF_LOG = $this->get_CR_LOGID($CR_REF);	
		}else{
		$REF_LOG = NULL;	
		}
		
		$activecall = array(
		'ACTIVECOUNT'=>$ACTIVECOUNT,
		'CR_ID'=>$CR_ID,
		'CR_LOGID'=>$CR_LOGID,
		'CR_REF'=>$REF_LOG,
		'CR_CALLER'=>$CR_CALLER,
		'CR_TEL'=>$CR_TEL,
		'CR_EMAIL'=>$CR_EMAIL,
		'CR_O_ID'=>$CR_O_ID,
		'CR_UNIT_ID'=>$CR_UNIT_ID,
		'CR_DC_ID'=>$CR_DC_ID,
		'CR_P_ID'=>$CR_P_ID,
		'CR_ISSUE'=>$CR_ISSUE,
		'CR_RESPONSE'=>$CR_RESPONSE,
		'ELAPSEDTIME'=>$ELAPSEDTIME,
		'CR_SOLVED'=>$CR_SOLVED,
		'CR_PV'=>$CR_PV,
		'CR_COMPANY'=>$CR_COMPANY
		);
		
        return $activecall;
    }
	
	
	
	public function get_call_info($CR_ID){	 
        $sql = "SELECT `main`.CR_ID,`main`.CR_LOGID,`main`.CR_REF,`main`.CR_CALLER,`main`.CR_TEL,`main`.CR_EMAIL,O_ID,O_NAME,UNIT_ID,UNIT_NAME,DC_ID,DC_NAME,P_ID,P_NAME,`main`.CR_ISSUE,`main`.CR_RESPONSE,`main`.CR_ON,TIMEDIFF(`main`.CR_END_ON,`main`.CR_ON) as `CALL DURATION`,`main`.CR_SOLVED,`main`.CR_DOC_ID,`main`.CR_PV,`main`.CR_COMPANY
				FROM call_records `main`
				INNER JOIN office ON `main`.CR_O_ID = O_ID
				INNER JOIN unit ON `main`.CR_UNIT_ID = UNIT_ID
				INNER JOIN documentcat ON `main`.CR_DC_ID = DC_ID 
				INNER JOIN priority ON `main`.CR_P_ID = P_ID
				LEFT OUTER JOIN call_records `ref`ON `main`.CR_REF = `ref`.CR_REF
				WHERE `main`.CR_ID = ".$CR_ID."  LIMIT 1";
				
		$dbc = new dbconnection;
        $stmt =  $dbc->dbconn->stmt_init();
        $stmt->prepare($sql);
        $stmt->execute();
        $stmt->bind_result($CR_ID,$CR_LOGID,$CR_REF,$CR_CALLER,$CR_TEL,$CR_EMAIL,$O_ID,$O_NAME,$UNIT_ID,$UNIT_NAME,$DC_ID,$DC_NAME,$P_ID,$P_NAME,$CR_ISSUE,$CR_RESPONSE,$CR_ON,$DURATION,$CR_SOLVED,$CR_DOC_ID,$CR_PV,$CR_COMPANY);
        $stmt->fetch();
        $stmt->free_result();
        $stmt->close();
        $dbc->dbconn->close();
		
		if($CR_REF){
		$REF_LOG = $this->get_CR_LOGID($CR_REF);	
		}else{
		$REF_LOG = NULL;	
		}
		
		$callinfo = array(
		'CR_ID'=>$CR_ID,
		'CR_LOGID'=>$CR_LOGID,
		'CR_REF_ID'=>$CR_REF,
		'CR_REF'=>$REF_LOG,
		'CR_CALLER'=>$CR_CALLER,
		'CR_TEL'=>$CR_TEL,
		'CR_EMAIL'=>$CR_EMAIL,
		'O_ID'=>$O_ID,
		'O_NAME'=>$O_NAME,
		'UNIT_ID'=>$UNIT_ID,
		'UNIT_NAME'=>$UNIT_NAME,
		'DC_ID'=>$DC_ID,
		'DC_NAME'=>$DC_NAME,
		'P_ID'=>$P_ID,
		'P_NAME'=>$P_NAME,
		'CR_ISSUE'=>$CR_ISSUE,
		'CR_RESPONSE'=>$CR_RESPONSE,
		'CR_ON'=>$CR_ON,
		'DURATION'=>$DURATION,
		'CR_SOLVED'=>$CR_SOLVED,
		'CR_DOC_ID'=>$CR_DOC_ID,
		'CR_PV'=>$CR_PV,
		'CR_COMPANY'=>$CR_COMPANY
		);

        return $callinfo;
    }
	
	
	public function get_CR_ID($CR_LOGID){
		
		$sql = "SELECT CR_ID FROM call_records WHERE CR_LOGID = '".$CR_LOGID."'";
        $dbc = new dbconnection;
        $stmt =  $dbc->dbconn->stmt_init();
        $stmt->prepare($sql);
        $stmt->execute();
        $stmt->bind_result($CR_ID);
        $stmt->fetch();
        $stmt->free_result();
        $stmt->close();
        $dbc->dbconn->close();
		
		return $CR_ID;
	}
	
	public function get_docid($CR_ID){
		
		$sql = "SELECT CR_DOC_ID FROM call_records WHERE CR_ID = ".$CR_ID;
        $dbc = new dbconnection;
        $stmt =  $dbc->dbconn->stmt_init();
        $stmt->prepare($sql);
        $stmt->execute();
        $stmt->bind_result($CR_DOC_ID);
        $stmt->fetch();
        $stmt->free_result();
        $stmt->close();
        $dbc->dbconn->close();
		
		return $CR_DOC_ID;
	}
	
	public function get_crid($DOC_ID){
		
		$sql = "SELECT CR_ID FROM call_records WHERE CR_DOC_ID = ".$DOC_ID;
        $dbc = new dbconnection;
        $stmt =  $dbc->dbconn->stmt_init();
        $stmt->prepare($sql);
        $stmt->execute();
        $stmt->bind_result($CR_ID);
        $stmt->fetch();
        $stmt->free_result();
        $stmt->close();
        $dbc->dbconn->close();
		
		return $CR_ID;
	}
	
	public function get_CR_LOGID($CR_ID){
		
		$sql = "SELECT CR_LOGID FROM call_records WHERE CR_ID = ".$CR_ID;
        $dbc = new dbconnection;
        $stmt =  $dbc->dbconn->stmt_init();
        $stmt->prepare($sql);
        $stmt->execute();
        $stmt->bind_result($CR_LOGID);
        $stmt->fetch();
        $stmt->free_result();
        $stmt->close();
        $dbc->dbconn->close();
		
		return $CR_LOGID;
	}
	
	
	public function markasresolved($values,$type){
		
		$dbc = new dbconnection;
		$REF_FOUND = $values[1];
		
		WHILE($REF_FOUND != NULL || $REF_FOUND != ''){
		
			$sql = "SELECT CR_ID, CR_REF FROM call_records WHERE CR_ID = ".$REF_FOUND;
			$dbc = new dbconnection;
			$stmt =  $dbc->dbconn->stmt_init();
			$stmt->prepare($sql);
			$stmt->execute();
			$stmt->bind_result($CR_ID,$REF_FOUND);
			$stmt->fetch();
			$stmt->free_result();
			$stmt->close();
			
			$up_stmt = $dbc->dbconn->prepare("UPDATE call_records SET CR_SOLVED = ".$type.", CR_LASTUPDATEBY = ? WHERE CR_ID = ?");
			$up_stmt->bind_param('dd', $values[0],$CR_ID);
			$up_stmt->execute();
			
			if ($up_stmt->errno) {
				$up_stmt->close();
				return FALSE;
			}else{
				$up_stmt->close();
			}
			
		}
		
		$up_stmt = $dbc->dbconn->prepare("UPDATE call_records SET CR_SOLVED = ".$type.", CR_LASTUPDATEBY = ? WHERE CR_REF = ?");
		$up_stmt->bind_param('dd', $values[0],$values[1]);
		$up_stmt->execute();
		
		if ($up_stmt->errno) {
			$up_stmt->close();
			$dbc->dbconn->close();
			return FALSE;
		}else{
			$up_stmt->close();
			$dbc->dbconn->close();
			return TRUE;
		}
	}
	
	public function numofcallcaseupdates($CR_ID,$dbc){	
		$sql = "SELECT count(DR_ID) as NUMOFUPDATES FROM `doc_remarks` INNER JOIN call_records ON DR_DOC_ID = CR_DOC_ID WHERE CR_ID = ".$CR_ID;
        //$dbc = new dbconnection;
        $stmt =  $dbc->dbconn->stmt_init();
        $stmt->prepare($sql);
        $stmt->execute();
        $stmt->bind_result($NUMOFUPDATES);
        $stmt->fetch();
        $stmt->free_result();
        $stmt->close();
        //$dbc->dbconn->close();
		return $NUMOFUPDATES;
	}
	
	public function callcasestatus($DOC_ID){
		$sql = "SELECT DST_ID,DST_NAME FROM `document` INNER JOIN `doc_status` ON DOC_STATUS = DST_ID WHERE DOC_ID = ".$DOC_ID;
        $dbc = new dbconnection;
        $stmt =  $dbc->dbconn->stmt_init();
        $stmt->prepare($sql);
        $stmt->execute();
        $stmt->bind_result($DST_ID,$DST_NAME);
        $stmt->fetch();
        $stmt->free_result();
        $stmt->close();
        $dbc->dbconn->close();
		$values = array(
		'DST_ID'=>$DST_ID,
		'DST_NAME'=>$DST_NAME
		);
		return $values;
	}
	
	public function archiveyears(){
		$archive_years = "SELECT DISTINCT YEAR(CR_ON) as YEAR FROM `call_records` WHERE CR_ACTIVE = 'N' ORDER BY YEAR DESC";
		$pagename = "callrecords";
	
		$dbc = new dbconnection;
		  $result =  $dbc->dbconn->query($archive_years);
		  $pages = array(
		  'result'=>False,
		  );

		if (mysqli_num_rows($result) > 0){
        while($rows=mysqli_fetch_assoc($result)){
            $arch_years[] = $rows['YEAR'] ;
        }
        $result->free();
        $dbc->dbconn->close();

        $pages['result'] = TRUE;
        $pages['years_array'] = $arch_years;
		$pages['page'] = $pagename;
        return $pages;

      }else{
        $result->free();
        $dbc->dbconn->close();
        return $pages;
      }
	}
	
	public function savesms_callrecord($values){
		if($this->takecall($values[0])){
			$formid = $this->formid($values[15]);
			if($formid == $values[16]) {
				if($this->savecall($values,$values[15])){
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		
		}else{
			
			return false;
		}
	
	}

}
?>