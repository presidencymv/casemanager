<?php
class sms extends Master{
	
	private function insertSMSToDB($values){
		$dbc = new dbconnection;
		$stmt = $dbc->dbconn->prepare("INSERT INTO sms_received (`SMS_R_SENDER_NUMBER`, `SMS_R_SENDER_NAME`, `SMS_R_MESSAGE`) VALUES (?, ?, ?)");
		$stmt->bind_param('sss', $SMS_R_SENDER_NUMBER, $SMS_R_SENDER_NAME, $SMS_R_MESSAGE);
		$SMS_R_SENDER_NUMBER = $values[0];
		$SMS_R_SENDER_NAME = $values[1];
		$SMS_R_MESSAGE = $values[2];
		$stmt->execute();
		if ($stmt->errno) {
			$stmt->close();
			$dbc->dbconn->close();
			return FALSE;
		}else{
			$stmt->close();
			$dbc->dbconn->close();
			return TRUE;
		}
	}
	
	private function validateInput($values,$case,$fieldlist){
		if($this->invalidchar($values,$fieldlist,$case,'on')){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	public function insertSMS($sendernum,$message){
		$splitmsg = explode(' ',$message, 2);
		$values = array($sendernum,$splitmsg[0],$splitmsg[1]);
		$case = array('else','else','else');
		$fieldlist = array('number','name','msg');
		
		if ($this->validateInput($values,$case,$fieldlist)){
			if ($this->insertSMSToDB($values)){
				return TRUE;
			}else{
				return FALSE;
			}
		}else{
		
			return FALSE;
		}
	}
	
	public function getSMSinfo($id){
		$sql="SELECT SMS_R_ID, SMS_R_SENDER_NUMBER, SMS_R_SENDER_NAME, SMS_R_MESSAGE, SMS_R_ON FROM sms_received WHERE SMS_R_ID = ".$id;
		$dbc = new dbconnection;
		$stmt =  $dbc->dbconn->stmt_init();
		$stmt->prepare($sql);
		$stmt->execute();
		$stmt->bind_result($SMS_R_ID,$SMS_R_SENDER_NUMBER,$SMS_R_SENDER_NAME,$SMS_R_MESSAGE,$SMS_R_ON);
		$stmt->fetch();
		$stmt->free_result();
		$stmt->close();
		$dbc->dbconn->close();
		$SMS_info = Array(
		'SMS_R_ID'=> $SMS_R_ID,
		'SMS_R_SENDER_NUMBER'=> $SMS_R_SENDER_NUMBER,
		'SMS_R_SENDER_NAME'=> $SMS_R_SENDER_NAME, 
		'SMS_R_MESSAGE'=> $SMS_R_MESSAGE,
		'SMS_R_ON'=> $SMS_R_ON
		);
		return $SMS_info; 
	}
	
	public function sendPRTGNotification($MESSAGE){
		$dbc = new dbconnection;
		$sql = "SELECT PRTG_N_G_ID FROM prtg_notification_group";
		$result = $dbc->dbconn->query($sql);
		while ($rows = mysqli_fetch_assoc($result)){
			$this->autoReplySMS($rows['PRTG_N_G_ID'],'PRTG-ALERT',$MESSAGE);
		}
		$result->free();
		$dbc->dbconn->close();
		return true;
	}
	
	public function deletesms($values){
		$dbc = new dbconnection;
		$up_stmt = $dbc->dbconn->prepare("UPDATE sms_received SET SMS_DELETED = 1, SMS_R_D_BY = ? WHERE SMS_R_ID = ?");
		$up_stmt->bind_param('dd', $values[0], $values[1]);
		$up_stmt->execute();
		if ($up_stmt->errno) {
			$up_stmt->close();
			$dbc->dbconn->close();
			return FALSE;
		}else{
			$up_stmt->close();
			$dbc->dbconn->close();
			return TRUE;
		}
	}
	
	private function updatepreviousmsg($msgid){

		$dbc = new dbconnection;
		
		$sql = "SELECT SMS_R_SENDER_NUMBER, SMS_R_SENDER_NAME, SMS_R_MESSAGE FROM sms_received WHERE SMS_R_ID = ".$msgid;
		$stmt = $dbc->dbconn->stmt_init();
		$stmt->prepare($sql);
		$stmt->execute();
		$stmt->bind_result($SMS_R_SENDER_NUMBER, $SMS_R_SENDER_NAME, $SMS_R_MESSAGE_PART2);
		$stmt->fetch();
		$stmt->free_result();
		$stmt->close();
		
		$sql = "SELECT `SMS_R_ID`,`SMS_R_MESSAGE` FROM `sms_received` LEFT OUTER JOIN `call_records` ON (`SMS_R_ID` = `CR_SMS_R_ID` AND (CR_ON > SMS_R_ON)) WHERE CR_ID IS NULL AND SMS_DELETED = 0 AND SMS_R_SENDER_NUMBER = '".$SMS_R_SENDER_NUMBER."' AND SMS_R_ID < ".$msgid." ORDER BY SMS_R_ID DESC LIMIT 1";
		$stmt =  $dbc->dbconn->stmt_init();
		$stmt->prepare($sql);
		$stmt->execute();
		$stmt->bind_result($SMS_R_ID, $SMS_R_MESSAGE_PART1);
		$stmt->fetch();
		$stmt->free_result();
		$stmt->close();	
		
		$newmsg = $SMS_R_MESSAGE_PART1.$SMS_R_SENDER_NAME.$SMS_R_MESSAGE_PART2;
		$up_stmt = $dbc->dbconn->prepare("UPDATE sms_received SET SMS_R_MESSAGE = ? WHERE SMS_R_ID = ?");
		$up_stmt->bind_param('sd',$newmsg,$SMS_R_ID);
		$up_stmt->execute();
		if ($up_stmt->errno){
			$up_stmt->close();
			$dbc->dbconn->close();
			return FALSE;
		}else{
			$up_stmt->close();
			$dbc->dbconn->close();
			return TRUE;
		}
		
	}
	
	public function mergesms($values){
		if($this->updatepreviousmsg($values[1]) == TRUE){
			$dbc = new dbconnection;
			$up_stmt = $dbc->dbconn->prepare("UPDATE sms_received SET SMS_DELETED = 1, SMS_R_M_BY = ? WHERE SMS_R_ID = ?");
			$up_stmt->bind_param('dd', $values[0], $values[1]);
			$up_stmt->execute();
			if ($up_stmt->errno) {
				$up_stmt->close();
				$dbc->dbconn->close();
				return FALSE;
			}else{
				$up_stmt->close();
				$dbc->dbconn->close();
				return TRUE;
			}
		}
	
	}
}
?>