<?php

//get the ip address
$ip_address = get_user_ip();

//block access from 192.168.245.7 (ie. user has not added server (intranet2.finance.gov.mv) to the proxy list)
if($ip_address == '192.168.245.7'){
	header('location: proxyalert.php');
    die();
}


function get_user_ip(){

    $ip_address = '';

    if(!empty($_SERVER['HTTP_CLIENT_IP']))
    {
        $ip_address = $_SERVER['HTTP_CLIENT_IP'];
    }
    elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
    {
        $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
        $ip_address = $_SERVER['REMOTE_ADDR'];
    }

    return $ip_address;

}   
?>