<?php
require_once ('library/HTMLPurifier.auto.php');
class master {
	
	private $purifier;
	private $pure_cofig;
	private $formkey = "8f78dyhf7e6fef8edf98sdfb6fg76nf6g4n5bfg6vberbwr908";
	
	public function  singlevaluecheck($value,$case){
	//_nr is for notrequired fields
			$loop = 0;
			$cleaned = trim($value);
			$flag =FALSE;
			switch ($case[$loop]){

					case 'tn':
						$final = $this->pre_cleanup(preg_replace( "/[^A-Za-z 0-9$]/","",$cleaned));
						if($final == '' || $final == NULL  ){
								$flag = TRUE;
						}
					break;
					
					case 'tn_nr':
						$final = $this->pre_cleanup(preg_replace( "/[^A-Za-z 0-9$]/","",$cleaned));
						if($final == '' || $final == NULL  ){
							if($value != '' && $value != NULL){
								$flag = TRUE;
							}
						}
					break;
					
					case 't':
						$final = $this->pre_cleanup(preg_replace( "/[^A-Za-z$]/","",$cleaned));
						if($final == '' || $final == NULL  ){
								$flag = TRUE;
						}
					break;
					
					case 't_nr':
						$final = $this->pre_cleanup(preg_replace( "/[^A-Za-z$]/","",$cleaned));
						if($final == '' || $final == NULL  ){
							if($value != '' && $value != NULL){
								$flag = TRUE;
							}
						}
					break;

					case 'n':
						$final = $this->pre_cleanup(preg_replace( "/[^0-9.$]/","",$cleaned));
						if($final == '' || $final == NULL  ){
								$flag = TRUE;
						}
					break;
					
					case 'n_nr':
						$final = $this->pre_cleanup(preg_replace( "/[^0-9.$]/","",$cleaned));
						if($final == '' || $final == NULL  ){
							if($value != '' && $value != NULL){
								$flag = TRUE;
							}
						}
					break;

					case 'date':
						$final = $this->pre_cleanup(preg_replace( "/[^0-9/$]/","",$cleaned));
						if($final == '' || $final == NULL  ){
								$flag = TRUE;
						}
						if(validateDate($cleaned)==FALSE){
							$flag = TRUE;
						}
					break;
					
					case 'date_nr':
						$final = $this->pre_cleanup(preg_replace( "/[^0-9/$]/","",$cleaned));
						if($final == '' || $final == NULL  ){
							if($value != '' && $value != NULL){
								$flag = TRUE;
							}
						}else{
							if(validateDate($cleaned)==FALSE){
							$flag = TRUE;
							}
						}
					break;

					case 'else':
						$final = $this->pre_cleanup($cleaned);
						if($final == '' || $final == NULL  ){
								$flag = TRUE;
						}
					break;
					
					case 'else_nr':
						$final = $this->pre_cleanup($cleaned);
						if($final == ''|| $final == NULL  ){
							if($value != '' && $value != NULL){
								$flag = TRUE;
							}
						}
					break;

                   }
	
	      if ($final != $value | $flag == TRUE){
				return FALSE;
		   }else{
				return TRUE;
		}
	
	}
	
	function validateDate($date, $format = 'd/m/Y')
	{
		$d = DateTime::createFromFormat($format, $date);
		return $d && $d->format($format) == $date;
	}
	
    public function invalidchar($valueset,$fieldset,$case,$notification){
	//_nr is for notrequired fields
        $loop = 0;
		$flag =FALSE;
        foreach ($valueset as $value ){
                
                 $cleaned = trim($value);

                   switch ($case[$loop]){

					case 'tn':
						$final = $this->pre_cleanup(preg_replace( "/[^A-Za-z 0-9]/","",$cleaned));
						if($final == '' || $final == NULL  ){
								$flag = TRUE;
						}
					break;
					case 'tn_nr':
						$final = $this->pre_cleanup(preg_replace( "/[^A-Za-z 0-9]/","",$cleaned));
						if($final == '' || $final == NULL  ){
							if($value != '' && $value != NULL){
								$flag = TRUE;
							}
						}
					break;
					
					case 't':
						$final = $this->pre_cleanup(preg_replace( "/[^A-Z a-z$]/","",$cleaned));
						if($final == '' || $final == NULL  ){
								$flag = TRUE;
						}
					break;
					case 't_nr':
						$final = $this->pre_cleanup(preg_replace( "/[^A-Z a-z$]/","",$cleaned));
						if($final == '' || $final == NULL  ){
							if($value != '' && $value != NULL){
								$flag = TRUE;
							}
						}
					break;

					case 'n':
						$final = $this->pre_cleanup(preg_replace( "/[^0-9.$]/","",$cleaned));
						if($final == '' || $final == NULL  ){
								$flag = TRUE;
						}
					break;
					case 'n_nr':
						$final = $this->pre_cleanup(preg_replace( "/[^0-9.$]/","",$cleaned));
						if($final == '' || $final == NULL  ){
							if($value != '' && $value != NULL){
								$flag = TRUE;
							}
						}
					break;

					case 'date':
						$final = $this->pre_cleanup(preg_replace( "/[^0-9/$]/","",$cleaned));
						if($final == '' || $final == NULL  ){
								$flag = TRUE;
						}
						if(validateDate($cleaned)==FALSE){
							$flag = TRUE;
						}
					break;
					case 'date_nr':
						$final = $this->pre_cleanup(preg_replace( "/[^0-9/$]/","",$cleaned));
						if($final == '' || $final == NULL  ){
							if($value != '' && $value != NULL){
								$flag = TRUE;
							}
						}else{
							if(validateDate($cleaned)==FALSE){
							$flag = TRUE;
							}
						}
					break;

					case 'else':
						$final = $this->pre_cleanup($cleaned);
						if($final == '' || $final == NULL  ){
								$flag = TRUE;
						}
					break;
					case 'else_nr':
						$final = $this->pre_cleanup($cleaned);
						if($final == ''|| $final == NULL  ){
							if($value != '' && $value != NULL){
								$flag = TRUE;
							}
						}
					break;

                   }

                   if ($final != $value || $flag == TRUE){
					   
						if ($notification == 'on'){
						echo 'Please fill out the '.$fieldset[$loop].' field correctly';
						die();
						}
						
                        return FALSE;
                   }
            
        $loop ++;
        
        }
        return TRUE;

    }
	
	
	public function formid($id){
	$today = date("Ymd");
	$formid = md5($today.sha1($this->formkey.$id.$_SESSION['sesskey']));
	return $formid;
	}
	
	
	
	public function invalidchar2($valueset,$fieldset,$case){
	//_nr is for notrequired fields
        $loop = 0;
		$flag =FALSE;
		
        foreach ($valueset as $value ){
                
                 $cleaned = trim($value);

                   switch ($case[$loop]){

					case 'tn':
						$final = $this->pre_cleanup(preg_replace( "/[^A-Za-z 0-9]/","",$cleaned));
						if($final == '' || $final == NULL  ){
								$flag = TRUE;
						}
					break;
					case 'tn_nr':
						$final = $this->pre_cleanup(preg_replace( "/[^A-Za-z 0-9]/","",$cleaned));
						if($final == '' || $final == NULL  ){
							if($value != '' && $value != NULL){
								$flag = TRUE;
							}
						}
					break;
					
					case 't':
						$final = $this->pre_cleanup(preg_replace( "/[^A-Z a-z$]/","",$cleaned));
						if($final == '' || $final == NULL  ){
								$flag = TRUE;
						}
					break;
					case 't_nr':
						$final = $this->pre_cleanup(preg_replace( "/[^A-Z a-z$]/","",$cleaned));
						if($final == '' || $final == NULL  ){
							if($value != '' && $value != NULL){
								$flag = TRUE;
							}
						}
					break;

					case 'n':
						$final = $this->pre_cleanup(preg_replace( "/[^0-9.$]/","",$cleaned));
						if($final == '' || $final == NULL  ){
								$flag = TRUE;
						}
					break;
					case 'n_nr':
						$final = $this->pre_cleanup(preg_replace( "/[^0-9.$]/","",$cleaned));
						if($final == '' || $final == NULL  ){
							if($value != '' && $value != NULL){
								$flag = TRUE;
							}
						}
					break;

					case 'date':
						$final = $this->pre_cleanup(preg_replace( "/[^0-9/$]/","",$cleaned));
						if($final == '' || $final == NULL  ){
								$flag = TRUE;
						}
						if(validateDate($cleaned)==FALSE){
							$flag = TRUE;
						}
					break;
					case 'date_nr':
						$final = $this->pre_cleanup(preg_replace( "/[^0-9/$]/","",$cleaned));
						if($final == '' || $final == NULL  ){
							if($value != '' && $value != NULL){
								$flag = TRUE;
							}
						}else{
							if(validateDate($cleaned)==FALSE){
							$flag = TRUE;
							}
						}
					break;

					case 'else':
						$final = $this->pre_cleanup($cleaned);
						if($final == '' || $final == NULL  ){
								$flag = TRUE;
						}
					break;
					case 'else_nr':
						$final = $this->pre_cleanup($cleaned);
						if($final == ''|| $final == NULL  ){
							if($value != '' && $value != NULL){
								$flag = TRUE;
							}
						}
					break;

                   }

                   if ($final != $value || $flag == TRUE){
					   
                        $resultsarray = array ('STATUS'=>'FAIL','FLD'=>$fieldset[$loop]);
						return $resultsarray;
                   }
            
        $loop ++;
        
        }
		
		$resultsarray = array ('STATUS'=>'PASS','FLD'=>'NONE');
        return $resultsarray;

    }
	
	
	
	public function pre_cleanup($value){
	$this->purifier = new HTMLPurifier();
	$clear = $this->purifier->purify($value);
	return $clear;
	}
	
    public function matching ($value1,$value2,$alertdesc,$condition){

        //CONDITION 0 WHEN YOU WANT TO MATCH
        //CONDITION 1 WHEN YOU DO NOT WANT TO MATCH

        if ($value1 == $value2){

            if ($condition == 0){
              return TRUE; 
            }else{
              echo '<div data-alert class="alert-box">
                    '.$alertdesc.'
                    <a href="#" class="close">&times;</a>
              </div>';
              return FALSE; 
            }
            
        }else{

            if ($condition == 0){
            echo '<div data-alert class="alert-box">
                    '.$alertdesc.'
                    <a href="#" class="close">&times;</a>
                </div>';
            return FALSE;
            }else{
            return TRUE;  
            }
        }
    }
	
	

    public function recordexists ($sql){
         $dbc = new dbconnection;
            $result =  $dbc->dbconn->query($sql);
			if($result){
			   if (mysqli_num_rows($result) == 0){
					//$result->free();
					$dbc->dbconn->close();
					return FALSE;
				}else{
					$result->free();
					$dbc->dbconn->close();
					return TRUE;
				}
			}
         
		return FALSE;
    }

        public function selectoptionsrequest ($valfield,$dispfield,$table,$condition){
            $dbc = new dbconnection;
            $tablename = preg_replace( "/[^a-z_$]/","",$table);

            if ($tablename != 'users'){
				if($condition !=''){
					$options = $this->selectoptions("Select ".mysqli_real_escape_string($dbc->dbconn,$valfield).",".mysqli_real_escape_string($dbc->dbconn,$dispfield)." FROM ".$table." WHERE ".$condition,$valfield,$dispfield,'');
				}else{
					$options = $this->selectoptions("Select ".mysqli_real_escape_string($dbc->dbconn,$valfield).",".mysqli_real_escape_string($dbc->dbconn,$dispfield)." FROM ".$table,$valfield,$dispfield,'');
				}
               
               $dbc->dbconn->close();
               return $options;
            }
    }
	
	
	public function selectoptionsrequest_selected ($valfield,$dispfield,$table,$condition,$selected,$emptyfield='N'){
            $dbc = new dbconnection;
            $tablename = preg_replace( "/[^a-z_$]/","",$table);

            if ($tablename != 'users'){
				if($condition !=''){
					$options = $this->selectoptions("Select ".mysqli_real_escape_string($dbc->dbconn,$valfield).",".mysqli_real_escape_string($dbc->dbconn,$dispfield)." FROM ".$table." WHERE ".$condition,$valfield,$dispfield,$selected,$emptyfield);
				}else{
					$options = $this->selectoptions("Select ".mysqli_real_escape_string($dbc->dbconn,$valfield).",".mysqli_real_escape_string($dbc->dbconn,$dispfield)." FROM ".$table,$valfield,$dispfield,$selected,$emptyfield);
				}
               
               $dbc->dbconn->close();
               return $options;
            }
    }
	
    private function selectoptions ($sql,$valfield,$dispfield,$selected,$emptyfield ='N'){
            $dbc = new dbconnection;
           $options = '';
		   if ($emptyfield == 'Y'){
			   $options .= '<option>NONE</option>';
		   }
		   
		   
           $result =  $dbc->dbconn->query($sql);
            if (mysqli_num_rows($result) > 0){
                while($rows=mysqli_fetch_assoc($result)){
					if($selected == $rows[$valfield]){
					$options .= '<option value="'.$rows[$valfield].'" selected>'.$rows[$dispfield].'</option>';
					}else{
					$options .= '<option value="'.$rows[$valfield].'">'.$rows[$dispfield].'</option>';
					}
                    
                }
                $result->free();
                $dbc->dbconn->close();  
                return $options;
            }else{
               $result->free();
                $dbc->dbconn->close();
				return $options;				
            }
    }

    public function tablecontent ($sql,$fieldset,$fieldclass,$actionfieldlink,$actiontext,$authority){
          $dbc = new dbconnection;
          $result = $dbc->dbconn->query($sql);
          $tablecontents = '';
          if (mysqli_num_rows($result) > 0){
            while ($rows = mysqli_fetch_assoc($result)){
                $fcount = 0;
                 $tablecontents .= '<tr>';

                        foreach ($fieldset as $field){
                        
                        if ($fieldclass[$fcount]=='dhi'){
                            $tablecontents .= '<td class="rtl"><p>';
                        }else{
                            $tablecontents .= '<td><p>';
                        }

                        if (count($fieldset)!=($fcount+1)){
                            $tablecontents .= $rows[$fieldset[$fcount]];
                        }else{
                            if ($authority == 'useronly'){
                                    if ($rows['U_NAME'] == $_SESSION['username']){
                                      $tablecontents .= '<a href="'.$actionfieldlink.$rows[$fieldset[$fcount]].'">'.$actiontext.'</a>';  
                                    }else{
                                      $tablecontents .= '<p></p>';  
                                    }

                            }else{
                                $tablecontents .= '<a href="'.$actionfieldlink.$rows[$fieldset[$fcount]].'">'.$actiontext.'</a>';  
                            }

                        }

                        $tablecontents .= '</P></td>';

                        $fcount ++;
                        }

                $tablecontents .= '</tr>';

            }
             $result->free();
             $dbc->dbconn->close();
             return $tablecontents;
          }else{
             $result->free();
             $dbc->dbconn->close();
          }

    }

    public function tablecontent2 ($sql,$fieldset,$fieldclass){
          $dbc = new dbconnection;
          $result = $dbc->dbconn->query($sql);
          $tablecontents = '';
          if (mysqli_num_rows($result) > 0){
            while ($rows = mysqli_fetch_assoc($result)){
                $fcount = 0;
                 $tablecontents .= '<tr>';

                        foreach ($fieldset as $field){
                        
                        if ($fieldclass[$fcount]=='dhi'){
                            $tablecontents .= '<td class="rtl"><p>';
                        }else{
                            $tablecontents .= '<td><p>';
                        }
                        $tablecontents .= $rows[$fieldset[$fcount]];
                        $tablecontents .= '</P></td>';

                        $fcount ++;
                        }

                $tablecontents .= '</tr>';

            }
             $result->free();
             $dbc->dbconn->close();
             return $tablecontents;
          }else{
             $result->free();
             $dbc->dbconn->close();
          }

    }

    public function getuserinfo($id){
          $sql = "SELECT U_NAME FROM user WHERE U_ID = ".$id;
           
			$dbc = new dbconnection;
			$stmt =  $dbc->dbconn->stmt_init();
			$stmt->prepare($sql);
			$stmt->execute();
			$stmt->bind_result($U_NAME);
			$stmt->fetch();
			$stmt->free_result();
			$stmt->close();
			$dbc->dbconn->close();
			$userinfo = Array( 
			'USERNAME'=> $U_NAME, 
			);
			return  $userinfo;
    }
	
	public function notification($doc_id,$desc,$nttype){
		$dbc = new dbconnection;
		$ins_stmt = $dbc->dbconn->prepare("INSERT INTO doc_history (`DH_DOC_ID`, `DH_DESC`,`DH_NT_ID`,`DH_SETBY`) VALUES ( ?, ?, ?, ?)");
		$ins_stmt->bind_param('dsdd', $DH_DOC_ID, $DH_DESC, $DH_NT_ID, $DH_SETBY);
		$DH_DOC_ID = $doc_id;
		$DH_DESC = $desc;
		$DH_NT_ID = $nttype;
		$DH_SETBY = $_SESSION['userid'];
		$ins_stmt->execute();
		$ins_stmt->close();
		$dbc->dbconn->close();
	}

    public function timeconverter($datetime){
        //INCOMING FORMAT: 04/26/2015 7:22 PM
        $components = explode(' ',$datetime);
        $dateparts  = explode('/',$components[0]);
        $timeparts  = explode(':',$components[1]);
        
        $AM = array('','01','02','03','04','05','06','07','08','09','10','11','00');
        $PM = array('','13','14','15','16','17','18','19','20','21','22','23','12'); 

        if($components[2] == 'AM'){
            $hour = $AM[$timeparts[0]];
        }else{
            $hour = $PM[$timeparts[0]]; 
        }
        $timestamp =  $dateparts[2].'-'.$dateparts[0].'-'.$dateparts[1].' '.$hour.':'.$timeparts[1].':00';

        return $timestamp;
    }
	
	
	public function sneakpeak($content,$length){
	
		$cleaned = strip_tags($content);
		$wordsarray = explode(" ",$cleaned);
		$sneakpeak = '';
		$charcount = 0;
		foreach( $wordsarray as $word ){
			$charcount = $charcount + strlen($word);
			if($charcount < $length){
				$sneakpeak .= $word." ";
			}else{
				$sneakpeak .= "...";
				break;
			}
		}
		return $sneakpeak;
	
	}
	
	
	public function getdefaultpriority($OFFICE){
        $sql = "SELECT P_ID from office INNER JOIN priority ON O_DEFAULTPRIO = P_ID WHERE O_ID = ".$OFFICE;
        $dbc = new dbconnection;
        $stmt =  $dbc->dbconn->stmt_init();
        $stmt->prepare($sql);
        $stmt->execute();
        $stmt->bind_result($DEFAULTPRIORITY);
        $stmt->fetch();
        $stmt->free_result();
        $stmt->close();
        $dbc->dbconn->close();
        return $DEFAULTPRIORITY;
    }

	protected function autoReplySMS($SMS_R_SENDER_NUMBER,$HEADER,$MESSAGE){
$xml_data= '<?xml version="1.0" encoding="UTF-8"?>
<TELEMESSAGE>
	<TELEMESSAGE_CONTENT>
		<MESSAGE>
			<MESSAGE_INFORMATION>
				<SUBJECT>'.$HEADER.'</SUBJECT>
			</MESSAGE_INFORMATION>
			<USER_FROM>
				<CIML>
					<NAML>
						<LOGIN_DETAILS>
							<USER_NAME>username</USER_NAME>
							<PASSWORD>password</PASSWORD>
						</LOGIN_DETAILS>
					</NAML>
				</CIML>
			</USER_FROM>
			<MESSAGE_CONTENT>
				<TEXT_MESSAGE>
					<MESSAGE_INDEX>0</MESSAGE_INDEX>
					<TEXT>'.$MESSAGE.'</TEXT>
				</TEXT_MESSAGE>
			</MESSAGE_CONTENT>
			<USER_TO>
				<CIML>
					<DEVICE_INFORMATION>
						<DEVICE_TYPE DEVICE_TYPE="SMS"/>
						<DEVICE_VALUE>'.$SMS_R_SENDER_NUMBER.'</DEVICE_VALUE>
					</DEVICE_INFORMATION>
				</CIML>
			</USER_TO>
		</MESSAGE>
	</TELEMESSAGE_CONTENT>
	<VERSION>1.6</VERSION>
</TELEMESSAGE>';
$URL = "https://bulkmessage.dhiraagu.com.mv/partners/xmlMessage.jsp";
$ch = curl_init($URL);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,0);
curl_setopt($ch, CURLOPT_PROXY, '192.168.245.7:8080');
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$output = curl_exec($ch);
return TRUE;
}

}
?>