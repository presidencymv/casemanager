<?php
class document extends master{
    public function createdocument($values,$stype = 0,$CMREF = 'CM'){
        $dbc = new dbconnection;
		
		
		if($stype == 0){
		$stmt = $dbc->dbconn->prepare("INSERT INTO document (`DOC_SYSID`,`DOC_SRC`,`DOC_SENDER_TYPE`, `DOC_OFFICE`, `DOC_TXT1`, `DOC_TXT2`, `DOC_CATEGORY`, `DOC_REFNUMBER`, `DOC_DESCRIPTION`, `DOC_DEADLINE`,`DOC_PRIORITY`, `DOC_STATUS`, `DOC_CREATEDBY`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $stmt->bind_param('sdddssdsssddd', $DOC_SYSID ,$DOC_SRC ,$DOC_SENDER_TYPE ,$DOC_OFFICE, $DOC_TXT1, $DOC_TXT2, $DOC_CATEGORY, $DOC_REFNUMBER, $DOC_DESCRIPTION, $DOC_DEADLINE, $DOC_PRIORITY , $DOC_STATUS, $DOC_CREATEDBY);
		$DOC_OFFICE = $values[1];
		}else{
		$stmt = $dbc->dbconn->prepare("INSERT INTO document (`DOC_SYSID`,`DOC_SRC`,`DOC_SENDER_TYPE`, `DOC_TXT1`, `DOC_TXT2`, `DOC_CATEGORY`, `DOC_REFNUMBER`, `DOC_DESCRIPTION`, `DOC_DEADLINE`,`DOC_PRIORITY`, `DOC_STATUS`, `DOC_CREATEDBY`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $stmt->bind_param('sddssdsssddd', $DOC_SYSID ,$DOC_SRC ,$DOC_SENDER_TYPE, $DOC_TXT1, $DOC_TXT2, $DOC_CATEGORY, $DOC_REFNUMBER, $DOC_DESCRIPTION, $DOC_DEADLINE, $DOC_PRIORITY , $DOC_STATUS, $DOC_CREATEDBY);
		}
		
        $DOC_SYSID = $this->generate_logid();
		$DOC_SRC=$values[0];
		
		$DOC_SENDER_TYPE=$stype;
        
		$DOC_TXT1 = $values[7];
        $DOC_TXT2 = $values[8];
		
		$DOC_CATEGORY=$values[2];
        
		if(strlen($values[3]) != 0){
		$DOC_REFNUMBER=$values[3];
		}else{
		$DOC_REFNUMBER_RUNNING= $this->autocaseref($DOC_SRC,date("Y"));
		$DOC_REFNUMBER = $CMREF.'/'.$DOC_SRC.'/'.$DOC_REFNUMBER_RUNNING.'/'.date("Y");
		}
		
        $DOC_DESCRIPTION=$values[4];
        $DOC_DEADLINE=$values[5];

        if ($values[6] == 1) {
		
			if($stype == 0){
				$DOC_PRIORITY = $this->getdefaultpriority($DOC_OFFICE);
			}else{
				$DOC_PRIORITY = 2;
			}
            
		}else{
            $DOC_PRIORITY = $values[6];
        }
		
        $DOC_STATUS=1;
        $DOC_CREATEDBY=$_SESSION['userid'];
		
        $stmt->execute();
        if ($stmt->errno) {
			$stmt->close();
			$dbc->dbconn->close();
			return FALSE;
        }
        else {

			$stmt->close();
			$dbc->dbconn->close();
			return TRUE;

        }
       
    }
	
	public function autocaseref($DOC_SRC,$YEAR){
		$dbc = new dbconnection;

        $sql = "SELECT CRC_NUM FROM case_ref_ctrl WHERE CRC_DSrc_ID = ".$DOC_SRC." AND CRC_YEAR = ".$YEAR; 

        //CREATE NUMBER RANGE IF IT DOES NOT EXIST
        if ($this->recordexists($sql) == false){
            $ins_stmt = $dbc->dbconn->prepare("INSERT INTO case_ref_ctrl (`CRC_DSrc_ID`, `CRC_YEAR`, `CRC_NUM`) VALUES (?, ?, ?)");
            $ins_stmt->bind_param('ddd', $CRC_DSrc_ID, $CRC_YEAR, $CRC_NUM);
            $CRC_DSrc_ID = $DOC_SRC;
            $CRC_YEAR = $YEAR;
            $CRC_NUM = 1;
            $ins_stmt->execute();
            $ins_stmt->close();
        }

		//GET NUMBER
        $dbc = new dbconnection;
        $sel_stmt =  $dbc->dbconn->stmt_init();
        $sel_stmt->prepare($sql);
        $sel_stmt->execute();
        $sel_stmt->bind_result($RUNNING_NUMBER);
        $sel_stmt->fetch();
        $sel_stmt->free_result();
        $sel_stmt->close();
                
        //ADD NUMBER
        $newnumber = $RUNNING_NUMBER + 1;
        $up_stmt = $dbc->dbconn->prepare("UPDATE case_ref_ctrl SET CRC_NUM = ? WHERE CRC_DSrc_ID = ".$DOC_SRC." AND CRC_YEAR = ".$YEAR);
        $up_stmt->bind_param('d', $newnumber);
        $up_stmt->execute();
        $up_stmt->close();

        //CLOSE CONNECTION
        $dbc->dbconn->close();

        //RETURN NUMBER
        return  $RUNNING_NUMBER;
	}
	
	
	public function updatedocument($values,$stype){
	
	//$docid,trim($_POST['source']),trim($_POST['sender']),trim($_POST['type']),$document->pre_cleanup(trim($_POST['description'])),trim($_POST['priority'])
	   	$dbc = new dbconnection;

		if($stype == 0){
		$stmt = $dbc->dbconn->prepare("UPDATE document SET `DOC_SRC` = ?, `DOC_OFFICE` = ?, `DOC_CATEGORY` = ?, `DOC_DESCRIPTION` = ?, `DOC_PRIORITY` = ?, `DOC_CREATEDBY` = ?, `DOC_TXT1` = ?, `DOC_TXT2` = ?,DOC_SENDER_TYPE = ?,DOC_REFNUMBER = ?,DOC_NAME = ? WHERE DOC_ID = ?");
        $stmt->bind_param('dddsddssdssd', $DOC_SRC, $DOC_OFFICE, $DOC_CATEGORY, $DOC_DESCRIPTION, $DOC_PRIORITY, $DOC_CREATEDBY, $DOC_TXT1, $DOC_TXT2,$DOC_SENDER_TYPE, $DOC_REFNUMBER, $DOC_NAME, $DOC_ID);
		$DOC_OFFICE = $values[2];
		}else{
		$stmt = $dbc->dbconn->prepare("UPDATE document SET `DOC_SRC` = ?, `DOC_CATEGORY` = ?, `DOC_DESCRIPTION` = ?, `DOC_PRIORITY` = ?, `DOC_CREATEDBY` = ?, `DOC_TXT1` = ?, `DOC_TXT2` = ?,DOC_SENDER_TYPE = ?,DOC_REFNUMBER = ?,DOC_NAME = ? WHERE DOC_ID = ?");
        $stmt->bind_param('ddsddssdssd', $DOC_SRC, $DOC_CATEGORY, $DOC_DESCRIPTION, $DOC_PRIORITY, $DOC_CREATEDBY, $DOC_TXT1, $DOC_TXT2,$DOC_SENDER_TYPE, $DOC_REFNUMBER, $DOC_NAME, $DOC_ID);
		}
		
		
		$DOC_SRC = $values[1];
		$DOC_CATEGORY = $values[3];
		$DOC_DESCRIPTION = $values[4];
		$DOC_PRIORITY = $values[5];
		$DOC_CREATEDBY = $_SESSION['userid'];
		$DOC_ID = $values[0];
		$DOC_TXT1 = $values[6];
		$DOC_TXT2 = $values[7];
		$DOC_SENDER_TYPE = $stype;
		$DOC_REFNUMBER = $values[8];
		$DOC_NAME = $values[9];
		
        $stmt->execute();
        if ($stmt->errno) {

            $dbc->dbconn->close();
            return FALSE;
        }else{
            $dbc->dbconn->close();
            return TRUE;
        }
	}
	
	
	public function call_updatedocument($values){
		$dbc = new dbconnection;

		$stmt = $dbc->dbconn->prepare("UPDATE document SET `DOC_OFFICE` = ?, `DOC_CATEGORY` = ?, `DOC_DESCRIPTION` = ?, `DOC_PRIORITY` = ?, `DOC_CREATEDBY` = ? WHERE DOC_ID = ?");
        $stmt->bind_param('ddsddd', $DOC_OFFICE, $DOC_CATEGORY, $DOC_DESCRIPTION, $DOC_PRIORITY, $DOC_CREATEDBY, $DOC_ID);
		
		$DOC_OFFICE = $values[1];
		$DOC_CATEGORY = $values[2];
		$DOC_DESCRIPTION = $values[3];
		$DOC_PRIORITY = $values[4];
		$DOC_CREATEDBY = $values[0];
		$DOC_ID = $values[5];
		
        $stmt->execute();
        if ($stmt->errno) {
            $dbc->dbconn->close();
            return FALSE;
        }else{
            $dbc->dbconn->close();
            return TRUE;
        }
	}

    public function generatedocname(){
        $dbc = new dbconnection;
        $sql="select doc_id,doc_file from document WHERE doc_createdby =".$_SESSION['userid']." ORDER BY DOC_ID DESC LIMIT 1" ;
        $result = $dbc->dbconn->query($sql);
        $docid = mysqli_fetch_assoc($result);
        $ext = substr($docid['doc_file'], strpos($docid['doc_file'],'.'), strlen($docid['doc_file'])-1);
        $newfilename = $docid['doc_id'].md5(mt_rand().time()).$ext;

        $this->changehistory($docid['doc_id'],'NEW DOCUMENT CREATED',1);

        $sql = "UPDATE document SET doc_file = '".$newfilename."' WHERE doc_id = ".$docid['doc_id'];
        $execute = $dbc->dbconn->query($sql);

        $result->free();
        $dbc->dbconn->close();
        return $newfilename;
    }


      private function generate_logid(){
        $sql = "SELECT count(DOC_ID)+1 as DOCSEQ ,DATE(CURDATE()) as DOCDATE from document WHERE DATE(`DOC_CREATEDON`) = CURDATE()";
        $dbc = new dbconnection;
        $stmt =  $dbc->dbconn->stmt_init();
        $stmt->prepare($sql);
        $stmt->execute();
        $stmt->bind_result($DOCSEQ,$DOCDATE);
        $stmt->fetch();
        $stmt->free_result();
        $stmt->close();
        $dbc->dbconn->close();
        return $DOCDATE.'/'.$DOCSEQ;
    }
	
	public function getlastdoc_id(){
		$dbc = new dbconnection;
        $sql="select doc_id from document WHERE doc_createdby =".$_SESSION['userid']." ORDER BY DOC_ID DESC LIMIT 1" ;
        $result = $dbc->dbconn->query($sql);
        $docid = mysqli_fetch_assoc($result);
		return $docid['doc_id'];
	}
	
	
    public function changehistory($docnumber,$description,$status){

        if ($description == 'STANDARDMSG'){
          switch ($status){
            case 1:
                $description = 'NEW FILE CREATED';
            break;  
                
            case 2:
                $description = 'TASK ASSIGNED';
            break;

            case 3:
                $description = 'STATUS SET AS ONGOIN';
            break;

            case 4:
                $description = 'DOCUMENT CLOSED';
            break;

          }
        }

        $dbc = new dbconnection;
        $stmt = $dbc->dbconn->prepare("INSERT INTO doc_history (`DH_DOC_ID`, `DH_DESC`, `DH_STATUS`, `DH_SETBY`) VALUES (?, ?, ?, ?)");
        $stmt->bind_param('dsdd', $DH_DOC_ID, $DH_DESC, $DH_STATUS, $DH_SETBY);
        
        $DH_DOC_ID = $docnumber;
        $DH_DESC = $description;
        $DH_STATUS = $status;
        $DH_SETBY=$_SESSION['userid'];
         $stmt->execute();
        $dbc->dbconn->close();
    }


    public function createddoc($id){
     $dbc = new dbconnection;
          $sql="SELECT DOC_ID FROM `document` where DOC_STATUS <> 4 AND DOC_HIDDEN = 'N' AND DOC_CREATEDBY = ".$_SESSION['userid']." AND doc_id = ".$id;
          $result = $dbc->dbconn->query($sql);
          if (mysqli_num_rows($result) == 1){
            $dbc->dbconn->close();
            return TRUE;   
          }else{
            $dbc->dbconn->close();
            return FALSE;   
          }
            
    }

    public function authorizeddoc($id){
          $dbc = new dbconnection;
          $sql="SELECT DOC_ID FROM `document` where DOC_HIDDEN = 'N' AND DOC_UNIT IN (".$_SESSION['handlerunits'].") AND doc_id = ".$id;
          $result = $dbc->dbconn->query($sql);
          if (mysqli_num_rows($result) == 1){
            $dbc->dbconn->close();
            return TRUE;   
          }else{
            $dbc->dbconn->close();
            return FALSE;   
          }
            
    }
    
    public function authorizedrejdoc($id){
          $dbc = new dbconnection;
          $sql="SELECT DOC_ID FROM `document` where DOC_HIDDEN = 'N' AND DOC_REJECT = 'Y' AND doc_id = ".$id;
          $result = $dbc->dbconn->query($sql);
          if (mysqli_num_rows($result) == 1){
            $dbc->dbconn->close();
            return TRUE;   
          }else{
            $dbc->dbconn->close();
            return FALSE;   
          }
            
    }

	public function authorizeddoc_assigned($id){
          $dbc = new dbconnection;
          $sql="SELECT DOC_ID FROM `document` where DOC_HIDDEN = 'N' AND DOC_ASSIGNEDTO = ".$_SESSION['userid']." AND doc_id = ".$id;
          $result = $dbc->dbconn->query($sql);

          if (mysqli_num_rows($result) == 1){
            $dbc->dbconn->close();
            return TRUE;   
          }else{
            $dbc->dbconn->close();
            return FALSE;   
          }
            
    }
	
	public function authorizeddoc_shared($id){
          $dbc = new dbconnection;
          $sql="SELECT * FROM `document` inner join doc_share on DS_DOC_ID = DOC_ID WHERE DOC_HIDDEN = 'N' AND DS_U_ID = ".$_SESSION['userid']." AND doc_id = ".$id;
          $result = $dbc->dbconn->query($sql);
          if (mysqli_num_rows($result) == 1){
            $dbc->dbconn->close();
            return TRUE;   
          }else{
            $dbc->dbconn->close();
            return FALSE;   
          }
            
    }
	
	public function callcentrecase($id){
		  $dbc = new dbconnection;
          $sql="SELECT * FROM `document` WHERE DOC_CR_DOC = 1 AND doc_id = ".$id;
          $result = $dbc->dbconn->query($sql);
          if (mysqli_num_rows($result) == 1){
            $dbc->dbconn->close();
            return TRUE;   
          }else{
            $dbc->dbconn->close();
            return FALSE;   
          }
	}
	
	public function authorizeddoc_counter($id){
          $dbc = new dbconnection;
		  if($_SESSION['counter'] == 'Y'){
			if(in_array($dbc->CounterUnitID,explode(',',$_SESSION['user_units']))){
				if($_SESSION['handler'] == 'Y' && in_array($dbc->CounterUnitID, explode(',' , $_SESSION['handlerunits'])) == TRUE ){
					$sql="SELECT * FROM `document` WHERE DOC_HIDDEN = 'N' AND DOC_UNIT IS NULL AND doc_id = ".$id;
					
				}else{
					$sql="SELECT * FROM `document` WHERE DOC_HIDDEN = 'N' AND DOC_UNIT IS NULL AND DOC_CREATEDBY IN (".$_SESSION['counterstaffs'].") AND doc_id = ".$id;
					
				}	
			}else{
				$sql="SELECT * FROM `document` WHERE DOC_HIDDEN = 'N' AND DOC_UNIT IS NULL AND DOC_CREATEDBY = ".$_SESSION['userid']." AND doc_id = ".$id;
				
			}
		 }			
		  
		  
          $result = $dbc->dbconn->query($sql);
          if (mysqli_num_rows($result) == 1){
            $dbc->dbconn->close();
            return TRUE;   
          }else{
            $dbc->dbconn->close();
            return FALSE;   
          }
            
    }
	
	
	public function counterchangeauth($id){
          $dbc = new dbconnection;
		  if($_SESSION['counter'] == 'Y'){
			if(in_array($dbc->CounterUnitID,explode(',',$_SESSION['user_units']))){
				$sql="SELECT * FROM `document` WHERE DOC_HIDDEN = 'N' AND DOC_UNIT IS NULL AND (DOC_CREATEDBY IN (".$_SESSION['counterstaffs'].") OR DOC_CREATEDBY = ".$_SESSION['userid']." ) AND doc_id = ".$id;
			}else{
				$sql="SELECT * FROM `document` WHERE DOC_HIDDEN = 'N' AND DOC_UNIT IS NULL AND DOC_CREATEDBY = ".$_SESSION['userid']." AND doc_id = ".$id;
			}
		 }			
		  
		  
          $result = $dbc->dbconn->query($sql);
          if (mysqli_num_rows($result) == 1){
            $dbc->dbconn->close();
            return TRUE;   
          }else{
            $dbc->dbconn->close();
            return FALSE;   
          }
            
    }
	
    public function getdocumentinfo($id){

         $sql = "SELECT DOC_SYSID, DOC_ID, DSrc_ID, O_ID, O_NAME, DC_ID, DC_NAME, DOC_UNIT, UNIT_NAME, IFNULL(DOC_REFNUMBER, 'NO REFERENCE') as REFNUMBER, DOC_DESCRIPTION, DEADLINEDATE, P_ID, P_NAME, DST_ID, DST_NAME, DOC_STATUS, DOCUMENTDETAILS.U_NAME AS CREATEDBY, IFNULL(ASSIGNED.U_NAME,'NONE') AS ASSIGNEDTO,IFNULL(ASSIGNED.U_ID,'NONE') AS ASSIGNEDID, LASTUPDATE, DOC_STATUSON, DOC_CR_DOC, DOC_TXT1, DOC_TXT2,DOC_SENDER_TYPE,IOR_DOC_ID,DOC_NAME
				FROM(
				SELECT DOC_SYSID, DOC_ID, DSrc_ID, O_ID, O_NAME, DC_ID, DC_NAME, DOC_UNIT, UNIT_NAME, DOC_REFNUMBER, DOC_DESCRIPTION, DATE(DOC_DEADLINE) as DEADLINEDATE, P_ID, P_NAME, DST_ID, DST_NAME, DOC_STATUS, U_NAME, DOC_ASSIGNEDTO, DATE(DOC_LASTUPDATE) as LASTUPDATE, DOC_STATUSON, DOC_CR_DOC, DOC_TXT1, DOC_TXT2,DOC_SENDER_TYPE,IOR_DOC_ID,DOC_NAME
				FROM document 
				left outer join office on DOC_OFFICE = O_ID
				inner join documentcat on DOC_CATEGORY = DC_ID
				left outer join unit on DOC_UNIT = UNIT_ID
				inner join priority on DOC_PRIORITY = P_ID
				inner join doc_status on DST_ID = DOC_STATUS
				inner join doc_source on DSrc_ID = DOC_SRC
				left outer join in_out_registery on DOC_ID = IOR_DOC_ID
				inner join user on DOC_CREATEDBY = U_ID
				WHERE DOC_HIDDEN <> 'Y' AND DOC_ID = ".$id.") as DOCUMENTDETAILS
				LEFT OUTER JOIN USER ASSIGNED ON DOCUMENTDETAILS.DOC_ASSIGNEDTO = ASSIGNED.U_ID
				ORDER BY DOC_ID DESC";
           
                $dbc = new dbconnection;
                $stmt =  $dbc->dbconn->stmt_init();
                $stmt->prepare($sql);
                $stmt->execute();
                $stmt->bind_result($DOC_SYSID,$DOC_ID,$DSrc_ID,$O_ID,$O_NAME,$DC_ID,$DC_NAME, $DOC_UNIT,$UNIT_NAME,$DOC_REFNUMBER,$DOC_DESCRIPTION, $DEADLINEDATE,$P_ID,$P_NAME,$DST_ID,$DST_NAME,$DOC_STATUS,$CREATEDBY,$ASSIGNEDTO,$ASSIGNEDID,$DOC_LASTUPDATE,$DOC_STATUSON,$DOC_CR_DOC, $DOC_TXT1, $DOC_TXT2,$DOC_SENDER_TYPE,$IOR_DOC_ID,$DOC_NAME);
                $stmt->fetch();
                $stmt->free_result();
                $stmt->close();
                $dbc->dbconn->close();

                $docinfo = Array( 
                'DOC_SYSID'=> $DOC_SYSID,
				'DOC_ID'=> $DOC_ID,
				'DSrc_ID'=> $DSrc_ID,
				'O_ID'=> $O_ID, 
                'O_NAME'=> $O_NAME,
				'DC_ID'=> $DC_ID,
                'DC_NAME'=> $DC_NAME,
                'DOC_UNIT'=> $DOC_UNIT,
                'UNIT_NAME'=> $UNIT_NAME,
                'DOC_REFNUMBER'=> $DOC_REFNUMBER, 
                'DOC_DESCRIPTION'=> $DOC_DESCRIPTION,
                'DEADLINEDATE'=> $DEADLINEDATE,
				'P_ID'=> $P_ID,
                'P_NAME'=> $P_NAME,
                'DST_ID'=> $DST_ID, 
                'DST_NAME'=> $DST_NAME, 
                'DOC_STATUS'=> $DOC_STATUS, 
                'CREATEDBY'=> $CREATEDBY,
                'ASSIGNEDTO'=> $ASSIGNEDTO,
                'ASSIGNEDID'=> $ASSIGNEDID,
				'DOC_LASTUPDATE'=> $DOC_LASTUPDATE,
				'DOC_STATUSON'=> $DOC_STATUSON,
				'DOC_CR_DOC'=>$DOC_CR_DOC,
				'DOC_TXT1'=> $DOC_TXT1,
				'DOC_TXT2'=>$DOC_TXT2,
				'DOC_SENDER_TYPE'=>$DOC_SENDER_TYPE,
				'IOR_DOC_ID'=>$IOR_DOC_ID,
				'DOC_NAME'=>$DOC_NAME
                );

                return  $docinfo;
    }
	
		
	public function getdocumentinfo_chancellery($id){

         $sql = "SELECT DOC_SYSID, DSrc_ID, O_ID, O_NAME, DC_ID, DC_NAME, DOC_UNIT, UNIT_NAME, IFNULL(DOC_REFNUMBER, 'NO REFERENCE') as REFNUMBER, DOC_DESCRIPTION, DEADLINEDATE, P_ID, P_NAME, DST_ID, DST_NAME, DOC_STATUS, DOCUMENTDETAILS.U_NAME AS CREATEDBY, 	IFNULL(ASSIGNED.U_NAME,'NONE') AS ASSIGNEDTO,IFNULL(ASSIGNED.U_ID,'NONE') AS ASSIGNEDID, LASTUPDATE, DOC_STATUSON, DOC_CR_DOC , DOC_TXT1, DOC_TXT2,DOC_SENDER_TYPE
				FROM(
				SELECT DOC_SYSID, DOC_ID, DSrc_ID, O_ID, O_NAME, DC_ID, DC_NAME, DOC_UNIT, UNIT_NAME, DOC_REFNUMBER, DOC_DESCRIPTION, DATE(DOC_DEADLINE) as DEADLINEDATE, P_ID, P_NAME, DST_ID, DST_NAME, DOC_STATUS, U_NAME, DOC_ASSIGNEDTO, DATE(DOC_LASTUPDATE) as LASTUPDATE, DOC_STATUSON, DOC_CR_DOC, DOC_TXT1, DOC_TXT2,DOC_SENDER_TYPE
				FROM document 
				left outer join office on DOC_OFFICE = O_ID
				inner join documentcat on DOC_CATEGORY = DC_ID
				left outer join unit on DOC_UNIT = UNIT_ID
				inner join priority on DOC_PRIORITY = P_ID
				inner join doc_status on DST_ID = DOC_STATUS
				inner join doc_source on DSrc_ID = DOC_SRC
				inner join user on DOC_CREATEDBY = U_ID
				inner join in_out_registery on DOC_ID = IOR_DOC_ID
				WHERE DOC_HIDDEN <> 'Y' AND DOC_ID = ".$id.") as DOCUMENTDETAILS
				LEFT OUTER JOIN USER ASSIGNED ON DOCUMENTDETAILS.DOC_ASSIGNEDTO = ASSIGNED.U_ID
				ORDER BY DOC_ID DESC";
           
                $dbc = new dbconnection;
                $stmt =  $dbc->dbconn->stmt_init();
                $stmt->prepare($sql);
                $stmt->execute();
                $stmt->bind_result($DOC_SYSID,$DSrc_ID,$O_ID,$O_NAME,$DC_ID,$DC_NAME, $DOC_UNIT,$UNIT_NAME,$DOC_REFNUMBER,$DOC_DESCRIPTION, $DEADLINEDATE,$P_ID,$P_NAME,$DST_ID,$DST_NAME,$DOC_STATUS,$CREATEDBY,$ASSIGNEDTO,$ASSIGNEDID,$DOC_LASTUPDATE,$DOC_STATUSON,$DOC_CR_DOC, $DOC_TXT1, $DOC_TXT2, $DOC_SENDER_TYPE);
                $stmt->fetch();
                $stmt->free_result();
                $stmt->close();
                $dbc->dbconn->close();

                $docinfo = Array( 
                'DOC_SYSID'=> $DOC_SYSID,
				'DSrc_ID'=> $DSrc_ID,
				'O_ID'=> $O_ID, 
                'O_NAME'=> $O_NAME,
				'DC_ID'=> $DC_ID,
                'DC_NAME'=> $DC_NAME,
                'DOC_UNIT'=> $DOC_UNIT,
                'UNIT_NAME'=> $UNIT_NAME,
                'DOC_REFNUMBER'=> $DOC_REFNUMBER, 
                'DOC_DESCRIPTION'=> $DOC_DESCRIPTION,
                'DEADLINEDATE'=> $DEADLINEDATE,
				'P_ID'=> $P_ID,
                'P_NAME'=> $P_NAME,
                'DST_ID'=> $DST_ID, 
                'DST_NAME'=> $DST_NAME, 
                'DOC_STATUS'=> $DOC_STATUS, 
                'CREATEDBY'=> $CREATEDBY,
                'ASSIGNEDTO'=> $ASSIGNEDTO,
                'ASSIGNEDID'=> $ASSIGNEDID,
				'DOC_LASTUPDATE'=> $DOC_LASTUPDATE,
				'DOC_STATUSON'=> $DOC_STATUSON,
				'DOC_CR_DOC'=>$DOC_CR_DOC,
				'DOC_TXT1'=>$DOC_TXT1,
				'DOC_TXT2'=>$DOC_TXT2,
				'DOC_SENDER_TYPE'=>$DOC_SENDER_TYPE
                );

                return  $docinfo;
	}
		
		

    public function getrejecteddocumentinfo($id){

         $sql = "SELECT DOC_SYSID, O_NAME, DC_NAME, IFNULL(DOC_REFNUMBER, 'NO REFERENCE') as REFNUMBER, DOC_DESCRIPTION, DOC_FILE, DEADLINEDATE, P_NAME, DST_ID, DST_NAME, DOC_STATUS, DOCUMENTDETAILS.U_NAME AS CREATEDBY
                FROM(
                SELECT DOC_SYSID, DOC_ID, O_NAME, DC_NAME, DOC_REFNUMBER, DOC_DESCRIPTION, DOC_FILE, DATE(DOC_DEADLINE) as DEADLINEDATE, P_NAME, DST_ID, DST_NAME, DOC_STATUS, U_NAME
                FROM document 
                left outer join office on DOC_OFFICE = O_ID
                inner join documentcat on DOC_CATEGORY = DC_ID
                inner join priority on DOC_PRIORITY = P_ID
                inner join doc_status on DST_ID = DOC_STATUS
                inner join user on DOC_CREATEDBY = U_ID
                WHERE DOC_HIDDEN <> 'Y' AND DOC_REJECT = 'Y' AND DOC_ID = ".$id.") as DOCUMENTDETAILS
                ORDER BY DOC_ID DESC";
           
                $dbc = new dbconnection;
                $stmt =  $dbc->dbconn->stmt_init();
                $stmt->prepare($sql);
                $stmt->execute();
                $stmt->bind_result($DOC_SYSID,$O_NAME,$DC_NAME,$DOC_REFNUMBER,$DOC_DESCRIPTION, $DOC_FILE, $DEADLINEDATE,$P_NAME,$DST_ID,$DST_NAME,$DOC_STATUS,$CREATEDBY);
                $stmt->fetch();
                $stmt->free_result();
                $stmt->close();
                $dbc->dbconn->close();

                $docinfo = Array( 
                'DOC_SYSID'=> $DOC_SYSID,
                'O_NAME'=> $O_NAME, 
                'DC_NAME'=> $DC_NAME,
                'DOC_REFNUMBER'=> $DOC_REFNUMBER, 
                'DOC_DESCRIPTION'=> $DOC_DESCRIPTION,
                'DOC_FILE'=> $DOC_FILE,
                'DEADLINEDATE'=> $DEADLINEDATE,
                'P_NAME'=> $P_NAME,
                'DST_ID'=> $DST_ID, 
                'DST_NAME'=> $DST_NAME, 
                'DOC_STATUS'=> $DOC_STATUS, 
                'CREATEDBY'=> $CREATEDBY,
                );

                return  $docinfo;
    }

    public function assigndocument($values){
        $dbc = new dbconnection;
        $stmt = $dbc->dbconn->prepare("UPDATE document SET DOC_ASSIGNEDTO = ? WHERE DOC_ID = ?");
        $stmt->bind_param('dd', $DOC_ASSIGNEDTO, $DOC_ID);
        $DOC_ASSIGNEDTO = $values[1];
        $DOC_ID = $values[0];
        $stmt->execute();
        if ($stmt->errno) {
			$stmt->close();
			$dbc->dbconn->close();
            return FALSE; 
        }else{
			$stmt->close();
			$dbc->dbconn->close();
            return TRUE;
        }
        
    }

    public function md_specificfields($values){
        $dbc = new dbconnection;
        $stmt = $dbc->dbconn->prepare("UPDATE document SET DOC_C_DIVISIONS = ?, DOC_NAME = ?, DOC_DIVISIONS = ? WHERE DOC_ID = ?");
        $stmt->bind_param('sssd', $DOC_C_DIVISIONS, $DOC_NAME, $DOC_DIVISIONS, $DOC_ID);
		$DOC_DIVISIONS = $values[3];
        $DOC_NAME = $values[2];
        $DOC_C_DIVISIONS = $values[1];
        $DOC_ID = $values[0];
        $stmt->execute();
        if ($stmt->errno) {
			$stmt->close();
			$dbc->dbconn->close();
            return FALSE; 
        }else{
			$stmt->close();
			$dbc->dbconn->close();
            return TRUE;
        }
    }

    public function sendsmsnotification($contactnumber,$header,$MESSAGE){
		$this->autoReplySMS($contactnumber,$header,$MESSAGE);
		return true;
	}

    public function attachrelateddocument($values){
        $dbc = new dbconnection;
        $stmt = $dbc->dbconn->prepare("INSERT INTO doc_related_cases (DRC_DOC_ID, DRC_RELATED_DOC_ID, DRC_ATTACHEDBY) VALUES (?,?,?)");
        $stmt->bind_param('ddd', $DRC_DOC_ID, $DRC_RELATED_DOC_ID, $DRC_ATTACHEDBY);
        $DRC_DOC_ID = $values[0];
        $DRC_RELATED_DOC_ID = $values[1];
        $DRC_ATTACHEDBY = $_SESSION['userid'];
        $stmt->execute();
        if ($stmt->errno) {
            $stmt->close();
            $dbc->dbconn->close();
            return FALSE;
        }else{
            $stmt->close();
            $dbc->dbconn->close();
            return TRUE;
        }
        
    }

        public function routedocument($values, $forwardedByUser = null){
        $dbc = new dbconnection;

        //check if this case is being forwarded
        //if the case is being forwarded, we should receive $forwardedByUser so that we can update the created by user to $forwardedByUser
        if($forwardedByUser){
            $stmt = $dbc->dbconn->prepare("UPDATE document SET DOC_UNIT = ?, DOC_ASSIGNEDTO = NULL, DOC_CREATEDBY = ?, DOC_REJECT = 'N', DOC_STATUS = 6 WHERE DOC_ID = ?");
            $stmt->bind_param('ddd', $DOC_UNIT, $DOC_CREATEDBY, $DOC_ID);
            $DOC_UNIT = $values[1];
            $DOC_CREATEDBY = $forwardedByUser;
            $DOC_ID = $values[0];
        }else{
            $stmt = $dbc->dbconn->prepare("UPDATE document SET DOC_ASSIGNEDTO = NULL, DOC_UNIT = ?, DOC_REJECT = 'N', DOC_STATUS = 1 WHERE DOC_ID = ?");
            $stmt->bind_param('dd', $DOC_UNIT,  $DOC_ID);
            $DOC_UNIT = $values[1];
            $DOC_ID = $values[0];
        }
        
        $stmt->execute();
        if ($stmt->errno) {
			$stmt->close();
			$dbc->dbconn->close();
            Return FALSE;
        }else{
            if ($this->docstatus($DOC_ID,1) == TRUE){
				$stmt->close();
				$dbc->dbconn->close();
				Return TRUE;
            }else{
				$stmt->close();
				$dbc->dbconn->close();
				Return FALSE;
            }
        }
    }


    public function docstatus($values){
        $dbc = new dbconnection;
        $DOC_STATUS = $values[1];
        $DOC_ID = $values[0];
        $callcase = false;

        if($this->callcentrecase($DOC_ID)){
            $callcase = true;
            if($DOC_STATUS == 4){
			
				if ($this->recordexists("SELECT DR_DOC_ID FROM doc_remarks WHERE DR_DOC_ID = ".$DOC_ID." AND DR_FILE = ''")){
					//FOR NOW DO NOTHING
				}else{
					die('You cannot close a Call Center Case without any updates, please add an update in order to close the case');
				}
			      
            }
        } 

        $stmt = $dbc->dbconn->prepare("UPDATE document SET DOC_STATUS = ?, DOC_STATUSON = NOW() WHERE DOC_ID = ?");
        $stmt->bind_param('dd', $DOC_STATUS, $DOC_ID);
        $stmt->execute();
        if ($stmt->errno) {
            $dbc->dbconn->close();
            return FALSE;
        }else{
            if($callcase == true){
                if($DOC_STATUS == 4){
                    $CR_SOLVED = 1;
                }else{
                    $CR_SOLVED = 0;
                }
                $stmt = $dbc->dbconn->prepare("UPDATE call_records SET CR_SOLVED = ? WHERE CR_DOC_ID = ?");
                $stmt->bind_param('dd', $CR_SOLVED, $DOC_ID);
                $stmt->execute();
            }
            $dbc->dbconn->close();
            return TRUE;
        }
    }

    public function dochide($docid){
        $dbc = new dbconnection;
        $stmt = $dbc->dbconn->prepare("UPDATE document SET DOC_HIDDEN = 'Y' WHERE DOC_ID = ? ");
        $stmt->bind_param('d', $DOC_ID);
        $DOC_ID = $docid;
        $stmt->execute();
        if ($stmt->errno) {
            $dbc->dbconn->close();
            return FALSE;
        }else{
            $dbc->dbconn->close();
            return TRUE;
        }
        
    }

    
    public function docreject($docid){
        $dbc = new dbconnection;
        $stmt = $dbc->dbconn->prepare("UPDATE document SET DOC_REJECT = 'Y', DOC_UNIT = NULL, DOC_ASSIGNEDTO = NULL, DOC_STATUS = 5 WHERE DOC_ID = ? ");
        $stmt->bind_param('d', $DOC_ID);
        $DOC_ID = $docid;
        $stmt->execute();
        if ($stmt->errno) {
            $dbc->dbconn->close();
            return FALSE;
        }else{
            $dbc->dbconn->close();
            return TRUE;
        }
        
    }
    
     public function docshare($values){
        $dbc = new dbconnection;
        $stmt = $dbc->dbconn->prepare("INSERT INTO doc_share (DS_U_ID, DS_DOC_ID, DS_BY) VALUES (?,?,?)");
        $stmt->bind_param('ddd', $DS_U_ID, $DS_DOC_ID, $DS_BY);
        $DS_U_ID = $values[1];
        $DS_DOC_ID = $values[0];
        $DS_BY = $_SESSION['userid'];
        $stmt->execute();
        if ($stmt->errno) {
			$stmt->close();
            $dbc->dbconn->close();
            return FALSE;
        }else{
			$stmt->close();
            $dbc->dbconn->close();
            return TRUE;
        }
        
    }

      public function docunshare($values){
        $dbc = new dbconnection;
        $stmt = $dbc->dbconn->prepare("DELETE FROM doc_share WHERE DS_ID = ? AND DS_DOC_ID = ?");
        $stmt->bind_param('dd', $DS_ID, $DS_DOC_ID);
        $DS_ID = $values[1];
        $DS_DOC_ID = $values[0];
        $stmt->execute();
        if ($stmt->errno) {
			$stmt->close();
            $dbc->dbconn->close();
            return FALSE;
        }else{
            if ($dbc->dbconn->affected_rows > 0){
				$stmt->close();
                $dbc->dbconn->close();
                return TRUE;
            }else{
				$stmt->close();
                $dbc->dbconn->close();
                return FALSE;
            }
        }
        
    }
	
	
	public function clearallshared($id){
        $dbc = new dbconnection;
        $stmt = $dbc->dbconn->prepare("DELETE FROM doc_share WHERE DS_DOC_ID = ?");
        $stmt->bind_param('d', $DS_DOC_ID);
        $DS_DOC_ID = $id;
        $stmt->execute();
        if ($stmt->errno) {
			$stmt->close();
            $dbc->dbconn->close();
            return FALSE;
        }else{
            if ($dbc->dbconn->affected_rows > 0){
				$stmt->close();
                $dbc->dbconn->close();
                return TRUE;
            }else{
				$stmt->close();
                $dbc->dbconn->close();
                return FALSE;
            }
        }
        
    }
	public function getfilename($id){
         $sql = "SELECT DOC_FILE FROM document WHERE DOC_ID = ".$id;
           
                $dbc = new dbconnection;
                $stmt =  $dbc->dbconn->stmt_init();
                $stmt->prepare($sql);
                $stmt->execute();
                $stmt->bind_result($DOC_FILE);
                $stmt->fetch();
                $stmt->free_result();
                $stmt->close();
                $dbc->dbconn->close();

                $filename = $DOC_FILE;
                return  $filename;

	}

    public function getrelatedcases($docid){
          $dbc = new dbconnection;
          $counter=0;
          $relatedcases = '';
          $sql = "select DRC_RELATED_DOC_ID from doc_related_cases where DRC_DOC_ID = ".$docid;
          $result = $dbc->dbconn->query($sql);
		  if ($result){
			    while ($rows = mysqli_fetch_assoc($result)){
                 if ($counter == 0){
                 $relatedcases .= $rows['DRC_RELATED_DOC_ID'];
                 $counter = 1;
                 }else{
                 $relatedcases .= ','.$rows['DRC_RELATED_DOC_ID'];    
                 }
				}
				$result->free();
		  }
        $dbc->dbconn->close();
        return $relatedcases;
    }

    public function getdocunitid($docid){
         $sql = "SELECT DOC_UNIT FROM document WHERE DOC_ID = ".$docid;
           
                $dbc = new dbconnection;
                $stmt =  $dbc->dbconn->stmt_init();
                $stmt->prepare($sql);
                $stmt->execute();
                $stmt->bind_result($DOC_UNIT);
                $stmt->fetch();
                $stmt->free_result();
                $stmt->close();
                $dbc->dbconn->close();

                $docunitid = $DOC_UNIT;
                return  $docunitid;

    }
	
	public function archiveyears($type){
      
	  
	  switch ($type){
		
		case 'my_active':
			$archive_years = "SELECT DISTINCT YEAR(DOC_CREATEDON) as YEAR FROM `document` WHERE DOC_HIDDEN <> 'Y' AND DOC_ASSIGNEDTO = ".$_SESSION['userid']." AND DOC_STATUS <> 4 ORDER BY YEAR DESC";
			$pagename = "home";
		break;
		
		case 'user':
			$archive_years = "SELECT DISTINCT YEAR(DOC_CREATEDON) as YEAR FROM `document` WHERE DOC_HIDDEN <> 'Y' AND DOC_ASSIGNEDTO = ".$_SESSION['userid']." ORDER BY YEAR DESC";
			$pagename = "mycases";
		break;
		
		case 'shared':
			$archive_years = "SELECT DISTINCT YEAR(DOC_CREATEDON) as YEAR FROM `document` inner join doc_share on DOC_ID = DS_DOC_ID WHERE  DOC_HIDDEN <> 'Y' AND DS_U_ID = ".$_SESSION['userid']." ORDER BY YEAR DESC";
			$pagename = "shared";
		break;
		
		case "statuscheck":
			$archive_years = "SELECT DISTINCT YEAR(DOC_CREATEDON) as YEAR FROM `document` WHERE DOC_HIDDEN <> 'Y' AND DOC_UNIT IN ( ".$_SESSION['user_units'].") ORDER BY YEAR DESC";
			$pagename = "statuscheck";
		break;
		
		case "unit":
			$archive_years = "SELECT DISTINCT YEAR(DOC_CREATEDON) as YEAR FROM `document` WHERE DOC_HIDDEN <> 'Y' AND DOC_UNIT IN (".$_SESSION['handlerunits'].")  AND DOC_STATUS <> 4  ORDER BY YEAR DESC";
			$pagename = "activecases";
		break;
		
		case "unit_all":
			$archive_years = "SELECT DISTINCT YEAR(DOC_CREATEDON) as YEAR FROM `document` WHERE DOC_HIDDEN <> 'Y' AND DOC_UNIT IN (".$_SESSION['handlerunits'].") ORDER BY YEAR DESC";
			$pagename = "sectioncases";
		break;
		
		case "maindirectory":
			$archive_years = "SELECT DISTINCT YEAR(DOC_CREATEDON) as YEAR FROM `document` WHERE DOC_HIDDEN <> 'Y' ORDER BY YEAR DESC";
			$pagename = "maindirectory";
		break;
		
		case "chancellery":
			$archive_years = "SELECT DISTINCT YEAR(DOC_CREATEDON) as YEAR FROM `in_out_registery` LEFT OUTER JOIN `document` ON DOC_ID = IOR_DOC_ID WHERE DOC_HIDDEN <> 'Y' AND DOC_STATUS <> 4  ORDER BY YEAR DESC";
			$pagename = "chancellery";
		break;
		
		case "chancelleryarchive":
			$archive_years = "SELECT DISTINCT YEAR(DOC_CREATEDON) as YEAR FROM `in_out_registery` LEFT OUTER JOIN `document` ON DOC_ID = IOR_DOC_ID WHERE DOC_HIDDEN <> 'Y' ORDER BY YEAR DESC";
			$pagename = "chancellery";
		break;
	  
	  
	  }
	  
		$dbc = new dbconnection;
		  $result =  $dbc->dbconn->query($archive_years);
		  $pages = array(
		  'result'=>False,
		  );

      if (mysqli_num_rows($result) > 0){
        while($rows=mysqli_fetch_assoc($result)){
            $arch_years[] = $rows['YEAR'] ;
        }
        $result->free();
        $dbc->dbconn->close();

        $pages['result'] = TRUE;
        $pages['years_array'] = $arch_years;
		$pages['page'] = $pagename;
        return $pages;

      }else{
        $result->free();
        $dbc->dbconn->close();
        return $pages;
      }
	
	
	}
	
		public function lastupdateinfo($docid){
		
		       $sql = "SELECT CONCAT (DR_DESC,'(DATE:',DR_ON,')') as DESCRIPTION FROM doc_remarks WHERE DR_DOC_ID = ".$docid." AND (DR_FILE IS NULL OR DR_FILE = '') ORDER BY DR_ON DESC LIMIT 1";
           
                $dbc = new dbconnection;
                $stmt =  $dbc->dbconn->stmt_init();
                $stmt->prepare($sql);
                $stmt->execute();
                $stmt->bind_result($DR_DESC);
                $stmt->fetch();
                $stmt->free_result();
                $stmt->close();
                $dbc->dbconn->close();

                $lastupdate = $DR_DESC;
                return  $lastupdate;

		
    }
    
    public function shared_staffs($docid){
		$dbc = new dbconnection;
		$sql = "SELECT U_NAME FROM doc_share INNER JOIN user ON DS_U_ID = U_ID WHERE DS_DOC_ID = ".$docid;
		$result = $dbc->dbconn->query($sql);
        $names = '';
        $initial = true;
		while ($unames = mysqli_fetch_assoc($result)){
          if ($initial==true){
            $names .= $unames['U_NAME'];
            $initial= false;
          }else{
            $names .= ', '.$unames['U_NAME'];
          } 
		}
		$result->free();
        $dbc->dbconn->close();
        
        $unames = ucwords(preg_replace( "/[^A-Za-z 0-9,$]/"," ",strtolower($names)));

		return $unames;
	}
	

	//START OF FOR SPECIAL CASES//
	public function listOfDocumentInSection($UnitID){
		$dbc = new dbconnection;
		$sql = "SELECT DOC_ID FROM document WHERE DOC_UNIT = ".$UnitID;
		$result = $dbc->dbconn->query($sql);
		$rows = array();
		while ($document = mysqli_fetch_assoc($result)){
		 $rows[] = $document;
		}
		$result->free();
		$dbc->dbconn->close();
		return $rows;
	}
	
	public function BudgetExDocs(){
		$dbc = new dbconnection;
		//$sql = "SELECT DOC_ID FROM document WHERE DOC_UNIT = 39 AND DOC_ASSIGNEDTO NOT IN (183,182,302,263)";
		$sql = "SELECT DOC_ID FROM document WHERE DOC_UNIT = 39 AND YEAR(DOC_CREATEDON) > 2017 ORDER BY `document`.`DOC_ID` ASC ";
		$result = $dbc->dbconn->query($sql);
		$rows = array();
		while ($document = mysqli_fetch_assoc($result)){
		 $rows[] = $document;
		}
		$result->free();
		$dbc->dbconn->close();
		return $rows;
	}
	
	//END OF FOR SPECIAL CASES//
	
	public function changedocunit($docid, $newunit){
		$dbc = new dbconnection;
        $stmt = $dbc->dbconn->prepare("UPDATE document SET DOC_UNIT = ? WHERE DOC_ID = ?");
        $stmt->bind_param('dd', $newunit,$docid);
        $stmt->execute();
        if ($stmt->errno) {
			$stmt->close();
            $dbc->dbconn->close();
            return FALSE;
        }else{
            if ($dbc->dbconn->affected_rows > 0){
				$stmt->close();
                $dbc->dbconn->close();
                return TRUE;
            }else{
				$stmt->close();
                $dbc->dbconn->close();
                return FALSE;
            }
        }
	}
	
		
	public function changecallrecordunit($docid, $newunit){
		$dbc = new dbconnection;
        $stmt = $dbc->dbconn->prepare("UPDATE call_records SET CR_UNIT_ID = ? WHERE CR_DOC_ID = ?");
        $stmt->bind_param('dd', $newunit,$docid);
        $stmt->execute();
        if ($stmt->errno) {
			$stmt->close();
            $dbc->dbconn->close();
            return FALSE;
        }else{
			$stmt->close();
			$dbc->dbconn->close();
			return TRUE;
        }
	}
	
	public function changeremarkunit($docid, $newunit, $oldunit){
		$dbc = new dbconnection;
        $stmt = $dbc->dbconn->prepare("UPDATE doc_remarks SET DR_FILE_UNIT = ? WHERE DR_FILE_UNIT = ? AND DR_DOC_ID = ?");
        $stmt->bind_param('ddd', $newunit,$oldunit,$docid);
        $stmt->execute();
        if ($stmt->errno) {
			$stmt->close();
            $dbc->dbconn->close();
            return FALSE;
        }else{
			$stmt->close();
			$dbc->dbconn->close();
			return TRUE;
        }
	}
	

}
?>