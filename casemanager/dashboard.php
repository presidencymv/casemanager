<?php
require('_ex/connections.php');
require('_ex/authentications.php');
require('_ex/master.php');
require('_ex/document.php');
require('_ex/attachments.php');

$currentuser = new authentication;

if (isset($_GET['page'])){
    $page = $_GET['page'];
}else{
	$page = 'home';
	
	if($_SESSION['callcentrestaff'] == 'Y'){
		$page = 'callcentre';
	}
	
	if($_SESSION['chancellerystaff'] == 'Y'){
		$page = 'chancellery';
	}
	
	if($_SESSION['admins'] == 'Y'){
		$page = 'maindirectory';
	}
}

$today = date("Y-m-d");
?>

<!doctype html>
<html class="no-js" lang="en" >
  <head>
    <meta charset="utf-8" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>MCM | Welcome</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href='css/fullcalendar.css' rel='stylesheet' />
    <link href='css/fullcalendar.print.css' rel='stylesheet' media='print' />
	<link href="css/datepicker3.css" rel="stylesheet">
  	<link href="css/ckedit.css" rel="stylesheet">
    <link href="css/bootstrap-datetimepicker.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">
	<link href="css/dropzone.css" rel="stylesheet">
	<link href="css/select2.css" rel="stylesheet">
	<link href="css/lib/jquery-ui.min.css" rel="stylesheet">
	<link href="css/lib/jquery-ui.structure.min.css" rel="stylesheet">		
    <link href="css/cust_styles.css?V=1" rel="stylesheet">
	
	<!--[if lt IE 9]>
    <script>alert('You are using an outdated browser. Please switch to Google Chrome or Mozilla Firefox.');</script>
    <![endif]-->

    <script src='js/lib/moment.min.js'></script>

	<script src='ckeditor/ckeditor.js'></script>
	<script src='js/ckedit.js'></script>
	<script src='js/lib/jquery.js'></script>
	<script src='js/lib/jquery-ui.min.js'></script>
	<script src="js/table2excel.js"></script>
	<script src="js/dropzone.js"></script>
	<script src="js/select2.min.js"></script>

	<script type="text/javascript">
    $(function() {
        $.xhrPool = [];
        $.xhrPool.abortAll = function() {
            $(this).each(function(i, jqXHR) {   //  cycle through list of recorded connection
                jqXHR.abort();  //  aborts connection
                $.xhrPool.splice(i, 1); //  removes from list by index
            });
        }
        $.ajaxSetup({
            beforeSend: function(jqXHR) { $.xhrPool.push(jqXHR); }, //  annd connection to list
            complete: function(jqXHR) {
                var i = $.xhrPool.indexOf(jqXHR);   //  get index for current connection completed
                if (i > -1) $.xhrPool.splice(i, 1); //  removes from list by index
            }
        });
    })
	</script>
	
  </head>
  
  <body>
    <!--PAGE CONTENT START-->
	<div id="load"></div>
		
       <div id="pagecontent">
	   
        <?php include ('_components/dashboardnav.php')?>
		<?php include ('_components/searchbar.php')?>
			<div>
				<?php include ('_pages/'.$page.'.php')?>
			</div>

        </div>
    <!--PAGE CONTENT END-->

	<!--SCRIPTS-->
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
    <script src="js/bootstrap-datetimepicker.min.js"></script>
	<script src="js/bootstrap-table.js"></script>
	<script>
		!function ($) {
		    $(document).on("click","ul.nav li.parent > a > span.icon", function(){
		        $(this).find('em:first').toggleClass("glyphicon-minus");
		    });
		    $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})


		document.onreadystatechange = function () {
		  var state = document.readyState
		  if (state == 'complete') {
				document.getElementById('interactive');
				document.getElementById('load').style.visibility="hidden";
				updateallselects();
		  }
		}
		
		function updateallselects(){
			
	
			 var e = document.getElementsByTagName("select");
				for (var i = 0; i < e.length; i++){
				  var name = e[i].getAttribute("id");
				   $('#'+name).select2();
				   
					 $('#'+name).select2()
					.on('select2:close', function(evt) {
						var context = $(evt.target);

						$(document).on('keydown.select2', function(e) {
							if (e.which === 9) { // tab
								var highlighted = context
												  .data('select2')
												  .$dropdown
												  .find('.select2-results__option--highlighted');
								if (highlighted) {
									var id = highlighted.data('data').id;
									context.val(id).trigger('change');
								}
							}
						});

						// unbind the event again to avoid binding multiple times
						setTimeout(function() {
							$(document).off('keydown.select2');
						}, 1);
					});
				
				   
				}

		}
		
	</script>

	<!--SCRIPTS END-->
  </body>
<?php ob_end_flush()?>
</html>
