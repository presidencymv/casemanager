	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Case Manager</a>
				<ul class="user-menu">
					<li class="dropdown pull-right">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span> <?php echo strtoupper($_SESSION['username']) ?> <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="dashboard.php?page=preferences"><span class="glyphicon glyphicon-cog"></span> Notification Settings</a></li>
							<li><a href="dashboard.php?page=help"><span class="glyphicon glyphicon-question-sign"></span> Help</a></li>
							<li class="divider"></li>
							<li><a href="index.php?user=logoff"><span class="glyphicon glyphicon-off"></span> Logout</a></li>
						</ul>
					</li>
				</ul>
			
				<ul class="user-menu"></ul>
				<ul class="user-menu">
					<li class="dropdown pull-right">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-bell"></span><span id="unreadalertcount"></span> <span class="caret"></span></a>
						<ul class="dropdown-menu message-dropdown" id="alertlist">
						</ul>
					</li>
				</ul>
			</div>
		</div><!-- /.container-fluid -->
	</nav>
		
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<ul class="nav menu">
	
		<?php if ($_SESSION['chancellerystaff'] == 'Y'): ?>
			<li <?php if (in_array($page ,array('chancellery','archive'))  ){ echo 'class="active"';} ?>><a href="dashboard.php?page=chancellery">Chancellery</a></li>
			<li role="presentation" class="divider"></li>
		<?php endif?>
		<?php if ($_SESSION['callcentrestaff'] == 'Y'): ?>
			<li <?php if (in_array($page ,array('callcentre','callrecords'))  ){ echo 'class="active"';} ?>><a href="dashboard.php?page=callcentre">Call Centre</a></li>
			<li role="presentation" class="divider"></li>
		<?php endif?>
		<?php if ( $_SESSION['admins'] != 'Y'): ?>
			<li <?php if ($page == 'home'){ echo 'class="active"';} ?>><a href="dashboard.php?page=home">My Active Cases (<?php include('_pages/data/count_assigned_active_cases.php'); ?>)</a></li>
			<li <?php if ($page == 'mycases'){ echo 'class="active"';} ?>><a href="dashboard.php?page=mycases">My Cases (<?php include('_pages/data/count_my_assigned_cases.php'); ?>)</a></li>
			<li <?php if ($page == 'shared'){ echo 'class="active"';} ?>><a href="dashboard.php?page=shared">Shared Cases (<?php include('_pages/data/count_shared_cases.php'); ?>)</a></li>
			<li <?php if ($page == 'statuscheck'){ echo 'class="active"';} ?>><a href="dashboard.php?page=statuscheck">Section Status Check</a></li>
			<li role="presentation" class="divider"></li>
		<?php endif?>
			
			<li <?php if ($page == 'maindirectory'){ echo 'class="active"';} ?>><a href="dashboard.php?page=maindirectory">Main Directory</a></li>
			<?php if ( $_SESSION['handler'] == 'Y'): ?>
				<li role="presentation" class="divider"></li>
				<li <?php if ($page == 'activecases'){ echo 'class="active"';} ?>><a href="dashboard.php?page=activecases"><span class="glyphicon glyphicon-folder-close"></span> Section Active Cases (<?php include('_pages/data/count_unit_active_cases.php'); ?>)</a></li>
				<li <?php if ($page == 'sectioncases'){ echo 'class="active"';} ?>><a href="dashboard.php?page=sectioncases"><span class="glyphicon glyphicon-folder-close"></span> Section Cases (<?php include('_pages/data/count_unit_closed_cases.php'); ?>)</a></li>
			<?php endif?>

			<?php if ( $_SESSION['reports'] == 'Y' || $_SESSION['admins'] == 'Y' || $_SESSION['handler'] == 'Y'): ?>
				<li role="presentation" class="divider"></li>
				<li <?php if ($page == 'sectionstats'){ echo 'class="active"';} ?>><a href="dashboard.php?page=sectionstats"><span class="glyphicon glyphicon-folder-close"></span> Section Stats</a></li>
				<li <?php if ($page == 'basicreport'){ echo 'class="active"';} ?>><a href="dashboard.php?page=basicreport"><span class="glyphicon glyphicon-list-alt"></span> Case Reports</a></li>
				<li role="presentation" class="divider"></li>
			<?php endif?>
			<?php if ($_SESSION['ministersdirectivestaff'] == 'Y'): ?>
				<li <?php if (in_array($page ,array('ministersdirective','mdarchive'))  ){ echo 'class="active"';} ?>><a href="dashboard.php?page=ministersdirective"><span class="glyphicon glyphicon-plus"></span> Minister's Directives</a></li>
			<?php endif?>
			<?php if ( $_SESSION['counter'] == 'Y'): ?>
				
				<li <?php if ($page == 'newcase'){ echo 'class="active"';} ?>><a href="dashboard.php?page=newcase"><span class="glyphicon glyphicon-plus"></span> New Case</a></li>
				<li <?php if ($page == 'cases'){ echo 'class="active"';} ?>><a href="dashboard.php?page=cases"><span class="glyphicon glyphicon-folder-close"></span> New / Rejected Cases (<?php include('_pages/data/count_new_rejected_cases.php'); ?>)</a></li>
			<?php endif?>
				
			<?php if ( $_SESSION['admins'] == 'Y'): ?>
			<li role="presentation" class="divider"></li>
			<li <?php if ($page == 'managesender'){ echo 'class="active"';} ?>><a href="dashboard.php?page=managesender"><span class="glyphicon glyphicon-edit"></span> Manage Senders</a></li>
			<li role="presentation" class="divider"></li>
			<li <?php if ($page == 'managesource'){ echo 'class="active"';} ?>><a href="dashboard.php?page=managesource"><span class="glyphicon glyphicon-edit"></span> Manage Sources</a></li>
			<li role="presentation" class="divider"></li>
			<li <?php if ($page == 'managecategory'){ echo 'class="active"';} ?>><a href="dashboard.php?page=managecategory"><span class="glyphicon glyphicon-edit"></span> Manage Categories</a></li>
			<li role="presentation" class="divider"></li>
			<li <?php if ($page == 'manageunit'){ echo 'class="active"';} ?>><a href="dashboard.php?page=manageunit"><span class="glyphicon glyphicon-edit"></span> Manage Sections</a></li>
			<li role="presentation" class="divider"></li>
			<li <?php if ($page == 'manageuser'){ echo 'class="active"';} ?>><a href="dashboard.php?page=manageuser"><span class="glyphicon glyphicon-edit"></span> Manage Users</a></li>
			<?php endif?>
			
			<li role="presentation" class="divider"></li>
			<!-- <li><a href="CASE MANAGER USER MANUAL.pdf" download>USER MANUAL</a></li> -->
			
		</ul>

	</div><!--/.sidebar-->
	
	<script type="text/javascript">
		var unreadalertcount_request;
		
		if(typeof unreadalertcount_request !== 'undefined')
        unreadalertcount_request.abort();
		unreadalertcount_request = $.ajax({
			url: '_pages/data/count_unseen_alerts.php',
			dataType: 'html',
			success: function (data) {
				var alertsunread = data;
				$("#unreadalertcount").html(alertsunread);
			}
		});
		
		var alertlist_request;
		if(typeof alertlist_request !== 'undefined')
        alertlist_request.abort();
		alertlist_request = $.ajax({
            url: '_pages/data/notifications.php',
            dataType: 'json',
            success: function (data) {
                var comments = data;
                for (var comment in comments) {
					$("#alertlist").append(comments[comment].commentitem);
					
                }
            }
        });
		var getunreadalerts_request;
		
        function getunreadalerts(){
			
			if(typeof getunreadalerts_request !== 'undefined')
			getunreadalerts_request.abort();
			getunreadalerts_request = $.ajax({
            url: '_pages/data/notifications.php',
            dataType: 'json',
            success: function (data) {
                var comments = data;
                $("#alertlist").empty();
                for (var comment in comments) {

					$("#alertlist").append(comments[comment].commentitem);
					
                }
            }
        });
		}
		var getunreadcount_request;
		function getunreadcount(){
				if(typeof getunreadcount_request !== 'undefined')
				getunreadcount_request.abort();
			   $.ajax({
					url: '_pages/data/count_unseen_alerts.php',
					dataType: 'html',
					success: function (data) {
						var alertsunread = data;
						$("#unreadalertcount").html(alertsunread);
					}
				});

		}

		var interval = setInterval(
			function(){
				getunreadcount(); 
				getunreadalerts();
			}
		,30000);
		
		// setInterval( "getunreadcount()", 5000 );

</script>

